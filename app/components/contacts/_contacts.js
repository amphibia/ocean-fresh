$(document).bind('vueLoaded', function () {
    function initMap() {
        var map = $('#mapContacts');
        var myLatLng = {lat: parseFloat(map.data('lat')), lng: parseFloat(map.data('lng'))};
        var icon = map.data('marker');

        var map = new google.maps.Map(document.getElementById('mapContacts'), {
            zoom: 16,
            disableDefaultUI: true,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            icon:     icon,
            position:   myLatLng,
            map:    map,
            flat:     true,
            optimized:  true,
            draggable:  false
        });
        
    }
    function initMobileMap() {
        var map = $('#m-mapContacts');
        var myLatLng = {lat: parseFloat(map.data('lat')), lng: parseFloat(map.data('lng'))};
        var icon = map.data('marker');

        var map = new google.maps.Map(document.getElementById('m-mapContacts'), {
            zoom: 16,
            disableDefaultUI: true,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            icon:     icon,
            position:   myLatLng,
            map:    map,
            flat:     true,
            optimized:  true,
            draggable:  false
        });
        
    }
    if($('#m-mapContacts').length) {
        initMobileMap();
    }
    if($('#mapContacts').length){
        initMap();
    }
});