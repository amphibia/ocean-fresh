
$(document).bind('vueLoaded', function () {
    if($(window).width() <= 1024) {
        var mNav = $('.sidebar');
        var mMenuOpen_link = $('.sandwich-menu');
        var mHeader = $('.m-header');
        var overlay = $('.overlay');
        mMenuOpen_link.on('click', function(){
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
                mNav.removeClass('open');
                mHeader.removeClass('open');
            } else {
                $(this).addClass('open');
                mNav.addClass('open');
                mHeader.addClass('open');
            }
        }); 
    }    
})