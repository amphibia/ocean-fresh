$(document).bind('vueLoaded', function () {
	var promoSlider = (function(){
        var promo = new Swiper('.promo-slider .swiper-container', {
            loop: true,
            effect: 'fade',
            speed: 1500,
            nextButton: '.swiper-next',
            prevButton: '.swiper-prev',
            pagination: '.swiper-pagination'
        });
    })();
});