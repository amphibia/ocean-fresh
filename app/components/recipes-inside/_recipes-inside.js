$(document).bind('vueLoaded', function () {
	var stickyEl = $('[data-stycky]');

	if(!stickyEl.length)
		return false

	var scrollBlock = $('.content'),
		stickyElHeight = stickyEl.height();
		contentIn = $('.content-in'),
		contentScroller = $('.content-main'),
		stickyOffset = stickyEl.offset(),
		stickyOffsetRight = $(window).width() - stickyOffset.left - stickyEl.outerWidth();
		contentScrolleOffsetBottom = $('.section-recipes-inside').outerHeight() - contentScroller.offset().top - contentScroller.outerHeight() + $('.top-banner').outerHeight();
		spacing = contentIn.length ? contentIn.css('padding-top') : 0,
		spacing = parseFloat(spacing);
		// console.log(contentScrolleOffsetBottom);
	scrollBlock.scroll(function() {
		var scrollTop = $(this).scrollTop();
		// console.log(contentScroller.height() - stickyElHeight , scrollTop)
		if($(window).width() <= 1700) {
			if (contentScroller.height() - stickyElHeight <= scrollTop) {
				stickyEl.removeClass('fixed').addClass('absolut').css({
					'bottom': contentScrolleOffsetBottom,
					'top': 'auto'
				});
			} else {
				stickyEl.removeClass('absolut').addClass('fixed').css({
					'right': stickyOffsetRight,
					'bottom': 'auto',
					'top': '40px'
				});
			}
		}
		if(stickyOffset.top <= scrollTop + spacing) {
			stickyEl.addClass('fixed').css('right',stickyOffsetRight);
		} else {
			stickyEl.removeClass('fixed').css('right','20px');
		}
	})
});