$(document).bind('vueLoaded', function () {
    if(!$('.about-map-section').length)
        return false

    var regions = JSON.parse("[" + $('#about-map').data('region').split('\'').join('"') + "]");
    var currentRegion;
    var timeOut;
    var zoom = 1;
    var _aboutModal = $('.about-map-modal');
    
    var colors = regions.reduce(function(acc, cur) {
        acc[cur] = '#6ba843'
        return acc
    }, {})

    var map = $('#about-map').vectorMap({ 
        map: 'world_en',
        backgroundColor: null,
        borderColor: '#818181',
        borderWidth: 1,
        color: '#25315b',
        colors: colors,
        hoverColor: false,
        selectedColor: false,
        onLoad: function(event, map){
            for(var i = 0; i < regions.length; i++) {
                var regionId = '#jqvmap1_'+regions[i];
                $(regionId).css({
                    "stroke": '#6ba843'
                })
            }
        },
        onRegionOver: function(element, code, region) {
            
            if(regions.indexOf(code) !== -1){
                currentRegion = code;
                var countryId = 'jqvmap1_'+code;
                var countryObj = $('#' + countryId);
            
                var bbox = document.getElementById(countryId).getBBox();
                var position = countryObj.position();
            
                var left = position.left + (bbox.width / 2) * zoom,
                top = position.top + (bbox.height / 2) * zoom;
                
                clearTimeout(timeOut);  
                
                $('.popover-about-map').hide();
                $('.popover-about__'+code).show()
                $('.popover-about__'+code).css('left', left).css('top', top);
            }
        },
        onRegionOut: function (element, code, region) {
            if(regions.indexOf(code) !== -1 && currentRegion == code){
                clearTimeout(timeOut);
                timeOut = setTimeout(function () {
                    currentRegion = '';
                    $('.popover-about-map').hide();
                }, 500);
            }
        }
    });

    $('body').on('mouseover', '.popover-about-map', function () {
        clearTimeout(timeOut);
    })

    $('body').on('mouseleave', '.popover-about-map', function () {
        clearTimeout(timeOut);
        timeOut = setTimeout(function () {
            $('.popover-about-map').hide();
        }, 500);
    })
    $('body').on('click', '.jqvmap-zoomin', function () {
        if(zoom != 4)
            zoom ++
    });
    $('body').on('click', '.jqvmap-zoomout', function () {
        if(zoom != 1)
            zoom --
    });
    // demo 
    $('body').on('click', '.country-info-modal__link', function (event) {
        event.preventDefault();
        var url = $(this).attr('href');
        _aboutModal.addClass('loading');
        $.get(url, function (response) {
            
            if(response) {
                $('.content').css('overflow','hidden');
                _aboutModal.find('.content-in').empty().html(response);
                _aboutModal.fadeIn(500);
                _aboutModal.mCustomScrollbar({
                    theme:"dark"
                });
                setTimeout(function () {
                    _aboutModal.removeClass('loading');
                },800);

            }
        })
    });
    $('body').on('click', '.hide-modal', function () {
        _aboutModal.addClass('loading');
        _aboutModal.mCustomScrollbar("destroy");
        $(this).parent('.about-map-modal').fadeOut();
        setTimeout(function () {
            $('.content').css('overflow','auto');
            _aboutModal.removeClass('loading');
        },300);
    });
});