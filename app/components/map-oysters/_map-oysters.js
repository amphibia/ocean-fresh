$(document).bind('vueLoaded', function () {
    
    if(!$('.oysters-map').length) return;

    var regions = JSON.parse("[" + $('.oysters-map').data('regions').split('\'').join('"') + "]");
    var currentRegion;
    var timeOut;
    var zoom = 1;
    var _modal = $('.oysters-map-modal');
    
    var colors = regions.reduce(function(acc, cur) {
        acc[cur] = '#6ba843'
        return acc
    }, {})

    var map = $('.oysters-map').vectorMap({
        map: 'france_2016',
        backgroundColor: null,
        borderColor: '#fff',
        borderWidth: 0,
        color: '#25315b',
        colors: colors,
        hoverColor: false,
        selectedColor: false,
        onRegionOver: function (elem, code, region) {
            if(regions.indexOf(code) !== -1){
                currentRegion = code;
                
                var countryId = 'jqvmap1_'+code;
                var countryObj = $('#' + countryId);
            
                var bbox = document.getElementById(countryId).getBBox();
                var position = countryObj.position();
            
                var left = position.left + (bbox.width / 2) * zoom,
                top = position.top + (bbox.height / 2) * zoom;
                
                clearTimeout(timeOut);  
                
                $('.popover-oysters-map').hide();
                $('.popover-oysters__'+code).show()
                $('.popover-oysters__'+code).css('left', left).css('top', top);
            }
        },
        onRegionOut: function (element, code, region) {
            if(regions.indexOf(code) !== -1 && currentRegion == code){
                clearTimeout(timeOut);
                timeOut = setTimeout(function () {
                    currentRegion = '';
                    $('.popover-oysters-map').hide();
                }, 500);
            }
        }
    });
    $('body').on('mouseover', '.popover-oysters-map', function () {
        clearTimeout(timeOut);
    })

    $('body').on('mouseleave', '.popover-oysters-map', function () {
        clearTimeout(timeOut);
        timeOut = setTimeout(function () {
            $('.popover-oysters-map').hide();
        }, 500);
    })
    $('body').on('click', '.jqvmap-zoomin', function () {
        if(zoom != 4)
            zoom ++
    });
    $('body').on('click', '.jqvmap-zoomout', function () {
        if(zoom != 1)
            zoom --
    });
    // demo 
    $('body').on('click', '.country-info-modal__link', function (event) {
        event.preventDefault();
        var url = $(this).attr('href');
        _modal.addClass('loading');        
        $.get(url, function (response) {
            
            if(response) {
                $('.content').css('overflow','hidden');
                _modal.find('.content-in').empty().html(response);
                _modal.fadeIn(500);
                _modal.mCustomScrollbar({
                    theme:"dark"
                });
                setTimeout(function () {
                    _modal.removeClass('loading');
                },800);

            }
        })
    });
    $('body').on('click', '.hide-modal', function () {
        _modal.addClass('loading');
        _modal.mCustomScrollbar("destroy");
        $(this).parent('.oysters-map-modal').fadeOut();
        setTimeout(function () {
            $('.content').css('overflow','auto');
            _modal.removeClass('loading');
        },300);
    });
});