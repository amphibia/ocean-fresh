$(document).bind('vueLoaded', function () {
    if ($(window).width() > 1024) {
        var navMain = (function(){
            var sidebar = $('.sidebar'),
                 navItem = $('.nav-item'),
                 dropdownMenu = $('.dropdown-menu'),
                 dropdownItem = $('.dropdown-item'),
                 subDropdownMenu = $('.sub-dropdown-menu'),
                 subDropdownItem = $('.sub-dropdown-item');

            var dropdownMenuClose = function () {
                navItem.removeClass('opened');
            }
            var dropdownMenuOpen = function(elem) {
                if(elem.find(dropdownMenu).length) {
                    dropdownMenuClose();
                    overlay.show();  //main.js file
                    elem.addClass('opened');
                }
            }
            
            var subDropdownMenuOpen = function(elem) {
              elem.toggleClass('opened').find(subDropdownMenu).slideToggle(300);
            }

            navItem.on('click', function(e){
                if($(this).find(dropdownMenu).length) {
                    e.preventDefault();
                    dropdownMenuOpen($(this));
                }
            });

            dropdownItem.on('click', function(e){
                if(!$(this).find(subDropdownMenu).length) {
                    if($(this).find('a').length) 
                        window.location.href = $(this).find('a').attr('href');
                } else {
                    if($(this).hasClass('opened')) {
                        console.log($(e.target))
                        if($(e.target).parent()[0].localName == 'a') {
                            
                            window.location.href = $(this).find('a:first').attr('href');
                        } else {
                            subDropdownMenuOpen($(this));    
                        }
                    } else {
                        subDropdownMenuOpen($(this));
                    }
                }
            });

            subDropdownItem.find('a').on('click', function (e) {
                e.stopImmediatePropagation()
                var url = $(this).attr('href');
                window.location.href = url;
            })
            $(document).on('click', function(e){
                
                if (!sidebar.is(e.target) && sidebar.has(e.target).length === 0) {
                    overlay.hide(); //main.js file
                    dropdownMenuClose();
                }
            });
            
        })();
    }
});