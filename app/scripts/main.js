var overlay = (function(){
	var obj = {};
	var elem = $('.overlay');

	obj.show = function() {
		elem.fadeIn(300);
	}
	
	obj.hide = function() {
		elem.fadeOut(300);
	}
	return obj;
})();

$(document).bind('vueLoaded', function () {
	(function () {
		var contentSlider = $('.content-slider');
		
		contentSlider.each(function () {
			var _self = $(this);
			var carousel = new Swiper(_self.find('.swiper-container'), {
				nextButton: _self.find('.swiper-next'),
				prevButton: _self.find('.swiper-prev'),
			});
		});
	})();
	var catalogNavbar = $('.catalog-navbar');
	if(catalogNavbar.length) {
		catalogNavbar.addClass('fixed');
		$('.products, .cart:first').css('padding-top', catalogNavbar.innerHeight() + 20);
	}
})