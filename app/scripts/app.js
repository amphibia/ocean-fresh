Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var number = this, 
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};

Number.prototype.isInt = function () {
	return this % 1 === 0;
}

function loadCart(key){
  if(localStorage.getItem(key)){
    return JSON.parse(localStorage.getItem(key));
  }
  return [];
}

function saveCart(key, value){
  localStorage.setItem(key, JSON.stringify(value));
}
var urlRegex = /(index$|index\/$|^\/|\/$)/g;
var catalogAjax = '';//'http://test.oceanfresh.kz/katalog/ryba/Dikaya';
var cartState = {
  data: {
    cart: loadCart("cart")
  },
  add: function (product) {
    var found = _.find(this.data.cart, ['id', product.id])

    if(typeof found != 'object') {
      this.data.cart.push({
        id: product.id,
        title: product.title,
        price: product.price,
        image: product.image,
        href: product.href,
        refinement: product.refinement,
        grams: product.grams,
        type_payment: product.type_payment,
        gram_weight: product.type_payment == "kilogram" ? (product.gram_weight ? product.gram_weight : 1) : 0,
        typeCutting: product.typeCutting,
        typeCuttingSelected: product.typeCuttingSelected,
        qty: product.qty ? product.qty : 1
      })
      saveCart('cart', this.data.cart);
    }
  },
  remove: function (product) {
      var found = _.find(this.data.cart, ['id', product.id])
      if(typeof found == 'object') {
        var index = _.indexOf(this.data.cart, found)
      
        this.data.cart.splice(index, 1)
        saveCart('cart', this.data.cart);
      }
      
  },
  inc: function (product) {
    var found = _.find(this.data.cart, ['id', product.id])
    
    if(typeof found == 'object') {
      var index = _.indexOf(this.data.cart, found)
      if(this.data.cart[index].type_payment == 'kilogram') {
        if (this.data.cart[index].gram_weight.toFixed(1) >= 1) {
          if(this.data.cart[index].gram_weight.toFixed(1) < 3) {
            +(this.data.cart[index].gram_weight += 0.5)
          } else {
            +(this.data.cart[index].gram_weight += 1)
          }
        } else {
          +(this.data.cart[index].gram_weight += 0.1)
        }
      } else {
        this.data.cart[index].qty++
      }
      saveCart('cart', this.data.cart);
    }
    
  },
  dec: function (product) {
    var found = _.find(this.data.cart, ['id', product.id])

    if(typeof found == 'object') {
      var index = _.indexOf(this.data.cart, found)
      if(this.data.cart[index].type_payment == 'kilogram') {
        if(this.data.cart[index].gram_weight.toFixed(1) == 0.1) {
          this.remove(product)
        } else {
          if (this.data.cart[index].gram_weight.toFixed() > 1) {
              if(this.data.cart[index].gram_weight.toFixed() <= 3) {
                +(this.data.cart[index].gram_weight -= 0.5)
              } else {
                +(this.data.cart[index].gram_weight -= 1)
              }
          } else {
            +(this.data.cart[index].gram_weight -= 0.1)
          }
        }
      } else {
        if(this.data.cart[index].qty == 1) {
          this.remove(product)
        } else {
          this.data.cart[index].qty--
        }
        
      }
      
      saveCart('cart', this.data.cart);
    }
  },
  removeAll: function () {
    this.data.cart = []
    saveCart('cart', []);
  }
 }

Vue.use(VeeValidate);

Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.event = function (event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.event)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.event)
  },
});

var TotalCartCount = {
    template: '<span class="total-cart-count">{{totalCount}}</span>',
    data: function () {
        return {
            shared: cartState.data
        }
    },
    computed: {
        totalCount: function () {
            return this.shared.cart.length
        }
    }
}
Vue.component('total-cart-count', TotalCartCount)

var Cart = {
  template: '<div class="cart">'+
    '<div v-if="total != 0" class="cart-title">Корзина'+
      '<span class="total-cart">(<total-cart-count></total-cart-count>)</span>'+
      '<button @click="removeAllItem"><i class="icon icon-close"></i> <span>Удалить все товары</span></button>'+
    '</div>'+
    '<p class="cart-empty" v-if="total == 0">Ваша корзина пуста</p>'+
    '<div class="items" v-else>'+
      '<div class="item clearfix" v-for="item in items">'+
        "<a :href='item.href' class='item-image' :style='{ backgroundImage: \"url(\" + item.image + \")\" }'></a>" +
        '<div class="item-details">'+
          '<p class="item-title">{{item.title}}</p>'+
          '<div class="item-controls">'+
            '<button class="item-dec" @click="dec(item)"><i class="icon icon-minus"></i></button>'+
            "<span v-if='item.type_payment == \"kilogram\"'>"+
              '{{ item.gram_weight.isInt() ? item.gram_weight.toFixed() : item.gram_weight.toFixed(1) }}'+
            "</span>"+
            "<span v-else>{{item.qty}}</span>"+
            '<button class="item-inc" @click="inc(item)"><i class="icon icon-plus"></i></button>'+
          '</div>'+
          '<div class="select-wrapper" v-if="item.typeCutting.length">'+
            '<select v-model="item.typeCuttingSelected" @change="changeTypeCutting(item.typeCuttingSelected)">'+
              '<option v-for="option in item.typeCutting" :value="option.id">{{option.title}}</option>'+
            '</select>'+
          '</div>'+
          '<div class="item-price">'+
            '<span>{{item.refinement}}</span>'+
            '<b>{{item.price.formatMoney(0, "", " ")}} ₸</b>'+
          '</div>'+
          "<div class='item-price item-price-helper' v-if='item.type_payment == \"hard\"'>"+
            '<div>'+
              "<span>Сумма</span>"+
              '<b>{{((item.price / 1000 * item.grams) * item.qty).formatMoney(0, "", " ")}} ₸</b>'+
            '</div>'+
          "</div>"+
          "<div class='item-price item-price-helper' v-if='item.type_payment == \"kilogram\"'>"+
            '<div>'+
              '<span>Сумма</span>'+
              '<b>{{(item.price / 1000 * (item.gram_weight * 1000)).formatMoney(0, "", " ")}} ₸</b>'+
            '</div>'+
          '</div>'+
          "<div class='item-price item-price-helper' v-if='item.type_payment == \"items\"'>"+
            '<div>'+
              '<span>Сумма</span>'+
              '<b>{{(item.price * item.qty).formatMoney(0, "", " ")}} ₸</b>'+
            '</div>'+
          '</div>'+
          '<span class="remove-item" @click="removeItem(item)"><i class="icon icon-close"></i></span>'+
        '</div>'+
      '</div>'+
    '</div>'+
    '<div class="cart-total">'+
      '<div class="checkout-block" :class="[total == 0 ? \'disable\': \'\']">'+
        '<div v-if="loader" class="loader"></div>'+
        '<span v-if="checkDeliveryPrice" class="total-sum total-sum-green">Доставка: {{delivery_price.formatMoney(0, "", " ")}} ₸</span>'+
        '<span v-if="!checkDeliveryPrice" class="total-sum">Итого: {{total.formatMoney(0, "", " ")}} ₸</span>'+
        '<span v-else class="total-sum">Итого: {{(total + delivery_price).formatMoney(0, "", " ")}} ₸</span>'+
        '<div class="checkout-form">'+
          '<h2 class="checkout-form-title">Форма для заказа</h2>'+
          '<p class="checkout-form-desc"><span class="required">*</span>Обязательно для заполнения</p>'+
          '<div class="form-group">'+
            '<div class="form-control-item">'+
              '<input class="form-control" v-validate="{ rules : { required: true, regex: /[A-zА-яЁё\\s]$/ } }" :class="{\'input\': true, \'is-danger\': errors.has(\'fio\') }" placeholder="Ф.И.О" name="fio" v-model="fio" type="text">'+
              '<span v-show="errors.has(\'fio\')" class="help is-danger">{{ errors.first(\'fio\') }}</span>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<input class="form-control" v-masked="\'false\'" v-mask="\'+7 (###) ###-##-##\'" v-validate="{ rules : { required: true, regex: /^(.7\\s\\([0-9]{3}\\)\\s)[0-9]{3}-[0-9]{2}-[0-9]{2}$/}}" :class="{\'input\': true, \'is-danger\': errors.has(\'phone\') }" placeholder="Телефон" name="phone" v-model="phone" type="text">'+
              '<span v-show="errors.has(\'phone\')" class="help is-danger">{{ errors.first(\'phone\') }}</span>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<input class="form-control" v-validate="\'required|email\'" :class="{\'input\': true, \'is-danger\': errors.has(\'email\') }" placeholder="Электронная почта" name="email" v-model="email" type="email">'+
              '<span v-show="errors.has(\'email\')" class="help is-danger">{{ errors.first(\'email\') }}</span>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<div class="radio">'+
                '<input name="delivery-type" :value="1" v-model="delivery_type" id="delivery-type-1" type="radio">'+
                '<label for="delivery-type-1">Доставка</label>'+
              '</div>'+
              '<div class="radio">'+
                '<input name="delivery-type" :value="0" v-model="delivery_type" id="delivery-type-2" type="radio">'+
                '<label for="delivery-type-2">Самовывоз</label>'+
              '</div>'+
            '</div>'+
            '<div v-if="delivery_type">'+
              '<div class="form-control-item">'+
                '<input class="form-control" placeholder="Адрес доставки (в Алматы)" name="delivery-address" v-model="delivery_address" type="text">'+
              '</div>'+
              '<div class="form-control-item">'+
                '<input class="form-control" placeholder="Желательное время доставки" name="delivery-time" v-model="delivery_time" type="text">'+
              '</div>'+
              '<div class="form-control-item">'+
                '<label class="label">Доставка</label>'+
                '<div class="radio">'+
                  '<input name="delivery-zone" value="in" v-model="delivery_zone" id="delivery-zone-1" type="radio">'+
                  '<label for="delivery-zone-1">В квадрате</label>'+
                '</div>'+
                '<div class="radio">'+
                  '<input name="delivery-zone" value="out" v-model="delivery_zone" id="delivery-zone-2" type="radio">'+
                  '<label for="delivery-zone-2">Вне квадрата</label>'+
                '</div>'+
              '</div>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<div class="radio">'+
                '<input name="type-payment" value="cash" v-model="type_payment" id="type-payment-1" type="radio">'+
                '<label for="type-payment-1">Наличными (предоплата)</label>'+
              '</div>'+
              '<div class="radio">'+
                '<input name="type-payment" value="online_card" v-model="type_payment" id="type-payment-2" type="radio">'+
                '<label for="type-payment-2">Картой</label>'+
              '</div>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<div class="checkbox">'+
                '<input name="type-payment" v-model="agree" id="agree" type="checkbox">'+
                '<label for="agree">С условиями оформления заказа и доставки ознакомлен.</label>'+
              '</div>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<button class="btn btn-success btn-block" @click="sendCart" :disabled="!agree">оформить заказ</button>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>'+
  '</div>',
  data: function () {
    return {
      loader: false,
      locale: 'ru',
      items: cartState.data.cart,
      delivery_type: 0,
      fio: "",
      phone: "",
      email: "",
      delivery_time: "",
      delivery_address: "",
      type_payment: 'cash',
      delivery_zone: 'in',
      statusForm: false,
      delivery_price: 1000,
      delivery_price_limit: 10000,
      agree: false
    }
  },
  methods: {
    sendCart: function () {
      var data = {
        "OrderForm[fio]": this.fio,
        "OrderForm[phone]": this.phone,
        "OrderForm[email]": this.email,
        "OrderForm[delivery_type]": this.delivery_type,
        "OrderForm[delivery_address]": this.delivery_address,
        "OrderForm[delivery_time]": this.delivery_time,
        "OrderForm[cart]": JSON.stringify(this.items),
        "OrderForm[type_payment]": this.type_payment,
        "OrderForm[delivery_zone]": this.delivery_zone,
      }
      var _self = this
      this.$validator.validateAll().then(function (result) {
        if (result && _self.total != 0) {
          $.post('/cart/send', data, function (response) {
            if(response.result == 1) {
              window.location.href = '/cart/onlinePayment'
              _self.removeAllItem()
            }
          })
          return;
        }
      });
    },
    changeTypeCutting: function (type) {
      // cartState.change('typeCuting')
    },
    removeAllItem: function () {
      this.items = []
      cartState.removeAll()
    },
    removeItem: function (index) {
      cartState.remove(index)
    },
    inc: function (item) {
      cartState.inc(item)
    },
    dec: function (item) {
      cartState.dec(item)
    }
  },
  created: function () {
    this.$validator.setLocale('ru');
  },
  computed: {
    checkDeliveryPrice: function () {
      var check = false;

        if(this.delivery_type == 1) {
          check = false;
          if(this.delivery_zone == 'out' || this.total < this.delivery_price_limit) {
            check = true;
          }
        }

      return check;
    },
    total: function () {
      return _.sumBy(this.items, function(item) {
        var total = 0;
        switch(item.type_payment) {
          case 'hard':
            total = (item.price / 1000 * item.grams) * item.qty
            break;
        
          case 'kilogram':
            total = (item.price / 1000 * (item.gram_weight * 1000))
            break;
        
          default:
            total = item.price * item.qty
            break;
        }
        return total;

      })
    }
  }
}

Vue.component('cart', Cart);

var Product = {
  props: ['product'],
  template: '<div class="product">'+
    '<div v-if="product.badge" class="badges-list">'+
      '<template v-for="badge in product.badge"> <span :class="\'product-badge badge-\' + badge.color ">{{badge.text}}</span></template>'+
    '</div>'+
    '<a :href="product.href">'+
      "<div class='product-image' :style='{ backgroundImage: \"url(\" + product.image + \")\" }'></div>" +
    '</a>'+
    
      '<p class="product-title">'+
        '<a :href="product.href">{{product.title}}</a>'+
      '</p>'+
    
    '<div class="product-info-wrapper" :class="[qtyInCart != 0 ? \'controls-show\': \'controls-hide\']">'+
        '<div v-if="product.old_price" class="product-old-price">{{product.old_price.formatMoney(0, "", " ")}} ₸</div>'+
        '<span class="product-price">{{product.price.formatMoney(0, "", " ")}} ₸</span>'+
        '<span class="product-info" v-if="qtyInCart == 0">{{product.refinement}}</span>'+
        '<template v-if="!product.type_not_cart">'+
          '<button class="product-add-btn" @click="addToCart" v-if="qtyInCart == 0"><i class="icon icon-cart"></i></button>'+
        '</template>'+
        '<template v-else>'+
          '<button class="product-add-btn" @click="document.location.href = product.href"><i class="icon icon-cart"></i></button>'+
        '</template>'+
        '<div class="product-controls">'+
            '<button class="product-dec" @click="dec"><i class="icon icon-minus"></i></button>'+
            '<span class="product-qty">{{qtyInCart}}</span>'+
            '<button class="product-inc" @click="inc"><i class="icon icon-plus"></i></button>'+
        '</div>'+
    '</div>'+
  '</div>',
  data: function () {
    return {
      shared: cartState.data
    }
  },
  methods: {
    addToCart: function () {
      if(this.product.typeCutting.length)
        this.product.typeCuttingSelected = this.product.typeCutting[0].id

      cartState.add(this.product)
    },
    inc: function () {
      cartState.inc(this.product)
    },
    dec: function () {
      cartState.dec(this.product)
    }
  },
  computed: {
    qtyInCart () {
      // наличие товара в корзине
      var found = _.find(this.shared.cart, ['id', this.product.id])
      if(typeof found == 'object') {
        if(found.type_payment == 'kilogram') {
          return found.gram_weight.isInt() ? found.gram_weight.toFixed() : found.gram_weight.toFixed(1)
        }else {
          return found.qty
        }
      } else {
        return 0
      }
    }
  }
}
Vue.component('product', Product);

var productInside = {
  template: '<div class="product-inside">'+
      "<div class='product-image' :style='{ backgroundImage: \"url(\" + product.imageBig + \")\" }'>"+
      '<div v-if="product.badge" class="badges-list">'+
        '<template v-for="badge in product.badge"> <span :class="\'product-badge badge-\' + badge.color ">{{badge.text}}</span></template>'+
      '</div>'+
      "</div>" +
      "<div class='m-product-image' :style='{ backgroundImage: \"url(\" + product.image + \")\" }'>"+
      '<span v-if="product.text_stock" class="product-badge">{{product.text_stock}}</span>'+
      "</div>" +
      '<div class="product-content">'+
        '<div class="product-title">{{product.title}}</div>'+
        '<div class="product-price">'+
        '<div v-if="product.old_price" class="product-old-price">{{product.old_price.formatMoney(0, "", " ")}} ₸</div>'+
        '{{product.price.formatMoney(0, "", " ")}} ₸'+
        '</div>'+
        '<div class="product-refinement">{{product.refinement}}</div>'+
        '<div class="controls-group">'+
          '<div class="item-controls" :class="{disabled: inCart}" v-if="!product.type_not_cart">'+
            '<button class="item-dec" @click="dec()"><i class="icon icon-minus"></i></button>'+
            "<span v-if='product.type_payment == \"kilogram\"'>"+
              '{{ qty.isInt() ? qty.toFixed() : qty.toFixed(1) }}'+
            "</span>"+
            "<span v-else>"+
              '<template v-if="inCart">'+
                '{{qtyInCart}}'+
              '</template>'+
              '<template v-else>'+
                '{{qty}}'+
              '</template>'+
            "</span>"+
            '<button class="item-inc" @click="inc()"><i class="icon icon-plus"></i></button>'+
          '</div>'+
          '<template v-if="!product.type_not_cart">'+
            '<button class="btn btn-success" v-if="!inCart && product.typeCutting.length > 0" @click.stop="sidebarShow">В корзину</button>'+
            '<button class="btn btn-success" v-else-if="product.typeCutting.length == 0 && !inCart" @click="addToCart">В корзину</button>'+
            '<button class="btn btn-success" disabled v-else>Товар уже в корзине</button>'+
          '</template>'+
          '<template v-else>'+
            '<button class="btn btn-success" @click.stop="sidebarProduct = true">Заказать</button>'+
          '</template>'+
        '</div>'+
        '<div class="product-recipe-link" v-if="product.recipeTitle && product.recipeHref">'+
          '<a href="#"> {{product.recipeTitle}}</a>'+
        '</div>'+
        '<hr>'+
        '<div class="product-text" v-html="product.html"></div>'+
        '<div class="recommended_products" v-if="product.recommended_products.length">'+
          '<h2>Вместе с этим покупают</h2>'+
          '<div class="recommended_products_carousel">'+
            '<div class="swiper-container">'+
                '<div class="swiper-wrapper">'+
                    '<div class="swiper-slide" v-for="rec_product in product.recommended_products"><a :href="rec_product.href">{{rec_product.title}}</a></div>'+
                '</div>'+
            '</div>'+
            '<div class="carousel-controls top-right">'+
              '<div class="recommended_products-button recommended_products-button-prev"><i class="icon icon-arrow-left"></i></div>'+
              '<div class="recommended_products-button recommended_products-button-next"><i class="icon icon-arrow-right"></i></div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<transition name="slide">'+
        '<div class="sidebar-product-treatment" v-if="sidebarVisibility" v-click-outside="sidebarHide">'+
          '<span class="product-name">{{product.name}}</span>'+
          '<span class="btn-sidebar-hide" @click="sidebarHide"><i class="icon icon-close"></i></span>'+
          '<h2>Выберите тип обработки товара</h2>'+
          '<p>Бесплатно чистим от чешуи, потрошим, моем. Полностью готовим к приготовлению.</p>'+
          '<ul class="sidebar-cutting-block">'+
            '<li class="sidebar-cutting-item" v-for="item in product.typeCutting">'+
            '<input type="radio" name="typeCutting" v-model="product.typeCuttingSelected" @change="changeTypeCutting" :value="item.id" :id="item.id">'+
            '<label :for="item.id">{{item.title}}</label>'+
            '</li>'+
          '</ul>'+
          '<button class="btn btn-success btn-block" @click="addToCart">продолжить покупки</button>'+
        '</div>'+
      '</transition>'+
      '<transition name="slide">'+
        '<div class="sidebar-product-treatment" v-if="sidebarProduct">'+
          '<span class="product-name">{{product.name}}</span>'+
          '<span class="btn-sidebar-hide" @click="sidebarProduct = false"><i class="icon icon-close"></i></span>'+
          '<h2>Заказать товар</h2>'+
          '<div class="form-group">'+
            '<div class="form-control-item">'+
              '<input class="form-control" v-validate="{ rules : { required: true, regex: /[A-zА-яЁё\\s]$/ } }" :class="{\'input\': true, \'is-danger\': errors.has(\'fio\') }" placeholder="Ф.И.О" v-model="fio" name="fio" type="text">'+
              '<span v-show="errors.has(\'fio\')" class="help is-danger">{{ errors.first(\'fio\') }}</span>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<input class="form-control" v-masked="\'false\'" v-mask="\'+7 (###) ###-##-##\'" v-validate="{ rules : { required: true, regex: /^(.7\\s\\([0-9]{3}\\)\\s)[0-9]{3}-[0-9]{2}-[0-9]{2}$/}}" :class="{\'input\': true, \'is-danger\': errors.has(\'phone\') }" placeholder="Мобильный телефон" v-model="phone" name="phone" type="text">'+
              '<span v-show="errors.has(\'phone\')" class="help is-danger">{{ errors.first(\'phone\') }}</span>'+
            '</div>'+
            '<div class="form-control-item">'+
              '<input class="form-control" placeholder="Дата планируемой поставки" name="delivery-time" v-model="delivery_time" type="text">'+
            '</div>'+
            '<div class="form-control-item">'+
            '<input class="form-control" v-validate="{ rules : { required: true } }" :class="{\'input\': true, \'is-danger\': errors.has(\'qty\') }" placeholder="Количество" v-model="form_qty" name="qty" type="text">'+
            '<span v-show="errors.has(\'qty\')" class="help is-danger">{{ errors.first(\'qty\') }}</span>'+
          '</div>'+
            '<p>{{product.sidebar_desc}}</p>'+
            '<div class="form-control-item">'+
              '<button class="btn btn-success btn-block" @click="sendDataCart">оформить заказ</button>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</transition>'+
      '<transition name="fade">'+
        '<div class="sidebar-overlay" v-if="sidebarVisibility || sidebarProduct"></div>'+
      '</transition>'+
  '</div>',
  data: function () {
    return {
      productsInCart: cartState.data.cart,
      url: window.location.href.replace(urlRegex, ''),
      product: {},
      qty: 1,
      fio: '',
      phone: '',
      form_qty: '',
      delivery_time: '',
      sidebarVisibility: false,
      sidebarProduct: false,
      typeCutting: [],
      typeCuttingSelected: ''
    }
  },
  created: function () {
    var _self = this

    axios.get(this.url+'/1')
      .then(function (response) {

      _self.product = response.data.item

      if (_self.product.typeCutting.length)
        _self.product.typeCuttingSelected = _self.product.typeCutting[0].id 

      if (_self.product.type_payment == "kilogram") {
          _self.product.gram_weight = 1
          if (_self.inCart) {
            var found = _.find(_self.productsInCart, ['id', _self.product.id])
            _self.qty = found.gram_weight
          }
      }

      if(_self.product.recommended_products.length) {
        setTimeout(function () {
          var recommended_products_carousel = new Swiper($('.recommended_products_carousel .swiper-container'), {
            spaceBetween: 20,
            autoHeight: true,
            slidesPerView: 2,
            nextButton: '.recommended_products-button-next',
            prevButton: '.recommended_products-button-prev',
          })
        },200)
      }

    }).catch( function (error) {
      console.log(error)
    })

  },
  mounted: function () {
    this.$watch("productsInCart", function(value){
      saveCart("cart", value);
    });
  },
  methods: {
    sendDataCart: function () {
      var product = this.product;
      var cart = [];
      cart.push(product)
      var data = {
        "OrderForm[fio]": this.fio,
        "OrderForm[phone]": this.phone,
        "OrderForm[delivery_type]": -1,
        "OrderForm[delivery_time]": this.delivery_time,
        "OrderForm[pre_order_qty]": this.form_qty,
        "OrderForm[cart]": JSON.stringify(cart),
        "OrderForm[typePayment]": this.typePayment
      }
      var _self = this

      this.$validator.validateAll().then(function (result) {
        if (result) {
          $.post('/cart/send', data, function (response) {
            
          })

          return;

        }
      });
    },
    changeTypeCutting: function () {
      console.log(this.product.typeCuttingSelected, this.typeCuttingSelected)
      // this.product.typeCuttingSelected = this.typeCuttingSelected
    },
    sidebarShow: function () {
      this.sidebarVisibility = true
    },
    sidebarHide: function () {
      this.sidebarVisibility = false
    },
    inc: function () {
      if (!this.inCart) {
        if (this.product.type_payment != 'kilogram') {
          if (!this.inCart)
            this.qty ++  
        } else {
          if (this.qty.toFixed(1) >= 1 && this.qty.toFixed(1) < 3) {
            this.qty += 0.5;
          } else if (this.qty.toFixed(1) >= 3) {
            this.qty += 1
          } else {
            this.qty += .1
          }
        }
      }
    },
    dec: function () {
      if (!this.inCart) {
        if (this.product.type_payment != 'kilogram') {
          if (this.qty > 1)
            this.qty --  
        } else {
          if (this.qty.toFixed(1) > 1 && this.qty.toFixed(1) <= 3) {
            this.qty -= 0.5;
          } else if (this.qty.toFixed(1) > 3) {
            this.qty -= 1
          } else {
            if (this.qty.toFixed(1) != 0.1)
              this.qty -= .1
          }
        }
      }
    },
    addToCart: function () {
        this.sidebarHide()
        if (this.product.type_payment != 'kilogram') {
          this.product.qty = this.qty
        } else {
          this.product.gram_weight = this.qty
        }
        cartState.add(this.product)
    }
  },
  computed: {
    qtyInCart () {
      var found = _.find(this.productsInCart, ['id', this.product.id])
      if(typeof found == 'object') {
        if(found.type_payment == 'kilogram') {
          return found.gram_weight.isInt() ? found.gram_weight.toFixed() : found.gram_weight.toFixed(1)
        }else {
          return found.qty
        }
      } else {
        return 0
      }
    },
    inCart () {
      // наличие товара в корзине
      var found = _.find(this.productsInCart, ['id', this.product.id])
      
      if(typeof found == 'object') {
        return true
      } else {
        return false
      }
    }
  }
}

Vue.component('product-inside', productInside);

var Products = {
  template: '<div class="products">'+
              '<product v-for="product in products" :product="product"></product>'+
            '</div>',
  data: function () {
    return {
      products: [],
      loading: true,
      url: catalogAjax ? catalogAjax : window.location.href.replace(urlRegex, ''),
      page: 1
    }
  },
  created: function () {
    var _self = this
    axios.get(this.url+ '/index/'+ this.page +'/1')
    .then(function (response) {
      _self.products = response.data.items
      for (var i = 0; i < _self.products.length; i++) {
        _self.products[i].typeCutting = response.data.typeCutting
      }
    }).catch(function(error) {
      console.log(error)
    })
  }
}

Vue.component('products', Products);

new Vue({
  el: '#app',
  data: {
      loading: true,
  },
  mounted: function() {
    this.loading = false
    $(document).trigger('vueLoaded');
  }
})