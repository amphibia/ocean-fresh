<?php

/**
 * Multi language component
 *
 * @title Language component
 * @description { Multi language component }
 * @preload yes
 * @vendor slaik
 * @package language
 */
class LangComponent extends CApplicationComponent
{

    /**
     * @var array languages array
     */
    public $languages = array(
        'ru' => array('short' => 'Рус', 'long' => 'Русский'),
        'en' => array('short' => 'Eng', 'long' => 'English'),
    );

    /**
     * @var boolean using subdomains for switch languages
     */
    public $useSubDomains = false;

    /**
     * @var string default language.
     */
    public $defaultLanguage = 'ru';

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__);

        // Прикрепление handleMissingTranslation для обработки недостающих переводов
        Yii::app()->messages->attachEventHandler('onMissingTranslation', array('LangComponent', 'handleMissingTranslation'));

        // Get language from domain
        if ($this->useSubDomains === true) {
            if (isset($_SERVER['HTTP_HOST'])) {
                $domainParts = explode('.', $_SERVER['HTTP_HOST']);
                if (array_key_exists($domainParts[0], $this->languages)) {
                    $newLanguage = $domainParts[0];
                }
            }
        } else {
            // Set the application language if provided by GET, POST, session or cookie
            if (isset($_POST['language'])) {
                $newLanguage = $_POST['language'];
            } elseif (isset($_GET['language'])) {
                $newLanguage = $_GET['language'];
            }
        }

        // Setting Language
        if (isset($newLanguage)) {
            $this->setLanguage($newLanguage);
        }

        if (isset(Yii::app()->request->cookies['language']) &&
            array_key_exists(Yii::app()->request->cookies['language']->value, $this->languages)
        ) {
            Yii::app()->language = Yii::app()->request->cookies['language']->value;
        }
    }

    /**
     * @param $language
     * @throws CHttpException
     */
    public function setLanguage($language)
    {
        if (!array_key_exists($language, $this->languages)) {
            throw new CHttpException(500, '');
        }
        $language = CHtml::encode($language);
        Yii::app()->language = $language;
        $cookie = new CHttpCookie('language', $language);
        $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
        Yii::app()->request->cookies['language'] = $cookie;
        return true;
    }

    /**
     * Handle missing translations
     * Автоматическая запись недостающих терминов в файлы переводов текущего языка
     *
     * @param $event
     */
    public static function handleMissingTranslation($event)
    {
        // обрабатываем отсутствие перевода
        $dir = $event->sender->basePath . '/' . $event->language;
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        $filename = $dir . '/' . $event->category . '.php';
        if (is_file($filename)) {
            $tArray = require $filename;
            if (!is_array($tArray)) {
                // Если в файле не массив (некоррентый синтаксис файла перевода) -
                // сбрасываем файл на пустой массив
                $tArray = array();
            }
        }
        $tArray[$event->message] = $event->message;
        $tString = "<?php\n\nreturn array(\n";
        foreach ($tArray as $key => $value) {
            $tString .= "	'" . str_replace("'", "\'", stripslashes($key)) . "'			=> '" . str_replace("'", "\'", stripslashes($value)) . "',\n";
        }
        $tString .= ");";
        // Обновляем файл с новыми записями
        file_put_contents($filename, $tString);
    }

}