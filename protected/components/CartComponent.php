<?php

/**
 * @title Cart application components
 * @description { Cart application components }
 * @preload yes
 * @vendor vitaszanoza
 */
class CartComponent extends CApplicationComponent
{
    /**
     * Тест корзины
     * @return array
     */
    public function testCart($cart, $delivery_type, $delivery_zone, $categoryCart)
    {
        //$cart = json_decode($cartJson);
        foreach($cart as $key => &$item){
            $page = StructPage::model()->findByPk((int)$item['id']);
            if($page && $page->issetCustomField('price') && $page->issetCustomField('grams')){
                $item['title'] = $page->title;
                if($delivery_type != -1) //Если не предзаказ
                {
                    $item['orig_price'] = $page->price;
                    $item['type_payment'] = ($page->category->type_payment == 'в товаре')?$page->type_payment:$page->category->type_payment;
                    if($item['type_payment'] == 'сложный'){
                        $item['grams'] = $page->grams;
                    }
                    $item = self::calcPrice($item);
                    /*
                    $item['delivery_zone'] =  $delivery_zone;
                    if($delivery_type == 1 && ($delivery_zone == 'out' || $item['price'] <= $categoryCart->limit_order)){
                        $item['price_delivery'] =  $categoryCart->price_delivery;
                    } else {
                        $item['price_delivery'] =  0;
                    }
                    */
                }
            }else unset($cart[$key]);
        }
        
        return $cart;
    }

    //Подсчет цены
    protected function calcPrice($item){
        if($item['type_payment'] == 'штуки'){
            $item['price'] = $item['orig_price'] * $item['qty'];
        }elseif($item['type_payment'] == 'килограмм'){
            //item.price / 1000 * (item.gram_weight * 1000)
            $item['price'] = ceil($item['orig_price'] / 1000 * ($item['gram_weight'] * 1000));
        }elseif($item['type_payment'] == 'сложный'){
            //(item.price / 1000 * item.grams) * item.qty)
            $item['price'] = ceil(($item['orig_price'] / 1000 * $item['grams']) * $item['qty']);
        }

        return $item;
    }

    /**
     * Конвертор корзины в html
     * @return string
     */
    public function getCartJsonToHtml($data)
    {
        $html = '<ul>';
        foreach($data as $key => $value){
            if($key == 'cart') continue;
            $html .= '<li>'.Yii::t('cart', $key).': '.$value.'</li>';
        }
        $html .= '</ul>';
        
        //cart
        if(isSet($data['cart']) && count($data['cart'])){
            $html .= '<table border="1"><tr>';
            foreach($data['cart'][0] as $key => $value){
                $html .= '<th>'.Yii::t('cart', $key).'</th>';
            }
            $html .= '</tr>';
            foreach($data['cart'] as $key => $items){
                $html .= '<tr>';
                foreach($items as $key => $value) {
                    $html .= '<td>' . $value . '</td>';
                }
                $html .= '</tr>';
            }
            $html .= '</tr></table>';
        }

        return $html;
    }
}
