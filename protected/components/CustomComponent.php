<?php

/**
 * @title Custom application components
 * @description { Initialize conjuncture of application. }
 * @preload yes
 * @vendor koshevy
 */
class CustomComponent extends CApplicationComponent
{

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__);
        Yii::import('application.services.*');
        Yii::app()->registerService('feedback', 'FeedbackAdminService', 'admin',
            array('title' => 'Feedback email options', 'menuGroup' => 'application',
                'dictionary' => array('actions' => array('form' => 'Feedback email options'))
            )
        );
    }

    /**
     * Обращение к разделу главной страницы.
     */
    public function getMainPageCategory(){
        // Метод перенесен в StructComponent.
        // Оставлен здесь для обратной совместимости.
        return Yii::app()->struct->getMainPageCategory();
    }

    /**
     * Обращение к необходимым разделам
     * @param array|string $categories Массив ключей категорий, либо 1 значение
     * @return StructCategory|array из StructCategory
     */
    public function getCategories($categories){
        // Метод перенесен в StructComponent.
        // Оставлен здесь для обратной совместимости.
        return Yii::app()->struct->getCategories($categories);
    }

    /**
     * Заголовок страницы.
     * @return string
     */
    public function getPageTitle()
    {
        // Формирование заголовка страницы
        $title = Yii::t('custom', 'app_name');
        if (($currentInstance = Yii::app()->struct->currentPage) ||
            ($currentInstance = Yii::app()->struct->currentCategory)
        ) {
            $title = "{$title} &ndash; {$currentInstance->title}";
        } else {
            $controllerName = Yii::app()->controller->id;
            $actionName = Yii::app()->controller->action->id;
            if ($controllerName == 'site' && $actionName == 'index') {
                // Main page
                $mainPageCategory = Yii::app()->custom->getMainPageCategory();
                $title = "{$title} &ndash; {$mainPageCategory->title}";
            } else {
                $title2 = Yii::t('titles', ucfirst($controllerName) . ' ' . $actionName);
                $title = "{$title} &ndash; {$title2}";
            }
        }

        return $title;
    }

    /**
     * Вывод хлебных крошек.
     *
     * @param bool $showMain показывать главную каткгорию
     * @param bool showLatestAlias отображать текущиую страницу
     * @param string $startCategoryAlias отсчетая точка
     * @param string startCategoryTechId startCategoryTechId
     * @return bool
     */
    public function breadCramps($options = array())
    {
        $showMain = (isSet($options['showMain'])) ? $options['showMain'] : true;
        $showLatestAlias = (isSet($options['showLatestAlias'])) ? $options['showLatestAlias'] : false;
        $startCategoryAlias = (isSet($options['startCategoryAlias'])) ? $options['startCategoryAlias'] : '';
        $startCategoryTechId = (isSet($options['startCategoryTechId'])) ? $options['startCategoryTechId'] : 'not';
        if ($startCategoryAlias == '' && $startCategoryTechId == '') $startCategoryAlias = 'main';

        if (!Yii::app()->struct->currentCategory)
            return NULL;

        $chains = array();
        if ($showMain)
            $chains[] = array(
                'alias' => 'main',
                'title' => Yii::t('custom', 'Главная'),
                'uri' => ($uri = '/'),
            );
        $latestAlias = 'main';

        // все предки данной категории
        $tableName = trim(StructCategory::singleTone()->tableName());
        $ancestors = Yii::app()->db->createCommand()
            ->select('id, alias, tech_id, title')
            ->from($tableName)
            ->where(
                '(_left <= :left) AND (_right >= :right) AND type != "group"',
                array(
                    ':left' => Yii::app()->struct->currentCategory->_left,
                    ':right' => Yii::app()->struct->currentCategory->_right
                )
            )
            ->order('_left ASC')
            ->queryAll();

        // включение категорий в цепочку
        $mainPageApproached = false;
        $uri = '';
        foreach ($ancestors as $ancestor) {
            // поиск отсчетной точки
            if (!$mainPageApproached) {
                if ($ancestor['alias'] == $startCategoryAlias || $ancestor['tech_id'] == $startCategoryTechId) {
                    $mainPageApproached = true;
                    if ($startCategoryAlias != 'main') {
                        $uri = StructCategory::cachedInstance($ancestor['id'])->createUrl();
                        if (!preg_match("/\/$/", $uri)) {
                            $uri .= '/';
                        }
                    }
                }

                continue;
            }

            $title = $ancestor['title'];

            $chains[] = array(
                'alias' => $ancestor['alias'],
                'title' => $title,
                //'title' => $ancestor['title'],
                'uri' => ($uri .= "{$ancestor['alias']}/"),
            );

            $latestAlias = $ancestor['alias'];

        }

        // включение активного материала
        if ($page = Yii::app()->struct->currentPage) {
            $chains[] = array(
                'alias' => $page->alias,
                'title' => $page->title,
                'uri' => ($uri .= "{$page->alias}/"),
            );
            $latestAlias = $page->alias;
        }

        $result = '';
        //$result .= CHtml::openTag('ul');
        foreach ($chains as $chain) {
            if ($chain['alias'] != $latestAlias) {
                $result .= CHtml::tag(
                    'li',
                    array('data-alias' => $chain['alias']),
                    CHtml::link($chain['title'], $chain['uri'])
                );
            } elseif ($showLatestAlias) {

                $result .= CHtml::tag(
                    'li',
                    array('data-alias' => $chain['alias']),
                    $chain['title']
                );
            }
        }
        //$result .= CHtml::closeTag('ul');

        return $result;
    }

    /**
     * Конвертор корзины в html
     * @return string
     */
    public function getCartJsonToHtml($json)
    {
        $html = json_decode($json);
        die("<pre>".print_r($html, true)."</pre>");
        return $html;
    }
}
