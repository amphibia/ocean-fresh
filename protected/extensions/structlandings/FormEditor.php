<?php

/**
 * @package structlandings
 * @author koshevy
 *
 * Виджет, использующийся в адиминстративной части сайта для редактирования списка полей,
 * которые будут выводится в лэндинг-формах.
 *
 * Примечание: виджет редактирует данные, хранящиейся в представлении массива,
 * в формате, совместимом с форматом хранения настроек полей и пресетов в модели LandingForm.
 *
 * Виджет использует функционал хэлпера FormEditorHelper, который также используется в валидаторе
 * FormDataValidator, который предполагается к использованию в связке с этим виджетом.
 *
 * При сохранении данных о полях (редактор полей виджета), используется флаг defaultFields, который,
 * будучи включенным, указывает виджету, что поля не нужно сохранять, и каждый раз брать настройки полей
 * по-умолчанию. При изменении конфигурации полей, флаг снимается. Однако, в редакторе есть возможность
 * вернуть настройки полей по-умолчанию.
 */
class FormEditor extends CInputWidget
{

    /**
     * @var ActiveRecord Модель, с которой завязан этот элемент управления.
     */
    public $model;

    /**
     * @var string Поле в используемой модели, к которому привязан элемент управления.
     */
    public $attribute;

    /**
     * @var array Финальное значение, которое используется в представлении.
     */
    public $value;

    /**
     * @var array представления, используемые виджетом.
     */
    public $views = array();

    /**
     * Максимальное количество полей.
     * @var int
     */
    public $maxFieldsNumber = NULL;


    public $assets = array();



    public function init()
    {
        Yii::import(Yii::localExtension('structlandings', 'models.*'));
        Yii::import(Yii::localExtension('structlandings', 'lib.*'));
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        $this->value = $this->model->{$this->attribute};
        FormEditorHelper::normalizeData($this->value);

        return true;
    }

    public function run()
    {
        $presets = FormEditorHelper::getRulePresets();
        $presetsList = array();
        foreach($presets as $key => $preset){
            $presetsList[$key] = $preset['title'];
        }

        $this->render('backend/formEditor', array(
            'attrPrefix' => get_class($this->model)."[{$this->attribute}]", // для именования полей, относящихся к виджету
            'presetsList' => $presetsList
        ));
    }

    /**
     * Хелпер для вывода наименования поля в редакторе полей.
     *
     * @param string|NULL $key
     * @param array $field
     * @param array $presetsList
     * @param $attrPrefix
     * @param boolean $bill
     */
    protected function _echoFieldEditorItem($key, $field, Array $presetsList, $attrPrefix, $bill = false){

        $ai_index = sizeof($this->value['fields']);
        $key or $key = "custom_field_{$ai_index}";
        $field or $field = FormEditorHelper::getFieldBlank();

        require __DIR__.'/views/backend/_fieldEditorItem.php';
    }


    /**
     * FormDataValidator - объект, хранящий подробную информацию
     * об ошибках, возникших при проверке данных.
     * @var null
     */
    protected $_formDataValidator = NULL;

    /**
     * Получение детальной информации об ошибках при заполнении полей валидатора.
     *
     * @param $attribute
     * @param $index
     * @return null
     */
    protected function _showErrorDetails($attribute, $index = 0)
    {
        // При первом обращении происходит поиск валидатора,
        // хранящего информацию об детальных ошибках.
        if(!$this->_formDataValidator){
            foreach($this->model->getValidators($this->attribute) as $validator){
                if($validator instanceof FormDataValidator)
                    $this->_formDataValidator = $validator;
            }

            // валидатор не был найден
            if(!$this->_formDataValidator)
                return NULL;
        }

        $errorInfo = &$this->_formDataValidator->errorInfo;
        if(isset($errorInfo[$attribute]) && isset($errorInfo[$attribute][$index])){
            foreach($errorInfo[$attribute][$index] as $message)
            {
                echo CHtml::tag('div', array(),
                    CHtml::tag('span', array(
                        'class' => 'help-inline error',
                    ), $message) .
                    CHtml::tag('div', array('class'=>'clear clearfix'), '')
                );
            }
        }
    }

}
?>