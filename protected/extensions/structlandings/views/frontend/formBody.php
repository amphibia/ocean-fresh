<?php
if($this->ajaxSend && !Yii::app()->clientScript->isScriptRegistered('beforeYiiValidate', CClientScript::POS_BEGIN))
{
    $beforeYiiValidate = <<<'EOT'
function beforeYiiValidate($form, errors, hasError)
{
    if(!hasError && !$form.hasClass('loading')){
        var data = $form.getFields();
        $form.addClass('loading');
        $form.request($form.attr('action'), data, function(data){
            var messages = data.__commands.message;
            setTimeout(function(){
                $form.removeClass('loading');
                $form.html('');
                for(var i in messages)
                    $form.append('<p class="'+(parseInt(data.result)?'success':'error')+'">'+messages[i]+'</p>');
            }, 600);
        });
    }

    return false;
}
EOT;

    Yii::app()->clientScript->registerScript(
        'beforeYiiValidate',
        $beforeYiiValidate,
        CClientScript::POS_BEGIN
    );
}

$model = &$this->model;
$controller = Yii::app()->controller;

// эта ли форма была отправлена
$isThisForm = isset($controller->formID)
    ? ($controller->formID == $this->id)
    : true; // по умолчанию ведет себя как единственная

if( !isset($controller->resultOfSend)
    || !$controller->resultOfSend
    || !$isThisForm)

     require __DIR__.'/_fields.php';
else require __DIR__.'/_sendMessage.php';

?>


