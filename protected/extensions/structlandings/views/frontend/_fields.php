<?php
$numberItem = 0;
?>
<?php foreach ($model->fields as $fieldName => $fieldOptions): ?>
    <?php $preset = $model->rulePresets[$fieldOptions['preset']]; ?>
    <?php

    if(isSet($this->template[$numberItem]['top_external'])){
        echo $this->template[$numberItem]['top_external'];
    }

    echo CHtml::openTag('div', array(
        'class' => 'form-group' . ($model->hasErrors($fieldName) ? ' error' : ''),
        'data-element-type' => $preset['element'],
        'required' => $fieldOptions['required'],
    ));

    $attributes = array(
        'class' => 'form-control',
        //'placeholder' => $model->getAttributeLabel($fieldName),
        'data-preset' => $fieldOptions['preset'],
    );

    if(isSet($this->template[$numberItem]['top_internal'])){
        echo $this->template[$numberItem]['top_internal'];
    }

    if(!$this->show_labels) $attributes['placeholder'] = $model->getAttributeLabel($fieldName);
    if($this->show_labels) echo $this->labelEx($model,$fieldName, array('class' => 'control-label'));

    echo CHtml::openTag('div', array(
        'class' => 'controls'
    ));

    switch ($preset['element']) {
        case 'text':
            echo $this->textField($model, $fieldName, $attributes);
            break;

        case 'textarea':
            echo $this->textArea($model, $fieldName, $attributes);
            break;

        case 'select':
            if (isset($fieldOptions['values'])) {
                $values = $fieldOptions['values'];
            } elseif (isset($fieldOptions['arguments'])) {
                //для обработки данных из MagicForm
                $values = explode("\n", str_replace('\r', '', $fieldOptions['arguments']));
            } else $values = $preset['values']['in'];

            echo $this->dropDownList(
                $model,
                $fieldName,
                $values,
                $attributes
            );
            break;

        case 'radio':
            if (isset($fieldOptions['arguments'])) {
                $values = explode("\n", str_replace('\r', '', $fieldOptions['arguments']));
                echo $this->radioButtonList($model,$fieldName,$values, $attributes);
            }
            break;

        case 'file':
            echo $this->fileField($model, $fieldName, $attributes);
            break;

        case 'captcha':
            $this->widget('CCaptcha');
            echo $this->textField($model, $fieldName, $attributes);
            //echo "<pre>".print_r($model->rules(), true)."</pre>";
            break;

        case 'hidden':
            echo $this->hiddenField($model, $fieldName, $attributes);
            break;

    }

    echo $this->error($model, $fieldName);

    if(isSet($this->template[$numberItem]['bottom_internal'])){
        echo $this->template[$numberItem]['bottom_internal'];
    }

    echo CHtml::closeTag('div');
    echo CHtml::closeTag('div');

    if(isSet($this->template[$numberItem]['bottom_external'])){
        echo $this->template[$numberItem]['bottom_external'];
    }

    $numberItem++;
    ?>
<?php endforeach; ?>

<?php
if(isSet($this->template['button']['top_external'])){
    echo $this->template['button']['top_external'];
}
?>

    <button type="submit" class="btn btn-default btn-submit"><!--- class btn-default устарел. используется для совместимости прежних проектов --->
        <?= $this->model->buttonTitle ? $this->model->buttonTitle : Yii::t('custom', 'Отправить') ?>
    </button>

<?php
if(isSet($this->template['button']['bottom_external'])){
    echo $this->template['button']['bottom_external'];
}
?>