<?php

echo CHtml::tag('li', array('class' => 'field-element alert alert-success'.($bill?' new bill':NULL)),

    // Ручка для перетаскивания и название
    CHtml::tag('div', array('class' => 'input-wrapper'),
        '<i class="icon-resize-vertical handle"></i> '.
        CHtml::textField(
            "{$attrPrefix}[fieldTitle][]", $field['title'],
            array(
                'disabled' => $bill ? 'disabled' : NULL,
                'required' => 'required',
                'maxlength' => 256,
                'class' => 'title-input',
                'placeholder' => Yii::t('structlandings', 'Введите название')
            )
        )
    ) .

    // Выбор пресета
    CHtml::tag('div', array('class' => 'list-wrapper'),
        CHtml::dropDownList("{$attrPrefix}[fieldPreset][]",
            $field['preset'], $presetsList, array(
                'disabled' => $bill ? 'disabled' : NULL,
            ))
    ) .

    // Управляющие кнопки
    CHtml::hiddenField(
        "{$attrPrefix}[fieldRequired][]",
        $field['required'],
        array(
            'disabled' => $bill ? 'disabled' : NULL,
            'class' => 'required-input'
        )
    ) .

    CHtml::tag('div', array('class' => 'controls-wrapper'),
        '<a class="required-control btn btn-warning'.($field['required']?' active':NULL).'" title="'.Yii::t('structlandings', 'Обязательно для заполнения').'"><i class="icon-asterisk icon-white"></i></a> '.
        '<a class="extended-control btn btn-info" title="'.Yii::t('structlandings', 'Расширенные настройки').'"><i class="icon-wrench icon-white"></i></a> '.
        '<a class="delete-control btn btn-danger" title="'.Yii::t('structlandings', 'Удалить поле').'"><i class="icon-remove icon-white"></i></a> '
    ) .

    // Расширенные настройки
    CHtml::tag('div', array('class' => 'extended-options'),

        // Сообщение "required", которое будет использоваться
        CHtml::tag('label', array(),
            Yii::t('structlandings', 'Сообщение "Необходимо заполнить":') .
            CHtml::textField("{$attrPrefix}[fieldRequiredMessage][]",
                $field['requiredMessage'],
                array(
                    'disabled' => $bill ? 'disabled' : NULL,
                    'class' => 'required-message',
                    'maxlength' => 64,
                    'placeholder' => Yii::t('structlandings', 'По умолчанию')
                )
            )
        ) .

        // Доболнительные аргументы
        CHtml::tag('label', array(),
            Yii::t('structlandings', 'Дополнительные аргументы (для опытных пользователей)') .
            CHtml::textArea(
                "{$attrPrefix}[fieldArguments][]",
                $field['arguments'],
                array(
                    'disabled' => $bill ? 'disabled' : NULL,
                    'maxlength' => 256,
                    'class' => 'extended-arguments',
                )
            )
        )
    ) .

    $this->_showErrorDetails('fieldData', $ai_index)
);