<?php

return array
(
    'buttonTitle' => Yii::t('structlandings', 'Отправить'),

    /**
     * Варианты возможных видов полей.
     * @var array
     */
    'rulePresets' => array
    (
        'simpleText' => array
        (
            'title' => Yii::t('structlandings', 'Текст в одну строку'),
            'requiredMessage' => Yii::t('structlandings', 'Поле обязательно для заполнения'),
            'element' => 'text',
            'validators' => array('simplePurify')
        ),

        'simpleTextarea' => array
        (
            'title' => Yii::t('structlandings', 'Многострочное текстовое поле'),
            'requiredMessage' => Yii::t('structlandings', 'Поле обязательно для заполнения'),
            'element' => 'textarea',
            'validators' => array('simplePurify')
        ),

        'email' => array
        (
            'title' => Yii::t('structlandings', 'Электронный адрес'),
            'requiredMessage' => Yii::t('structlandings', 'Эл. адрес обязателен'),
            'element' => 'text',
            'validators' => array('email'),
            'params' => array
            (
                'email' => array(
                    'message' => Yii::t('structlandings', 'Неправильный электронный адрес')
                )
            )
        ),

        'date' => array
        (
            'title' => Yii::t('structlandings', 'Дата'),
            'requiredMessage' => Yii::t('structlandings', 'Укажите дату бронирования'),
            'element' => 'text',
            'validators' => array('match'),
            'params' => array
            (
                'match' => array(
                    'pattern' => '/^(\d{2}\.){2}\d{4}$/',
                    'message' => Yii::t('structlandings', 'Дата имеет неправильный формат')
                )
            )
        ),

        'time' => array
        (
            'title' => Yii::t('structlandings', 'Время'),
            'requiredMessage' => Yii::t('structlandings', 'Укажите время'),
            'element' => 'text',
            'validators' => array('match'),
            'params' => array
            (
                'match' => array(
                    'pattern' => '/^(([0-1][0-9])|(2[0-3])):([0-5][0-9])$/',
                    'message' => Yii::t('structlandings', 'Неправильное время')
                )
            )
        ),

        'phone' => array(
            'title' => Yii::t('structlandings', 'Номер телефона'),
            'requiredMessage' => Yii::t('structlandings', 'Укажите номер телефона'),
            'element' => 'text',
            'validators' => array('match'),
            'params' => array(
                'match' => array(
                    'pattern' => '/^\+\d\s\((701|702|775|778|705|777|771|776|707|747|700)\)\s\d{3}\-\d{2}\-\d{2}$/',
                    'message' => Yii::t('structlandings', 'Укажите правильный номер телефона')
                )
            )
        ),

        'numerical' => array
        (
            'title' => Yii::t('structlandings', 'Ввод числа'),
            'requiredMessage' => Yii::t('structlandings', 'Поле обязательно для заполнения'),
            'element' => 'text',
            'validators' => array('numerical'),
            'params' => array(
                'numerical' => array(
                    'min' => 1,
                )
            )
        ),

        'select' => array
        (
            'title' => Yii::t('structlandings', 'Выпадающий список'),
            'requiredMessage' => Yii::t('structlandings', 'Поле обязательно для заполнения'),
            'element' => 'select',
            'validators' => array('simplePurify'),
            'params' => array()
        ),

        'radio' => array
        (
            'title' => Yii::t('structlandings', 'radioButtonList'),
            'requiredMessage' => Yii::t('structlandings', 'Поле обязательно для заполнения'),
            'element' => 'radio',
            'validators' => array('simplePurify'),
            'params' => array()
        ),

        'file' => array
        (
            'title' => Yii::t('structlandings', 'Выбрать файл'),
            'requiredMessage' => Yii::t('structlandings', 'Поле обязательно для заполнения'),
            'element' => 'file',
            'validators' => array('file'),
            'params' => array(
                'allowEmpty' => true,
                'types' => array('doc', 'docx', 'rtf', 'odm', 'pdf', 'odt'),
                'message' => Yii::t('structlandings', 'Не верный тип файла. Допустимые типы: doc, docx, rtf, odm, pdf')
            )

        ),

        'captcha' => array
        (
            'title' => Yii::t('structlandings', 'Код проверки'),
            'requiredMessage' => Yii::t('structlandings', 'Не верный код проверки'),
            'element' => 'captcha',
            'validators' => array('captcha'),
            'params' => array(
                'captcha' => array(
                    'allowEmpty' => !CCaptcha::checkRequirements(),
                    'message' => Yii::t('structlandings', 'Не верный код проверки')
                )
            )

        ),

        'hidden' => array
        (
            'title' => Yii::t('structlandings', 'Скрытое поле'),
            'requiredMessage' => Yii::t('structlandings', 'Не верный код проверки'),
            'element' => 'hidden',
            'validators' => array('simplePurify')
        ),
    ),

    /**
     * Поля для этой формы.
     * @var array
     */
    'fields' => array
    (
        'field_fullName' => array(
            'requiredMessage' => Yii::t('structlandings', 'Укажите имя и фамилию'),
            'title' => Yii::t('structlandings', 'Имя и фамилия'),
            'preset' => 'simpleText',
            'required' => true,
        ),

        'field_date' => array(
            'title' => Yii::t('structlandings', 'Дата'),
            'preset' => 'date',
            'required' => true,
        ),

        'field_phone' => array(
            'title' => Yii::t('structlandings', 'Телефон'),
            'preset' => 'phone',
            'required' => true,
        ),

        'field_time' => array(
            'title' => Yii::t('structlandings', 'Время'),
            'preset' => 'time',
            'required' => true,
        ),

        'field_email' => array(
            'title' => Yii::t('structlandings', 'Электронный адрес'),
            'preset' => 'email',
            'required' => true,
        ),

        'field_guestsNumber' => array(
            'title' => Yii::t('structlandings', 'Количество гостей'),
            'preset' => 'numerical',
            'required' => true,
        ),
    ),

);