<?php
/**
 * @package structlandings
 * @author koshevy
 *
 * Статический хелпер для вынесения общих функций в FormEditor, FormDataValidator
 * и models LandingForm.
 */

class FormEditorHelper
{
    /**
     * Экземпляр формы по-умолчанию, на основании настроек которой,
     * будет создаваться массив данных для редактора и ваидатора данных форм.
     * @var LandingForm
     */
    protected static $_formModelForInstance = NULL;

    /**
     * Образец данных формы.
     *
     * Используется для создания образа данных для виджета FormEditor, валидатора
     * FormDataValidator, (на основании объекта модели LandingForm или при инициализации
     * данных). А также, собственно, для инициализации модели  LandingForm.
     *
     * @var array
     */
    protected static $_formDataBlank = array(
        'title' => NULL,
        'description' => NULL,
        'buttonTitle' => NULL,
        'defaultFields' => true,    // если этот флаг действителен, данные по полям не сохраняются
        'buttonTitle' => NULL,
        'fields' => array(

            // образец свойств поля
            array(
                'title' => NULL,
                'preset' => 'simpleText',
                'required' => false,
                'requiredMessage' => NULL,
                'arguments' => NULL,
            )
        ),

        // настройки, как предполагается, из FeedbackOptionsForm
        'theme' => NULL,
        'email' => array(),
    );

    /**
     * Получение заготовки данных новой формы.
     * @return array
     */
    public static function getFormDataBlank(){
        return self::$_formDataBlank;
    }

    /**
     * Образец модели формы по-умолчанию.
     * @return LandingForm
     */
    public static function getFormModelForInstance()
    {
        if(!self::$_formModelForInstance)
            self::$_formModelForInstance = new LandingForm;

        return self::$_formModelForInstance;
    }

    /**
     * Пресеты форм.
     * @return array
     */
    public static function getRulePresets()
    {
        return self::getFormModelForInstance()->rulePresets;
    }

    /**
     * Создание заготовки данных.
     * @param LandingForm $formInstance
     * @return array
     */
    public static function createFormDataBlank($formInstance = NULL)
    {
        if(!$formInstance) $formInstance = self::getFormModelForInstance();

        $data = self::$_formDataBlank;
        $data['fields'] = self::normalizeFields($formInstance->fields);

        return $data;
    }

    /**
     * Получение нормализованных полей на основании заданных,
     * или с нуля.
     *
     * @param $fields
     * @return mixed
     */
    public static function normalizeFields($fields)
    {
        // эта функция может использоваться отдельно от ::createFormDataBlank(),
        // тогда она болжна генерить поля сама
        if(!$fields){
            $formInstance = self::getFormModelForInstance();
            $fields = $formInstance->fields;
        }

        foreach($fields as $key => $fieldData)
        {
            $fields[$key] = array_merge(
                current(self::$_formDataBlank['fields']),
                $fieldData
            );

            // фильтр внедрения/разрыва для текстовых данных в  форме данных
            $fields[$key]['title'] = htmlspecialchars(strip_tags($fields[$key]['title']));
            $fields[$key]['requiredMessage'] = htmlspecialchars(strip_tags($fields[$key]['requiredMessage']));
            $fields[$key]['arguments'] = strip_tags($fields[$key]['arguments']);
            $fields[$key]['required'] = (bool)$fields[$key]['required'];
        }

        return $fields;
    }

    /**
     * Заготовка данных для нового поля.
     * @return mixed
     */
    public static function getFieldBlank(){
        return current(self::$_formDataBlank['fields']);
    }

    /**
     * Получение массива данных формы на основе источника:
     *  - пустое значение (по умолчанию)
     *  - сохраненное в JSON значение
     *  - уже нормальизованное значение (не изменяется)
     *
     * Примечание: валидация не производится.
     *
     * @param $srcData
     * @return void
     */
    public static function normalizeData(&$srcData)
    {
        Yii::import(Yii::localExtension('structlandings', 'models.*'));

        if(!$srcData)
            $srcData = self::createFormDataBlank();
        else
        {
            if(!is_array($srcData))
                $srcData = json_decode($srcData, true);

            $srcData = array_merge(self::createFormDataBlank(), $srcData);
        }

        // если нет полей — значит режим "по-умолчанию"
        if(!isset($srcData['fields']) || !is_array($srcData['fields']) || !count($srcData['fields']))
            $srcData['defaultFields'] = true;

        isset($srcData['defaultFields'])
        or $srcData['defaultFields'] = false;

        $srcData['fields'] = self::normalizeFields(
            isset($srcData['fields']) ? $srcData['fields'] : array()
        );

        // фильтр внедрения/разрыва для текстовых данных в  форме данных
        $textData = array('title', 'description', 'theme', 'buttonTitle');
        foreach($textData as $item){
            if(!isset($srcData[$item]) || !$srcData[$item]) continue;
            $srcData[$item] = strip_tags($srcData[$item]);
            $srcData[$item] = htmlspecialchars($srcData[$item]);
        }

        // очиска пустых email
        foreach($srcData['email'] as $k => $v){
            if(!$v) unset($srcData['email'][$k]);
        }
    }

    /**
     * Штамп-подтверждение запроса для категории.
     * @param $sourceID
     * @param $field
     * @param $timestamp
     * @return string
     */
    public static function createAliasHash($sourceID, $field, $timestamp){
        return mb_substr(md5("__ALIAS_HASH {$sourceID} {$field} {$timestamp}"), 0, 8, 'utf8');
    }

} 