<?php

/**
 * @author koshevy
 *
 * Виджет таблицы для работы с ActiveRecord-моделями; с поддержкой пагинации,
 * сортировки, поиска по содержимому, фильтров.
 *
 * Также, есть возможность добавлять элементы управления (кнопки) к строке или
 * верхней панели. Используются Bootstrap - элементы управления. Можно использовать,
 * как одиночные кнопки, так и кнопки с выпадающими меню.
 *
 * ПРИМЕЧАНИЕ: формат настроек кнопки, передаваемых таблице, идентичен формату
 * настроек виджетов 'BootButton' и 'BootButtonGroup', за исключением того, что
 * вместо параметров 'url', там где это требуется, нужно отправлять 'route' по образцу:
 * array(<route>, 'attrName1', 'attrName2') для кнопок в строках (по именам
 * подставляются из атрибутов строки).
 *
 * Для кнопок в шапке таблицы, вместо имён атрибутов указываются значения.
 *
 * @todo: В шапке таблицы пока-что не реализована поддержка BootButtonGroup.
 *
 *
 * @example
 * Прежде чем использовать виджет, следует определиться, какие из возможностей
 * будут использоваться (сортировка, фильтры, поиск, постраничный вывод).
 * По умолчанию, используются все; фильтры используются, если заполнено свойство
 * @see ActiveTable::$scopes.
 *
 * Каждый из этих инструментов требует своего аргумента в действии, использующем
 * виджет, т.к. все аргументы передаются через GET.
 * Имена аргументов, с которыми будет работать виджет, указываются в следующих свойствах:
 * @see ActiveTable::$searchVar
 * @see ActiveTable::$scopeVar
 * @see ActiveTable::$sortVar
 * Аргумент, указывающий номер страницы всегда 'page'.
 * Также можно указать имя аргумента, в CSort свойстве CActiveDataProvider:
 * $this->model->getActiveDataProvider()->sort->sortVar = 'orderBy';
 *
 * Чтобы не использовать какие-то из перечисленных инструментов, используйте следующие
 * свойства:
 * @see ActiveTable::$useHeader = false - отключение заголовков с сортировкой
 * @see ActiveTable::$useSearch = false - отключение поиска
 * @see ActiveTable::$usePagination = false - отключение посртаничного вывода
 *
 * Чтобы указать управляющие элементы, используйте:
 * @see ActiveTable::$buttons
 * @see ActiveTable::$headerButtons
 *
 * Чтобы указать роут действия, с которым работает таблица, укажите (обязательно)
 * @see ActiveTable::$route.
 *
 * Пример конфигурации, используемой для вызова виджета:
 * return array(
 *      'fieldsOrder' => array('id', 'title', 'update'),
 *      'route' => 'index',
 *      'buttons' => array(
 *          'update' => array(
 *              'buttonType'=>'link',
 *              'size'=>'small',
 *              'icon'=>'icon-pencil',
 *              'label'=>Yii::t('activetable', 'Edit'),
 *              'route' => array('update', 'id'),
 *          )
 *      )
 *      'pageSize' => 25,
 *      'sortVar' => 'orderBy',
 *  ),
 * .
 * .
 *
 * Пример действия, используещего виджет (вызыввается не напрямую, а через
 * представление)
 * .
 * .
 * public function actionIndex($orderBy = NULL, $scope = NULL, $search = NULL, $page = NULL){
 *      $page = intval($page?($page-1):$page);
 *      $this->model->getActiveDataProvider()->pagination->currentPage = $page;
 *      $this->indexTableOptions['model'] = $this->model;
 *      $this->render($this->views['index'], array('config' => $this->indexTableOptions));
 * }
 * .
 * .
 * // представление, вызываемое выше
 * $this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), $config);
 * .
 * .
 *
 */
class ActiveTable extends CWidget
{
    /**
     * Счётчик используемых в приложении таблиц
     * @var integer
     */
    protected static $_instCount = 0;


    /**
     * Идентификатор таблицы.
     * @var string
     */
    public $id = NULL;

    /**
     * Представления, с которыми работает виджет (при необходимости, можно
     * поменять представления).
     * @var array
     */
    public $views = array();

    /**
     * Общий роут, который применяется для постраничногоо вывода и сортировки.
     * ПРИМЕЧАНИЕ: для кнопок используется другая система создания ссылок.
     * @var string
     */
    public $route = NULL;

    /**
     * Параметры для формирования URL.
     * @var array
     */
    public $params = array();

    /**
     * Использование альтернативного перелистывания (перелистывание с помощью
     * mousewheel, активные границы).
     * Работает только при включенных ActiveTable::$useAjax и ActiveTable::$usePagination.
     * @var type
     */
    public $useAlternativePagination = NULL;

    /**
     * Использовать ли AJAX для этой таблицы
     * @var boolean
     */
    public $useAjax = NULL;

    /**
     * HTML-контейнер на странице,
     * @var string
     */
    public $ajaxField = NULL;


    /**
     * Модель, с которой работает таблица.
     * @var ActiveRecord
     */
    public $model = NULL;

    /**
     * Подписи к обычным полям (по умолчанию задаются в модели).
     * Также здесь могут указываться подписи к дополнительным полям.
     *
     * @var array
     */
    public $labels = array();

    /**
     * Массив с конверторами-предобработчиками данных перед выводом.
     * Конвертор назначается для каждого поля (например:
     * $preConvertors["id"] = function($fieldName, $fieldValue, $row, $isHeader = FALSE, $model){ ... }.
     *
     * @var array
     */
    public $preConvertors = array();

    /**
     * Дополнительные поля. Для формирования значений используйте $preConvertors.
     * @var array
     */
    public $additionalFields = array();

    /**
     * Поля, которые будут использоваться.
     * Если NULL, будут использоваться все поля.
     *
     * Может использоваться вместе с $useFields, применятся в первую очередь.
     * $ingnoreFields работает по остаточному принципу.
     * @var array
     */
    public $useFields = NULL;

    /**
     * Поля, которые будут игнорироваться.
     * Если NULL, будут использоваться все поля.
     *
     * Может использоваться вместе с $useFields, который применятся в первую очередь.
     * В этом случае, работает по остаточному принципу.
     * @var array
     */
    public $ignoreFields = NULL;

    /**
     * Порядок указания полей (включая дополнительные поля и поля с управляющими
     * кнопками). Если свойство не установлено, используется стандартный порядок:
     * требуемые плоя модели в естественном порядке, дополнительные поля, поля
     * управляющих элементов.
     *
     * Примечание: при использовании данного свойства, $useFields и $ignoreFields
     * игнорируются.
     *
     * @var array
     */
    public $fieldsOrder = NULL;

    /**
     * Массив с настройками для кнопок.
     * Совпадает с форматом BootstrapButton, за исключением того, что вместо 'url'
     * указывается 'route'.
     *
     * 'route' применяется для createUrl():
     * нужно указывать array(<route>, 'arg_name1', 'arg_name2'...).
     * Если NULL, применяется createUrl($name, array('id'=>row[id])).
     *
     * @var array
     */
    public $buttons = array();

    /**
     * Свойства элементов управления по умолчанию.
     * @var type
     */
    public $defualtButtonOptions = array();

    /**
     * Использование заголовков - полей с названием и функции сортировки.
     * ПРИМЕЧАНИЕ: если ActiveTable::$useHeader == true, но в действии, использующем
     * виджет нет аргумента с именем, указанным в ActiveTable::$sortVar, будет
     * вызвано исключение 'The action was unnecessary arguments', т.к. именно
     * в ней указывается значение поля сортировки.
     *
     * @var boolean
     */
    public $useHeader = NULL;

    /**
     * Кнопки, которые должны отобразиться в хэдере.
     * Свойства полностью совместимы с BootstrapButton, за исключением того,
     * что в качестве 'url' принимаются 'route' - шаблоны пути.
     * @var array
     */
    public $headerButtons = array();

    /**
     * Содержимое, добавляемое к заголовку в начале.
     * @var string
     */
    public $headerPrepend = array();

    /**
     * Содержимое, добавляемое к заголовку в конце.
     * @var string
     */
    public $headerAppend = array();

    /**
     * Использовать форму поиска или нет.
     * ПРИМЕЧАНИЕ: если используется поиск, среди аргументов действия должен
     * аргумент, имя которого указано ActiveTable::$searchVar.
     * @var boolean
     */
    public $useSearch = NULL;

    /**
     * Название индекса в $_POST, в котором хранится запрос на поиск по
     * своему содержимому (отсортировка).
     * Виджет сам достаёт переменную и обрабатывает.
     * @var type
     */
    public $searchVar = NULL;

    /**
     * Значение поля поиска.
     * @var string
     */
    public $search = NULL;

    /**
     * Функция поиска.
     * Аргументы функции: $model, $search
     *
     * @var callback
     */
    public $searchFunction = NULL;

    /**
     * Попись к форме поиска.
     * @var string
     */
    public $searchLabel = NULL;

    /**
     * Функции - условия для отсортировки (по фильтрам).
     * Формат: $scopes["published"] => function($model, $scopeValue){ ... }
     *
     * Параметры функции: $model, $scopeValue.
     *
     * Задача функции - настроить отсортировку в $model.
     *
     *
     * ПРИМЕЧАНИЕ: имя "reset" будет игнорироваться, т.к. значение "reset" для
     * $this->scope зарезервировано для сброса фильтра.
     *
     * @var array (of callback)
     */
    public $scopes = array();

    /**
     * Подписи к фильтрам.
     * @var array
     */
    public $scopeLabels = array();

    /**
     * Описания фильтров.
     * @var array
     */
    public $scopeDescriptions = array();

    /**
     * Фильтр по умолчанию.
     * @var string
     */
    public $defaultScope = NULL;

    /**
     * Текущий фильтр отсортировки.
     * Значение "reset" означает, что фильтр сброшен.
     *
     * @var string
     */
    public $scope = NULL;

    /**
     * Переменная в $_GET, хранящий текущий фильтр.
     * @var string
     */
    public $scopeVar = NULL;

    /**
     * Дополнительный параметр, который позволяет указывать значение фильтра
     * для $scopesFunction. Например, если идёт отсортировка "по алфавиту",
     * параметр может передавать "А-Б".
     *
     * ПРИМЕЧАНИЕ: пока нигде не используется.
     * @var string
     */
    public $scopeValue = NULL;

    /**
     * Переменная в $_GET, хранящая значение текущего фильтра.
     * @var string
     */
    public $scopeValueVar = NULL;

    /**
     * Поле сортировки.
     * @var string
     */
    public $sortField = NULL;

    /**
     * Имя параметра, в котором хранится поле сортировки.
     * @var string
     */
    public $sortVar = NULL;

    /**
     * Направление сортировки (ASC/DESC).
     * @var string
     */
    public $sortDirection = NULL;

    /**
     * Выводить или не выводить постраничную листалку.
     * ПРИМЕЧАНИЕ: если используется пагинация, среди аргументов действия должен
     * быть аргумент $page.
     * @var boolean
     */
    public $usePagination = NULL;

    /**
     * Настройки элемента пагинации.
     * @var array
     */
    public $pagerOptions = array();

    /**
     * Количество элементов на одной странице.
     * Можно указывать напрямую, но здесль указывать нагляднее - все настройки
     * в одном месте.
     * @var integer
     */
    public $pageSize = NULL;


    /**
     * Использовать выделение строк и групповые операции над этими строками.
     * @var boolean
     */
    public $useGroupOperations = NULL;

    /**
     * Роут действия, по которому будет автоматически подгружаться панель
     * управления групповыми операциями. В качестве аргумента будет передаваться
     * массив с ID  выделенных элементов.
     *
     * Параметр должен называться $ids - для соместимости с клиентской частью
     * ActiveTable.
     *
     * @var string
     */
    public $groupOpeartionsBarRoute = NULL;

    /**
     * Тэг таблицы.
     * @var string
     */
    public $tableTag = NULL;

    /**
     * Тэг, оборачивающий заголовок таблицы.
     * Может быть не указанным.
     * @var string
     */
    public $tableHeadTag = NULL;

    /**
     * Тэг, оборачивающий тело таблицы.
     * Может быть не указанным.
     * @var string
     */
    public $tableBodyTag = NULL;

    /**
     * Тэг, группирующий строки.
     * @var string
     */
    public $rowGroupTag = NULL;

    /**
     * Количество строк в группе.
     * @var string
     */
    public $rowGroupSize = NULL;

    /**
     * Тэг строки.
     * @var string
     */
    public $rowTag = NULL;

    /**
     * Тэг ячейки.
     * @var string
     */
    public $cellTag = NULL;

    /**
     * HTML - атрибуты различных элементов таблицы.
     * Например $this->htmlAttributes["row"] = array("valign"=>"top");
     *
     * @var array
     */
    public $htmlAttributes = array();

    /**
     * Классы для различных элементов таблицы.
     * Например $this->classes["headRow"] = array("row", "headRow");
     *
     * @var array
     */
    public $classes = array();

    /**
     * Опубликованные ассетсы для этого виджета.
     * @var string
     */
    public $assets = array();


    protected $_attributeNames = array();

    protected $_count = NULL;

    public function init()
    {
        defined('ACTVE_TABLE_MT') or define('ACTVE_TABLE_MT', microtime(1));

        // загрузка конфига по умолчанию
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        if (!$this->params || !sizeof($this->params))
            $this->params = $_GET;

        $this->labels = array_merge($this->model->attributeLabels(), $this->labels);
        if (!$this->id) $this->id = get_class() . (self::$_instCount ? "_" . self::$_instCount : NULL);
        self::$_instCount++;

        $this->_attributeNames = $this->model->attributeNames();


        /**
         * @todo: Непонятки c 0-based номером страницы.
         */

        if ($this->usePagination) {
            $pagination = $this->model->activeDataProvider->pagination;;

            // номер страницы всегда добвляется к параметрам роута
            if ($pagination->currentPage) $this->params["page"] = ((int)$pagination->currentPage
                ? ($pagination->currentPage + 1) : 1);
            if ($this->pageSize) $pagination->pageSize = $this->pageSize;
        } else $this->model->activeDataProvider->pagination->pageSize = NULL;

        if ($this->sortVar)
            $this->model->activeDataProvider->sort->sortVar = $this->sortVar;

        // подготовка параметров
        $this->_prepareHtmlAttributes();

        // свойства по умолчанию для кнопок
        foreach ($this->buttons as $buttonName => $button)
            $this->buttons[$buttonName] = array_merge(
                $this->defualtButtonOptions, $button
            );

        // применение условий отсортировки
        if (array_key_exists($this->scopeVar, $this->params) && sizeof($this->scopes)) {
            $this->scope = $this->params[$this->scopeVar];
        }
        if (!$this->scope || ($this->scope == "reset"))
            $this->scope = $this->defaultScope;
        if (array_key_exists($this->scopeVar, $this->params))
            $this->params[$this->scopeVar] = urlencode($this->scope);
        if ($this->scope && ($this->scope != "reset")) {
            // значение для фильра
            if (isset($this->params[$this->scopeValueVar]))
                $this->scopeValue = $this->params[$this->scopeValueVar];

            if (isset($this->scopes[$this->scope]))
                call_user_func_array(
                    $this->scopes[$this->scope],
                    array($this->model, $this->scopeValue)
                );

            // указанного условия не существует
            else throw new CException(Yii::t('AdminTable',
                "Scope {$this->scope} not exists!"));
        }

        // применение отсортировки - поиск
        if (!$this->search && isset($_GET[$this->searchVar]))
            $this->search = $_GET[$this->searchVar];
        if ($this->search) {
            /** @todo: urlencode не срабатывает!!! */
            if (array_key_exists($this->searchVar, $this->params))
                $this->params[$this->searchVar] = urlencode($this->search);
            if (is_callable($this->searchFunction))
                call_user_func_array(
                    $this->searchFunction,
                    array($this->model, $this->search)
                );

            // вывод сообщения о недоступности поиска
            else Yii::app()->user()->setFlash('alert',
                Yii::t('AdminTable', 'Search not supported'));
        }

        $this->_count = $this->model->count;
        if (!$this->model->activeDataProvider->pagination->pageSize)
            $this->model->activeDataProvider->pagination->pageSize = $this->_count;

        $this->sortDirection = strtolower($this->sortDirection);
        $sortVar = $this->model->getActiveDataProvider()->sort->sortVar;
        if (isset($_GET[$sortVar])) {
            $this->sortField = $_GET[$sortVar];
            $this->params[$sortVar] = $this->sortField;
        } else $_GET[$sortVar] = $this->sortField . ($this->sortDirection == 'desc' ? "." . $this->sortDirection : NULL);
        if (preg_match('/(?P<field>\w+)\.(?P<direction>asc|desc)/', $this->sortField, $matches)) {
            $this->sortField = $matches['field'];
            $this->sortDirection = $matches['direction'];
        }
        if (array_key_exists($sortVar, $this->params))
            if (!$this->params[$sortVar]) $this->params[$sortVar] = $this->sortField;

        // роут для постраничного вывода
        if ($this->usePagination) {
            if (!is_null($this->route)) $pagination->route = $this->route;
            else $pagination->route = NULL;
            $pagination->params = $this->params;
        }

        if (!$this->ajaxField) $this->ajaxField = $this->id;

        if ($this->useAjax) {
            $this->htmlAttributes['container']['ajaxTable'] = 'true';
            if ($this->useAlternativePagination && $this->usePagination)
                $this->htmlAttributes['container']['alternativePagination'] = 'true';
        }

        if ($this->useGroupOperations) {
            $this->htmlAttributes['container']['groupOperations'] = 'true';
            $groupBarUrl = Yii::app()->controller->createUrl($this->groupOpeartionsBarRoute, false);

            Yii::app()->clientScript->registerScript('groupOperationsUrl',
                "$('.ActiveTable#{$this->id}').data('groupOperationsUrl', '$groupBarUrl')");
        }

        // URI последней подгрузки
        $this->htmlAttributes['table']['uri'] = $_SERVER['REQUEST_URI'];

        $this->htmlAttributes['table']['sortField'] = $this->sortField;
        $this->htmlAttributes['table']['sortDirection'] = $this->sortDirection;

        $this->htmlAttributes['table']['pageCount'] = ceil($this->_count
            / $this->model->activeDataProvider->pagination->pageSize);
        $this->htmlAttributes['table']['pageNumber'] = $pagination->currentPage;
        if ($pagination->pageCount == ($pagination->currentPage + 1)) {
            $this->htmlAttributes['table']['lastPage'] = 'true';
        }

        // на последок убеждается что указанная страница существует

        if ($this->htmlAttributes['table']['pageCount'] <= $pagination->currentPage)
            $this->params['page'] = $_GET['page'] = (($this->htmlAttributes['table']['pageCount'] > 1)
                ? $this->htmlAttributes['table']['pageCount'] - 1 : 1);

        /** @TODO: вопрос: не потребуется ли проверять на пересечения имён полей в
         * родных полях модели, дополнительных полях и кнопках? */
    }

    /**
     * Подготовка HTML-аттрибутов для элементов в представлениях.
     * @return void
     */
    protected function _prepareHtmlAttributes()
    {
        // добавление классов в параметры элементов
        foreach ($this->classes as $element => $elementClasses) {
            if (!isset($this->htmlAttributes[$element]))
                $this->htmlAttributes[$element] = array();

            if (isset($this->htmlAttributes[$element]["class"]))
                $this->htmlAttributes[$element]["class"] = array_merge(
                    $this->htmlAttributes[$element]["class"], $elementClasses);

            else $this->htmlAttributes[$element]["class"] = implode(" ", $elementClasses);
        }
    }

    protected function _fieldsOrder()
    {
        // указан пределённый порядок вывода полей
        if ($this->fieldsOrder) $fields = $this->fieldsOrder;

        // в противном случае (по умолчнию) список полей формируется
        else {
            $fields = $this->_attributeNames;

            // указаны конкретные поля
            if ($this->useFields) $fields = array_intersect($fields, $this->useFields);

            // игнорируемые поля
            if ($this->ignoreFields) {
                $mask = array_intersect($fields, $this->ignoreFields);
                $fields = array_diff($fields, $mask);
            }

            // добавление дополлнительных полей и полей для управляющих элементов
            $fields = array_merge($fields, $this->additionalFields);
            $fields = array_merge($fields, array_keys($this->buttons));
        }

        return $fields;
    }

    protected function _echoHeaderCell($field)
    {
        // данных нет, выводить нечего
        if (!sizeof($this->model->activeDataProvider->data)) return;
        $cellAttributes = $this->htmlAttributes["cell"];
        $cellAttributes["data-field"] = $field;

        // выделение активного поля
        if ($this->sortField == $field) {
            $cellAttributes["class"] .= " selected";
            $cellAttributes["class"] .= " {$this->sortDirection}";
        }

        echo CHtml::openTag($this->cellTag, $cellAttributes);

        $columnText = NULL;

        // "родные поля" наиболее приоритетны
        if (in_array($field, $this->_attributeNames)) {
            $columnText = $this->_cellСontent($field, $this->labels[$field], array(), true);

            /**
             * @todo необходимо проеверить работоспособность системы сортировки
             */

            $sortVar = $this->model->activeDataProvider->sort->sortVar;

            // инверсия для текущего поля
            if ($this->sortField == strtolower($field)) {
                $image_class = ($this->sortDirection == "asc")
                    ? 'icon-chevron-up' : 'icon-chevron-down';
                if ($this->sortDirection == "asc")
                    $direction = '.desc';
                else $direction = '';
            } else {
                $image_class = '';
                $direction = "";
            }


            // по умолчанию параметры берёт на себя
            if (!$this->params)
                $params = array($field . $direction);
            else $params = $this->params;

            $params[$sortVar] = $field . $direction;

            // создание URL стандартными средствами Yii
            $url = Yii::app()->controller->createUrl($this->route, $params);
            echo CHtml::link(
                $columnText . CHtml::tag('span', array('class' => $image_class)), $url,
                array(
                    'rel' => 'tooltip',
                    'title' => Yii::t('activetable', 'Sort field by field "{field}"',
                            array('{field}' => $columnText))
                )
            );
        } else

            // затем идут дополнительные поля
            if (in_array($field, $this->additionalFields)) {
                // заголовок указывается в $this->labels, если его нет, выводится
                // имя поля
                $title = isset($this->labels[$field]) ? $this->labels[$field] : $field;

                echo $this->_cellСontent($field, $title, array(), true);
            }


        echo CHtml::closeTag($this->cellTag);

    }

    protected function _echoCell($row, $field)
    {
        // данных нет, выводить нечего
        if (!sizeof($this->model->activeDataProvider->data)) return;

        /**
         * @todo: необходимо донастраивать работу с AJAX!
         */

        // атрибуты столбца
        $htmlAttributes = $this->htmlAttributes["cell"];
        if (!isset($htmlAttributes["class"])) $htmlAttributes["class"] = $field;
        else $htmlAttributes["data-field"] = $field;

        echo CHtml::openTag($this->cellTag, $htmlAttributes);

        // "родные поля" наиболее приоритетны
        if (in_array($field, $this->_attributeNames))
            echo $this->_cellСontent($field, $row[$field], $row);

        else

            // затем идут дополнительные поля
            if (in_array($field, $this->additionalFields))
                echo $this->_cellСontent($field, NULL, $row);

            else

                // и затем управляющие элементы
                if (isset($this->buttons[$field])) {
                    $buttonOptions = $this->buttons[$field];
                    $this->_renderButton($field, $buttonOptions, $row);
                }

        echo CHtml::closeTag($this->cellTag);
    }

    /**
     * Вывод содержимого ячейки.
     * Метод проверяет наличие конвертора для этой ячейки, и если таковой имеется,
     * пропускает данные через него.
     * @param string $fieldName
     * @param string $fieldValue
     * @param array $row
     * @param boolean $isHeader
     */
    protected function _cellСontent($fieldName, $fieldValue, $row, $isHeader = false)
    {
        if (isset($this->preConvertors[$fieldName]))
            return call_user_func_array($this->preConvertors[$fieldName],
                array($fieldName, $fieldValue, $row, $isHeader, $this->model));

        else return $fieldValue;
    }

    protected function _renderButton($name, Array $buttonOptions, $row)
    {
        if (isset($buttonOptions['buttons'])) {
            foreach ($buttonOptions['buttons'] as $index => $section) {
                if (isset($section['items'])) foreach ($section['items'] as $name => $item)
                    $this->_prepareUrl(
                        $name,
                        $buttonOptions['buttons'][$index]['items'][$name],
                        $row
                    );
            }

            $this->widget(
                Yii::localExtension('bootstrap', 'widgets.BootButtonGroup'),
                $buttonOptions);
        } else {
            $this->_prepareUrl($name, $buttonOptions, $row);
            $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
                $buttonOptions);
        }
    }

    protected function _prepareUrl($name, Array &$buttonOptions, &$row)
    {
        if (isset($buttonOptions['url'])) throw new CException(Yii::t('activetable',
            'Don`t use "url" attribute. Use "route" template.'));
        if (!isset($buttonOptions['route']) || !$buttonOptions['route']) {
            $buttonOptions['url'] = Yii::app()->controller->createUrl($name,
                array('id' => (int)$row['id']));
        } else {
            $buttonOptions['url'] = Yii::app()->controller->createUrl(
                array_shift($buttonOptions['route']),
                array_combine(
                    array_keys($buttonOptions['route']),
                    $row->getAttributes(array_values($buttonOptions['route']))
                )
            );
        }

        unset($buttonOptions['route']);
    }

    protected function _formAttributes()
    {
        $htmlAttributes = $this->htmlAttributes["form"];
        // шаблон строки для формирования адреса
        $actionArgs = $this->params;
        if (array_key_exists($this->searchVar, $actionArgs)) $actionArgs['search'] = ':search';
        $htmlAttributes['action'] = Yii::app()->controller->createUrl(
            Yii::app()->controller->lastActionID,
            $actionArgs
        );

        return $htmlAttributes;
    }

    public function run()
    {
        $this->render($this->views["main"]);
    }

}

?>