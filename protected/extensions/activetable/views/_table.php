<?php
// посредник между моделью и таблицей
$dataProvider = $this->model->activeDataProvider;
$fields = $this->_fieldsOrder();


// ничего нет
if(!sizeof($dataProvider->data)){
    if(!isset($this->htmlAttributes['table']['class']))
        $this->htmlAttributes['table']['class'] = '';
    $this->htmlAttributes['table']['class'] .= ' noItems';
    echo CHtml::tag('p', $this->htmlAttributes['table'],
        Yii::t('ActiveTable', 'No items'));
}

// вывод содержимого
else 
{
    // начало таблицы
    echo CHtml::openTag($this->tableTag, $this->htmlAttributes["table"]);

    // шапка таблицы (заголовки и форма)
    if($this->useHeader)
    {
        if($this->tableHeadTag)
            echo CHtml::openTag($this->tableHeadTag);

        echo CHtml::openTag($this->rowTag, $this->htmlAttributes["headRow"]);
        foreach($fields as $field) $this->_echoHeaderCell($field);
        echo CHtml::closeTag($this->rowTag);

        if($this->tableHeadTag)
            echo CHtml::closeTag($this->tableHeadTag);

    }

    $prevSortValue = NULL;
    $oddClass = "even";

    if($this->tableBodyTag)
        echo CHtml::openTag($this->tableBodyTag);

    // тело таблицы
    foreach($dataProvider->data as $i => $row)
    {
    	// начало нового/конец предыдущего группирующего тега
    	if($this->rowGroupTag && $this->rowGroupSize){
    		if(!round($i % $this->rowGroupSize))
    		{
    			if($i) echo CHtml::closeTag($this->rowGroupTag);
				echo CHtml::openTag($this->rowGroupTag, array('class'=>'rowGroup'));
    		}
    	}

        if($prevSortValue != $row[$this->sortField]){
            $oddClass = $oddClass == "even" ? "odd" : "even";
            $prevSortValue = $row[$this->sortField];
        }

        $htmlAttributes = $this->htmlAttributes["row"];
        $htmlAttributes['class'] .= " $oddClass";
        $htmlAttributes['id'] = $row['id'];

        echo CHtml::openTag($this->rowTag, $htmlAttributes);

        foreach($fields as $field) $this->_echoCell($row, $field);
        echo CHtml::closeTag($this->rowTag);

    }

   	// конец последнего группирующего тега
	if($this->rowGroupTag && $this->rowGroupSize)
		echo CHtml::closeTag($this->rowGroupTag);

    if($this->tableBodyTag)
        echo CHtml::closeTag($this->tableBodyTag);


    // конец таблицы
    echo CHtml::closeTag($this->tableTag);

	if($this->useGroupOperations){
		echo CHtml::openTag('div', array('class'=>'groupOperationsBar well clientContent'));
		Yii::app()->controller->actionGroupOperationsBar(array());
		echo CHtml::closeTag('div');
	}

    if($this->usePagination){
    $this->pagerOptions['pages'] = $dataProvider->pagination;
    ?><div class="pagination"><?
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootPager'), $this->pagerOptions);
    ?></div><?
    }

}
