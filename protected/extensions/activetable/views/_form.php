<?php

echo CHtml::openTag("div", $this->htmlAttributes["searchBlock"]);
echo CHtml::openTag("form", $this->_formAttributes());


// кастумный текст
if($this->headerPrepend)
    echo CHtml::tag('div', array('class'=>'headerPrepend'), $this->headerPrepend);

if(is_array($this->headerButtons) && is_array($this->headerButtons)){
    ?><div class="buttons"><?
    foreach($this->headerButtons as $buttonOptions){
        $buttonOptions['url'] = Yii::app()->controller->createUrl(
            array_shift($buttonOptions['route']),
            $buttonOptions['route']
        );
        unset($buttonOptions['route']);
        $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
            $buttonOptions);
        echo '&nbsp;';
    }
    ?></div><?
}

// условия

if(sizeof($this->scopes)){
    ?><div class="scopes"><?
    $buttons = array();
    $params = $this->params;
    foreach($this->scopes as $scopeName => $callback){
        $params[$this->scopeVar] = ($this->scope == $scopeName) ? 'reset' : $scopeName;

        $htmlOptions = array();
		if(isset($this->scopeDescriptions[$scopeName])){
			$htmlOptions['rel'] = 'tooltip';
			$htmlOptions['title'] = $this->scopeDescriptions[$scopeName];
		}

        $buttons[] = array(
            'label'=>isset($this->scopeLabels[$scopeName]) ? $this->scopeLabels[$scopeName] : $scopeName,
            'active'=>($this->scope == $scopeName),
            'buttonType'=>'link',
            'url'=>Yii::app()->controller->createUrl($this->route, $params),
            'htmlOptions' => $htmlOptions
        );
    }
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButtonGroup'), array(
        'type' => 'info',
        'toggle' => 'radio',
        'buttons' => $buttons
    ));
    ?></div><?
}

// форма поиска
if($this->useSearch){
    ?><div class="searchSub controls"><div><?
    $this->htmlAttributes["searchField"]['placeholder'] = $this->searchLabel;
    echo CHtml::textField($this->searchVar, $this->search, $this->htmlAttributes["searchField"]);
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array('buttonType'=>'submit', 'icon'=>'search'));
    ?></div></div><?
}

?><div class="clear"></div><?


// информационная панель
echo CHtml::openTag("div", $this->htmlAttributes["info"]);
    $pageNumber = isset($_GET['page']) ? $_GET['page'] : 0;
    $pageNumber = $pageNumber = intval($pageNumber?($pageNumber):$pageNumber+1);

	if($this->_count)
		$progressPercent = round((($pageNumber>1?(($pageNumber-1)*$this->pageSize):0) + $this->model->activeDataProvider->itemCount)
			/ $this->_count * 100);
	else 
		$progressPercent = 0;

    // уровень страницы
    if($this->useAlternativePagination){
    ?><div class="subitem" data-placement="right" data-original-title="<?= Yii::t('activetable', 'Position of page end'); ?>"<?
    ?> data-content="<?
        echo Yii::t('activetable',
            'This element indicate position of page end in summary content.<br/><br/>Current position is {position}%.<br/><br/>'.
            '<i>Click to place of element for jumping to needest position!</i>',
            array('{position}'=>$progressPercent)); 
    ?>" navigation-bar="true" current-page="<?=$pageNumber?>" page-size="<?=$this->model->activeDataProvider->pagination->limit?>" total-items="<?=$this->model->activeDataProvider->totalItemCount?>" data-url-template="<?
        $params = $this->params;
        $params['page'] = ':page';
        echo Yii::app()->controller->createUrl($this->route, $params);
    ?>" rel="popover">
        <?
        if($this->model->activeDataProvider->totalItemCount > 0)
        $this->widget('bootstrap.widgets.BootProgress', array(
            'type'=>'info',
            'percent'=> $progressPercent,
        ));
    ?></div><?
    }

    ?><div class="subitem"><?
    echo Yii::t(
        "ActiveTable",
        "Page {page}. Showed {itemCount} in {totalCount}.", array(
            "{itemCount}" => $this->model->activeDataProvider->itemCount,
            "{totalCount}" => $this->_count,
            "{page}" => $pageNumber
        )
    );
    ?></div><?
    ?><div class="clear"></div><?
echo CHtml::closeTag("div");


// кастумный текст
if($this->headerAppend)
    echo CHtml::tag('div', array('class'=>'headerAppend'), $this->headerAppend);

echo CHtml::closeTag("form");
echo CHtml::closeTag("div");


if($this->useAlternativePagination && $this->model->activeDataProvider->pagination->pageCount>1){
    ?><p class="keyboardHint"><?
    echo Yii::t(
        'activetable', 'Use {ctrl}<strong>←</strong> &nbsp;and&nbsp; {ctrl}<strong>→</strong>',
        array('{ctrl}'=>(strpos($_SERVER['HTTP_USER_AGENT'], 'Mac OS')?'<span class="cmd_key"></span>':'ctrl'))
    );
    ?></p><?
}

