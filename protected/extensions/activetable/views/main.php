<?php

$containerAttributes = $this->htmlAttributes["container"];
$containerAttributes["id"] = $this->id;

$dataProvider = $this->model->activeDataProvider;
$pagination = $this->model->activeDataProvider->pagination;

if(!Yii::app()->request->isAjaxRequest){
	echo CHtml::openTag("div", array('class'=>'ActiveTableRoot'));
    echo CHtml::openTag("div", $containerAttributes);
}
else
{
	?><script><?
	foreach(Yii::app()->clientScript->getStaticScripts(CClientScript::POS_BEGIN) as $scripts)
		echo $scripts.";\n";
	?></script><?
}

    // вывод формы с фильтрами
    if($this->useHeader)
        $this->render("_form");

    // вывод таблицы
    $this->render("_table");


if(!Yii::app()->request->isAjaxRequest)
{
	echo CHtml::closeTag("div");
	echo CHtml::closeTag("div");
}
else
{
	?><script><?
	foreach(Yii::app()->clientScript->getStaticScripts(CClientScript::POS_END) as $scripts)
		echo $scripts.";\n";
	
	foreach(Yii::app()->clientScript->getStaticScripts(CClientScript::POS_READY) as $scripts)
		echo $scripts.";\n";
	foreach(Yii::app()->clientScript->getStaticScripts(CClientScript::POS_LOAD) as $scripts)
		echo $scripts.";\n";

	?></script><?
}

?>

