<?php
/**
 * @package mtmselect
 * @author koshevy
 * @desc
 * Представление для вывода выбранного элемента в виджете MTMSelect, а также
 * для создания шаблона нового элемента (будет применятся при создании элемента
 * в клиентском скрипте).
 * 
 * Используются следующие параметры:
 * @param int $id - идентификатор записи (указывайте ':id' при создании шаблона)
 * @param string $title - подпись (указывайте ':title' при создании шаблона)
 * @param string $image - иллюстрация (@todo: изображение пока не поддерживается)
 * 
 */
?>
<span class="mtmitem label label-info" id="<?=$id?>">
    <span class="cuted"><?= $title ?></span>
    <span class="remove-item" title="<?= Yii::t('mtmselect', 'Remove element from list'); ?>" rel="tooltip">×</span>
    <input name="<?= get_class($this->model) ?>[<?= $this->attribute ?>][]" value="<?=$id?>" type="hidden" />
</span>