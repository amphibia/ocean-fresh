<?

return array
(

	/**
	 * Заголовки таблицы календаря.
	 * @var array
	 */
	'headings' => array
	(
		Yii::t('calendar', 'Sun'),
		Yii::t('calendar', 'Mon'),
		Yii::t('calendar', 'Tue'),
		Yii::t('calendar', 'Wed'),
		Yii::t('calendar', 'Thu'),
		Yii::t('calendar', 'Fri'),
		Yii::t('calendar', 'Sat')
	),

	/**
	 * Подпись к ссылке "назад".
	 * @var string
	 */
	'prevLabel' => htmlspecialchars('<<'),

	/**
	 * Подпись к ссылке "вперед".
	 * @var string
	 */
	'nextLabel' => htmlspecialchars('>>'),

	/**
	 * Активные месяцы.
	 * @var array
	 */
	'acceptedMonths' => NULL,

	/**
	 * Активные года.
	 * @var array
	 */
	'acceptedYears' => NULL,

	/**
	 * Роут для формирования URL в календаре.
	 * @var string
	 */
	'route' => NULL,

	/**
	 * Роут для обращения по AJAX.
	 * @var string
	 */
	'ajaxRoute' => NULL,

	/**
	 * Параметры, с которыми формируется URL.
	 * 
	 * ПРИМЕЧАНИЕ: порядок, в котором указываются параметры влияет на порядок
	 * компоновки пврвметров при создании URL. 
	 * @var array
	 */
	'params' => array
	(
		'year' => $component->year,
		'month' => $component->month,
		'day' => $component->day,
	),

	/**
	 * HTML-настройки, соответствующие различным элементам календаря:
	 * 'calendarHeader' - блок с заголовками календаря
	 * 'prevMonth' - "кнопка предыдущий месяц"
	 * 'nextMonth' - "кнопка предыдущий месяц"
	 * 
	 * 'monthSelect' - элемент выбора месяца
	 * 'yearSelect' - элемент выбора года
	 * @var array
	 */
	'elementsHtmlOptions' => array
	(
		'inlineCalendar' => array(
			'id' => $component->id,
			'class' => 'inlineCalendar clientContent clientContentReplace'
		),

		'calendarHeader' => array(
			'class' => 'calendarHeader'
		),

		'prevMonth' => array(
			'class' => 'monthChange prevMonth'
		),

		'nextMonth' => array(
			'class' => 'monthChange nextMonth'
		),

		'monthSelect' => array(
			'class' => 'calendarDropdown monthSelect'
		),

		'yearSelect' => array(
			'class' => 'calendarDropdown yearSelect'
		),

		'weekDayRow' => array(
			'class' => 'calendarRow weekDaysRow'
		),

		'weekDayCell' => array(
			'class' => 'calendarCell weekDaysCell',
			'valign' => 'middle',
			'align' => 'center'
		),
		
		'bodyRow' => array(
			'class' => 'calendarRow bodyRow'
		),
		

		'emptyCell' => array(
			'class' => 'emptyCell'
		),

		'currentDay' => array(
			'valign' => 'middle',
			'align' => 'center',
			'class' => 'currentDay'
		),

		'selectedDay' => array(
			'valign' => 'middle',
			'align' => 'center',
			'class' => 'selectedDay'
		),
		
		'day' => array(
			'class' => 'day',
			'valign' => 'middle',
			'align' => 'center'
		),
		
		'weekendDay' => array(
			'class' => 'day weekendDay',
			'valign' => 'middle',
			'align' => 'center'
		),

		'dayNumber' => array(
			'class' => 'dayNumber'
		),

		'activeDayNumber' => array(
			'class' => 'dayNumber activeDayNumber'
		),

	)

);


?>