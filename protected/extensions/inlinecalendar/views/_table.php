<?
/**
 * @author koshevy
 * Вывод таблицы календаря. За основу взято представление виджета FlowCalendar
 * из библиотеки расширений Yii, но было сильно переработано.
 */

?><table width="100%" cellpadding="0" cellspacing="0"><?

$weekDayCellOptions = CHtml::renderAttributes($this->_elementHtmlOptions('weekDayCell'));
?>
<tr <?= $weekDayCellOptions ?>>
	<td><?= implode("</td><td {$weekDayCellOptions}>",$this->headings); ?></td>
</tr><?

/* days and weeks vars now ... */
$running_day = date('w',mktime(0,0,0,$this->month,1,$this->year));
if(!$running_day) $running_day = 7; $running_day--;
$days_in_month = date('t',mktime(0,0,0,$this->month,1,$this->year));
$days_in_this_week = 1;
$day_counter = 0;
$dates_array = array();

/* row for week one */
?><tr <?= CHtml::renderAttributes($this->_elementHtmlOptions('bodyRow')); ?>><?

/* print "blank" days until the first of the current week */
for($x = 0; $x < $running_day; $x++):
	?><td <?= CHtml::renderAttributes($this->_elementHtmlOptions('emptyCell')); ?>> </td><?
	$days_in_this_week++;
endfor;

/* keep going with days.... */
for($list_day = 1; $list_day <= $days_in_month; $list_day++):

	if($this->day && ($list_day == $this->day))
	{
		?><td <?= CHtml::renderAttributes($this->_elementHtmlOptions('selectedDay')); ?>><?
	}
	else if($list_day == date("j",mktime(0,0,0,$this->month)) && date("n") == $this->month && date("Y") == $this->year)
	{	
		?><td <?= CHtml::renderAttributes($this->_elementHtmlOptions('currentDay')); ?>><?
	}
	else			
	{	
		if(($running_day == "5") || ($running_day == "6"))
		{
		 ?><td <?= CHtml::renderAttributes($this->_elementHtmlOptions('weekendDay')); ?>><?
		}
		else
		{
		 ?><td <?= CHtml::renderAttributes($this->_elementHtmlOptions('day')); ?>><?	
		}
	}

		/* add in the day number */
		if(is_array($this->acceptedDays) && !in_array($list_day, $this->acceptedDays)){
			?><span <?= CHtml::renderAttributes($this->_elementHtmlOptions('dayNumber')); ?>><span><?= $list_day ?></span></span><?
		}
		else echo $this->_dateLink(
			"<span>{$list_day}</span>",
			array('year'=>$this->year, 'month'=>$this->month, 'day' => $list_day),
			$this->_elementHtmlOptions('activeDayNumber')
		);

	?></td><?
	if($running_day == 6):
		?></tr><?
		if(($day_counter+1) != $days_in_month):
		 ?><tr <?= CHtml::renderAttributes($this->_elementHtmlOptions('bodyRow')); ?>><?
		endif;
		$running_day = -1;
		$days_in_this_week = 0;
	endif;
	$days_in_this_week++; $running_day++; $day_counter++;
endfor;

/* finish the rest of the days in the week */
if($days_in_this_week < 8 && $days_in_this_week > 1) :
	for($x = 1; $x <= (8 - $days_in_this_week); $x++):
		?><td <?= CHtml::renderAttributes($this->_elementHtmlOptions('emptyCell')); ?>> </td><?
	endfor;
endif;

/* final row */
?></tr><?

/* end the table */
?></table><?