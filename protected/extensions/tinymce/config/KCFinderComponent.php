<?php

return array
(
    /**
     * Директория - хранилище изображений.
     * Если приложением используется компонент storageComponent,
     * свойство будет переопределено в соответствии с его настройками.
     * 
     * @var string 
     */
    'imagesDir' => NULL,

    /**
     * Поддериктория для конкретных задачи/пользователя.
     * Если приложением используется компонент storageComponent,
     * свойство будет переопределено в соответствии с его настройками.
     * 
     * @var string 
     */
    'subdir' => NULL,
    
    /**
     * ID контроллера - KCFinder.
     * @return string 
     */
    'controllerID' => 'kcfinder',

    /**
     * Класс контроллера - KCFinder.
     * @return string 
     */
    'controllerClass' => 'KCFinderController', 

    /**
     * ID компонента - хранилища. Если находит этот компонент в системе, берёт всю
     * информаци. с него.
     * @var string
     */
    'storageComponentID' => 'storage',

    /**
     * Имя js-функции, обрабатывающей комманду TinyMCE, открывающей окно загрузки
     * (file_browser_callback).
     * @var string
     */
    'jsBrowseFunction' => 'TinyMCE_browse',
    
);

?>
