<?php
Yii::import(Yii::localExtension('bootstrap', 'widgets.*', __FILE__));
Yii::import(Yii::localExtension('bootstrap', 'widgets.input.*', __FILE__));

/**
 * Ленивая форма с сипользованием Yii Bootstrap.
 * Использует оригинальный объект CForm, одевая его как форму Yii Bootstrap.
 * Также использует настройки по умолчанию.
 * 
 * Если не указывать CForm::elements, они генерируются автоматически на основании
 * данных из модели и таблиц свойсвтв элементов по умолчанию.
 * 
 * Если указать тип элемента как 'default', его свойства также дозаполняются
 * автоматически.
 * 
 * /** @TODO Возможно, работу с валидаторами можно оптимизировать! @/
 */
class BootAutoForm extends TbActiveForm
{
    /**
     * Обязательный параметр - исходная форма.
     * @var CForm
     */
    public $form = NULL;

    /**
     * Требуется ли использовать опцию "не возвращаться назад".
     * @var boolean
     */
    public $repeatActionOption = true;
    
    /**
     * Класс строки с кнопками.
     * @var string
     */
    public $buttonsRowClass = NULL;

	/**
	 * @var Тип формы (смотрите константы Bootstrap.BootActiveForm).
	 */
	public $type = NULL;

	/**
	 * @var boolean отображение ошибок.
	 */
	public $inlineErrors = true;

    /**
     * Порядок вывода полей.
     * @var array
     */
    public $fieldsOrder = array();

    /**
     * URL страницы, на которую требуется вернуть форму по сохранении.
     * @var string
     */
    public $referer = NULL;

    /**
     * Форма собирает данные о недастоющих элементах в любом случае.
     * @var boolean
     */
    public $autoElements = NULL;

    /**
     * Методы (веджета BootActiveForm), соответствующие типам элементов.
     * Столбец 'items' говорит использует ли элемент множественные значения.
     * @var array 
     */
    public $typesMethods = array();
	
	/**
	 * Стандартные элементы (объект CFormInputElement).
	 * @var array
	 */
	public $standartElements = array();

    /**
     * Типы соответствующих кнопок.
     * @var array
     */
    public $buttonTypes = array();

    /**
     * Размеры соответствующих кнопок.
     * @var array
     */
    public $buttonSizes = array();

    /**
     * Иконки соответствующих кнопок.
     * @var type 
     */
    public $buttonIcons = array();

    /**
     * Типы элементов, использующиеся для реляционных атрибутов.
     * @var array 
     */
    public $relationElementTypes = array();

    /**
     * Классы по умолчанию для соответствующих элементов.
     * @var array
     */
    public $defaultClasses = array();

    /**
     * Поля, используемые для вывода названий и заголовков (в порядке приоритета)
     * @var array
     */
    public $titleFields = array();

    /**
     * Таблица с определением типов проверки (и типов данных) для элементов.
     * @var array 
     */
    protected $_attributesRules = array();

    /**
     * Таблица для временного хранения данных по отношениям моделей (для экономии
     * памяти).
     * @var array
     */
    protected $_modelsRelations = array();



	public function init(){
		Yii::loadExtConfig($this, __FILE__);

		if(!$this->id) $this->id = get_class($form->model).'-form';
		if($this->form->attributes) $this->htmlOptions = $this->form->attributes;
		parent::init();

		$this->_render($this->form);
		return true;
	}

    /**
     * Страница, на которую следует произвести переадресацию после сохранения
     * формы.
     * @return string
     */
    public function refererUri()
    {
        /**
         * @todo: записать в документации, что поля _formOptions[]
         * используются для управляющих свойств формы */
        static $referer = NULL;
        if($referer) return $referer;

        if(isset($_POST['_formOptions']['referer']))
            $referer = $_POST['_formOptions']['referer'];

        if(!$referer && isset($_SERVER['HTTP_REFERER']))
            $referer = $_SERVER['HTTP_REFERER'];

        return $referer ? $referer : NULL;
    }


    /**
     * Просчёт содержимого формы.
     * @param CForm $form 
     */
    protected function _render(CForm $form)
    {
        $bootForm = &$this;

        if(!sizeof($form->elements) || $this->autoElements)
            $this->_defaultElements($form);

        // порядок вывода
        if(sizeof($this->fieldsOrder)){
            $elements = array();
            foreach($this->fieldsOrder as $element)
                if(isset($form->elements[$element]))
                    $elements[$element] = $form->elements[$element];
        }
        else $elements = &$form->elements;

        $modelClass = get_class($form->model);
        if(!isset($this->_modelsRelations[$modelClass]))
            $this->_modelsRelations[$modelClass] = $form->model->relations();
        foreach($elements as $name => $element)
        {
            // если это не массив, это обычный текст
            if(!is_array($element) && !is_object($element)){
				?><pre><? print_r($element); die();
                ?><div class="inlineText"><?= $element ?></div><?
                continue;
            }
			else if($element instanceof CFormStringElement){
				echo $element->render();
				continue;
			}
            
            // конфигурируется по умолчанию
            if($element->type == 'default') $this->_defaultElement($element);
            elseif($element->type == 'checkboxlist'){
                $rules = $this->_elementRules($element);
                $validator = $rules['CheckBoxListValidator'];
                if(is_array($validator->items) && sizeof($validator->items) && !$element->items)
                    $element->items = array_combine($validator->items, $validator->items);
            }

            if(isset($this->_modelsRelations[$modelClass][$name]) && is_array($form->model->$name)){
                $elementValues = array();
                foreach($form->model->$name as $item){
                    $elementValues[] = ($item instanceof CModel) ? $item->primaryKey : $item;}
                $form->model->$name = $elementValues;
            }

            // вложенная форма
            /** @todo: проверить работу с вложенными формами */
            if($element instanceof CForm) $this->_render($element);
            // стандартный элемент
            else if(isset($this->typesMethods[$element->type]))
			{

				if(isset($this->typesMethods[$element->type]['attributes']))
					$attributes = array_merge($element->attributes,
					$this->typesMethods[$element->type]['attributes']);
				else
				$attributes = &$element->attributes;

                $method = $this->typesMethods[$element->type][0];

                if($this->typesMethods[$element->type]['items'])
                     echo $bootForm->$method($form->model, $element->name, $element->items, $attributes);
                else echo $bootForm->$method($form->model, $element->name, $attributes);
            }
            // кастумный элемент
            else
            {

                echo CHtml::openTag('div', array('class'=>'control-group', 'attr'=>$element->name));
				echo CHtml::activeLabel($form->getModel(), $element->name, array(
					'class' => 'control-label',
					'label' => $element->getLabel(),
					'for' => "{$modelClass}[{$element->name}]",
					'required' => $element->getRequired()
				));

				echo '<div class="controls">';

                if(in_array($element->type, $this->standartElements))
                {
					echo $element->renderHint();
                	echo $element->renderInput();

					echo $bootForm->error(
						$this->form->getModel(),
						$element->name,
						$element->errorOptions,
						$element->enableAjaxValidation,
						$element->enableClientValidation
					);
                }
                else
				{
					echo $element->renderHint();

					$element->attributes['model'] = $form->model;
					$element->attributes['attribute'] = $element->name;
                	$this->widget($element->type, $element->attributes);

					echo $bootForm->error(
						$this->form->getModel(),
						$element->name,
						$element->errorOptions,
						$element->enableAjaxValidation,
						$element->enableClientValidation
					);
				}

                echo '</div>';
                echo CHtml::closeTag('div');
            }

        }


        if(!($form->parent instanceof CForm)){
            echo CHtml::openTag('div', array('class'=>$this->buttonsRowClass));

            $cookies = &Yii::app()->request->cookies;
            if(!isset($cookies['_formOptions_repeatAction']))
                $cookies['_formOptions_repeatAction'] = new CHttpCookie(
                    '_formOptions_repeatAction', false);

            // что делать после сохранения
            if($this->repeatActionOption)
            echo
                '<label title="'.Yii::t('app', 'Don`t return to previous page (ex. index).').'" rel="tooltip" class="return-element-label checkbox">' .
                    '<input '.($cookies['_formOptions_repeatAction']->value?'checked="checked"':NULL).
                    ' type="checkbox" value="1" name="_formOptions[repeatAction]">' .
                    Yii::t('app', 'Repeat action after save (don`t return).') .
                 '</label><br/>';


            if(!isset($_POST['_formOptions'])) $_POST['_formOptions'] = array();

            // переадрессация формы по завершении
            if(!$this->referer) $this->referer = $this->refererUri();

            echo CHtml::hiddenField('_formOptions[referer]', $this->referer);
			echo CHtml::hiddenField('_formOptions[language]', Yii::app()->language);

            foreach($form->buttons as $key => $button){
                $this->widget('bootstrap.widgets.TbButton',
                    array(  'buttonType'=>$button->type,
                            'type'=>isset($this->buttonTypes[$key]) ? $this->buttonTypes[$key] : NULL,
                            'icon'=>isset($this->buttonIcons[$key]) ? $this->buttonIcons[$key] : NULL,
							'size'=>isset($this->buttonSizes[$key]) ? $this->buttonSizes[$key] : NULL,
                            'label'=>$button->label,
                            'htmlOptions'=>array('id'=>get_class($form->model)."-$key")
                    ) );
                echo " &nbsp;";
            }
            echo CHtml::closeTag('div');
        }

        $this->run();
    }
    
    /**
     * Авто-создание элементов (элементы по умолчанию).
     * @param CForm $form 
     */
    protected function _defaultElements(CForm $form){
        $defaultElements = array_merge(array_keys($form->model->relations()),
            $form->model->safeAttributeNames);
        foreach($defaultElements as $elementName)
            if(!isset($form->elements[$elementName]))
                $form->elements[$elementName] = array('type'=>'default');
    }

    /**
     * Конфигурация элемента по умолчанию.
     * @param CFormInputElement $element 
     */
    protected function _defaultElement(CFormInputElement $element)
    {
        $rules = $this->_elementRules($element);

        // при использовании AR-моделей, реляционные свойства также обрабатываются
        // конструктором форм
        if($element->parent->model instanceof CActiveRecord)
        {
            $modelClass = get_class($element->parent->model);
            if(!isset($this->_modelsRelations[$modelClass]))
                $this->_modelsRelations[$modelClass] = $element->parent->model->relations();
            $relations = &$this->_modelsRelations[$modelClass];

            
            // свойство привязывается к реляционным связям модели
            if(isset($relations[$element->name])){
                $relation = &$relations[$element->name];
                if(isset($this->relationElementTypes[$relation[0]]))
                    $element->type = $this->relationElementTypes[$relation[0]];

                if(!$element->items){
                    $relatedModel = $relation[1]::model();

                    // поле для вывода в списке
                    $titleField = 'id';
                    foreach($this->titleFields as $field)
                        if(array_key_exists($field, $relatedModel->attributes)){
                            $titleField = $field; break; }
                            
                    // исключение самого элемента из списка своих родителей,
                    if($relation[1] == $modelClass && $element->parent->model->primaryKey){
                        $relation[1]::model()->notChildren($element->parent->model);
                    }

                    if($relation[1]::model() instanceof ExtCoreNestedAR){
                        //$relation[1]::model()->dbCriteria->addCondition(
                        //    $relation[1]::model()->levelAttribute.'>1');
                        $relation[1]::model()->dbCriteria->order=$relation[1]::model()->leftAttribute.' ASC';

                        $element->items = array();
                        foreach($relation[1]::model()->findAll() as $row){
                            $element->items[$row->id] = ($row->{$relation[1]::model()->levelAttribute} > 1)
                                ? $row->$titleField  : Yii::t('app', 'No parent');
                        }
                    }
                    else{
                        $element->items = CHtml::listData(
                            $relation[1]::model()->findAll(),
                            'id', $titleField);
                    }

                }

                if(isset($relation[2]))
				{
					/**
					 * @todo: работает только с одиночными ключами.
					 */
					if(is_array($relation[2])) $key = key($relation[2]);
					else $key = $relation[2];
					if(isset($element->parent->model->$key)){
						$elementName = $element->name;
						$element->parent->model->$elementName = $element->parent->model->$key;
					}
				}
            }
        }
        

        $htmlOptions = &$element->attributes;
        if(!isset($htmlOptions['class'])) $htmlOptions['class'] = NULL;

        // не реляционное свойство
        if($element->type == 'default')
        { 
            // примитивная логика определения типа элемента
            if(isset($rules['CBooleanValidator'])) $element->type = 'checkbox';
            if(isset($rules['CNumberValidator'])){
                $element->type = 'text'; $htmlOptions['class'] .= ' rule-numeric';
                $intRule = $rules['CNumberValidator'];
                if($intRule->max) $htmlOptions['max-number'] = $intRule->max;
                if($intRule->min) $htmlOptions['min-number'] = $intRule->min;
                if($intRule->numberPattern) $htmlOptions['numberPattern'] = $intRule->numberPattern;
            }
            if(isset($rules['CStringValidator'])){
                $element->type = 'text'; $htmlOptions['class'] .= ' rule-string';
                $stringRule = $rules['CStringValidator'];
                if($stringRule->max){
                    $htmlOptions['max-length'] = $stringRule->max;
                    if($stringRule->max > 255) $element->type = 'textarea';
                }
                if($stringRule->max){
                    $htmlOptions['min-length'] = $stringRule->min;
                    if($stringRule->min > 16) $element->type = 'textarea';
                }
            }
            if(isset($rules['CDateValidator'])){ $element->type = 'text'; $htmlOptions['class'] .= ' rule-date'; }
            if(isset($rules['CEmailValidator'])){ $element->type = 'text'; $htmlOptions['class'] .= ' rule-email'; }
            if(isset($rules['CFileValidator'])) $element->type = "file";
            if(isset($rules['CRangeValidator'])){

                $in = $rules['CRangeValidator'];
                $element->type = 'dropdownlist';
                if(is_array($in->range) && sizeof($in->range) && !$element->items)
                    $element->items = array_combine($in->range, $in->range);
            }

			if(strpos($element->name, 'password') !== false) $element->type = 'password';
			if(strpos($element->name, 'date') !== false) $element->type = 'date';
			if(strpos($element->name, 'toggle') !== false) $element->type = 'toggle';
			if(strpos($element->name, 'date_range') !== false) $element->type = 'date_range';

			// если ничего не подошло
            if($element->type == 'default')
				$element->type = 'text';
        }
        
        if(!isset($htmlOptions['class']) || !$htmlOptions['class'])
		{
			if(isset($this->defaultClasses[$element->type]))
				$htmlOptions['class'] = $this->defaultClasses[$element->type];
		}
    }

    /**
     * Выяснение правил заполнения элемента.
     * @param CFormInputElement $element 
     */
    protected function _elementRules(CFormInputElement $element)
    {
        $modelIndex = get_class($element->parent->model);
        if(!isset($this->_attributesRules[$modelIndex])){
            $this->_attributesRules[$modelIndex] = array();
            foreach($element->parent->model->getValidators() as $validator){
                foreach($validator->attributes as $name){
                    if(!isset($this->_attributesRules[$modelIndex][$name]))
                        $this->_attributesRules[$modelIndex][$name] = array();

                    $this->_attributesRules[$modelIndex][$name][get_class($validator)] = $validator;
                }
            }
        }

        return isset($this->_attributesRules[$modelIndex][$element->name])
                ? $this->_attributesRules[$modelIndex][$element->name] : NULL;
    }
}

?>
