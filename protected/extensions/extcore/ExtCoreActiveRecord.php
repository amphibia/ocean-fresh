<?php

/**
 * Базовый класс для AR-моделей Yii.
 * Реализует типовые операции выборки, стандартные предобработки параметров
 * и стандартизируемые рутинные методы (определение таблицы модели и т.д.)
 */
class ExtCoreActiveRecord extends CActiveRecord
{

    public function __construct($scenario = 'insert')
    {
        // Проверка существования таблицы с текущим языком
        $tableName = $this->tableName();
        if (($this->getDbConnection()->getSchema()->getTable($tableName)) === null) {
            $originalTableName = str_replace('_' . Yii::app()->language, '', $tableName);
            if (($this->getDbConnection()->getSchema()->getTable($originalTableName)) !== null) {
                Yii::app()->db->createCommand('CREATE TABLE IF NOT EXISTS ' . $tableName . ' LIKE ' . $originalTableName . ';
                INSERT ' . $tableName . ' SELECT * FROM ' . $originalTableName . ';')->query();
            }
        }

        parent::__construct($scenario);
    }

    /**
     * Статичный объект-одиночка, для обращения к публичным нестатическим методам.
     * @staticvar ExtCoreActiveRecord $instance
     * @return ExtCoreActiveRecord
     */
    public static function singleTone()
    {
        static $instance = NULL;
        if (!$instance) $instance = self::model();
        $instance->init();
        return $instance;
    }

    /**
     * Обращение к элементу по ID, при котором запись кэшируется в первый раз,
     * в дальнейшем же будет браться из временного кэша.
     *
     * @param integer $id
     */
    public static function cachedInstance($id)
    {
        static $_cache = array();
        if (!isset($_cache[$id])) $_cache[$id] = self::model()->findByPk($id);
        return $_cache[$id];
    }

    /**
     * Возвращает готовую к исполнению db-комманду (в зависимости от критериев
     * и групп модели).
     *
     * Пригождается в случаях, когда на основе AR-модели, с её сложными
     * свойствами, требуется создать комманду, выходящую за рамки средств ActiveRecord.
     *
     * @return CDbCommandBuilder
     */
    public function cloneCommand($extCriteria = NULL)
    {
        // дальнейшее изменение условий комманды не влияет на оригинал
        $criteria = clone $this->dbCriteria;
        if ($extCriteria) $criteria->mergeWith($extCriteria);

        return $this->commandBuilder->createFindCommand(
            $this->tableName(), $criteria
        );
    }

    /**
     * Поле даты, используемое таблицей по умолчанию (в соответствии с конфигами,
     * из числа полей таблицы).
     *
     * @return string
     */
    public function getDefaultDateField()
    {
        return current(
            array_intersect(Yii::app()->params["dateFields"],
                array_keys($this->getAttributes()))
        );
    }

    /** @category хелперы для типовых операций */

    /**
     * Извлечение используемых данных из модели.
     * Месяцы и дни выводятся только по материалам, относящихся к указанному
     * году и месяцу, соответственно. В целях экономии памяти и ресурсов на запрос.
     *
     * @param int $year испольуемый год
     * @param int $month испольуемый месяц
     * @param int $usedDateField испольуемый год
     *
     * @return array массив вида array('years'=>array(...), 'months'=>array(...),
     * 'days'=>array(...))
     */
    public function extractDates($year, $month, $usedDateField)
    {
        $result = array('years' => array(), 'months' => array(), 'days' => array());
        $modelClass = get_class($this);

        // года, за которые есть записи
        $yearsModel = new $modelClass;
        $yearsModel->dbCriteria = clone $this->dbCriteria;
        $yearsModel->dbCriteria->group = new CDbExpression("YEAR(FROM_UNIXTIME({$usedDateField}))");
        foreach ($yearsModel->findAll() as $page) {
            if (!ctype_digit($page->{$usedDateField})) {
                $page->{$usedDateField} = CDateTimeParser::parse($page->{$usedDateField}, Yii::app()->locale->getDateFormat('medium'));
            }
            $result['years'][] = (int)date('Y', $page->{$usedDateField});
        }

        // месяцы, за которые есть  материал (только в указанном году)
        $monthsModel = new $modelClass;
        $monthsModel->dbCriteria = clone $this->dbCriteria;
        $monthsModel->dateRange(array('year' => $year), $usedDateField);
        $monthsModel->dbCriteria->group = new CDbExpression("MONTH(FROM_UNIXTIME({$usedDateField}))");
        foreach ($monthsModel->findAll() as $page) {
            if (!ctype_digit($page->{$usedDateField})) {
                $page->{$usedDateField} = CDateTimeParser::parse($page->{$usedDateField}, Yii::app()->locale->getDateFormat('medium'));
            }
            $result['months'][] = (int)date('m', $page->{$usedDateField});
        }

        // дни, за которые есть материал (только в указанный месяц)
        $daysModel = new $modelClass;
        $daysModel->dbCriteria = clone $this->dbCriteria;
        $daysModel->dateRange(
            array('year' => $year, 'month' => $month), $usedDateField
        );
        $daysModel->dbCriteria->group = new CDbExpression("DAY(FROM_UNIXTIME({$usedDateField}))");
        foreach ($daysModel->findAll() as $page) {
            if (!ctype_digit($page->{$usedDateField})) {
                $page->{$usedDateField} = CDateTimeParser::parse($page->{$usedDateField}, Yii::app()->locale->getDateFormat('medium'));
            }
            $result['days'][] = (int)date('d', $page->{$usedDateField});
        }

        return $result;
    }


    private $_dataProvider = NULL;

    /**
     * Свойство-хелпер для работы с CActiveDataProvider.
     *
     * @param type $pageNumber номер текущей страницы (по основанию 1)
     * @param type $sortField поле сортировки
     */
    public function getActiveDataProvider($pageNumber = NULL, $sortField = NULL, $pageSize = NULL)
    {
        // инициализация объекта
        if (!$this->_dataProvider) {
            $this->_dataProvider = new CActiveDataProvider($this);

            // установка номера страницы 
            if (!is_null($pageNumber))
                $this->_dataProvider->pagination->currentPage = $pageNumber ? $pageNumber - 1 : 0;
            if ($pageSize) $this->_dataProvider->pagination->pageSize = $pageSize;

            /** @todo: не совсем ясно с логикой просчёта */
            $this->_dataProvider->pagination->validateCurrentPage = false;
            //$this->_dataProvider->pagination->setItemCount($this->_dataProvider->totalItemCount);

            // название аргумента, который передаётся в "arguments" для createUrl,
            // для указания страницы
            if (Yii::app()->params["defaultPageVar"])
                $this->_dataProvider->pagination->pageVar = Yii::app()->params["defaultPageVar"];


            /**
             * @TODO:
             * Необходимо внести в документацию:
             *
             * логика ActiveDataProvider подразумевает, что для указания
             * поля сортировки будет использоваться стандартный GET-параметр
             * (Yii::app()->params["defaultSortVar"]).
             *
             * Желательно использовать ActiveDataProvider внутри действия, в котором
             * есть аргумент, одноименный со значением в Yii::app()->params["defaultSortVar"],
             * т.к. контроллеры автоматически подставляют GET-параметры под
             * аргументы действия.
             *
             * */
            $this->_dataProvider->sort->sortVar = Yii::app()->params["defaultSortVar"];
        }

        return $this->_dataProvider;
    }


    /**
     * @var type mixed размер страницы при работе с этой моделью (может
     * иметь числовое значение или пресет из Yii::app()->params["pagination"])
     */
    protected $_pageSize = "mediumSize";

    /**
     * Активное свойство для получения количества записей по указанным критериям,
     * в отличие от CAtiveRecord::count(), не сбрасывает критерий, после
     * использования.
     * Поэтому очень удобен при проверках перед обращением.
     * @return type
     */
    public function getCount()
    {
        $baseCriteria = $this->getDbCriteria(false);
        if ($baseCriteria !== null)
            $baseCriteria = clone $baseCriteria;
        $count = $this->count($this->getDbCriteria());
        $this->setDbCriteria($baseCriteria);
        return $count;
    }


    /**
     * Установка периода выборки по модели.
     *
     * @param array $arguments $arguments перечисление аргументов (hour, minute, second,
     * month, day, year) - все опционально; можно указывать одну дату - период
     * одного месяца/дня/часа и т.д., а можно две - выборка между двумя периодами
     * (для этого передаётся вложенный массив, например:
     * [ [year, month, day, hour], [year, month, day] ]
     *
     * @param string $dateField поле, к которому прикрепляется выборка (по умолчанию
     * берётся из конфига
     *
     * @return ActiveRecord ссылка на объект для продолжений операций
     */
    public function dateRange($arguments, $dateField = NULL)
    {
        // поле даты по умолчанию
        if (!$dateField) $dateField = $this->getDefaultDateField();

        // можно указывать одну дату - период одного месяца/дня/часа и т.д.,
        // а можно две - выборка между двумя периодами
        if (is_array(current($arguments)))
            $endDate = $this->_getEndDate(next($arguments));
        else {
            // очистка пустых значений, чтобы поверх них
            // записались дефолтные
            foreach ($arguments as $key => $value)
                if (!$value) unset($arguments[$key]);

            $endDate = $this->_getEndDate($arguments);
        }


        $criteries = array
        (
            'condition' =>
                "t.$dateField >= :start_date AND t.$dateField <= :end_date",

            'params' => array
            (
                "start_date" => $this->_getStartDate($arguments),
                "end_date" => $endDate
            ),
        );

        // добавление условий выборки по дате
        $this->getDbCriteria()->mergeWith($criteries);


        return $this;
    }

    /**
     * Формирование начальной даты периода в соответствии с заданными аргументами.
     * @param array $arguments перечисление аргументов (hour, minute, second,
     * month, day, year) - все опционально
     * @return integer UNIX TIMESTAMP
     */
    protected function _getStartDate($arguments)
    {
        $arguments = array_merge(
            Yii::app()->params["blankStartDate"], $arguments
        );

        extract($arguments);

        // проверка корректности параметров
        if (!checkdate((int)$month, (int)$day, (int)$year))
            throw new CHttpException(400, Yii::t('models', 'Error end date'));


        // превращение аргументов 
        if (!$unixTime = mktime($hour, $minute, $second, $month, $day, $year))
            throw new CHttpException(400, Yii::t('models', 'Error start date'));

        return $unixTime;
    }

    /**
     * Формирование конечной даты периода в соответствии с заданными аргументами.
     * @param array $arguments перечисление аргументов (hour, minute, second,
     * month, day, year) - все опционально
     * @return integer UNIX TIMESTAMP
     */
    protected function _getEndDate($arguments)
    {
        $arguments = array_merge(
            Yii::app()->params["blankEndDate"], $arguments
        );

        extract($arguments);

        // количество дней в указаном месяце
        if (!$day) $day = date("t", mktime($hour, $minute, $second, $month));


        // проверка корректности параметров
        if (!checkdate((int)$month, (int)$day, (int)$year))
            throw new CHttpException(400, Yii::t('models', 'Error end date'));


        // превращение аргументов 
        if (!$unixTime = mktime($hour, $minute, $second, $month, $day, $year))
            throw new CHttpException(400, Yii::t('models', 'Error end date'));

        return $unixTime;
    }




    /** @category типовая предобработка полей */

    /**
     * Предобработка данных перед валидацией и сохранением.
     */
    public function beforeValidate()
    {
        $this->_relationsFromPost();
        return parent::beforeValidate();
    }

    /**
     * Принятие данных по связанным таблицам от форм.
     */
    protected function _relationsFromPost()
    {
        if (isset($_POST[get_class($this)])) {
            $input = $_POST[get_class($this)];
            $relations = array_intersect(array_keys($input), array_keys($this->relations()));
            foreach ($relations as $field) $this->$field = $input[$field];
        }
    }

    /** @category рутинные методы CActiveRecord */

    /**
     * Теперь эта функция не требует постоянного переопределения.
     * @return Модель этого клсса
     */
    public static function model($className = NULL)
    {
        return parent::model($className ? $className : get_called_class());
    }

    /**
     * Автоматическое формирование подписей к полям таблицы,
     * с использованием стандартных словарей.
     *
     * Помимо перегрузки метода, можно использовать свойство $this->dictionary.
     *
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $labels = array();

        // подписи проставляются автоматом
        foreach (array_merge($this->attributeNames(), array_keys($this->relations())) as $field)
            $labels[$field] = Yii::label($field, "attributes", true,
                isset($this->dictionary) ? array("attributes" => $this->dictionary) : NULL);

        return $labels;
    }


    protected function beforeSave()
    {
        if (Yii::app()->hasEventHandler('onARModelBeforeSave'))
            Yii::app()->raiseEvent('onARModelBeforeSave', new CEvent($this));
        return parent::beforeSave();
    }

    protected function afterSave()
    {
        if (Yii::app()->hasEventHandler('onARModelAfterSave'))
            Yii::app()->raiseEvent('onARModelAfterSave', new CEvent($this));
        return parent::afterSave();
    }

}


