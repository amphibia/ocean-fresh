<?php

/**
 * Базовый класс для конфигурационных моделей (моделей, хранящих данные в
 * конфигурационных файлах).
 */
abstract class ExtCoreConfigModel extends CFormModel
{	
	/**
	 * Имя конфигурационного файла, в котором хранятся настройки.
	 * @var string
	 */
	protected $_configFilename = NULL;


	protected static $_instance = NULL;

	/**
	 * @return ExtCoreConfigModel
	 */
	public static  function instance(){
		$class = get_called_class();
		if(!$class::$_instance) self::$_instance = new $class;
		return self::$_instance;
	}


	/**
	 * При загрузке автоматически считывает данные из конфига.
	 * 
	 * ПРИМЕЧАНИЕ: в в конфигурационной директории обязательно должна быть
	 * папка 'custom'.
	 */
	public function init() {
		$configDir = Yii::getPathOfAlias('application.config.custom');
		$this->_configFilename = "{$configDir}/".get_class($this).".php" ;

		if(is_file($this->_configFilename))
			 $this->setAttributes(require $this->_configFilename);
		else $this->_putFile();
	}

	/**
	 * Сохранение состояя модели в конфигурационный файл.
	 */
	protected function _putFile()
	{
		$layout = "<?php\nreturn array(\nCONTENT);\n?>";
		$text = NULL;

		foreach($this->getAttributes() as $name => $value){
			$text .= "\t'$name' => '". addslashes($value)."',\n";
		}

		$text = preg_replace('/CONTENT/', $text, $layout);
		if(file_put_contents($this->_configFilename, $text) !== false)
			 return true;
		else return false;
	}

	public function save($runValidation = true){
		if(!$this->validate()) return false;
		return $this->_putFile();
	}

	public function relations(){
        return array();
    }
}


