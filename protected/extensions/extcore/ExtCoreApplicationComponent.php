<?php

require_once "interfaces.php";

/**
 * Компонент приложения, хранящий собственный ID в приложении.
 */
class ExtCoreApplicationComponent extends CApplicationComponent{

    /**
     * ID этого компонента (для самодентификации в при динамическом изменении
     * среды).
     * @var string
     */
    public $id = NULL;
}
