<?php

interface IServiceAction
{
	public function getArguments();

	public function getHasRequieredArguments();
}
