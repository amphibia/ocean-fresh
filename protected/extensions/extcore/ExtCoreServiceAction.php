<?php

require_once("interfaces.php");

/* Встроенное действие сервиса. */
class ExtCoreServiceInlineAction extends CInlineAction implements IServiceAction 
{
	public $title;


	/**
	 * @param array
	 * Массив с аргументами (ключ - имя, тип, обязательный/нет и т.д.).
	 * Служит для внутренних целей (когда нужна информация о требуемых аргументах).
	 */
	protected $_arguments;

	/**
	 * @param boolean
	 * имеет ли действие обязательные аргументы
	 */
	protected $_hasRequieredArguments = false;


	/**
	 * @return array
	 * Массив с аргументами (ключ - имя, тип, обязательный/нет и т.д.).
	 * Служит для внутренних целей (когда нужна информация о требуемых аргументах).
	 */
	public function getArguments(){ return $this->_arguments; }

	/**
	 * @return boolean
	 * имеет ли действие обязательные аргументы
	 */
	public function getHasRequieredArguments(){ return $this->_hasRequieredArguments; }


	/**
	 * Constructor.
	 * @param CController $controller the controller who owns this action.
	 * @param string $id id of the action.
	 */
	public function __construct($controller, $id)
	{
		parent::__construct($controller, $id);
	
		// заголовок по умолчанию
		$title = Yii::label($id, "actions", true,
			isset($controller->dictionary) ? array("actions" => $controller->dictionary) :  array());

		// обращение к аргументам метода
		$reflector = new ReflectionClass(get_class($this));
		$reflectorMethod = $reflector->getMethod("run");
		$arguments = $reflectorMethod->getParameters();

		$this->_hasRequieredArguments = false;
		$this->_arguments = array();

		// сбор информации по аргументам
		foreach($arguments as $argument)
		{
			$argumentOptions = array();

			// необязательный параметр и значение по умолчанию
			if(!$argument->isDefaultValueAvailable())
			{
                $this->_hasRequieredArguments = true;
                $argumentOptions["isOptional"] = false;
			}
			else
			{
				$argumentOptions["defaultValue"] = $argument->getDefaultValue();
				$argumentOptions["isOptional"] = true;
			}

			$argumentOptions["isArray"] = $argument->isArray();
			
			// указаанный класс аргумента
			if($class = $argument->getClass())
				 $argumentOptions["class"] = $class->getName();
			else $argumentOptions["class"] = NULL;
			
			// подшивание к данным об аргументах метода
			$this->_arguments[$argument->getName()] = $argumentOptions;
		}
	}
    
     /**
     * @return boolean
     */
    public function isAjax(){
        return $this->controller->isAjaxAction($this);
    }

}