<?php

/**
 * Надстройка для работы cо скриптами.
 * Добавлен метод getStaticScripts.
 * 
 * Также, теперь перед выводом  происходит сжатие стилей и скриптов (управляется
 * свойством $packAssets).
 * 
 * Также, была добавлена новая позиция расположения скриптов: POS_HEAD_PURE.
 * Т.е. скрипты в головной части, не подвергающиеся сжатию.
 * 
 * ПРИМЕЧАНИЕ: эксперементальный компонент.
 */
class ExtCoreClientScript extends CClientScript
{
	const POS_HEAD_PURE = 5;
	
	/**
	 * Период смены кэша (в часах).
	 * @var integer
	 */
	public $cachePeriodLength = 6200;

	/**
	 * Требуется ли сжимать ресурсы перед выводом.
	 * @var boolean 
	 */
	public $packAssets = false;


	public function init()
	{
		Yii::import('ext.extcore.lib.minify.*');
		Yii::app()->controllerMap['min'] = array(
			'class'=>'ext.extcore.lib.minify.controllers.ExtMinScriptController'
		);

		parent::init();
	}
	
	
	/**
	 * Получение статичных скриптов, вщитых в страницу.
	 * @param mixed $position расположение скриптов
	 * @return array
	 */
	public function getStaticScripts($position = NULL){
		if(!is_NULL($position)) return isset($this->scripts[$position])
			? $this->scripts[$position] : array();
		else return $this->scripts;
	}
	
	/**
	 * Removes duplicated scripts from {@link scriptFiles}.
	 * @since 1.1.5
	 * 
	 * Также, группирует скрипты и сжимает в один.
	 */
	protected function unifyScripts()
	{
		$retValue = parent::unifyScripts();

		if($this->packAssets)
		{
			if(isset($this->scriptFiles[self::POS_HEAD]))
				$this->_createBroom($this->scriptFiles[self::POS_HEAD], 'js');

			if(isset($this->scriptFiles[self::POS_BEGIN]))
				$this->_createBroom($this->scriptFiles[self::POS_BEGIN], 'js');

			if(isset($this->scriptFiles[self::POS_END]))
				$this->_createBroom($this->scriptFiles[self::POS_END], 'js');

			if(is_array($this->cssFiles) && sizeof($this->cssFiles)){
				$medias = array();
				foreach($this->cssFiles as $file => $media){
					if(!isset($medias[$media])) $medias[$media] = array();
					$medias[$media][] = $file;
				}

				$this->cssFiles = array();
				foreach($medias as $media => $styles){
					$this->_createBroom($styles, 'css', $media);
					$this->cssFiles = array_merge($this->cssFiles, $styles);
				}
			}
		}

		// скрипты, которые не требуют сжатия (например, если имеют сложную
		// структуру - ткакую как у TinyMCE)
		if(isset($this->scriptFiles[self::POS_HEAD_PURE])){
			if(!isset($this->scriptFiles[self::POS_HEAD]))
				$this->scriptFiles[self::POS_HEAD] = array();
			$this->scriptFiles[self::POS_HEAD] = array_merge(
				$this->scriptFiles[self::POS_HEAD],
				$this->scriptFiles[self::POS_HEAD_PURE]
			);
		}

		return $retValue;
	}
	
	/**
	 * Группировка файлов, создание подписи, подготовка к сжатию в один файл.
	 * @param array $items массив с файлами
	 * @param string $type (js|css)
	 * @param string $media группировка файлов по  "media" (для CSS)
	 */
	protected function _createBroom(&$items, $type, $media = NULL){
		$pack = array();
		$hash = NULL;
		foreach($items as $file){
			$hash .= $file;
			$pack[] = $file;
		}

		if($type!='css') $media = 'script';
		else if(!$media) $media = 'all';
		$hash = md5($hash) . "_" . sizeof($pack) . "_{$media}_period" . floor(time()/($this->cachePeriodLength*3600));
		$items = $this->_pack($hash, $pack, $type, $media);
	}

	/**
	 * Группировка в один файл.
	 * @param string $hash имя комплексного файла
	 * @param array $pack связка файлов
	 * @param string $type (js|css)
	 * @return string имя комбинированного файла
	 */
	protected function _pack($hash, $pack, $type, $media = NULL)
	{
		$assetsPath = Yii::app()->assetManager->getBasePath();
		$fileName = "$assetsPath/$hash.$type";

		if(!is_file($fileName)){
			$file = fopen($fileName, 'w');
			foreach($pack as $scriptFilename){
				$content = file_get_contents($_SERVER['DOCUMENT_ROOT'].$scriptFilename);
				// замена относительных адресов на абсолютные
				if($type == 'css')
				{
					$relDir = preg_replace("/[\w\.]+\.$type/", '', $scriptFilename);
					$content = preg_replace_callback('/((?>u|U)(?>r|R)(?>l|L)\((?>\'|")?)(.)/',
						function($matches) use($relDir){
							if($matches[2] != '/') return $matches[1] . "$relDir" . $matches[2];
							else return $matches[1] . $matches[2];
						},$content);

					$content = CssCompressor::process($content);
				}
				else if($type == 'js') $content = JSMin::minify($content).';';

				fwrite($file, $content);
			}
			fclose($file);
		}

		$file = Yii::app()->assetManager->getBaseUrl() . "/$hash.$type";
		if($type == "js") return array($file => $file);
		else return array($file => $media);
	}

}