<?php

/**
 * Базовый клас для административных сервисов, связанных с созданием,
 * выводом, редактированием записей и удалением записей.
 *
 * Реализует стандартные действия:
 * @see actionIndex, @see actionCreate, @see actionUpdate, @see actionDelete.
 *
 * Кроме того, предоставляет JSON-интерфейс для изменения безопасного изменения
 * аттрибутов напрямую с клиентской стороны - @see actionSetAttributes (например,
 * для быстрых переключателей).
 *
 * Также, преодоставлены инструменты управления сообщениями при использовании
 * JSON-обмена с клиентом и методы упрощения работы с формами.
 *
 * Сервис не отвечает за вывод, представления реализуются отдельно для потомков
 * класса.
 */
class ExtCoreAdminService extends ExtCoreService
{
    /**
     * Конфигурация формы создания.
     * @var array
     */
    public $createForm = array();

    /**
     * Конфигурация формы редактирования.
     * @var array
     */
    public $updateForm = array();

    /**
     * Настройки для виджета @see ActiveTable (actionIndex).
     * Аттрибуты 'model', 'scope' и настройки постраничного вывода указывать не
     * нужно.
     * @var array
     */
    public $indexTableOptions = array();

    /**
     * Текст запроса подтверждения на удаление элементов.
     * @var string
     */
    public $deleteConfirmationMessage = NULL;

    /**
     * Текст сообщения об успешном удалении элемента.
     * @var string
     */
    public $deleteSuccessMessage = NULL;

    /**
     * Текст сообщения о неудачном удалении элемента.
     * @var string
     */
    public $deleteFailMessage = NULL;

    /**
     * Адреса представлений, используемых сервисом.
     * @var array
     */
    public $views = array();


    public function getRbacOperation()
    {
        return 'Administration service';
    }

    public function filters()
    {
        return array('ajaxOnly + setAttributes, simpleAttr, groupOperationsBar');
    }

    protected function beforeAction($action)
    {
        Yii::loadExtConfig($this, __FILE__, __CLASS__ . ".php");
        if (!isset($_POST['_formOptions'])) $_POST['_formOptions'] = array();

        return parent::beforeAction($action);
    }


    /**
     * Таблица элементов.
     * Предполагается, что представление 'index', используемое функцией, будет
     * использовать виджет ActiveTable - для него подготавливается массив
     * с настройками и передаётся представлению (аргумент 'config' в представлении).
     *
     * Представление 'index' может выглядеть следующим образом:
     * .
     * .
     * <ЗАГОЛОВОК>
     * <?
     * $this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), $config);
     * ?>
     * .
     * .
     *
     * Основные настройки хранятся в свойстве ExtCoreAdminService::$indexTableOptions,
     * список параметров смотрите в описании виджета @see ActiveTable
     *
     * ПРИМЕЧАНИЕ: очень важно, чтобы сохранилось соответствие имён аргументов,
     * отправляемых действию, т.к. по умолчанию свойства виджета ActiveTable
     * берутся из GET, и на основании GET генерирутся ссылки для управляющих
     * элементов (пагинация, форма поиска, кнопки фильтров).
     * Если потребуется изменить имена аргументов, измените соответствующие свойства
     * в ExtCoreAdminService::$indexTableOptions: searchVar и т.п.
     *
     * @param string $orderBy
     * @param string $scope
     * @param integer $page
     */
    public function actionIndex($orderBy = NULL, $scope = NULL, $search = NULL, $page = NULL, $parent = NULL)
    {
        $page = intval($page ? ($page - 1) : $page);
        $this->model->getActiveDataProvider()->pagination->currentPage = $page;
        $this->indexTableOptions['model'] = $this->model;
        $this->render($this->views['index'], array('config' => $this->indexTableOptions));
    }

    /**
     * Страница создания элемента.
     * @param integer $parent_id В данной реализации не используется, оставлена для
     * совместивмости с сервисами, работающими с иерархическими моделями.
     */
    public function actionCreate($parent_id = NULL)
    {
        $form = new CForm($this->createForm, $this->model);
        if (isset($_POST[$this->modelClass])) {
            $form->loadData();
            $this->_formSaved($form->model->save(), $form, true, true);
        }

        $this->render($this->views['create'], compact('form'));
    }

    /**
     * Страница редактирования существующей записи.
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $this->loadModel($id);
        $form = new CForm($this->createForm, $this->model);
        if (isset($_POST[$this->modelClass])) {
            $form->loadData();
            $this->_formSaved($form->model->save(), $form);
        }

        $this->render($this->views['update'], compact('form'));
    }

    /**
     * Удаление записи (или массива записей).
     * @param array $id
     * @param boolean $confirmation подтверждённое удаление
     * @param string $customMessage своё сообщение по удалении
     */
    public function actionDelete(Array $id, $confirmation = false, $customMessage = NULL, $returnUrl = NULL)
    {
        if (is_array($id))
            $elements = $this->model->findAllByPk($id);
        else $elements = array($this->loadModel($id));

        // подстановка своего сообщения
        if (!$customMessage)
            $message = $this->_deleteConfirmation($elements);
        else $message = $this->_customAjaxMessage($customMessage);

        // удаление по JSON
        if (Yii::app()->request->isAjaxRequest) {
            ExtCoreJson::confirm($message);
            $this->_processDelete($elements);

            ExtCoreJson::response();
        } // удаление напрямую
        else {
            if ($confirmation) {
                $this->_processDelete($elements);

                if (!$returnUrl) $returnUrl = isset($_SERVER["HTTP_REFERER"])
                    ? $_SERVER["HTTP_REFERER"] : $this->createUrl('index');
                $this->redirect($returnUrl);
            } else $this->render('ext.extcore.views.deleteForm', compact('id', 'message'));
        }
    }

    /**
     * Непосредственно удаление.
     *
     * Удаление происходит по одному, чтобы при удалении вызывались события,
     * прикрепленные к этим объектам. Соответственно, метод не предполагает
     * работу с большим количеством удаляемых элементов. Учитывая специфику
     * административных интерфейсов (не для массового использования), это
     * считается допустимым.
     *
     * @param array $items
     */
    protected function _processDelete(Array $elements)
    {
        $itemsCount = count($elements);
        foreach ($elements as $element) {
            if ($element->delete()) {
                $message = Yii::t('admin', $this->deleteSuccessMessage, array("{id}" => $element->id));
                if (Yii::app()->request->isAjaxRequest)
                    ExtCoreJson::message($message);
                else Yii::app()->user->setFlash('success', $message);
            } else {
                $message = Yii::t('admin', $this->deleteFailMessage, array("{id}" => $element->id));
                if ($element->hasErrors()) {
                    $message .= '<br/><br/>';
                    foreach ($element->getErrors() as $attr => $errors)
                        foreach ($errors as $error)
                            $message .= "<strong>$attr</strong>: $error<br/>";
                }

                if (Yii::app()->request->isAjaxRequest)
                    ExtCoreJson::error($message);
                else Yii::app()->user->setFlash('error', $message);
            }
        }
    }

    /**
     * Текст сообщения с подтверждением удаления
     * @param array $elements
     */
    protected function _deleteConfirmation(Array $elements)
    {

        $ids = array();
        foreach ($elements as $element) $ids[] = '#' . $element->id;
        $ids = implode(', ', $ids);

        return Yii::t($this->messageContext,
            $this->deleteConfirmationMessage,
            array('{elements}' => $ids)
        );
    }

    /**
     * Страница, на которую следует произвести переадресацию по завершении
     * операции.
     * @return string
     */
    public function refererUri()
    {
        /**
         * @todo: записать в документации, что поля _formOptions[]
         * используются для управляющих свойств формы */
        static $referer = NULL;
        if ($referer) return $referer;

        if (isset($_POST['_formOptions']['referer']))
            $referer = $_POST['_formOptions']['referer'];

        if (!$referer && isset($_SERVER['HTTP_REFERER']))
            $referer = $_SERVER['HTTP_REFERER'];

        return $referer ? $referer : $this->createUrl('index');
    }

    /**
     * Действия после сохранения данных с формы. Обрабатывает cookies формы,
     * выводит сообщения, производит переадресацию.
     * @param boolean $result
     * @param CForm $form
     * @param boolean $doRedirect выполнять ли возврат на предыдущую страницу
     */
    protected function _formSaved($result = true, CForm &$form, $doRedirect = true, $isNewRecord = false)
    {
        // чтение из кукисов свойства: повторять действие по завершении
        $cookies = & Yii::app()->request->cookies;
        $repeatAction = false;
        if (isset($_POST['_formOptions']['repeatAction']))
            $repeatAction = $_POST['_formOptions']['repeatAction'];

        $cookies['_formOptions_repeatAction'] = new CHttpCookie(
            '_formOptions_repeatAction', $repeatAction
        );


        if ($result) {
            // уведомление и переадрессация (если потребуется)
            Yii::app()->user->setFlash('success', $this->_successMessage());
            if ($doRedirect && !$repeatAction) {
                $this->redirect($this->refererUri());
            } elseif (!$isNewRecord) {
                $this->redirect($_SERVER['REQUEST_URI']);
            }
        } else {
            $text = '<h3>' . Yii::t('admin', 'Some errors occured') . '</h3>';

            $labels = $form->model->attributeLabels();
            foreach ($form->model->errors as $field => $messages) {
                foreach ($messages as $message)
                    $text .= '<div><span class="label label-important">' . $labels[$field] . '</span> ' . $message . '</div>';
            }

            // сообщения об ошибках
            Yii::app()->user->setFlash('error', $text);
        }
    }

    /**
     * Сообщение об успешном сохранении элемента.
     * @return string
     */
    protected function _successMessage()
    {
        if ($this->lastActionID == 'create')
            return Yii::t('news', 'Element #{id} created. For editing element, go to {edit form}.',
                array(
                    '{id}' => $this->_model->id,
                    '{edit form}' => CHtml::link(
                            Yii::t('admin', 'edit form'),
                            $this->createUrl('update', array($this->_model->id))
                        )
                )
            );

        else return Yii::t('news', 'Element #{id} saved.',
            array('{id}' => $this->_model->id));
    }

    /**
     * Изменние атрибутов основной модели контроллера напрямую (только JSON).
     * @param integer $id
     * @param array $values
     * @param boolean $returnMessage требуется ли вывести сообщение о результате
     * @param mixed $customMessage своё сообщение об удачном выполнении
     */
    public function actionSetAttributes($id, Array $values, $returnMessage = true, Array $customMessage = NULL)
    {
        // подстановка своего сообщения
        if (!$customMessage) $customMessage = Yii::t(
            $this->messageContext, 'Attributes was changed.');
        else $customMessage = $this->_customAjaxMessage($customMessage);


        $this->loadModel($id);
        $this->model->setAttributes($values);

        if ($this->model->save(false)) {
            if ($returnMessage) ExtCoreJson::message($customMessage);
        } else {
            if ($returnMessage) {
                $errors = Yii::t('admin', 'Error with change attriburtes:<br/><br/>');
                if (is_array($this->model->errors))
                    foreach ($this->model->errors as $field => $fieldErrors)
                        $errors .= "<b>$field</b><br/>" . implode('<br/>\n', $fieldErrors);

                ExtCoreJson::error(Yii::t('admin', $errors));
            } else ExtCoreJson::$data['result'] = 0;
        }

        ExtCoreJson::response();
    }

    public $simpleAttrPkArgs = array('id', 'pk', 'item', 'element', 'element_id');
    public $simpleAttrNameArgs = array('name', 'attribute', 'attribute_name', 'attr', 'attr_name', 'field');
    public $simpleAttrValueArgs = array('value', 'attribute_value', 'attr_value');

    /**
     * Упрощенная установка атрибута, совместимая с формами быстрого редактирования
     * в расширениях типа YiiBooster. Все параметры указываются в POST.
     */
    public function actionSimpleAttr()
    {
        // ID записи
        foreach ($this->simpleAttrPkArgs as $pkArg)
            if (isset($_POST[$pkArg])) {
                $id = $_POST[$pkArg];
                break;
            }

        // имя аттрибута
        foreach ($this->simpleAttrNameArgs as $attrArg)
            if (isset($_POST[$attrArg])) {
                $attribute = $_POST[$attrArg];
                break;
            }

        // значение
        foreach ($this->simpleAttrValueArgs as $valueArg)
            if (isset($_POST[$valueArg])) {
                $value = $_POST[$valueArg];
                break;
            }


        if (!isset($id) || !isset($attribute) || !isset($value))
            throw new CHttpException(400);

        try {
            $this->actionSetAttributes($id, array($attribute => $value));
        } catch (Exception $error) {
            echo Yii::t('app', 'Error {error_code}:',
                array('{error_code}' => $error->getCode()));
            echo " " . $error->getMessage();

            Yii::app()->end();
        }
    }

    /**
     * Общий формат для сообщений, приходящих со стороны клиента:
     * message | [message:message_value, param1:param1_value, param2:param2_value ... ]
     * @param type $value mixed значение
     */
    protected function _customAjaxMessage($value)
    {
        if (is_array($value)) {
            return Yii::t($this->messageContext,
                array_shift($value), $value);
        } else return Yii::t($this->messageContext, $value);
    }

    /**
     * Текущий контекст для Yii::t().
     * @staticvar string $conext
     * @return string
     */
    public function getMessageContext()
    {
        static $conext = NULL;
        if (!$conext) $conext = Yii::app()->activeModule
            ? Yii::app()->activeModule->id : 'app';
        return $conext;
    }


    /**
     * Вывод панели управления выделенными элементами таблицы (групповые операции).
     *
     * @param array $ids
     * @return type
     */
    public function actionGroupOperationsBar(Array $ids = array())
    {
        if (!$ids || !sizeof($ids)) {

            echo CHtml::tag('span', array(), Yii::t('admin', 'No items selected.'));
            echo " &nbsp; ";
            echo CHtml::link(Yii::t('admin', 'Select all'), 'javascript:;', array('class' => 'selectAll'));

            return;
        }

        $count = sizeof($ids);

        echo CHtml::tag('span', array(),
            Yii::t('admin', 'Selected {count} item.|Selected {count} items.',
                array($count, '{count}' => $count)
            )
        );

        echo " &nbsp; ";

        echo CHtml::link(Yii::t('admin', 'Select all'), 'javascript:;', array('class' => 'selectAll'));
        echo " / ";
        echo CHtml::link(Yii::t('admin', 'Select none'), 'javascript:;', array('class' => 'selectNone'));

        echo " &nbsp; ";

        $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
            array(
                'label' => Yii::t('admin', 'Delete item|Delete items', array($count)),
                'type' => 'danger',
                'icon' => 'icon-trash icon-white',
                'url' => $this->createUrl('delete', array('id' => $ids)),

                'htmlOptions' => array('class' => 'deleteButton'),
            )
        );
    }

}