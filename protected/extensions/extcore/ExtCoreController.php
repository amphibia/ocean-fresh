<?php
/**
 * Базовый класс-контроллер для всех контроллеров
 */
class ExtCoreController extends CController
{
    /**
     * Перемення для передачи
     * заголовка страницы в лайаут
     *
     * @var string
     */
    public $title = null;

	/**
	 * Название операции, которую использует RBAC авторизация.
	 * 
	 * @return mixed Cтрока с названием операции, или массив пар ключ => значение,
	 * в котором ключ соответсвует действию, значение название операции. Если
	 * указанно NULL, контроль RBAC авторизации не применяется.
	 * 
	 * @example
	 * return array
	 * (
	 *     "edit"       => "editPost",
	 *     "delete"     => "deletePost",
	 *     "default"    => "controllerAction",
	 *     "error"      => NULL,
	 * );
	 */
	public function getRbacOperation(){
		return Yii::app()->params["rbacDefaultOperation"];
	}

    /**
     * ID последнего действия из вызываемых
     * @var string
     */
    public $lastActionID = NULL;

    /**
     * Модель, ассоциируемая с этим контроллером в данный момент.
     * Информация о текущей модели поможет обращаться к привязанным данным
     * (например комментариям страницы) без жестких привязок.
     * @var Model 
     */
    protected $_model = NULL;

    public function getModel(){
        $modelClass = $this->modelClass;
        return $this->_model ? $this->_model : $this->_model = new $modelClass;
    }

    public function setModel($value){
        $this->_model = $value; }


    /**
     * Класс основной модели контроллера.
     * @var 
     */
    public $modelClass = NULL;

    /**
     * Список параметров, зарезервированных заранее (ИМЯ=>ПРАВИЛО).
     * Если они встретятся среди значений, а последующее значение будет
     * удовлетворять его правилу, значит это директива перед следующим параметром.
     * 
     * @var array
     */
    public $reservedArguments = array(
        'page' => '/^[0-9]{1,5}$/',
    );


    //protected $_action;

	/**
     * Переопределение обращения к контроллеру, чтобы подставить аргументы
     * действия из URL. Тепер вместо /controller/action/argument_name/value
     * будет использоваться /controller/action/value.
     * 
	 * Runs the named action.
	 * Filters specified via {@link filters()} will be applied.
	 * @param string $actionID action ID
	 * @throws CHttpException if the action does not exist or the action name is not proper.
	 * @see filters
	 * @see createAction
	 * @see runAction
	 */
	public function run($actionID)
	{
		// при ошибках не надо лишних действий
		if($actionID == 'error'){
			parent::run($actionID);
			Yii::app()->end();
		}

        // При первом вхождениии аргументы проверяются на соответствие.
        static $firstEntry = true;

        // имя метода действия
        $actionMethod = "action".ucfirst($actionID);

        // Аргументы, поступающие прямо из строки запроса (в виде строки).
        // Параметр '_actionArguments' приходит из urlManager. Чтобы он заполнялся,
        // должны быть соответствующие правила обработки URL.
        if(isset($_GET['_actionArguments'])){
            if(is_array($_GET['_actionArguments']))
                 $arguments = $_GET['_actionArguments'];
            else if($_GET['_actionArguments'])
                 $arguments = mb_split("\/", trim($_GET['_actionArguments'],'\/'));
            else $arguments = array();

			unset($_GET['_actionArguments']);
		}

        else{
        	$arguments = $_GET;
			if(!$firstEntry) $_GET = array();
        }

        if(method_exists($this, $actionMethod))
        {
            $requieredArguments = array();
            
            $reflection = new ReflectionMethod($this, $actionMethod);
            
            // список параметров переводится в ассоциативный массив
            foreach($reflection->getParameters() as $parameter){
                $requieredArguments[$parameter->getName()] = &$parameter;
            }
            
            // Формируется список "указанных аргументов" из отправленных.
            $oldArguments = $arguments;
            $prevReserved = NULL;
            foreach($oldArguments as $key => $value)
            {
                // это значание отнесено к ранее встреченному упоминанию
                if( !is_null($prevReserved) &&
                    isset($this->reservedArguments[$arguments[$prevReserved]]) &&
                    preg_match($this->reservedArguments[$arguments[$prevReserved]], $value))
                {
                    unset($arguments[$prevReserved]);
					unset($_GET[$prevReserved]);
                    unset($arguments[$key]);
					unset($_GET[$key]);

                    $arguments[$oldArguments[$prevReserved]] = $value;
                    $prevReserved = NULL;
                }

                // Встретилось упоминание "зарезервированного аргумента" - следующее
                // значение будет этим аргументом
                if( !is_array($value) && is_numeric($key) &&
                    isset($this->reservedArguments[$value]) &&
                    !isset($arguments[$value]) )
                { 
                    $prevReserved = $key;
                    continue;
                }

                $prevReserved = NULL;
            }

            // Аргументы сортируются в обратном порядке, чтобы вначале
            // оказались именованные аргументы. Соответственно, простановка
            // аргументов производится в обратном порядке.
			$associated = array(); $indexed = array();
			foreach($arguments as $key => $value)
				if(is_numeric($key)) $indexed[$key] = $value;
				else $associated[$key] = $value;

			$arguments = array_merge($associated, $indexed);
				
            if((sizeof($arguments) > sizeof($requieredArguments)) && $firstEntry){
                throw new CHttpException(400, Yii::t('extcore',
                    'Too many arguments'));
            }

            foreach($arguments as $argumentName => $argument)
            {
				if(!is_numeric($argumentName)){
                    if(!isset($requieredArguments[$argumentName]) && $firstEntry)
                        throw new CHttpException(400, Yii::t('extcore',
                            'Unknown argument "{argument}"', array('{argument}'=>$argumentName)));

                    $_GET[$argumentName] = $argument;
                    unset($requieredArguments[$argumentName]);
                }
                else{
                    reset($requieredArguments);
                    $key = key($requieredArguments);
                    $_GET[$key] = $argument;
					unset($_GET[$argumentName]);
                    unset($requieredArguments[$key]);
                }
            }
        }

        $firstEntry = false;
		Yii::app()->raiseEvent('onBeginControllerAction', new CEvent($this));
        $this->lastActionID = $actionID;
		
		//echo "ACTION RUN END ($actionID): " . round(microtime(1) - MICRO_TIME, 2) . "<br/>";
		
        parent::run($actionID);
		Yii::app()->raiseEvent('onEndControllerAction', new CEvent($this));
	}

	public function render($view, $data=null, $return=false)
	{
		if(Yii::app()->request->isAjaxRequest)
			 return $this->renderPartial($view, $data, $return);
		else return parent::render($view, $data, $return);
	}
	
/*
    public function render($view, $data=null, $return=false)
    {
        if(Yii::app()->request->isAjaxRequest)
            return $this->renderPartial($view, $data, $return);

        if($return || !Yii::app()->hasComponent('output')){
            return parent::render($view,$data,$return);}

        $controller = $this;
        $contentRegion = Yii::app()->output->region('content');
        $contentRegion->wrapTags = false;
        $contentRegion->append($data)->setConvertor(
            function($region, $data) use($view, $controller){
                return $controller->render($view, $data, true);
        });
    }

    public function renderPartial($view, $data=NULL, $return=false, $processOutput=false)
    {   
        if($return || Yii::app()->output->depth || !Yii::app()->hasComponent('output'))
            return parent::renderPartial($view, $data, $return, $processOutput);

        static $partIndex = 0;
        $controller = $this;
        $partRegion = Yii::app()->output->region('content')
            ->region('regionPart_'.time()."_{$partIndex}");

        $partRegion->wrapTags = false;
        $partRegion->append($data)->setConvertor(
            function($region, $data) use($view, $controller, $processOutput){
                return $controller->renderPartial($view, $data, true, $processOutput);
        });

        $partIndex++;
    }
*/

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id, $modelClass = NULL) {
        if(!$modelClass && !$modelClass=$this->modelClass)
            throw new CException(Yii::t('models', 'Undefined model class.'));

        $this->model = $modelClass::model()->findByPk($id);
        if(!$this->model->primaryKey)
            throw new CHttpException(404, Yii::t('models', 'The requested page does not exist.'));
        return $this->model;
    }

	protected function beforeAction($action){
        Yii::app()->raiseEvent('onActionRun', new CEvent($this));
        return parent::beforeAction($action);
    }

	public function actionError()
    {
	    if(!$error = Yii::app()->errorHandler->error) die(Yii::t('extcore', 'Unknown error'));

        if(Yii::app()->request->isAjaxRequest){
            if(ExtCoreJson::isUsed()){
                ExtCoreJson::message($error['message']);
                ExtCoreJson::$data['result'] = 0;
                ExtCoreJson::$data['message'] = $error['message'];
                ExtCoreJson::$data['code'] = $error['code'];
                ExtCoreJson::$data['errorCode'] = $error['code'];
                ExtCoreJson::response();
            }
        }

		// предыдущее содержимое отображается только в режиме отладки
		//if(!defined('YII_DEBUG') or !YII_DEBUG) Yii::app()->output->clear();

		$this->renderPartial('/system/error', $error);
		//Yii::app()->output->render();
		//Yii::app()->end();
	}


	public function forward($route, $exit = true) {
		$_GET = array();
		return parent::forward($route, $exit);
	}
}