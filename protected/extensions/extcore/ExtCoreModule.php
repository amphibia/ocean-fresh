<?php

/**
 * Надстройка для работы с модулями (релизация поддржки сквозных сервисов).
 */
class ExtCoreModule extends CWebModule
{
    public $assets = array();
    
    /**
     * @var ExtCoreModule модуль, через который выполнялся вход
     */
    protected static $_activeModule = NULL;
    
    /**
     * Обращение к активному модулю
     * @return ExtCoreModule активный модуль
     */
    public static function getActiveModule(){
        return self::$_activeModule;
    }
    
    /**
     * Перехват и обработка преодпределённых действий (например, обработка
     * запросов к портам сервисов).
     */
	public function init()
	{
        // импорт инфраструктуры модуля
		$this->setImport(array(
			$this->name.'.models.*',
			$this->name.'.components.*',
			$this->name.'.services.*',
            $this->name.'.views.*',
		));

        self::$_activeModule = &$this;
        $this->moduleInit();
	}

    /**
     * Инициализация потомков класса (чтобы не перезаписывать init()).
     */
    public function moduleInit(){}
}
