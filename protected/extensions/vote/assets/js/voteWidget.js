$(document).ready(function () {
    $(".voteWidget_vote").submit(function () {
        url = $(this).attr('action');
        voteID = $(this).attr('data-vote-id');
        field = $(this).attr('data-vote-field');
        answer = $(this).find('input[type="radio"]:checked').val();
        hash = $(this).find('input[name="hash"]').val();
        if (url && voteID) {
            url = url + 'vote/';
            $(this).request(url, {voteID: voteID, answer: answer, field: field, hash: hash});
        }
        return false;
    });
});