<div class="alert alert-info uploadInformer uploadInformerStart clientContent clientContentReplace" upload_id="<?=$fileId?>">

    <span class="label label-info"><?=Yii::t('upload', 'Upload started');?></span>
    
    <p>
        <div class="fileName"><?=$fileName?></div>
        <div class="fileSize"><?=round($fileSize/(1024*1024), 2)?> Mb</div>

        <div class="progress progress-striped active">
            <div class="bar" style="width:1%;"></div>
        </div>
        
        <?
        $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
            array('buttonType'=>'button', 'icon'=>'icon-ban-circle', 'label'=>Yii::t('app', 'Cancel'),
                'htmlOptions'=>array('class'=>'abortUpload'),
                'size'=>'mini',
            )
        );
        ?>
    </p>

</div>