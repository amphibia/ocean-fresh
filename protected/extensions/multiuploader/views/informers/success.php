<?
	/**
	 * Информер успешно прикрепленного файла.
	 * Используется виджетом UploadsFormElement и контроллером UploadElement,
	 * реализующим серверную часть работы с эти виджетом.
	 * 
	 * Вытаскивает указанные в POST координаты обрезки изображения.
	 * Сохраненные изображения проходят процедуру извлечения координат, после
	 * которой координаты записыватся в $_POST['UploadsFormElement'][FILENAME],
	 * а в имени файлов остается только непосредственно имя файла.
	 *
	 * Поэтому процедура вывода этого представления не изменяется, как для отправленных
	 * данных из POST, так и только что прочитанных из БД.
	 */
	$fileNameMd5 = md5($newFileName);
	$UploadsFormElement = isset($_POST['UploadsFormElement'])
		? $_POST['UploadsFormElement'] : array();

	$fileOptions = isset($UploadsFormElement[$fileNameMd5])
		? $UploadsFormElement[$fileNameMd5] : array();

	isset($title) or $title = NULL;
	isset($description) or $description = NULL;
	$x = isset($fileOptions['x']) ? $fileOptions['x'] : NULL;
	$y = isset($fileOptions['y']) ? $fileOptions['y'] : NULL;
	$width = isset($fileOptions['width']) ? $fileOptions['width'] : NULL;
	$height = isset($fileOptions['height']) ? $fileOptions['height'] : NULL;
	
	$fileName = isset($compressedFileName)?$compressedFileName:$newFileName;
	$imageUrl = $storage->createUrl($fileName);
	$previewImageUrl = $storage->createUrl($fileName, "uploadPreview");
	$isImage = Yii::app()->upload->isImage($fileName);
	$fileMime = CFileHelper::getMimeTypeByExtension($fileName);
	
	$pathinfo = pathinfo($newFileName);
	$baseName = $pathinfo['basename'];
?>
<div data-file-type="<?= $isImage ? 'image' : 'document' ?>"
	 data-original-url="<?= $isImage ? $storage->createUrl($newFileName, "default") : NULL ?>"
	 data-preview-url="<?= $isImage ? $storage->createUrl($newFileName, "cropPreview") : NULL ?>"
	 class="alert alert-success uploadInformer uploadInformerSuccess clientContent clientContentReplace"
	 upload_id="<?=$fileId?>">

	<? if($isImage){ ?>
    <a class="imagePreview" target="_blank" href="<?= $imageUrl ?>">
        <img src="<?= $previewImageUrl ?>" />
    </a>
	<? }else{ ?>
		<div class="fileTypeIcon" data-mime="<?= $fileMime ?>">
			<span><?= $fileMime ?></span>
		</div>
	<? } ?>

    <div class="right">

		<strong class="fileTypeLabel"><?= $isImage ? Yii::t('upload', 'Image') : Yii::t('upload', 'Document'); ?></strong>

        <div class="fileName" data-default-value="<?= $baseName ?>"><?= $title ? $title : $baseName ?></div>
        <div class="description"><?= $description ?></div>
        <? if($fileSize){ ?><div class="fileSize"><?=round($fileSize/(1024*1024), 3)?> Mb</div> <? } ?>

		<!-- Прикрепленные свойства -->
		<input class="inputTitle" name="UploadsFormElement[<?=$fileNameMd5?>][title]" value="<?=$title?>" type="hidden" />
		<input class="inputDescription" name="UploadsFormElement[<?=$fileNameMd5?>][description]"  value="<?=$description?>" type="hidden" />
		<input class="inputX" name="UploadsFormElement[<?=$fileNameMd5?>][x]" value="<?=$x?>" type="hidden" />
		<input class="inputY" name="UploadsFormElement[<?=$fileNameMd5?>][y]" value="<?=$y?>" type="hidden" />
		<input class="inputWidth" name="UploadsFormElement[<?=$fileNameMd5?>][width]" value="<?=$width?>" type="hidden" />
		<input class="inputHeight" name="UploadsFormElement[<?=$fileNameMd5?>][height]"  value="<?=$height?>" type="hidden" />

		<!-- Значение -->
        <input name="<?= $attribute ?>[]" value="<?=$newFileName?>" type="hidden" />

        <br/>
        <?

		// кнопка "удалить из списка"
        $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
            array('buttonType'=>'button', 'icon'=>'icon-ban-circle', 'label'=>Yii::t('app', 'Remove from list'),
                'htmlOptions'=>array('class'=>'deleteItem'),
                'size'=>'mini',
            )
        );

		?>&nbsp;<?

		$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'),
			array('buttonType'=>'button', 'icon'=>'icon-edit', 'label'=>Yii::t('app', 'Edit'),
				'htmlOptions'=>array('class'=>'itemOptions'),
				'size'=>'mini',
			)
		);

		?>

    </div>
    
    <div class="clear"></div>

</div>