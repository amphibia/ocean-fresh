// инициализация объекта
$(document).ready(function(){

    // Сам факт использования виджета, к которому прилагается этот скрипт,
    // означает, что прямая отправка содержимого INPUT:FILE вместе с формой
    // недопустима.
    $("form").submit(function(){
        $("input:file.imagesUpload", $(this)).attr("disabled", "disabled");
    });

    // нажатие кнопки "Удалить изображение из списка"
    $(".imagesUploads .deleteItem").live('click', function(){
        $(this).removeInformer();
    });

    // Теперь все объекты-файлы будут отправлять файлы
    // сразу после выбора файлов.
    $("input.imagesUpload").bind("change", function(){$(this).uploadFiles();});

    // при нажатии на "закачать новый" открывается диалог с файлами
    $(".controlElement.imagesUploads .uploadNew").live('click',
        function(){$("input", $(this).parent()).click();}
    );

    $(".imagesUploads .resultsArea").sortable({
        placeholder:"uploadsPlaceholder well",
        cancel:'.uploadInformerFail, .uploadInformerStart, .uploadInformerDeleted, .noScroll'
    });

    $('.imagesUploads .imagesUpload').each(function(){ $(this).update(); });

    // инициализация инструмента обрезки
    $('.imagesUploads .itemOptions').live('click', function(){

        $(this).closest('.uploadInformer').toggleClass('noScroll');

        var $informer = $(this).closest('.uploadInformer'),
            $uploadElement = $(this).closest('.controlElement.imagesUploads'),
            $input = $uploadElement.find('input.imagesUpload');

        if(!$(this).hasClass('active'))
        {
            $(this).addClass('active');
            var $optionsDialog = $informer.find('.imagesUploads_options');
            if($optionsDialog.length) $optionsDialog.slideDown(200);
            else $input.initOptionsForm($informer);

            $('html, body').animate({
                scrollTop: $(this).offset().top - 62
            }, 200);
        }
        else
        {
            $(this).removeClass('active');
            $informer.find('.imagesUploads_options').slideUp(200);

            $('html, body').animate({
                scrollTop: $informer.offset().top - 62
            }, 200);
        }
    });
});