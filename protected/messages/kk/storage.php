<?php

return array(
	'Element deleted.'			=> 'Element deleted.',
	'Cancel'			=> 'Cancel',
	'Upload started'			=> 'Upload started',
	'Upload failed'			=> 'Upload failed',
);