<?php

return array(
	'Width'			=> 'Ширина',
	'Height'			=> 'Высота',
	'Fixed ratio:'			=> 'Фиксированные пропорции:',
	'Original'			=> 'Оригинал',
	'Preview'			=> 'Предпросмотр',
	'Fixed ratio'			=> 'Фиксированные пропорции',
	'Ivalid parametres. You must have correct coords.'			=> 'Неправильные параметры. Вы должны откорректировать указанные координаты.',
);