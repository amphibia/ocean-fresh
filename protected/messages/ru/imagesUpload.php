<?php

return array(
	'select exists image'			=> 'Выбрать из уже закачанных',
	'upload new image'			=> 'Закачать новый файл',
	'Not accepted file host.'			=> 'Not accepted file host.',
	'Not allowed file mime type.'			=> 'Not allowed file mime type.',
	'Image'			=> 'Image',
	'Document'			=> 'Document',
	'MIME type error'			=> 'MIME type error',
);