<?php

return array(

    'urlManager' => array(
        'class' => 'application.extensions.extcore.ExtCoreUrlManager',
        'urlFormat' => 'path',
        'showScriptName' =>  false,
        'rules' => array(
            'services/<serviceID:\w+>/<actionID:\w+><args:.*>' => 'services/enter',
            '<module:\w+>/services/<serviceID:\w+>/<actionID:\w+><args:.*>' => '<module>/services/enter',
            '<module:\w+>/<controller:\w+>/<action:\w+><_actionArguments:.*>' => '<module>/<controller>/<action>',
            '<controller:\w+>/<id:\d+>' => '<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            '<controller:\w+>/<action:\w+><_actionArguments:.*>' => '<controller>/<action>',
        ),
    ),
    'db' => require 'database.php',

    'cache' => array(
        'class' => 'system.caching.CDummyCache',
    ),
    'errorHandler' => array('errorAction' => 'site/error'),

    'clientScript' => array(
        'class' => 'application.extensions.extcore.ExtCoreClientScript',
    ),
    'custom' => array(
        'class' => 'application.components.CustomComponent',
    ),
    'translate' => array(
        'modelClass' =>  NULL,
        'class' => 'application.modules.translate.components.TranslateComponent',
    ),
    'admin' => array(
        'blankComponent' => array(
        ),
        'configFiles' => array(
        ),
        'default' => array(
        ),
        'errors' => array(
        ),
        'aliases' => array(
        ),
        'cacheExpire' => '86400',
        'class' => 'application.modules.admin.components.AdminComponent',
    ),
    'image' => array(
        'driver' => 'GD',
        'params' => array(
        ),
        'class' => 'application.extensions.image.ImageComponent',
    ),
    'lang' => array(
        'useSubDomains' => false,
        'defaultLanguage' => 'ru',
        'class' => 'application.components.LangComponent',
    ),
    'upload' => array(
        'allowedDocumentMime' => array(
        ),
        'allowedImageMime' => array(
        ),
        'maxFileNameLentgh' =>  NULL,
        'views' => array(
        ),
        'formElementAttributes' => array(
        ),
        'controllerID' =>  NULL,
        'storageComponentID' =>  NULL,
        'storage' =>  NULL,
        'fileRenameFunction' =>  NULL,
        'informersViews' => array(
        ),
        'acceptedHosts' => array(
        ),
        'srcFileInfo' =>  NULL,
        'newFileName' =>  NULL,
        'fileDirecory' =>  NULL,
        'filePath' =>  NULL,
        'storageHost' =>  NULL,
        'storageRoot' =>  NULL,
        'mimeErrorMessage' =>  NULL,
        'class' => 'application.extensions.multiuploader.UploadComponent',
    ),
    'mailer' => array(
        'accounts' => array(
        ),
        'class' => 'application.extensions.mailer.MailerComponent',
    ),
    'storage' => array(
        'storageDir' =>  NULL,
        'cacheDir' =>  NULL,
        'subdir' =>  NULL,
        'fileNameRule' =>  NULL,
        'rules' => array(
        ),
        'blankPreset' => array(
        ),
        'watermark' => array(
        ),
        'presets' => array(
        ),
        'urlParseRules' => array(
        ),
        'controllerID' =>  NULL,
        'forcePreset' =>  NULL,
        'hostAliases' => array(
        ),
        'signatureSalt' =>  NULL,
        'attributesParseReg' =>  NULL,
        'userCropOptions' =>  NULL,
        'useForceRedirect' => 'true',
        'class' => 'application.extensions.storage.StorageComponent',
    ),
    'struct' => array(

        'rules' => array(),
        'structControllerID' => 'struct',
        'currentCategory' =>  NULL,
        'currentPage' =>  NULL,
        'cacheExpire' => '172800',
        'structController' =>  NULL,
        'class' => 'application.modules.struct.StructComponent',
    ),
    'kcfinder' => array(
        'imagesDir' =>  NULL,
        'subdir' =>  NULL,
        'controllerID' =>  NULL,
        'controllerClass' =>  NULL,
        'storageComponentID' =>  NULL,
        'jsBrowseFunction' =>  NULL,
        'assets' => array(
        ),
        'class' => 'application.extensions.tinymce.KCFinderComponent',
    ),
    'redactorjsUploadFile' => array(
        'imagesDir' =>  NULL,
        'subdir' =>  NULL,
        'controllerID' =>  NULL,
        'controllerClass' =>  NULL,
        'storageComponentID' =>  NULL,
        'jsBrowseFunction' =>  NULL,
        'assets' => array(
        ),
        'class' => 'application.extensions.redactorjs.RedactorjsUploadFileComponent',
    ),
    'users' => array(
        'modelClass' => NULL,
        'roleModelClass' => NULL,
        'operationModelClass' => NULL,
        'loginModelClass' => NULL,
        'profileModelClass' => NULL,
        'userComponentClass' => NULL,
        'formModelClass' => NULL,
        'registrationModelClass' => NULL,
        'recoveryModelClass' => NULL,
        'changeModelClass' => NULL,
        'userIdentityClass' => NULL,
        'needVerification' => 'true',
        'rbacDefaultOperation' => NULL,
        'loginUrl' => NULL,
        'logoutUrl' => NULL,
        'loginDuration' => NULL,
        'loginRequiredAjaxResponse' => NULL,
        'mailerAccount' => array(),
        'class' => 'application.modules.users.components.UsersComponent',
    ),
    'cart' => array(
        'class' => 'application.components.CartComponent',
    ),

);