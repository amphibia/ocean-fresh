<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array
(
    'basePath'			=> dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'				=> 'CMS',
    'sourceLanguage'	=> 'source',
    'language'			=> 'ru',

	'assets'		=> require "assets.php",
	'preload'		=> require "preload.php",
    'import'		=> require "import.php",
    'modules'		=> require "modules.php",
    'components'	=> require 'components.php',
    'params'		=> require 'params.php',
    'controllerMap'	=> require 'controllerMap.php',

);