<?php
$mimeTypes = require Yii::getPathOfAlias('system.utils.mimeTypes').'.php';

return array_merge($mimeTypes, array(
    'mp4' => 'video/mp4',
    'webm' => 'video/webm',
));