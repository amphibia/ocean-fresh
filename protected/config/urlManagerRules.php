<?php

return array
(
	// обработчик сервисов
	'services/<serviceID:\w+>/<actionID:\w+><args:.*>'=>'services/enter',
	'<module:\w+>/services/<serviceID:\w+>/<actionID:\w+><args:.*>'=>'<module>/services/enter',

	'<controller:\w+>/<id:\d+>'=>'<controller>/view',
	'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
	'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
	'<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
);