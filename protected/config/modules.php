<?php

return array(

	'admin' => array(
		'layout' => 'admin.views.layouts.shell',
		'class' => 'application.modules.admin.AdminModule',
	),
);