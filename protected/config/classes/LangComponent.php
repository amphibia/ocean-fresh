<?php
return array(
    'useSubDomains' => false,
    'defaultLanguage' => 'ru',
    'languages' => array(
        'ru' => array('short' => 'Рус', 'long' => 'Русский'),
        'en' => array('short' => 'Eng', 'long' => 'English'),
        //'kk' => array('short' => 'Каз', 'long' => 'Казахский'),
    )
);