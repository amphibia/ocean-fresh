<?php

return array
(
	'templateRange' => array
	(
		'default' => 'Default content page',
		'mainpage' => 'Main page',
		'about' => 'О компании',
		'catalog' => 'Каталог',
		'cart' => 'Корзина',
		'our_work' => 'Как мы работаем',
		'our_products' => 'Наши продукты',
		'recipes' => 'Рецепты',
		'contacts' => 'Контакты',
		'order' => 'Заказы',
		'delivery' => 'Доставка и оплата',
		'map_oysters' => 'Карта устриц',
		'stock' => 'Акции',
	)
	
);
