<?php
return array
(
	// пресеты изменения файлов
	"presets" => array
	(
		// здесь только стандартные размеры
		// дополнительные размеры указывайте в общем конфиге или динамически -
		// в подключаемых зависимых конпонентов
		'default' => array(),
		'original' => array(),
		'avatarLarge' => array('width' => 42, 'height' => 42),
		'cropPreview' => array('side' => 450),
		'uploadPreview' => array('width' => 96, 'height' => 96),
		'_1to2_small' => array('width' => 96, 'height' => 192),
		'_2to1_small' => array('width' => 192, 'height' => 96),
		'_long_small' => array('width' => 230, 'height' => 96),

        'inContent' => array('side' => 600),
        'thumb' => array('width' => 86, 'height' => 86),

		/*
		 * Sample using watermarks
		 *
		 *'waterMarkSample' => array(
            'side' => 800,
            'watermarkImage' => $_SERVER['DOCUMENT_ROOT'] . '/images/watermark.png',
            'watermarkRight' => 20,
            'watermarkBottom' => 20,
        ),
		 */
	),

	'hostAliases' => array('cms', 'test.oceanfresh.kz', 'oceanfresh.kz'),
);