<?

$secure = FeedbackOptionsForm::instance()->secure;

return array
(
    'accounts' => array
    (
        "common" => array
        (
            'type' => 'smtp',
            'html' => true,
            'view' => '/email/common',
            'bcc' => FeedbackOptionsForm::instance()->bcc,

            'options' => array
            (
                'From' => FeedbackOptionsForm::instance()->from_email,
                'FromName' => Yii::t('emails', 'Система отправки сообщений'),
                'ReturnPath' => FeedbackOptionsForm::instance()->from_email,
                'Subject' => Yii::t('emails', 'Сообщение с сайта'),

                'Host' => FeedbackOptionsForm::instance()->host,
                'Port' => FeedbackOptionsForm::instance()->port,
                'Username' => FeedbackOptionsForm::instance()->username,
                'Password' => FeedbackOptionsForm::instance()->password,

                'SMTPSecure' => ($secure != 'none') ? $secure : NULL,
                'SMTPAuth' => (int)FeedbackOptionsForm::instance()->auth,
                'SMTPDebug' => (int)FeedbackOptionsForm::instance()->debug,

                'CharSet' => 'utf-8'
            )
        ),

        "default" => array
        (
            'type' => 'smtp',
            'html' => true,
            'view' => '/email/default',
            'bcc' => FeedbackOptionsForm::instance()->bcc,

            'options' => array
            (
                'From' => FeedbackOptionsForm::instance()->from_email,
                'FromName' => Yii::t('emails', 'Система отправки сообщений'),
                'ReturnPath' => FeedbackOptionsForm::instance()->from_email,
                'Subject' => Yii::t('emails', 'Сообщение с сайта'),

                'Host' => FeedbackOptionsForm::instance()->host,
                'Port' => FeedbackOptionsForm::instance()->port,
                'Username' => FeedbackOptionsForm::instance()->username,
                'Password' => FeedbackOptionsForm::instance()->password,

                'SMTPSecure' => ($secure != 'none') ? $secure : NULL,
                'SMTPAuth' => (int)FeedbackOptionsForm::instance()->auth,
                'SMTPDebug' => (int)FeedbackOptionsForm::instance()->debug,

                'CharSet' => 'utf-8'
            )
        ),
    )
);
