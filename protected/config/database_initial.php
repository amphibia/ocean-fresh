<?php

return array
(
    'connectionString' => 'mysql:host=localhost;dbname=',
    'emulatePrepare' => true,
    'username' => '',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix' => '',
    'enableParamLogging' => true,
    'enableProfiling' => true,

    'schemaCachingDuration' => (3600 * 240),
);