<?

class CartItemForm extends CFormModel
{
    public $id;
    public $gram_weight;
    public $typeCuttingSelected;
    public $qty;

    public function rules()
    {
        $rules = array(
            array('id', 'required', "message" => Yii::t('custom', 'Поле обязательно для заполнения')),

            array('id, gram_weight, qty', 'numerical', 'min'=>0),
            array
            (
                'typeCuttingSelected', 'length', 'min' => 5, 'max' => 64,
                'tooShort' => Yii::t('custom', 'Слишком короткое имя'),
                'tooLong' => Yii::t('custom', 'Слишком длинное имя'),
            ),
        );

        return $rules;
    }

    public function beforeValidate(){
        $this->id = htmlspecialchars($this->id);
        $this->gram_weight = htmlspecialchars($this->gram_weight);
        $this->qty = htmlspecialchars($this->qty);
        $this->typeCuttingSelected = htmlspecialchars($this->typeCuttingSelected);
        return parent::beforeValidate();
    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels(){
        return array(
            //'name' => Yii::t('custom', 'Ваши имя и фамилия'),
            //'phone' => Yii::t('custom', 'Контакнтый телефон'),
            //'email' => Yii::t('custom', 'E-mail'),
            //'text' => Yii::t('custom', 'Текст сообщения'),
            //'captcha' => Yii::t('custom', 'Секретный код'),
        );
    }

}
