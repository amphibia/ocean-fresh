<?php

class FeedbackOptionsForm extends ExtCoreConfigModel
{
    // данные формы
	public $from_email;

	public $to_email;
	public $bcc;

	public $type;
	public $username;
	public $host;
	public $port;
	public $password;
    public $auth;
    public $secure;
    public $debug;


    public function rules()
    {
        return array
        (
            array('from_email, to_email, type', 'required'),

			array('from_email, to_email', 'email'),
            array('bcc', 'match', 'pattern' => '/^([a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-\.]+\.[a-zA-Z]{2,4}(\s?\,\s?)?)+$/'),
			array('port', 'numerical', 'integerOnly'=>'true', 'min'=>1, 'max'=>'1024'),
			array('type', 'in', 'range'=>array('mail', 'smtp')),
			array('host, username, password', 'safe'),

            array('auth', 'in', 'range'=>array('0', '1')),
            array('secure', 'in', 'range' => array('none', 'ssl', 'tls')),
            array('debug', 'in', 'range' => array('0', '1')),
        );
    }

	public function attributeLabels() {
		return array(
			'from_email' => Yii::t('custom', 'Email in "from"'),

			'to_email' => Yii::t('custom', 'Receiver email'),

			'type' => Yii::t('custom', 'Email type'),
			'username' => Yii::t('custom', 'Username'),
			'host' => Yii::t('custom', 'Host'),
			'password' => Yii::t('custom', 'User password'),
			'bcc' => Yii::t('custom', 'Hidden copies reciever'),
		);
	}

	/**
	 * Для POP3 и SMTP требуются авторизационные данные.
	 * @return boolean
	 */
	protected function beforeValidate()
	{
		$attrubutes = $this->getAttributes();
		if(isset($attrubutes['password']) && !$attrubutes['password'])
			unset($attrubutes['password']);

		$this->setAttributes(
			array_merge(require $this->_configFilename, $attrubutes));

		if($this->type != 'mail')
		{
			if(!$this->host) $this->addError('host', Yii::t('custom', 'Hostname required'));
			if(!$this->port) $this->addError('port', Yii::t('custom', 'Port requiered'));
			if(!$this->username) $this->addError('username', Yii::t('custom', 'Username required'));
			if(!$this->password) $this->addError('password', Yii::t('custom', 'Password required'));
		}

		return parent::beforeValidate();
	}
}



?>
