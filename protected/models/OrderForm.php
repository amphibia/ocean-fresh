<?

class OrderForm extends CFormModel
{
    public $fio;
    public $phone;
    public $email;
    public $delivery_type;
    public $delivery_time;
    public $pre_order_qty; //Для предзаказа
    public $cart;
    public $type_payment;
    public $delivery_zone;
    public $delivery_address;
     //public $captcha;

    public function rules()
    {
        $rules = array(
            array('fio, phone, delivery_type, cart, type_payment', 'required', "message" => Yii::t('custom', 'Поле обязательно для заполнения')),

            array
            (
                'fio, delivery_time, pre_order_qty, type_payment, delivery_zone', 'length', 'min' => 1, 'max' => 64,
                'tooShort' => Yii::t('custom', 'Слишком короткое имя'),
                'tooLong' => Yii::t('custom', 'Слишком длинное имя'),
            ),
            array
            (
                'delivery_address', 'length', 'min' => 1, 'max' => 500,
                'tooShort' => Yii::t('custom', 'Слишком короткое имя'),
                'tooLong' => Yii::t('custom', 'Слишком длинное имя'),
            ),
            //array('phone', 'match', 'pattern' => '/^\+\d\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/'),
            array('email', 'email', 'message' => Yii::t('custom', 'Неправильный электронный адрес')),
            array('delivery_type', 'numerical', 'integerOnly'=>true, 'min'=>-1, 'max'=>1),
            array('cart', 'length', 'message' => Yii::t('custom', 'Неправильный электронный адрес')),
        );

        return $rules;
    }

    public function beforeValidate(){
        $this->fio = htmlspecialchars($this->fio);
        $this->phone = htmlspecialchars($this->phone);
        $this->email = htmlspecialchars($this->email);
        $this->delivery_type = htmlspecialchars($this->delivery_type);
        $this->delivery_time = htmlspecialchars($this->delivery_time);
        $this->delivery_zone = htmlspecialchars($this->delivery_zone);
        $this->delivery_address = htmlspecialchars($this->delivery_address);
        //$this->cart = htmlspecialchars($this->cart);
        return parent::beforeValidate();
    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels(){
        return array(
            //'name' => Yii::t('custom', 'Ваши имя и фамилия'),
            //'phone' => Yii::t('custom', 'Контакнтый телефон'),
            //'email' => Yii::t('custom', 'E-mail'),
            //'text' => Yii::t('custom', 'Текст сообщения'),
            //'captcha' => Yii::t('custom', 'Секретный код'),
        );
    }

}
