<?php
/**
 * @title Translation management
 * @description { Component for translate messages }
 * @preload yes
 * @vendor Slaik
 * Date: 26.03.13
 * Time: 16:12
 */

class TranslateComponent extends CApplicationComponent
{
    /**
     * Модель пользователя, с которой работает компонент.
     * @var string
     */
    public $modelClass = NULL;

    public function init()
    {
        Yii::loadExtConfig($this, __FILE__);

        Yii::import('application.modules.translate.components.*');
        Yii::import('application.modules.translate.services.*');
        Yii::import('application.modules.translate.models.*');

        // сервис управления пользователями
        Yii::app()->registerService('translate', 'TranslateAdminService', 'admin',
            array('title' => 'Translation management', 'menuGroup' => 'options',
                'dictionary' => array('actions' => array('index' => 'Translation management'))
            ));
    }
}