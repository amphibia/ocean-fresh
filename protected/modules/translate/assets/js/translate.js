$(".translateElement button.delete").live('click', function () {
    var element = $(this).closest('.translateElement');

    var rowDeleted = $(this).closest('form')
        .find('.rowDeleted.to_clone').clone();
    var elementClone = element.clone();

    rowDeleted.removeClass('to_clone').
        find('.cancel').click(function () {
            elementClone.find('.tooltip').remove();
            rowDeleted.replaceWith(elementClone);
            $('[rel="tooltip"]').tooltip({trigger: 'hover'});
        });

    element.replaceWith(rowDeleted.show());

    return false;
});