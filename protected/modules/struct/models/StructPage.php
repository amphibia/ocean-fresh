<?php

/**
 * @author koshevy
 *
 * Модель - материал сайта, привязанный к его разделам.
 * Имеет связи MANY_TO_MANY с разделами сайта (StructCategory).
 *
 * Также, для специфических задач (например, для формирования with-запросов),
 * реализована промежуточная модель StructBind, имеющая связь HAS_MANY
 * c StructPage и StructCategory.
 *
 * Для конфигурирования материала используется модель StructCategoryOptions.
 * Привязка к ней осуществляется через категорию, к которой привязывается
 * материал (главная категория - идущая как самая первая).
 */
class StructPage extends ActiveRecord
{
    public $first_category = NULL;


    /**
     * @var array Варианты статуса опубликованности страницы.
     */
    public $publishedRange = array();

    /**
     * Настройки валидатора CHtmlPurifier.
     * @var array
     */
    public $purifierOptions = array();

    /**
     * Авто-формирование алиаса, если он не заполнен.
     * @var boolean
     */
    public $autoAlias = NULL;

    /**
     * Максимальная длина предисловия к материалу.
     * @var integer
     */
    public $forewordLength = NULL;

    /**
     * Пометка, что модель работает в безопасном режиме (внутреннее использование),
     * при котором отключаются некоторые фильтры.
     * @var boolean
     */
    public $safeMode = NULL;


    /** ** ** ** ** ** **
     * Методы - утилиты */

    /**
     * Получение списка кастумных полей модели. Обращается к модели настроек.
     * Возвращает массив c ["name" => "label"]
     * @return array
     */
    public function getCustomAttributes()
    {
        if (($options = $this->getOptionsModel()))
            return $options->getCustomAttributes();
        else return array();
    }

    /**
     * Обращение к модели настроек (для материалов).
     * @return StructCategoryOptions
     */
    public function getOptionsModel()
    {
        static $_categoryCahces = array();
        $firstCategoryIndex = $this->getFirstCategoryIndex();

        if (!isset($_categoryCahces[$firstCategoryIndex])) {
            if ($category = $this->getCategory())
                $_categoryCahces[$firstCategoryIndex] = $category->getPageOptions();
            else $_categoryCahces[$firstCategoryIndex] = NULL;
        }

        return $_categoryCahces[$firstCategoryIndex];
    }

    /**
     * Проверка существования расширенного поля, указанного через
     * StructPageOptions.
     *
     * @param string $fieldName
     * @return boolean
     */
    public function issetCustomField($fieldName)
    {
        $pageOptions = $this->getOptionsModel();
        if (!$pageOptions) return false;
        $customAttributes = $pageOptions->getCustomAttributes();
        return isset($customAttributes[$fieldName]);
    }

    public function createUrl()
    {
        if ($this->isNewRecord) throw new CException(
            Yii::t('struct', 'Cant create url of new page'));

        if (!$category = $this->getCategory()) return NULL;
        else return $category->createUrl() . "/{$this->alias}";
    }

    public function getFirstCategoryIndex()
    {
        if (!$this->first_category_index) {
            if (($categories = $this->getRelated('categories')) && sizeof($categories)) {
                $category = $categories[0];
                if (!is_object($category))
                    $this->first_category_index = $category;
                else $this->first_category_index = $category->id;
            } else {
                if ($this->isNewRecord)
                    $this->first_category_index = $this->first_category = NULL;
                else {
                    if (!$this->first_category) {
                        $this->first_category = StructCategory::model()
                            ->containsPages(array(100), $compareType = 'OR')
                            ->published()
                            ->find();
                    }

                    $this->first_category_index = $this->first_category
                        ? $this->first_category->id : NULL;
                }
            }

            if (!$this->isNewRecord) {
                // сохранение индекса - главной категории
                Yii::app()->db->createCommand(
                    "UPDATE " . $this->tableName() . " " .
                    "SET `first_category_index`='{$this->first_category_index}' " .
                    "WHERE id='{$this->id}'")
                    ->execute();
            }
        }

        return $this->first_category_index;
    }

    /**
     * Обращение к основной (первой) категории этого материала.
     */
    public function getCategory()
    {
        if (($categories = $this->getRelated('categories')) && sizeof($categories)) {
            $category = $categories[0];
            if (!is_object($category))
                $category = StructCategory::cachedInstance($category);
        } else {
            if ($this->isNewRecord) $category = NULL;
            else {
                // при обновлении ключа, обновится и информация о первой категории
                $this->first_category_index = $this->getFirstCategoryIndex();
                return $this->first_category;
            }
        }

        return $category;
    }


    /** ** ** ** ** ** ** ** ** ** **
     * Внутренние технические методы */

    public function init()
    {
        parent::init();
        Yii::loadExtConfig($this, __FILE__);
    }

    public function behaviors()
    {
        return array
        (
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_created',
                'updateAttribute' => 'date_changed',
            ),

            // Работа с POST-ом для MANY_TO_MANY.
            'CAdvancedArBehavior' => array(
                'class' => Yii::localExtension('AdvancedArBehavior', 'CAdvancedArBehavior')),
        );
    }

    public function relations()
    {
        $relations = array
        (
            // категории

            // все привязанные категории
            'categories' => array(self::MANY_MANY,
                'StructCategory',
                StructBind::model()->tableName() . '(page_id, category_id)',

                // эти параметры указываются, чтобы сортировка происходила
                // в порядке указывания элементов
                'together' => true, 'order' => 'categories_categories.id ASC'
            ),

            // только опубликованные привязанные категории
            'publishedCategories' => array(self::MANY_MANY,
                'StructCategory',
                StructBind::model()->tableName() . '(page_id, category_id)',

                // эти параметры указываются, чтобы сортировка происходила
                // в порядке указывания элементов
                'together' => true, 'order' => 'publishedCategories.id ASC',
                'condition' => "(publishedCategories.published = 'published')"
            ),

            // модели промежуточных связей с категориями
            'binds' => array(self::HAS_MANY, 'StructBind', 'page_id', 'joinType' => 'LEFT JOIN'),

            // связь с категориями через промежуточную связь
            'categoriesBinds' => array(self::HAS_MANY, 'StructCategory', array('category_id' => 'id'), 'through' => 'binds', 'joinType' => 'LEFT JOIN'),


            // тэги

            'tags' => array(self::MANY_MANY,
                'StructTag',
                StructTagPage::model()->tableName() . '(page_id, tag_id)',

                // эти параметры указываются, чтобы сортировка происходила
                // в порядке указывания элементов
                'together' => true, 'order' => 'tags_tags.id ASC'
            ),

            // с использованием моделей промежуотчных связей
            'tagsBinds' => array(self::HAS_MANY, 'StructTagPage', 'page_id', 'joinType' => 'LEFT JOIN'),
            'throughTagsBinds' => array(self::HAS_MANY, 'StructPage', array('category_id' => 'id'), 'through' => 'tagsBinds', 'joinType' => 'LEFT JOIN'),

        );

        // автор
        if (Yii::app()->hasComponent('users')) {
            $relations['user'] = array(
                self::BELONGS_TO,
                Yii::app()->users->modelClass,
                'user_id'
            );
        }

        return $relations;
    }

    public function tableName()
    {
        return self::commonTableName('struct_page');
    }


    /** ** ** ** ** ** ** **
     * Методы отсортировки */

    /**
     * Отсортировка материалов по категориям, к которым они принадлежит.
     * В зависимости от параметра $compareType ищет материалы, имеющие всех
     * перечисленных предков, либо одного из них.
     *
     * @param array $categories перечисление ID категорий
     * @param string $compareType тип формирования условия (AND или OR)
     */
    public function categories(Array $categories, $compareType = 'OR')
    {
        $this->dbCriteria->mergeWith(array(
            'with' => 'categoriesBinds',
            'together' => true,
        ));

        if ($compareType == 'OR') {
            $this->dbCriteria->group = 't.id';
            $this->dbCriteria->having = 'COUNT(binds.page_id) < 2';
            $this->dbCriteria->addInCondition('category_id', $categories);
        } else if ($compareType == 'AND') {
            $bindsValues = array();
            $binds = StructBind::model();
            $binds->dbCriteria->addInCondition('category_id', $categories);
            $binds->dbCriteria->group = 'page_id';
            $binds->dbCriteria->having = new CDbExpression('COUNT(category_id)=' . sizeof($categories));
            $binds->dbCriteria->select = 'id';
            foreach ($binds->findAll() as $row) $bindsValues[] = $row->id;

            $this->dbCriteria->addInCondition('binds.id', $bindsValues);
        }

        return $this;
    }

    /**
     * Отсортировка материалов по под-категориям у заданной категории.
     * Обертка для функции categories() для вывода материалов у вложенных разделов.
     * В зависимости от параметра $compareType ищет материалы, имеющие всех
     * перечисленных предков, либо одного из них.
     *
     * @param integer $category ID категорий
     * @param string $compareType тип формирования условия (AND или OR)
     * @return StructPage
     */
    public function categoriesTree($category, $compareType = 'OR')
    {
        $categoriesArr = array();
        $category = StructCategory::model()->findByPk($category);
        foreach ($category->children() as $categoryChild) {
            $categoriesArr[] = (int)$categoryChild->id;
        }
        return $this->categories($categoriesArr, $compareType);
    }

    /**
     * Отсортировка материалов, непривязанных ни к одной из категорий.
     */
    public function uncategorized()
    {
        $this->dbCriteria->mergeWith(array(
            'with' => 'categoriesBinds',
            'together' => true,
            'group' => 't.id',
            'condition' => 'category_id IS NULL',
        ));

        return $this;
    }

    /**
     * @param array $tags список ID с тэгами
     * @param string $compareType тип формирования условия (AND или OR)
     */
    public function tags(Array $tags, $compareType = 'OR')
    {
        $this->dbCriteria->mergeWith(array(
            'with' => 'tagsBinds',
            'together' => true,
        ));

        if ($compareType == 'OR')
            $this->dbCriteria->addInCondition('tag_id', $tags);

        else if ($compareType == 'AND') {
            $bindsValues = array();
            $binds = StructTagPage::model();
            $binds->dbCriteria->addInCondition('tag_id', $tags);
            $binds->dbCriteria->group = 'page_id';
            $binds->dbCriteria->having = new CDbExpression('COUNT(tag_id)=' . sizeof($tags));
            $binds->dbCriteria->select = 'id';
            foreach ($binds->findAll() as $row) $bindsValues[] = $row->id;

            $this->dbCriteria->addInCondition('tagsBinds.id', $bindsValues);
        }

        return $this;
    }

    public function scopes()
    {
        return array
        (
            'draft' => array('condition' => "t.published='draft'"),
            'published' => array('condition' => "t.published='published'"),
            'hidden' => array('condition' => "t.published='hidden'"),
            'manualPosition' => array('order' => "t.position"),
        );
    }


    /** ** ** ** ** ** ** ** ** ** **
     * Методы и настройки валидации */

    public function rules()
    {
        $rules = array
        (
            array('title, published' . ($this->autoAlias ? NULL : ', alias'), 'required'),
            array('position, date_created, date_changed', 'numerical', 'integerOnly' => true),

            array('alias', 'uniqueInCategories'),
            array('categories', Yii::localExtension('mtmselect', 'ManyToManyValidator'), 'max' => 16),
            array('tags', Yii::localExtension('mtmselect', 'ManyToManyValidator'), 'max' => 16),

            array('title', 'length', 'max' => 255),
            array('alias', 'length', 'max' => 64),
            array('in_search', 'boolean'),

            array('published', 'in', 'range' => array_keys($this->publishedRange)),

            array('alias', 'match', 'pattern' => '/^\w+$/'),
        );

        if (!$this->safeMode) {
            $purifier = new CHtmlPurifier();
            $purifier->options = $this->purifierOptions;

            $rules[] = array('title, short_text, full_text', 'filter', 'filter' => array($purifier, 'purify'));
        } else
            $rules[] = array('title, short_text, full_text', 'safe');

        // применение валидаторов для расширенных полей
        if ($optionsModel = $this->getOptionsModel())
            $optionsModel->applyRules($rules);

        return $rules;
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $tableName = $this->tableName();
            $lastPositionItem = $this->getDbConnection()
                ->createCommand("SELECT MAX(position) AS max_position FROM $tableName")
                ->queryRow();

            $this->position = (is_array($lastPositionItem) && isset($lastPositionItem['max_position']))
                ? (int)$lastPositionItem['max_position'] + 1 : 1;
        }

        // автор
        if (Yii::app()->hasComponent('users')) {
            $user_id = (isset(Yii::app()->user->model) && Yii::app()->user->model)
                ? Yii::app()->user->model->id : NULL;
            if ($this->isNewRecord) $this->user_id = $user_id;
            $this->last_change_user_id = $user_id;
        }

        if ($this->autoAlias && !$this->alias)
            $this->alias = mb_substr(ExtCoreTextHelper::transliterate_cyr($this->title), 0, 32);

        // индексируется список тегов по категориям, в зависимости от привязанных
        // материалов
        if ($this->tags && sizeof($this->tags)) {
            $tags = array();
            foreach ($this->tags as $tag) {
                $tags[] = ($tag instanceof StructTag)
                    ? $tag->id : $tag;
            }

            // список тегов для этой категории
            foreach ($this->categories as $category) {
                if (!is_object($category))
                    $category = StructCategory::cachedInstance($category);

                $categoryTags = array();
                foreach ($category->tags as $categoryTag) {
                    $categoryTags[] = ($categoryTag instanceof StructTag)
                        ? $categoryTag->id : $categoryTag;
                }

                $category->tags = array_unique(array_merge($categoryTags, $tags));
                $category->writeManyManyTables(); // сохранение индексов
            }
        }

        return parent::beforeValidate();
    }

    /**
     * Удаление связей вслед за объектом.
     */
    public function afterDelete()
    {
        parent::afterDelete();
        StructBind::model()->deleteAll(
            "page_id=:page_id", array(':page_id' => $this->id));
        StructTagPage::model()->deleteAll(
            "page_id=:page_id", array(':page_id' => $this->id));
    }

    /**
     * Валидатор для страницы, который убеждается, что в указанной категории
     * нет других страниц с таким 'alias'.
     * @param string $attribute
     * @param array $params
     */
    public function uniqueInCategories($attribute, $params)
    {
        $categories = array();
        if (!$this->categories || !is_array($this->categories)) return;

        foreach ($this->categories as $category)
            $categories[] = is_object($category) ? $category->id : $category;

        $adjasentPages = StructPage::model()->categories($categories);
        if (!$this->isNewRecord) $adjasentPages->dbCriteria->addCondition("t.id <> {$this->id}");
        if ($adjasentPages->exists('t.alias=:alias', array(':alias' => $this->alias)))
            $this->addError('alias', Yii::t('struct', 'Alias must be unique for pages in used categories!'));
    }


    ///////////////////////////////
    // МОДИФИКАЦИЯ ЯДРА КОМПОНЕНТА

    // десериализация сложных данных
    public function save($runValidation = true, $attributes = null)
    {
        $result = parent::save($runValidation, $attributes);
        if ($this->data && !is_array($this->data)) $this->data = unserialize($this->data);
        if ($this->data && !is_array($this->data)) $this->data = unserialize($this->array());

        /** @todo проверить как сбрасывается индекс */
        // сброс индекса - главной категории
        $this->first_category_index = NULL;
        $this->getFirstCategoryIndex();

        return $result;
    }

    // сериализация сложных данных
    public function beforeSave()
    {
        $result = parent::beforeSave();
        if (is_array($this->data)) $this->data = serialize($this->data);
        Yii::app()->struct->flushCache();
        return $result;
    }

    public function afterFind()
    {
        if ($this->data && !is_array($this->data)) $this->data = unserialize($this->data);
        if ($this->data && !is_array($this->data)) $this->data = array();
        return parent::afterFind();
    }


    // переопределение работы со свойствами таким образом, чтобы можно было обращаться
    // к данным из DATA как к свойствам

    public function __get($name)
    {
        // если не сделать этого, произойдет ситуация, когда обращение
        // к $this->getCustomAttributes() произойдет раньше, чем произошли
        // первые обращения к реальным полям
        if ($this->hasAttribute($name))
            return parent::__get($name);

        if (!parent::__isset($name)) {
            $customAttributes = $this->getCustomAttributes();
            if (isset($customAttributes[$name])) {
                return isset($this->data[$name])
                    ? $this->data[$name] : NULL;
            }
        }

        return parent::__get($name);
    }

    public function getAttribute($name)
    {
        // если не сделать этого, произойдет ситуация, когда обращение
        // к $this->getCustomAttributes() произойдет раньше, чем произошли
        // первые обращения к реальным полям
        if ($this->hasAttribute($name)) return parent::getAttribute($name);

        if (!is_null($value = parent::getAttribute($name)))
            return $value;
        else {
            $customAttributes = $this->getCustomAttributes();
            if (isset($customAttributes[$name])) {
                return isset($this->data[$name])
                    ? $this->data[$name] : NULL;
            }
        }

        return $value;
    }

    public function __set($name, $value)
    {
        // если не сделать этого, произойдет ситуация, когда обращение
        // к $this->getCustomAttributes() произойдет раньше, чем произошли
        // первые обращения к реальным полям
        if ($this->hasAttribute($name)) return parent::__set($name, $value);

        if (!parent::__isset($name)) {
            $customAttributes = $this->getCustomAttributes();
            if (isset($customAttributes[$name])) {
                $values = $this->data;
                $values[$name] = $value;
                $this->data = $values;
                return;
            }
        }

        return parent::__set($name, $value);
    }

    public function getAttributes($names = true)
    {
        $customAttributes = array();
        foreach ($this->getCustomAttributes() as $name => $label) {
            if (is_array($names) && !in_array($name, $names))
                continue;

            $customAttributes[$name] = isset($this->data[$name])
                ? $this->data[$name] : NULL;
        }

        return array_merge($customAttributes, parent::getAttributes($names));
    }

    public function attributeNames()
    {
        // определить настройки можно только у материалов с ID
        if ($this->getIsNewRecord() || !$this->id)
            return array_keys($this->getMetaData()->columns);

        return array_merge(
            array_keys($this->getMetaData()->columns),
            array_keys($this->getCustomAttributes())
        );
    }

    public function attributeLabels()
    {
        // определить настройки можно только у материалов с ID
        if (!$this->id && !$this->categories)
            return parent::attributeLabels();

        return array_merge(
            parent::attributeLabels(),
            $this->getCustomAttributes()
        );
    }

}