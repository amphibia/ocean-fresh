<?php

/**
 * @author koshevy
 *
 * Модель - категория страниц (материалов) сайта, другими словами, раздел сайта.
 * Использует паттерн Nested Sets, наследуя класс {@see ExtCoreNestedAR}, который,
 * в свою очередь, является переделкой бихевера "nestedsetbehavior" в модель
 *
 * Подробная информация по оригиналу класса "nestedsetbehavior"
 * http://www.yiiframework.com/extension/nestedsetbehavior/
 *
 *
 * Имеет следующие отношения:
 *
 *    "parent" - родительская категория (для вложенных категорий);
 *
 *  "pages" - MANY TO MANY отношение с материалами этой категории (для упрощенной
 *    схемы назначения материалов);
 *
 *  "binds" - HAS MANY отношение с промежуточными записями  между разделами и
 *  их материалами (часть альтернативной связки с метериалами, используя двсторонние
 *  отношения HAS MANY);
 *
 *  "pagesBinds" - HAS MANY отношение, через промежуточные "binds". Аналогично
 *  отношению "pages", но может использоваться при выборке с использованием "this".
 *
 *  "selfOptions", "childrenOptions", "pagesOptions" - HAS MANY связи с настройками
 *  для конкретного раздела, касающихся, соответственно, собственных настроек,
 *  настроек дочерних разделов, настройки для материалов, принадлежащих разделу.
 *
 */
class StructCategory extends ExtCoreNestedAR
{
    /**
     * Уровень, на который/с которого могут перемещать/добавлять категории
     * только некоторые пользователи (основа сайта).
     * @var integer
     */
    public $lockedLevel = NULL;


    /**
     * Диапазон значений для типа категории.
     *
     * Категория может быть привязана к стандартному функционалу отображения
     * страниц, к контроллеру, модулю и т.п.
     *
     * Набор маркеров может отличаться в зависимости от контекста приложения,
     * и изменяется в конфиге.
     * @var array
     */
    public $typeRange = array();

    /**
     * Шаблоны раздела
     * Вид отображения содержимого раздела и его материалов
     * @var array
     */
    public $templateRange = array();


    /**
     * Диапазон значений для способа интеграции с лэйоутом.
     *
     * Способ интеграции с лэйаутом определяет как будет производиться вывод
     * модуля/контроллера, прикремлённого к категории:
     *  - полная передача управления (свой лэйаут);
     *  - частичная передача (вывод в лэйоуте модуля 'struct');
     *  - указать лэйаут (можно указать собственный лэйоут из папки protected/views/layouts).
     *
     * @var array
     */
    public $layoutIntegrationRange = array();

    /**
     * Диапазон значений для статуса категории.
     *
     * Свойство определяет отображается ли категория в меню, доступна ли при
     * выводе и т.д.
     * @var array
     */
    public $publishedRange = array();

    /**
     * @var boolean Авто-формирование алиаса, если он не заполнен.
     */
    public $autoAlias = NULL;

    /**
     * Конфигурация для CPurifier - фильтра.
     * @var array
     */
    public $purifierOptions = array();

    /**
     * Пометка, что модель работает в безопасном режиме (внутреннее использование),
     * при котором отключаются некоторые фильтры.
     * @var boolean
     */
    public $safeMode = NULL;

    /**
     * Ссылка на категорию - текущего или назначаемого родителя (объект).
     * Указываеся в beforeValidate. Используется в проверках параметра.
     * @var StructCategory
     */
    protected $_parent = NULL;

    /**
     * Флаг, сообщающий модели о необходимости произвести применение настроек из PHPDOC.
     * Не записывается в таблицу.
     * @var boolean
     */
    public $parse_phpdoc = NULL;

    /**
     * Кэш для цепочек алиасов предков (процедура их формирования достаточно
     * затратна).
     * @var array
     */
    protected static $_ancestorsCache = array();



    /** ** ** ** ** ** **
     * Методы - утилиты */

    /**
     * Получение списка кастумных полей модели. Обращается к модели настроек.
     * Возвращает массив c ["name" => "label"]
     * @return array
     */
    public function getCustomAttributes()
    {
        if (($options = $this->getOptionsModel()))
            return $options->getCustomAttributes();
        else return array();
    }


    protected $_optionsModel = false;

    /**
     * Обращение к модели настроек (для категорий).
     * Если у каотегории нет своих настроек, берутся вложенные настройки
     * предка (настройки, предназначенные для потомков).
     *
     * @return StructCategoryOptions
     */
    public function getOptionsModel()
    {
        if ($this->_optionsModel !== false)
            return $this->_optionsModel;

        $model = NULL;

        if ($selfOptions = $this->getRelated('selfOptions'))
            $model = $selfOptions;
        else {
            // для новых записей сразу применяются настройки указанных предков
            if ($this->getIsNewRecord()) {
                if ($this->parent_id) $parent = self::model()->findByPk($this->parent_id);
                else $parent = NULL;
            } else $parent = $this->getRelated('parent');

            if ($parent && ($parent instanceof StructCategory))
                $model = $parent->getInheritOptions();
        }

        return $this->_optionsModel = $model;
    }

    public function resetOptionsModel(){
        $this->_optionsModel = false;
    }

    /**
     * Обращение к вложенным настройкам предка.
     * Проходит рекурсивно - снизу верх, до тех пор, пока не найдет настройки
     * с пометкой "children" или не достигнет корня.
     *
     * @return StructCategoryOptions
     */
    public function getInheritOptions()
    {
        if ($childrenOptions = $this->getRelated('childrenOptions')) $model = $childrenOptions;
        else if ($parent = $this->getRelated('parent')) $model = $parent->getInheritOptions();
        else $model = NULL;

        return $model;
    }

    /**
     * Рекурсивный проход в поиске  настроек для материалов категории.
     * Настройки распространяются, в том числе, и на вложенные разделы, до тех пор,
     * пока не будут перезаписаны в потомках.
     *
     * @return StructCategoryOptions
     */
    public function getPageOptions()
    {
        if ($this->pagesOptions) $model = $this->pagesOptions;
        else if ($this->parent) $model = $this->parent->getPageOptions();
        else $model = NULL;

        return $model;
    }

    /**
     * Проверка существования расширенного поля, указанного через
     * StructCategoryOptions.
     *
     * @param string $fieldName
     * @return boolean
     */
    public function issetCustomField($fieldName)
    {
        $pageOptions = $this->getOptionsModel();
        if (!$pageOptions) return false;
        $customAttributes = $pageOptions->getCustomAttributes();
        return isset($customAttributes[$fieldName]);
    }

    /**
     * Список предков в порядке иерархии (от корня к источнику)
     * Результат кешируется, чтобы не запрашивать всю цепочку каждый раз (с учетом аргументов).
     * @return array
     */
    public function getAncestors($withRoot = false, $withFirstNode = false, $withGroups = false)
    {
        if ($this->isNewRecord) return NULL;

        // подгрузка из кэша
        $cacheIndex = $this->parent_id . $withRoot . $withFirstNode . $withGroups;
        if (!isset(self::$_ancestorsCache[$cacheIndex])) {
            $ancestors = $this->ancestors();
            if (!$withFirstNode) $ancestors->dbCriteria->addCondition("t.{$this->levelAttribute}>2");
            else if (!$withRoot) $ancestors->dbCriteria->addCondition("t.{$this->levelAttribute}>1");
            self::$_ancestorsCache[$cacheIndex] = $ancestors->findAll();
        }

        return self::$_ancestorsCache[$cacheIndex];
    }

    /**
     * Список alias`ов предков в порядке
     * Результат кешируется, чтобы не запрашивать всю цепочку каждый раз (с учетом аргументов).
     * @return array
     */
    public function getAncestorsAliases($withRoot = false, $withFirstNode = false, $withGroups = false)
    {
        $ancestors = $this->getAncestors($withRoot, $withFirstNode, $withGroups);
        $result = array();

        foreach ($ancestors as $ancestor)
            if ($withGroups || ($ancestor->type != 'group'))
                $result[] = $ancestor->alias;

        $level = $this->{$this->levelAttribute};
        if (($withRoot || ($withFirstNode && ($level > 1)) || ($level > 2))
            && ($this->type != 'group')
        )
            $result[] = $this->alias;

        // запись в кэш
        return $result;

    }

    /**
     * Получение URL раздела. Использует {@see getAncestorsAliases}.
     * @return string
     * @throws CException
     */
    public function createUrl()
    {
        if ($this->isNewRecord) throw new CException(
            Yii::t('struct', 'Cant create url of new category'));

        $aliases = $this->getAncestorsAliases();
        if (!$aliases) return '/';
        return '/' . implode('/', $aliases ? $aliases : NULL);
    }


    /** ** ** ** ** ** ** ** ** ** **
     * Внутренние технические методы */

    public function behaviors()
    {
        return array
        (
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_created',
                'updateAttribute' => 'date_changed',
            ),

            // Работа с POST-ом для MANY_TO_MANY.
            'CAdvancedArBehavior' => array(
                'class' => Yii::localExtension('AdvancedArBehavior', 'CAdvancedArBehavior')),
        );
    }

    public function init()
    {
        parent::init();
        Yii::loadExtConfig($this, __FILE__);
    }

    public function __destruct()
    {
        $depency = Yii::app()->struct->getCategoriesCacheDepency();

        parent::__destruct();
    }

    public function tableName()
    {
        return self::commonTableName('struct_category');
    }

    public function relations()
    {
        return array
        (
            // материалы

            'parent' => array(self::BELONGS_TO, 'StructCategory', 'parent_id'),
            'children' => array(self::HAS_MANY, 'StructCategory', 'parent_id',
                'order' => "{$this->leftAttribute}",
                'condition' => "type<>'group' AND published='published'"
            ),

            // выбор всех потомков, включая группы и неопубликованные
            'childrenTech' => array(self::HAS_MANY, 'StructCategory', 'parent_id',
                'order' => "{$this->leftAttribute}",
            ),

            'pages' => array
            (
                self::MANY_MANY, 'StructPage', StructBind::model()->tableName() . '(category_id, page_id)',
                'order' => "position",
                'condition' => "published='published'"
            ),

            // модели промежуточных связей
            'binds' => array(self::HAS_MANY, 'StructBind', 'category_id', 'joinType' => 'LEFT JOIN'),

            // связь с материалами через промежуточную связь
            'pagesBinds' => array(self::HAS_MANY, 'StructPage', array('category_id' => 'id'), 'through' => 'binds', 'joinType' => 'LEFT JOIN'),

            // Связи с записями настроек для этого раздела, соответственно:
            // собственные настройки, настройки для разделов-потомков, настройки
            // для привязанных к категории материалов.
            'selfOptions' => array(self::HAS_ONE, 'StructCategoryOptions', 'category_id', 'condition' => "((rel='self') AND enabled)"),
            'childrenOptions' => array(self::HAS_ONE, 'StructCategoryOptions', 'category_id', 'condition' => "((rel='children') AND enabled)"),
            'pagesOptions' => array(self::HAS_ONE, 'StructCategoryOptions', 'category_id', 'condition' => "((rel='pages') AND enabled)"),

            // Связи с записями настроек, вне зависимости от того, включены они или нет.
            'selfOptionsTech' => array(self::HAS_ONE, 'StructCategoryOptions', 'category_id', 'condition' => "rel='self'"),
            'childrenOptionsTech' => array(self::HAS_ONE, 'StructCategoryOptions', 'category_id', 'condition' => "rel='children'"),
            'pagesOptionsTech' => array(self::HAS_ONE, 'StructCategoryOptions', 'category_id', 'condition' => "rel='pages'"),

            // тэги (индексирование данных от материалов)
            'tags' => array(self::MANY_MANY,
                'StructTag',
                StructTagCategory::model()->tableName() . '(category_id, tag_id)',

                // эти параметры указываются, чтобы сортировка происходила
                // в порядке указывания элементов
                'together' => true, 'order' => 'tags_tags.id ASC'
            ),

            // с использованием моделей промежуотчных связей
            'tagsBinds' => array(self::HAS_MANY, 'StructTagCategory', 'tag_id', 'joinType' => 'LEFT JOIN'),
            'throughTagsBinds' => array(self::HAS_MANY, 'StructTag', array('category_id' => 'id'), 'through' => 'tagsBinds', 'joinType' => 'LEFT JOIN'),

        );
    }


    /** ** ** ** ** ** ** **
     * Методы отсортировки */

    /**
     * @param alias string
     * @return Category категория с указанным alias, либо NULL
     */
    public function alias($alias)
    {
        if (!preg_match("/^[a-zA-Z0-9_]{1,32}$/", $alias))
            // Для облегчения использования в конструкциях типа
            // $model->alias('blablabla')->exists().
            // Заведомо ложное условие
            $criteries = array('condition' => "1 = 0");

        else $criteries = array(
            'condition' => "alias=:alias",
            'params' => array("alias" => $alias),
        );

        $this->getDbCriteria()->mergeWith($criteries);

        return $this;
    }

    /**
     * Отсортировка категорий, содержащих указанные материалы.
     *
     * @param array $pages перечисление ID страниц
     * @param string $compareType определяет, должны ли категории содержать все
     * указанные материалы (AND) или один из них (OR)
     */
    public function containsPages(Array $pages, $compareType = 'OR')
    {
        $this->dbCriteria->mergeWith(array(
            'with' => 'pagesBinds',
            'together' => true,
        ));

        if ($compareType == 'OR')
            $this->dbCriteria->addInCondition('page_id', $pages);

        else if ($compareType == 'AND') {
            foreach ($pages as $page_id)
                $this->dbCriteria->addColumnCondition(compact(category_id), 'AND');
        }

        return $this;
    }

    public function scopes()
    {
        return array(
            'published' => array(
                'condition' => '(t.published =:published)',
                'params' => array(
                    ':published' => 'published'
                )
            ),
        );
    }

    /** ** ** ** ** ** ** ** ** ** **
     * Методы и настройки валидации */

    public function rules()
    {
        $rules = array
        (
            array('parent_id', 'numerical'),

            array('parent', 'checkNestedParent'),
            array('title, published' . ($this->autoAlias ? NULL : ', alias'), 'required'),
            array('across', 'boolean'),
            array('in_search', 'boolean'),

            array('alias', 'uniqueInParent'),

            array('title', 'length', 'max' => 255),
            array('alias', 'length', 'max' => 64),

            array('external_reference, parse_phpdoc', 'safe'),

            array('alias', 'match', 'pattern' => '/^[a-zA-Z_][a-zA-Z0-9_]{1,64}$/', 'message' => Yii::t('struct', 'Alias must be a technical name.')),
            array('type, handler, layout_name', 'match', 'pattern' => '/^[a-zA-Z0-9_]{1,64}$/'),
            array('type', 'default', 'value' => 'pages'),
            array('layout_integration', 'default', 'value' => 'content'),

            array('published', 'in', 'range' => array_keys($this->publishedRange)),
            array('template', 'in', 'range' => array_keys($this->templateRange)),

            array('tech_id', 'unique'),
        );

        if (!$this->safeMode) {
            $purifier = new CHtmlPurifier();
            $purifier->options = $this->purifierOptions;

            $rules[] = array('title, short_text, full_text', 'filter', 'filter' => array($purifier, 'purify'));
        } else $rules[] = array('title, short_text, full_text', 'safe');

        // применение валидаторов для расширенных полей
        if ($optionsModel = $this->getOptionsModel())
            $optionsModel->applyRules($rules);

        return $rules;
    }

    public function beforeValidate()
    {
        // Замена всех упоминаний текущего домена в URL'ах на универсальное '/'.
        $this->full_text = preg_replace("|http://" . $_SERVER['HTTP_HOST'] . "/|", "/", $this->full_text);

        if ($this->autoAlias && !$this->alias)
            $this->alias = mb_substr(ExtCoreTextHelper::transliterate_cyr($this->title), 0, 32);

        // проверка родителя
        if ($this->parent || $this->parent_id) {

            // обращение к родителю
            if ($this->parent instanceof StructCategory)
                $this->_parent = $this->parent;
            else {
                $this->_parent = StructCategory::model()->findByPk($this->parent);
                if (!$this->_parent) {
                    $this->addError('parent_id', Yii::t('struct', 'Please, select category'));
                    return;
                }
            }

            if (Yii::app()->hasComponent('users') && Yii::app()->hasComponent('user')) {
                $sameCategory = $this->isNewRecord
                    ? NULL : self::model()->findByPk($this->id);

                if (isset(Yii::app()->user->model) && ($userModel = Yii::app()->user->model)) {
                    if (($this->_parent->{$this->leftAttribute} < $this->lockedLevel)
                        && !$userModel->role->super_user
                        && ($sameCategory && ($sameCategory->parent_id != $this->_parent->id))
                    ) {
                        $this->addError('parent', Yii::t('struct',
                            'You can`t move base categories!'));
                    }
                }
            }
        }

        return parent::beforeValidate();
    }

    /**
     * Валидатор предка.
     * Синхронизирует parent_id с данными по предку.
     */
    public function checkNestedParent($attribute, $params)
    {
        $object = $this;

        // в случае успешного прохождения всех проверок,
        // выполняется синхронизация
        $object->onAfterValidate = function (CEvent $event)
        use ($object, $attribute) {
            if (!sizeof($object->errors)) {
                // если были отправлены данные о предке,
                // запись перемещается в дереве, если нет - предок задаётся согласно
                // положению в дереве
                if (isset($_POST[get_class($object)]['parent'])) {
                    $objectClass = get_class($object);
                    $parent = new $objectClass;
                    if ($object->primaryKey && ($object->parent_id != $object->parent))
                        $object->moveAsLast($parent->findByPk($object->parent));

                    /**
                     * @todo: не происходит корректного добавления в нужного предка,
                     * т.к. в первой из использующих модель форм это делалось на месте,
                     * через appendChild(). Это неправильно и всё нужно вынести сюда.
                     */

                    $object->parent_id = $object->parent;
                } else if (!$object->isNewRecord) {
                    $object->parent_id = $object->parent()->find()->id;
                }
            }
        };
    }

    /**
     * Валидатор для категории, который убеждается, что в родительской категории
     * нет других страниц с таким 'alias'.
     * @param string $attribute
     * @param array $params
     */
    public function uniqueInParent($attribute, $params)
    {
        // были переданы данные по предку
        if ($this->_parent) {
            $_left = $this->leftAttribute;
            $_right = $this->rightAttribute;

            // Поиск ближайшего предка - не группы.
            if (($this->_parent->type == 'group')
                && ($this->_parent->{$this->levelAttribute} > 2)
            ) {
                $closestCategoriesModel = self::model();
                $closestCategoriesModel->dbCriteria->order = "{$this->leftAttribute} DESC";
                $closestCategory = $closestCategoriesModel->find(
                    "(t.type<>'group') AND (t.{$_left} < :left) AND (t.{$_right} > :right)",
                    array(':left' => $this->_parent->$_left, ':right' => $this->_parent->$_right)
                );

                if (!$closestCategory) {
                    $this->addError($attribute, Yii::t('struct',
                        'Group must have at least one non-group category parent!'));

                    return false;
                }
            } else $closestCategory = $this->_parent;

            // Обращение ко всей ветке ближайшего предка - не группы,
            // с целью поиска всех его непосредственных потомков, а также
            // сгруппированных потомков.
            $adjasentsCategoriesModel = self::model();
            $adjasentsCategoriesModel->dbCriteria->order = "$_left ASC";
            $adjasentsCategories = $adjasentsCategoriesModel->findAll(
                "(t.{$_left} > :left) AND (t.{$_right} < :right)",
                array(
                    ':left' => $closestCategory->$_left,
                    ':right' => $closestCategory->$_right
                )
            );

            // определение, какие из потомков будут учитываться при проверке
            // псевдонима категории
            $groups = array($closestCategory->id);
            $idx = array();
            foreach ($adjasentsCategories as $adgasentCategory) {

                if ($adgasentCategory->type != 'group') {
                    if (($adgasentCategory->id == $closestCategory->id) ||
                        ($adgasentCategory->id == $this->id)
                    )
                        continue;

                    if (in_array($adgasentCategory->parent_id, $groups))
                        $idx[] = $adgasentCategory->id;
                } else {
                    if (in_array($adgasentCategory->parent_id, $groups))
                        $groups[] = $adgasentCategory->id;
                }
            }


            $model = self::model();
            $model->dbCriteria->addInCondition('id', $idx);

            if ($model->exists("t.$attribute=:alias", array(':alias' => $this->$attribute)))
                $this->addError($attribute, Yii::t('struct', 'Alias must be unique in the parent category and its subgroups!'));

        }
    }

    /**
     * Удаление страниц, привязанных к категории и её потомкам.
     */
    protected function beforeDelete()
    {
        /** @todo: эта функция достаточно накладная и не годится для
         * приложений с большим объемом таблицы материалов!  **/

        if (!parent::beforeDelete()) return false;

        $descendantIds = array($this->id);
        foreach ($this->descendants()->findAll() as $descendant)
            $descendantIds[] = $descendant->id;

        // удаление всех материалов, имеющих единственную привязку к категории
        // удаление всех BIND-записей

        // поиск связанных страниц
        $pagesModel = StructPage::model()->categories($descendantIds);

        // список используемых страниц
        $pagesIds = array();
        foreach ($pagesModel->findAll() as $page) $pagesIds[] = $page->id;

        // запрос эти же страницы с учетом всех категорий (для поиска страниц,
        // привязанных только к этим категориям)
        $ownedPages = StructPage::model();
        $ownedPages->dbCriteria->mergeWith(array(
            'with' => 'categoriesBinds',
            'together' => true,
            'group' => 'page_id',
            'having' => 'COUNT(category_id) = 1',
        ));
        $ownedPages->dbCriteria->addInCondition('t.id', $pagesIds);

        $ownedPagesIds = array();
        foreach ($ownedPages->findAll() as $page)
            $ownedPagesIds[] = $page->id;

        // удаление страниц и связей
        StructBind::model()->deleteAll('category_id IN (' . implode(',', $descendantIds) . ')');
        StructPage::model()->deleteByPk($ownedPagesIds);
        StructTagCategory::model()->deleteAll("category_id=:category_id",
            array(':category_id' => $this->id));

        return true;
    }


    ///////////////////////////////
    // МОДИФИКАЦИЯ ЯДРА КОМПОНЕНТА

    protected function _useTemplateOptions()
    {
        $templatePath = Yii::app()->struct->getTemplatePath('category', $this->template);
        $templateContent = file_get_contents($templatePath);

        // свойства PHPDOC
        if (preg_match_all(
            "/(?>\*[\s^\n]+(?>(?>@(?P<properties>\w+)\s+(?>(?P<value>[^\n]*)\n))))/",
            $templateContent, $matches, PREG_SET_ORDER)
        ) {
            // разбор директив
            $errorLine = NULL;
            $errorText = NULL;
            $usedContexts = array();
            $customFields = array();
            foreach ($matches as $line => $directive) {
                $propertiesRange = array('option', 'param');
                $contextRange = array('self', 'children', 'pages');
                $optionsRange = array(
                    "enabled" => array('on' => 1, 'off' => 0),
                    "show_tech_fields" => array('on' => 1, 'off' => 0),
                    "show_tech_fields" => array('on' => 1, 'off' => 0),
                    "show_gallery" => array('on' => 1, 'off' => 0),

                    "foreword" => array('none', 'html', 'text'),
                    "foreword_required" => array('on' => 1, 'off' => 0),
                    "foreword_length_min" => array('none' => '', '_rule' => '/^\d{1,3}$/'),
                    "foreword_length_max" => array('none' => '', '_rule' => '/^\d{1,3}$/'),

                    "content" => array('none', 'html', 'text'),
                    "content_required" => array('on' => 1, 'off' => 0),
                    "content_length_min" => array('none' => '', '_rule' => '/^\d{1,3}$/'),
                    "content_length_max" => array('none' => '', '_rule' => '/^\d{1,3}$/'),
                );

                $property = & $directive['properties'];
                if (!in_array($property, $propertiesRange))
                    continue;

                if (!preg_match_all('/(?P<properties>(?>\w+)|(?>\".+?\"))/', $directive['value'], $valueMatches)) {
                    $errorText = Yii::t('struct', 'Error in preg_match for string: "{value}".', array('{value}' => $directive['value']));
                    $errorLine = $line;
                    break;
                }

                $properties = & $valueMatches['properties'];
                if (!sizeof($properties)) {
                    $errorText = Yii::t('struct', '$properties is not array. {properties}', array('{properties}' => $properties));
                    $errorLine = $line;
                    break;
                }
                foreach ($properties as $index => $value)
                    $properties[$index] = trim($value, '"');

                $context = array_shift($properties);
                if (!in_array($context, $contextRange)) {
                    $errorText = Yii::t('struct', '"{context}" not in contextRange array.', array('{context}' => $context));
                    $errorLine = $line;
                    break;
                }

                if (!isset($usedContexts[$context])) {
                    $usedContexts[$context] = $this->getRelated("{$context}OptionsTech");
                    if (!$usedContexts[$context]) {
                        $usedContexts[$context] = new StructCategoryOptions;
                        $usedContexts[$context]->rel = $context;
                        $usedContexts[$context]->category_id = $this->id;
                        /** @todo: работает только для уже созданных категорий! */
                    }
                }

                switch ($property) {
                    case 'option':
                        $option = array_shift($properties);
                        $value = array_shift($properties);
                        if (!isset($optionsRange[$option])) {
                            $errorLine = $line;
                            $errorText = Yii::t('struct', '{option} not in range', array('{option}' => $option));
                            break;
                        }
                        $valuesRange = & $optionsRange[$option];
                        if (isset($valuesRange[$value]) && ($value != '_rule'))
                            $value = $optionsRange[$option][$value];
                        else if (
                            !in_array($value, $optionsRange[$option]) &&
                            !(isset($optionsRange[$option]['_rule']) && preg_match($optionsRange[$option]['_rule'], $value))
                        ) {
                            $errorLine = $line;
                            $errorText = Yii::t('struct', 'Incorrect value for option "{option}"', array('{option}' => $option));
                            break;
                        } // неправильное значение

                        $usedContexts[$context]->$option = $value;
                        break;

                    case 'param':
                        if (!isset($customFields[$context])) $customFields[$context] = array();
                        $customFields[$context][] = array(
                            'preset' => array_shift($properties),
                            'name' => array_shift($properties),
                            'label' => array_shift($properties),
                            'value' => array_shift($properties),
                        );

                        break;
                }

                // внутри switch нет возможности выйти из цикла
                if (!is_null($errorLine)) break;
            }

            if (!is_null($errorLine))
                $this->addError('template',
                    Yii::t('struct', 'PHPDOC options error at line {line}. {error}', array(
                        '{line}' => $errorLine,
                        '{error}' => $errorText
                    )));

            else foreach ($usedContexts as $context => $optionsModel) {
                if (isset($customFields[$context])) {
                    $optionsModel->custom_fields = $customFields[$context];
                }

                $optionsModel->save();
                $this->_optionsModel = false;
                $this->getRelated("{$context}Options", true);
                if ($optionsModel->hasErrors()) $this->addError(
                    'template', Yii::t('custom', 'Error custom fields data. Stopped.')
                );
            }

        }
    }

    public function afterFind()
    {
        if ($this->data && !is_array($this->data)) $this->data = unserialize($this->data);
        if ($this->data && !is_array($this->data)) $this->data = array();
        return parent::afterFind();
    }

    // сериализация сложных данных
    public function beforeSave()
    {
        $result = parent::beforeSave();
        if (is_array($this->data)) $this->data = serialize($this->data);
        return $result;
    }

    public function afterSave()
    {
        $result = parent::afterSave();
        if($this->parse_phpdoc && isset($_POST[get_class($this)]['template']))
            $this->_useTemplateOptions();
        if ($this->data && !is_array($this->data)) $this->data = unserialize($this->data);
        if ($this->data && !is_array($this->data)) $this->data = $this->array();
        Yii::app()->struct->flushCache();
        return $result;
    }


    /**
     * Вызывается при неудачной попытке сохранения.
     */
    protected function afterSaveFail()
    {
        if ($this->data && !is_array($this->data)) $this->data = unserialize($this->data);
        if ($this->data && !is_array($this->data)) $this->data = $this->array();
    }


    // переопределение работы со свойствами таким образом, чтобы можно было обращаться
    // к данным из DATA как к свойствам

    public function __get($name)
    {
        // если не сделать этого, произойдет ситуация, когда обращение
        // к $this->getCustomAttributes() произойдет раньше, чем произошли
        // первые обращения к реальным полям
        if ($this->hasAttribute($name))
            return parent::__get($name);

        if (!parent::__isset($name)) {
            $customAttributes = $this->getCustomAttributes();
            if (isset($customAttributes[$name])) {
                return isset($this->data[$name])
                    ? $this->data[$name] : NULL;
            }
        }

        return parent::__get($name);
    }

    public function getAttribute($name)
    {
        // если не сделать этого, произойдет ситуация, когда обращение
        // к $this->getCustomAttributes() произойдет раньше, чем произошли
        // первые обращения к реальным полям
        if ($this->hasAttribute($name)) return parent::getAttribute($name);

        if (!is_null($value = parent::getAttribute($name)))
            return $value;
        else {
            $customAttributes = $this->getCustomAttributes();
            if (isset($customAttributes[$name])) {
                return isset($this->data[$name])
                    ? $this->data[$name] : NULL;
            }
        }

        return $value;
    }

    public function __set($name, $value)
    {
        // если не сделать этого, произойдет ситуация, когда обращение
        // к $this->getCustomAttributes() произойдет раньше, чем произошли
        // первые обращения к реальным полям
        if ($this->hasAttribute($name)) return parent::__set($name, $value);

        if (!parent::__isset($name)) {
            $customAttributes = $this->getCustomAttributes();
            if (isset($customAttributes[$name])) {
                $values = $this->data;
                $values[$name] = $value;
                $this->data = $values;
                return;
            }
        }

        return parent::__set($name, $value);
    }

    public function __toString()
    {
        return $this->id;
    }

    public function getAttributes($names = true)
    {
        $customAttributes = array();
        foreach ($this->getCustomAttributes() as $name => $label) {
            if (is_array($names) && !in_array($name, $names))
                continue;

            $customAttributes[$name] = isset($this->data[$name])
                ? $this->data[$name] : NULL;
        }

        return array_merge($customAttributes, parent::getAttributes($names));
    }

    public function attributeNames()
    {
        // определить настройки можно только у материалов с ID
        if ($this->getIsNewRecord() || !$this->id)
            return array_keys($this->getMetaData()->columns);

        return array_merge(
            array_keys($this->getMetaData()->columns),
            array_keys($this->getCustomAttributes())
        );
    }

    public function attributeLabels()
    {
        // определить настройки можно только у материалов с ID
        if ($this->getIsNewRecord() || !$this->id)
            return parent::attributeLabels();

        return array_merge(
            parent::attributeLabels(),
            $this->getCustomAttributes()
        );
    }

}