<?php

class StructTagCategory extends ActiveRecord
{
	public function relations()
	{
		return array
		(
			'tagsBinds' => array(self::HAS_MANY, 'StructTag', 'tag_id'),
			'categoriesBinds' => array(self::HAS_MANY, 'StructCategory', 'category_id'),
		);
	}

	public function tableName(){
		return self::commonTableName('struct_tag_category');
	}
}