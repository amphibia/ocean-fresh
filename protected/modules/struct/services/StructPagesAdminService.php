<?php

/*
 * Административный сервис - управление новостями
 */

class StructPagesAdminService extends ExtCoreAdminService
{
    public $techSection = array();
    public $contentSection = array();
    public $gallerySection = array();

    /**
     * Заголовок, который идет перед расширенными полями.
     * @var string
     */
    public $customFieldsHeader = NULL;


    /**
     * Список параметров, зарезервированных заранее (ИМЯ=>ПРАВИЛО).
     * Если они встретятся среди значений, а последующее значение будет
     * удовлетворять его правилу, значит это директива перед следующим параметром.
     *
     * @var array
     */
    public $reservedArguments = array(
        'page' => '/^[0-9]{1,5}$/',
        'parent' => '/^[0-9]{1,5}$/',
    );

    public $modelClass = 'StructPage';

    public $dictionary = array(
        'actions' => array(
            'index' => 'Page manegement',
            'create' => 'Create page',
        )
    );

    public $views = array();
    public $assets = array();

    /**
     * Список выделенных категорий.
     * @var array
     */
    protected $_selectedCategories = array();

    /**
     * Список выделенных тегов.
     * @var array
     */
    protected $_selectedTags = array();


    protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias('application.modules.struct.views');
        Yii::import('application.modules.struct.models.*');
        Yii::loadExtConfig($this, __FILE__);

        // 'ui.sortble.multupages.js' - скрипт только для индекса
        if ($action->id != 'index') {
            unset($this->assets['js']['sortble.multupages']);
        }

        Yii::applyAssets($this, __FILE__);

        return parent::beforeAction($action);
    }

    /**
     * @param string $orderBy
     * @param string $scope
     * @param string $search
     * @param number $page
     */
    public function actionIndex($orderBy = NULL, $scope = NULL, $search = NULL, $page = NULL, $parent = NULL)
    {
        // Данные, отправляемые на сторону клиента.
        Yii::app()->clientScript->registerScript('actionDrag_url',
            "\$(document).data('actionDrag_url', '" . $this->createUrl('drag', false) . "')");
        Yii::app()->clientScript->registerScript('actionDelete_url',
            "\$(document).data('actionDelete_url', '" . $this->createUrl('delete', false) . "')");
        Yii::app()->clientScript->registerScript('actionSetAttributes_url',
            "\$(document).data('actionSetAttributes_url', '" . $this->createUrl('setattributes', false) . "')");
        Yii::app()->clientScript->registerScript('actionSetPublishStatus_url',
            "\$(document).data('actionSetPublishStatus_url', '" . $this->createUrl('setPublishStatus', false) . "')");

        // внешний фильтр по тегам
        if (isset($_POST['selectedCategories'])) {
            if (is_array($_POST['selectedCategories'])
                && sizeof($_POST['selectedCategories'])
            )
                $this->_selectedCategories = $_POST['selectedCategories'];

            // применение фильтра по категориям
            $this->model->categories($this->_selectedCategories);
        }

        // внешний фильтр по категориям
        if (isset($_POST['selectedTags'])) {
            if (is_array($_POST['selectedTags'])
                && sizeof($_POST['selectedTags'])
            )
                $this->_selectedTags = $_POST['selectedTags'];

            // применение фильтра по категориям
            $this->model->tags($this->_selectedTags);
        } // открыты страницы, принадлежащие конкретной категории
        else if ($parent) {
            // применение фильтра по категориям
            $this->model->categories(compact('parent'));

            $_GET['parent'] = $category = StructCategory::model()->findByPk($parent);
            if (!$category) throw new CHttpException(404,
                Yii::t('struct', 'Category not found!'));

            // учет дополнительных атрибутов из StructCategoryOptions, помеченных
            // как "showInIndex"
            if ($optionsModel = $category->getPageOptions()) {
                $customFields = $optionsModel->getCustomAttributes();
                $nativeColumns = $this->model->getMetaData()->columns;

                $indexedCustomFields = array_intersect_key($customFields, $nativeColumns);

                if (sizeof($this->indexTableOptions['fieldsOrder'])) {
                    foreach ($indexedCustomFields as $name => $label) {
                        $maxIndex = sizeof($this->indexTableOptions['fieldsOrder']) - 1;
                        $lastValue = $this->indexTableOptions['fieldsOrder'][$maxIndex];
                        $this->indexTableOptions['fieldsOrder'][$maxIndex] = $name;
                        $this->indexTableOptions['fieldsOrder'][] = $lastValue;

                        // применение преконвертора для этого поля
                        foreach ($optionsModel->custom_fields as $fieldAttributes) {
                            if ($fieldAttributes['name'] == $name) {
                                $preset = $fieldAttributes['preset'];
                                $presetAttributes = $optionsModel->fieldPresetsAttributes[$preset];
                                $this->indexTableOptions['preConvertors'][$name] =
                                    create_function('$fieldName, $fieldValue, $row, $isHeader = FALSE',
                                        $presetAttributes['indexFieldPreconvertor']);

                                break;
                            }
                        }
                    }
                }
            }

            $this->_selectedCategories = array($category->id);
        }

        parent::actionIndex($orderBy, $scope, $search, $page, $parent);
    }

    /**
     * Страница создания элемента.
     * @param integer $parent_id В данной реализации не используется, оставлена для
     * совместивмости с сервисами, работающими с иерархическими моделями.
     */
    public function actionCreate($category_id = NULL)
    {
        // Инициируется свойство модели "parent". После загрузки данных из модели,
        // оно булет перезаписано и его потребуется перезаписать.
        $this->model->categories = $category_id ? array($category_id) : NULL;

        // настройки формы
        StructCategoryOptions::applyFormOptions($this, 'category');

        // инструмент выбора будет заменен на статическую надпись
        //if($this->model->category)
        //	$this->createForm['elements']['categories'] = array(
        //		'type' => 'text',
        //		'value' => $this->model->category->title,
        //		'disabled' => 'disabled',
        //		'class' => 'span6'
        //	);

        $form = new CForm($this->createForm, $this->model);
        if (isset($_POST[$this->modelClass])) {
            // учет предопределенной категории при создании материала
            if ($category_id) {
                if (isset($_POST[$this->modelClass]['categories'])) {
                    if (!is_array($_POST[$this->modelClass]['categories']))
                        $_POST[$this->modelClass]['categories'] = array();
                    if (($pos = array_search($category_id, $_POST[$this->modelClass]['categories'])) !== FALSE)
                        unset($_POST[$this->modelClass]['categories'][$pos]);
                    array_unshift($_POST[$this->modelClass]['categories'], $category_id);
                } else $_POST[$this->modelClass]['categories'] = array($category_id);
            }

            $form->loadData();
            $this->_formSaved($form->model->save(), $form, true, true);
        }

        $this->render($this->views['create'], compact('form', 'category_id'));
    }

    public function actionUpdate($id)
    {
        $this->loadModel($id);

        // настройки формы
        StructCategoryOptions::applyFormOptions($this, 'category');

        $form = new CForm($this->createForm, $this->model);
        if (isset($_POST[$this->modelClass])) {
            // настройки формы, взятые из StructCategoryOptions
            $form->loadData();
            $this->_formSaved($form->model->save(), $form);
        }

        $this->render($this->views['update'], compact('form'));
    }

    /**
     * Перетаскивание элемента в определённую позицию или на страницу.
     * @param integer $pageID
     * @param MIXED $newPosition номер позиции, или first/last
     */
    public function actionDrag($elementID, $newPosition = NULL)
    {
        $this->loadModel($elementID);

        $modelClass = $this->modelClass;
        $oldPosition = $this->model->position;
        if ($newPosition == 'first') {
            $destElement = $modelClass::model();
            $destElement->dbCriteria->order = 'position ASC';
            $destElement = $destElement->find();
            $newPosition = $destElement->position;
        } else if ($newPosition == 'last') {
            $destElement = $modelClass::model();
            $destElement->dbCriteria->order = 'position DESC';
            $destElement = $destElement->find();
            $newPosition = $destElement->position;
        } else {
            $newPosition = (int)$newPosition;
            $destElement = $modelClass::model()->findByAttributes(array(
                "position" => $newPosition));
            if (!$destElement) throw new CHttpException(404);
        }

        // позиция не изменилась
        if ($newPosition == $this->model->position) return;

        $transaction = Yii::app()->db->beginTransaction();

        // перемещение снизу вверх
        if ($newPosition > $oldPosition) {
            $tableName = $modelClass::model()->tableName();
            $command = Yii::app()->db->createCommand(
                "UPDATE `$tableName` SET position=(position-1) " .
                "WHERE (position >= $oldPosition) AND (position <= $newPosition)"
            );
            $this->model->position = $newPosition;
        }

        // перемещение сверху вниз
        if ($newPosition < $oldPosition) {
            $tableName = $modelClass::model()->tableName();
            $command = Yii::app()->db->createCommand(
                "UPDATE `$tableName` SET position=(position+1)" .
                "WHERE (position <= $oldPosition) AND (position >= $newPosition)"
            );
            $this->model->position = $newPosition;
        }

        // выполнение действий и выдача ответа
        if ($command->execute() && $this->model->save()) {
            $transaction->commit();
            ExtCoreJson::message(
                Yii::t('struct', 'Page was moved successfull.')
            );
        } else {
            $transaction->rollback();
            ExtCoreJson::error(
                Yii::t('struct', 'Cant complete operation. May be it has no effect.'));
            ExtCoreJson::message(print_r($this->model->errors, true));
        }

        ExtCoreJson::response();
    }

    protected function _successMessage()
    {
        if ($this->lastActionID == 'create')
            return Yii::t('struct',
                'Page "{title}" created. For editing "{title}" page, go to {edit form}.<br/><br/>' .
                '<strong>Notice!</strong> This form will create new category again. If you want to update previous form, go to {edit form}.',
                array(
                    '{title}' => $this->_model->title,
                    '{edit form}' => CHtml::link(
                            Yii::t('struct', 'edit form'),
                            $this->createUrl('update', array($this->_model->id))
                        )
                )
            );

        else return Yii::t('struct', 'Page "{title}" saved.',
            array('{title}' => $this->_model->title));
    }

    /**
     * Текст сообщения с подтверждением удаления
     * @param array $elements
     */
    protected function _deleteConfirmation(Array $elements)
    {

        $titles = array();
        foreach ($elements as $element) $titles[] = '"' . $element->title . '"';
        $titles = implode(', ', $titles);

        return Yii::t($this->messageContext,
            $this->deleteConfirmationMessage,
            array('{elements}' => $titles)
        );
    }

    /**
     * Отдача страницы для работы виджета MTMSelect (при перелистывании)
     * @param integer $pageNumber
     * @param mixed $pageSize
     */
    public function actionCategoriesMTMPage($pageNumber, $pageSize, $search = NULL)
    {
        Yii::import(Yii::localExtension('MTMSelect', '*'));
        MTMSelect::getPage($this->model, 'StructCategory', $pageNumber, $pageSize, 'title', $search);
    }

    /**
     * Отдача страницы для работы виджета MTMSelect (при перелистывании)
     * @param integer $pageNumber
     * @param mixed $pageSize
     */
    public function actionTagsMTMPage($pageNumber, $pageSize, $search = NULL)
    {
        Yii::import(Yii::localExtension('MTMSelect', '*'));
        MTMSelect::getPage($this->model, 'StructTag', $pageNumber, $pageSize, 'title', $search);
    }

    /**
     * Вывод панели управления выделенными элементами таблицы (групповые операции).
     *
     * @param array $ids
     * @return type
     */
    public function actionGroupOperationsBar(Array $ids = array())
    {
        $this->renderPartial($this->views['_groupOperationsBar'], compact('ids'));
    }

    /**
     * Назначение статуса опубликованности группе выделенных страниц.
     *
     * @param array $id
     * @param string $status
     */
    public function actionSetPublishStatus(Array $ids, $status)
    {
        if (!$ids || !sizeof($ids)) {
            ExtCoreJson::error(Yii::t('struct', 'Invalid arguments passed'));
            ExtCoreJson::response();
            return;
        }

        $pages = StructPage::model();
        $pages->dbCriteria->addInCondition('id', $ids);

        $appliedElementsIds = array();
        $unappliedErrors = array();

        foreach ($pages->findAll() as $page) {
            $page->published = $status;
            if ($page->save(true, array('published', 'date_changed')))
                $appliedElementsIds[] = $page->id;
            else $unappliedErrors[$page->title] = $page->errors;
        }

        if (!$appliedCount = sizeof($appliedElementsIds)) {
            ExtCoreJson::error(Yii::t('struct', 'Elements status was not changed'));
        } else {
            ExtCoreJson::message(
                Yii::t('struct', 'Changed status of {count} elements.', array('{count}' => $appliedCount))
            );
        }

        // вывод сообщений об ошибках
        foreach ($unappliedErrors as $title => $errors) {
            $message = $title . '<br/>';
            foreach ($errors as $field => $error) {
                $message .= CHtml::tag('strong', array(), $field);
                $message .= $error;
            }
        }

        ExtCoreJson::response();
    }
}

