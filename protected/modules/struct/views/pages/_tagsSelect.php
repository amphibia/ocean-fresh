
<div class="row-fluid tagsList">

	<div class="span2"><strong><?= Yii::t('struct', 'Tags filter'); ?>:</strong></div>
	<div class="span8">
    <?

    $this->widget(Yii::localExtension('mtmselect', 'MTMSelect'), array(
        'relationClassName' => 'StructTag',
        'model' => $this->model,
        'value' => $this->_selectedCategories,
        'sortable' => false,
        'route' => 'TagsMTMPage',
        'buttonLabel' => Yii::t('struct', 'Select tags'),
        'hintTitle' => Yii::t('struct', 'Tag filter'),
        'hintText' => Yii::t('struct', 'You can set tag filter by some tags.'),
        'noItemsMessage' => Yii::t('struct', 'No tags')
    ));

    ?>
	</div>

</div>