<?

/**
 * @author koshevy
 *
 * Макет представления по умолчанию для структры сайта.
 * Не изменяйте этот файл. Если потребуется изменить вывод, создайте макет
 * и представления для конкретного приложения (в папке protected/views) и
 * сошлитесь нан него в конфигурации StructController (свойства $layout и $views).
 *
	 * ПРИМЕЧАНИЕ: макет создавался с учетом использования Bootstrap.
 */

/* Свойства, используемые в макете. */
$category = Yii::app()->struct->currentCategory;
$title = Yii::app()->name . ($category ? ' - ' . $category->title:NULL);
$description = $category ? CHtml::encode(ExtCoreTextHelper::cutTextAtWords($category->short_text, 500)) : NULL;

?>
<!DOCTYPE html>
<html>
<head>

	<title><?= $title ?></title>

	<meta name="description" content="<?= $description ?>" />
	<meta http-equiv="Content-Language" content="<?= Yii::app()->locale->id ?>" />
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

</head>

<body id="top" data-spy="scroll" data-target=".subnav" data-offset="50">

	<div class="container root-container">

        <div id="header">
		<?
			$this->widget('application.modules.struct.StructMenu', array(
				'id' => 'mainMenu'
			));
		?>
		</div>

		<?php echo $content; ?>

        <div id="footer">
		</div>

	</div>

</body>
</html>