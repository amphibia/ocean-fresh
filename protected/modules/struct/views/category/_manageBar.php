<?

/**
 * Класс модели, с которой работает серис.
 * @var string
 */
$modelClass = $this->modelClass;


$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size' => 'small',
    'buttonType' => 'link',
    'icon' => 'icon-check',
    'label' => Yii::t('struct', 'Select all'),
    'url' => 'javascript:;',
    'htmlOptions' => array(
        'id' => 'selectRoots',
        'title' => Yii::t('struct', 'Select all'),
        'rel' => 'tooltip',
    ),
));
?><br><?

// общая часть
if (sizeof($selectedItems)) {
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
        'size' => 'small',
        'type' => 'danger',
        'buttonType' => 'link',
        'icon' => 'icon-trash icon-white',
        'label' => Yii::t('struct', 'Delete'),
        'url' => 'javascript:void;',
        'htmlOptions' => array(
            'id' => 'deleteCategories',
            'data-title' => Yii::t('struct', 'Delete all selected categories'),
            'rel' => 'tooltip',
            'data-url' => $this->createUrl('delete', array('id' => $selectedItems))
        ),
    ));
}
?>

<?
if (sizeof($selectedItems) < 2) {
    $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
        'size' => 'small',
        'buttonType' => 'link',
        'type' => 'info',
        'icon' => 'icon-plus icon-white',
        'label' => Yii::t('struct', 'Add category'),
        'url' => $this->createUrl('create', array('parent_id' => current($selectedItems))),
        'htmlOptions' => array(
            'data-title' => $selectedItems
                ? Yii::t('struct', 'Add subcategory to selected category')
                : Yii::t('struct', 'Create root category'),
            'rel' => 'tooltip'
        ),
    ));
}


// вывод текстовой части
if (!sizeof($selectedItems)) {
    ?>
    <hr/><?
    ?><p><?= Yii::t('struct', 'You can select one or more items (use CTRL or ALT for multiple selecting).'); ?></p><?
    ?><p><?= Yii::t('struct', 'You can drag and move categories to change place.'); ?></p><?
} else {
    $labels = $modelClass::model()->attributeLabels();

    $usedItems = 0;
    if (sizeof($categories = $modelClass::model()->findAllByPk($selectedItems)) == 1)
        foreach ($categories as $category) {
            $categoryUrl = $this->createUrl('update', array('id' => $category->id));
            $usedItems++;

            ?>
            <hr/><?
            ?>
            <section><?

            ?><h4><?= $category->title; ?></h4>
            <div><a href="<?= $categoryUrl ?>" class="categoryEdit"><i
                        class="icon-edit"></i> <?= Yii::t('struct', 'Edit'); ?></a></div>

            <div>
                <a href="<?= Yii::app()->getService('pages')->createUrl('index', array('parent' => $category->id)); ?>"
                   class="categoryPages" class="showPages">
                    <span class="icon-list-alt"></span> <?= Yii::t('struct', 'Category pages'); ?>
                </a>
            </div>

            <div>
                <a href="<?= $this->createUrl('options', array('category' => $category->id)); ?>"
                   class="categoryOptions">
                    <span class="icon-wrench"></span> <?= Yii::t('struct', 'Category options'); ?>
                </a>
            </div>

            <div class="categoryLinks"><?
            if ($category->type != 'group') {

                $categoryUrl = $category->createUrl();
                if (strlen($categoryUrl) > 1) {
                    ?><a class="categoryPreview" rel="tooltip"
                         title="<?= Yii::t('struct', 'Go to category page(in new tab)') ?>" target="_blank"
                         href="<?= $categoryUrl ?>">
                    <span class="icon-eye-open"></span> <?= Yii::t('struct', 'Category preview') ?>
                    </a><?
                }
            }
            ?></div>


            <?

            // редактирование статуса на месте
            ?>
            <div class="controls_a"><?
            echo CHtml::activeDropDownList($category, 'published', $category->publishedRange,
                array(
                    'data-id' => $category->id,
                    'data-title' => $category->title,
                    'class' => 'publishedControll',
                    'rel' => 'tooltip',
                    'title' => Yii::t('struct', 'Edit publish status in place')
                )
            );

            $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
                'size' => 'small',
                'type' => 'primary',
                'buttonType' => 'link',
                'icon' => 'icon-plus icon-white',
                'label' => Yii::t('struct', 'Create page in this category'),
                'url' => Yii::app()->getService('pages')->createUrl('create', array('category_id' => $category->id)),
                'htmlOptions' => array(
                    'rel' => 'tooltip',
                    'title' => Yii::t('struct', 'Add page to this category')
                )
            ));

            ?></div>




            <?

            ?>
            <div class="contentPreview"><?
            if (Yii::app()->hasComponent('storage') && !is_array($category->gallery) && $category->gallery)
                $gallery = Yii::app()->storage->decodeImages($category->gallery);
            if (isset($gallery) && $gallery) {
                ?>
                <div class="categoryImages"><?
                foreach ($gallery as $index => $image) {
                    if ($index >= 18) break;
                    $url = Yii::app()->storage->createUrl($image, 'avatarLarge');
                    ?><a class="categoryImage" target="_blank"
                         href="<?= Yii::app()->storage->createUrl($image, 'default') ?>"><?
                    ?><img src="<?= $url ?>" /><?
                    ?></a><?
                }
                ?></div><?
            }
            if ($category->short_text) {
                ?><p
                    class="categoryshort_text"><?= ExtCoreTextHelper::cutTextAtWords(strip_tags($category->short_text), 120) ?></p><?
            }
            ?></div><?

            ?></section>



            <div>
                <a href="<?= Yii::app()->getService('categories')->createUrl('exports', array('categoryId' => $category->id)); ?>"
                   class="categoryOptions">
                    <span class="icon-wrench"></span> <?= Yii::t('struct', 'Экспорт дерева категорий'); ?>
                </a>
            </div>
                <?php
                $this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
                    'size' => 'small',
                    'type' => 'primary',
                    'buttonType' => 'link',
                    'icon' => 'icon-plus icon-white',
                    'label' => Yii::t('struct', 'Импортировать дерево сайта'),
                    //'url' => '#',
                    'htmlOptions' => array(
                        'id' => 'uploadButtonInports',
                        'rel' => 'tooltip',
                    )
                ));
                ?>
                <div class="errorMessage" id="errorMessageInports"></div>
                <script>
                    $(document).ready(function() {

                        var button = $('#uploadButtonInports');
                        //$('#divUploadButtonInports').find('div').css('cursor', 'pointer');

                        $.ajax_upload(button, {
                            action : '<?= $this->createUrl('/admin/services/categories/imports', array('categoryId' => $category->id)) ?>',
                            name : 'zip_file',
                            onSubmit : function(file, ext) {
                                /*
                                 * Выключаем кнопку на время загрузки файла
                                 */
                                this.disable();

                            },
                            onComplete : function(file, response) {
                                // снова включаем кнопку
                                this.enable();
                                if(response == 'ok'){
                                    location.reload();
                                }
                                else{
                                    // показываем что файл загружен
                                    $("#errorMessageInports").text(response);

                                }


                            }
                        });
                    });
                </script>



            <?

            ?><p class="technicalInfo">
            <?= $labels['alias'] ?>: <?= $category->alias ?><br/>
            <?= $labels['type'] ?>: <?= $category->type ?><br/>
            <?php
            if ($category->type == 'controller') {
                ?>
                <?= $labels['handler'] ?>: <?= $category->handler ?><br/>
                <?php
            }
            ?>
            <?= $labels['layout_integration'] ?>: <?= $category->layout_integration ?><br/>
            <?php
            if ($category->template) {
                echo $labels['template'] ?>:
                <i><?= isset($category->templateRange[$category->template]) ? $category->templateRange[$category->template] . " ({$category->template})" : NULL ?></i>
                <br/><?
            }

            if ($category->tech_id) {
                ?>
                <?= $labels['tech_id'] ?>: <?= $category->tech_id ?><br/>
                <?php
            }
            ?>
            </p><?

        }

}
