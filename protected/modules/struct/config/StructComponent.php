<?php

return array(

    /**
     * Псевдоним главного раздела.
     * @var string
     */
    'mainCategoryAlias' => 'main',

    /**
     * Правила интерпретации URL, вставляемые модулем как наиболее приорететные.
     * @var array
     */
    'rules' => array(
        array(
            'class' => 'StructUrlRule',
            'structComponentID' => 'struct'
        )
    ),

    /**
     * ID контроллера-обработчика.
     * @return string
     */
    'structControllerID' => 'struct',

    /**
     * Текущая категория.
     * @var StructCategory
     */
    'currentCategory' => NULL,

    /**
     * Текущая открытая страница
     * @var StructPage
     */
    'currentPage' => NULL,

    /**
     * Срок жизни кэша страниц в секундах (используется, если в таблице не производилось
     * никаких изменений, если данные в таблице категорий менялись, кэш все равно
     * сбрасывается).
     * @var integer
     */
    'cacheExpire' => 172800,

    /**
     * Контроллер, обрабатывающий запросы модуля.
     * Используется для доступа к настрйкам контроллера из вне.
     * @var StructController
     */
    'structController' => NULL,

    /**
     * Путьи к файлам шаблонов.
     * @var array
     */
    'templatesRules' => array
    (
        'assets' => '{viewsPath}.templates.{name}.assets',
        'previews' => '{viewsPath}.templates.{name}.previews',
        'category' => '{viewsPath}.templates.{name}.template',
        'page' => '{viewsPath}.templates.{name}.item'
    ),

    /**
     * Алиас главной страницы по умолчанию.
     * @var array
     */
    'mainPageAlias' => 'mainPage',

);