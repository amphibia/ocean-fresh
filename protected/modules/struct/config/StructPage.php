<?php

return array
(

    /**
     * @var array Варианты статуса опубликованности страницы.
     */
    'publishedRange' => array(
		'published' => Yii::t('struct', 'Published page'),
        'draft' => Yii::t('struct', 'Draft page'),
        'hidden' => Yii::t('struct', 'Hidden page')
    ),

	/**
	 * @var boolean Авто-формирование алиаса, если он не заполнен.
	 */
	'autoAlias' => true,

	/**
	 * Настройки валидатора CHtmlPurifier.
	 * @var array
	 */
	'purifierOptions' => array(
		'Attr.EnableID' => true,
		'Attr.AllowedFrameTargets'=> array('_blank','_top'),
	),

    /**
     * Максимальная длина предисловия к материалу.
     * @var integer
     */
	'forewordLength' => 4096,

);