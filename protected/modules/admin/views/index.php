<?
//CVarDumper::dump(Yii::app()->admin->componentsMenu(), 6, 1); exit();
?>
<h2><?= ucfirst(str_replace('www.', '', $_SERVER['HTTP_HOST'])) ?></h2>
<br/>
<div id="widgetsContainer" class="js-masonry"
     data-masonry-options='{ "columnWidth": 270, "itemSelector": ".bootstrap-widget", "gutter": 20 }'>
    <?
    // тоже самое что и в меню, только в виджете
    Yii::app()->bootstrap->registerAssetCss('bootstrap-box.css');

    foreach (Yii::app()->admin->componentsMenu() as $adminItem) {
        ?>
        <div class="bootstrap-widget pull-left"
             style="margin-bottom:0px; width: 270px; padding: 8px 0; clear:none;">
            <div class="bootstrap-widget-header"><h3><?= $adminItem['label'] ?></h3></div>
            <div class="bootstrap-widget-content" style="padding: 8px 0;">
                <ul class="nav nav-list"><?
                    if (isset($adminItem['items']) && $adminItem['items']) {
                        foreach ($adminItem['items'] as $menuItem) {
                            if ($menuItem == '---') {
                                echo CHtml::tag('li', array('class' => 'divider'));
                                continue;
                            }

                            $url = $menuItem['url'];
                            if (is_array($url)) {
                                $url = $url[0] . ($url['tag'] ? '/' . $url['tag'] : '');
                            }
                            ?>
                            <li><a <?= isset($menuItem['url']['tag']) ? 'tag="' . $menuItem['url']['tag'] . '"' : '' ?>"
                            href="<?= $url ?>" style="color:black"><i
                                class="icon-<?= $menuItem['icon'] ?>"></i> <?= $menuItem['label'] ?></a></li><?
                        }
                    }
                    ?></ul>
            </div>
        </div>
    <?
    }
    ?>
</div>