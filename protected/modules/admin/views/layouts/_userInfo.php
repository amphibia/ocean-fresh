<div class="userInfo" style="">
<?

if(!Yii::app()->hasComponent('users')){
	?><p><?= Yii::t('admin', 'You must install "users" module for safety.') ?></p><?
}
else
{
	if(Yii::app()->user->isGuest){
		?><p><?= Yii::t('admin', 'Guest user.') ?></p><?
	}
	else if(!isset(Yii::app()->user->model)){
		?><p><?= Yii::t('admin', 'Can`t find user data.') ?></p><?
	}
	else
	{
		if(Yii::app()->user->model->profile)
		{
			$avatar = Yii::app()->user->model->profile->getProfileAvatar();

			// вывод аватара (в соответствии с конфигурацией профиля)
			if($avatar && Yii::app()->hasComponent('storage'))
				 echo CHtml::image(Yii::app()->storage->createUrl($avatar, 'avatarLarge'), '', array('align'=>'left'));
			else echo CHtml::tag('div', array('class'=>'no-image'), ' ');

			?><div class="username"><?= Yii::app()->user->model->profile->getFullName() ?></div><br/><?
		}
		else{
			?><div class="username"><?= Yii::app()->user->model->username ?></div><br/><?
		}
		
		echo CHtml::link(Yii::t('admin', 'Logout'),
			Yii::app()->users->logoutUrl,
			array('class'=>'logout')
		);
	}
}

?>
	
</div>
<div class="clear"></div>