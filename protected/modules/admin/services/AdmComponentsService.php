<?php

class AdmComponentsService extends ExtCoreService
{
    public function getRbacOperation(){
        return 'Application management';
    }

	
	public $assets = array();


	public function __construct($id, CWebModule $module=NULL){
		parent::__construct($id, $module);
		Yii::loadExtConfig($this, __FILE__);
	}

	public function filters(){
		return array('ajaxOnly + enabled, preload, approve, arguments'); }


	public function _componentHtmlId($component){
		return "{$component->type}_{$component->id}"; }


	public function actionIndex()
	{
		$this->viewPath = Yii::getPathOfAlias('admin.views');
		Yii::applyAssets($this, __FILE__);

		$components = Component::model()->findAll();
		$this->render('components/index', compact("components"));
	}

    public function actionClearCache(){
        Yii::app()->admin->clear();
        Yii::app()->admin->scanApplicationComponents();
        $this->redirect($this->createUrl("index"));
    }

	public function actionReload(){
        Yii::app()->admin->scanApplicationComponents();
		$this->redirect($this->createUrl("index"));
	}

	public function actionEnabled($id, $value)
	{
		$this->viewPath = Yii::getPathOfAlias('admin.views');
		$value = $value=="false"?false:(bool)$value;

		if(!$component = Component::model()->findByPk($id))
			ExtCoreJson::error(Yii::t('admin', 'Error: cant find component! Refresh page and try again.'));
		else{
			if((bool)$component->active == $value){
				ExtCoreJson::error(Yii::t('admin', 'Error: component already have requiered status! Refresh page.'));
				ExtCoreJson::response(); }
			
			$enableResult = $component->enable((int)$value);
			if(!$enableResult["result"]){
				foreach($component->errors as $errorFieldName => $errorField)
					foreach($errorField as $error) ExtCoreJson::error("$errorFieldName: $error");

				$component = Component::model()->findByPk($id); // сброс данных как было
			}
			else
			{
				ExtCoreJson::message(Yii::t('admin', 'Use "Approve" button for apply your changes.'));
				ExtCoreJson::message(Yii::t('admin',
					ucfirst($component->type)." {alias} ".($value?"enabled":"disabled"),
					array("{alias}"=>$component->alias))
				);
			}

			// задетые компоненты
			foreach($enableResult["enabledRequirements"] as $enabledRequirement)
				ExtCoreJson::replace_element($this->_componentHtmlId($enabledRequirement),
					$this->renderPartial('components/_component', array("component"=>$enabledRequirement), true));

			ExtCoreJson::replace_element($this->_componentHtmlId($component),
				$this->renderPartial('components/_component', compact("component"), true));
		}

		ExtCoreJson::response();
	}


	public function actionPreload($id, $value){

		$this->viewPath = Yii::getPathOfAlias('admin.views');
	
		if(!$component = Component::model()->findByPk($id))
			ExtCoreJson::error(Yii::t('admin', 'Error: cant find component! Refresh page and try again.'));
		else
		{
			$component->preload = ($value=='false')?0:(bool)$value;
			if(!$component->save()){
				foreach($component->errors as $errorFieldName => $errorField)
					foreach($errorField as $error) ExtCoreJson::error("$errorFieldName: $error");	
			
				$component = Component::model()->findByPk($id); // исходное состояние
			}
			else
			{
				ExtCoreJson::message(Yii::t('admin',
				ucfirst($component->type)." {alias} ".($value?" now preload at start.":" now use easy loading."),
				array("{alias}"=>$component->alias)));
				ExtCoreJson::message(Yii::t('admin', 'Use "Approve" button for apply your changes.'));
			}
			
			ExtCoreJson::replace_element($this->_componentHtmlId($component),
				$this->renderPartial('components/_component', compact("component"), true));
		}
		
		ExtCoreJson::response();
	}

	public function actionApprove(){
		Yii::app()->admin->approve();
		ExtCoreJson::message(Yii::t('admin', 'Changes approved.'));
		ExtCoreJson::response();
	}

	public function actionArguments($id){
		ExtCoreJson::message(Yii::t('admin', 'Coming soon! :)'));
		ExtCoreJson::response();
	}

}