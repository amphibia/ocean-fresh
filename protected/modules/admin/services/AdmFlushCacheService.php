<?php

class AdmFlushCacheService extends ExtCoreService
{

    public function getRbacOperation()
    {
        return 'Flush cache and assets';
    }

    public function __construct($id, CWebModule $module = NULL)
    {
        parent::__construct($id, $module);
        Yii::loadExtConfig($this, __FILE__);

        Yii::import('admin.helpers.*');
    }

    public function actionCache()
    {
        Yii::app()->cache->flush();
        FileDirHelper::delTree($_SERVER['DOCUMENT_ROOT'] . '/protected/runtime', false);
        FileDirHelper::delTree($_SERVER['DOCUMENT_ROOT'] . '/_cache', false);

        if ($_SERVER['HTTP_REFERER']) {
            Yii::app()->request->redirect($_SERVER['HTTP_REFERER']);
        } else {
            Yii::app()->request->redirect('/admin');
        }
    }

    public function actionAssets()
    {
        FileDirHelper::delTree($_SERVER['DOCUMENT_ROOT'] . '/assets', false);

        if ($_SERVER['HTTP_REFERER']) {
            Yii::app()->request->redirect($_SERVER['HTTP_REFERER']);
        } else {
            Yii::app()->request->redirect('/admin');
        }
    }

}