<?php

class FileDirHelper
{

    public static function delTree($dir, $delMainDir = true)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) { (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file"); }
        if ($delMainDir) { return rmdir($dir); } else { return true; }
    }
}