<?

class Component extends ActiveRecord
{

	public function tableName(){
    	return '{{app_relations}}';
	}

	public function relations()
	{
		return array
        (
            'package' => array(self::BELONGS_TO, 'Digest', 'digest_id'),
		);
	}

}