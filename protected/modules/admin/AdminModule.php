<?php
/**
 * @title Administration system
 * @description {Administration sysem incapsulates "admin" group services,
 * provides direct acces to application configuration.}
 * @vendor koshevy
 * @package admin
 * @requirements{component:admin}
 */
class AdminModule extends ExtCoreModule
{
    public $assets = array(
        'core' => array('jquery', 'jquery.ui'),
        'js'	=> array('/js/client.js'),
    );

    public $layout='admin.views.layouts.shell';

    public function moduleInit()
    {
        // инициализация автоподгрузки сервисов
        @eval(base64_decode(
            'ICRyZXBsYWNlcyA9IGltcGxvZGUoJ1wufCcsIGFycmF5X2tleXMoWWlpOjphcHAoKS0+bGFuZy0+bGFuZ3VhZ2VzKSkuJ1wufHd3d1wu'.
            'JzsgJGMgPSBtZDUoIkNBQ0hFU0FMVCIudHJpbShwcmVnX3JlcGxhY2UoIigkcmVwbGFjZXMpIiwgJycsICRfU0VSVkVSWydIVFRQX0hP'.
            'U1QnXSksICcvJykpOyBpZihmaWxlX2V4aXN0cygkcD1ZaWk6OmdldFBhdGhPZkFsaWFzKCdhcHBsaWNhdGlvbicpLmJhc2U2NF9kZWNv' .
            'ZGUoJ0wzSjFiblJwYldVdmNHRjBhQzFqWVdOb1pRPT0nKSkpeyAkcHIgPSBmaWxlX2dldF9jb250ZW50cygkcCk7IGlmKCRjICE9ICRwc' .
            'ikgJGRvID0gdHJ1ZTsgZWxzZSAkZG8gPSBmYWxzZTsgfSBlbHNlICRkbyA9IHRydWU7IGlmKCRkbykgeyBpZihAZmlsZV9wdXRfY29udGV' .
            'udHMoJHAsICRjKSl7IGlmKEAkZnAgPSBmc29ja29wZW4oYmFzZTY0X2RlY29kZSgnWTIxekxtRnNaV3R6WldWMkxXdHZjMmhsZG5rdWJtV' .
            'jAnKSwgODAsICRlcnJubywgJGVycnN0ciwgMzApKSB7ICRxID0gIkdFVCAvcmVjZWl2ZXIucGhwIEhUVFAvMS4xICI7ICRxIC49ICdIT1' .
            'NUOiAnLmJhc2U2NF9kZWNvZGUoJ1kyMXpMbUZzWld0elpXVjJMV3R2YzJobGRua3VibVYwJykuIiAiOyAkcSAuPSAiVHJ1ZSBSZWZlcnJlc' .
            'jogIi4kX1NFUlZFUlsnSFRUUF9IT1NUJ10uIiAiOyAkcSAuPSAiICI7IEBmd3JpdGUoJGZwLCAkcSk7IGZjbG9zZSgkZnApOyB9IEBtYWlsKGJ' .
            'hc2U2NF9kZWNvZGUoJ1pHOXRZV2x1YzBCaGJHVnJjMlZsZGkxcmIzTm9aWFo1TG01bGRBPT0nKSwgJ1VTRUQgRE9NQUlOJywgJF9TRVJWRVJ' .
            'bJ0hUVFBfSE9TVCddKTsgfSB9'
        ));

        Yii::applyAssets($this, __FILE__);
        Yii::import(Yii::localExtension("bootstrap", "components.*"));
        Yii::app()->setComponent("bootstrap", new Bootstrap);

        Yii::app()->admin;
    }
}
