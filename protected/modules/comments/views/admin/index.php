<? if(!Yii::app()->request->isAjaxRequest){ ?>
    <div class="page-header">

		<div>
			<h1><?= Yii::t('struct', 'Comments management'); ?></h1><br/>

			<?
			
			$this->widget(
				Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__)
			);

			// кнопка "в индекс админки"
			$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
				'size'=>'small',
				'buttonType'=>'link',
				'icon'=>'icon-arrow-left',
				'label'=>Yii::t('admin', 'Administration index'),
				'url'=>'/admin/',
			));
			
			?>
		</div>
		
	</div>
<?
}

// область кэшируется перед выводом
$cacheId = md5(serialize($_POST) . $_SERVER['REQUEST_URI']) . "__indexViewPages";
$cacheOptions = array(
	'duration' => Yii::app()->struct->cacheExpire,
	'dependency' => Yii::app()->struct->getCategoriesCacheDepency(),
);

if($this->beginCache($cacheId, $cacheOptions)){
	$this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), $config);
$this->endCache(); }

?>
