<?
/**
 * Представление, выводящее форму отправки коммнетариев.
 * Не использует стандартные механизмы AJAX-валидации CActiveForm.
 * Использует CExtCoreJson-протокол для обращения к серверу.
 *
 * Используется, как для форм комментариев первого уровня, так и для комментариев
 * ответов.
 * 
 * АРГУМЕНТЫ:
 * $owner_id - ID хозяина
 * $model - класс модели хозяина
 * $parent_id - родительский комментарий
 * $timestamp - текущая временная отметка
 * 
 * Подпись отправлять не нужно, она создается на месте.
 * 
 * Форма спроектирована таким образом, чтобы завершить отправку комментария
 * в случае сбоя JS - за счет прямой отправки формы.
 */
?>
<form action="/comments/input/add" method="POST" style="display:none;">

	<?= $this->_showUserInfo(); ?>
	
	<? if($this->useSubject){ ?>
	<div <?= CHtml::renderAttributes($this->elementsAttributes['subjectBlock']) ?>>
		<? if($this->subjectLabel) echo CHtml::tag('label', array(), $this->subjectLabel); ?>
		<?= CHtml::textField('CommentForm[subject]', '', $this->elementsAttributes['subjectField']) ?>
	</div>
	<? } ?>

	<div <?= CHtml::renderAttributes($this->elementsAttributes['textBlock']) ?>>
		<? if($this->textLabel) echo CHtml::tag('label', array(), $this->textLabel); ?>
		<?= CHtml::textArea('CommentForm[text]', '', $this->elementsAttributes['textField']) ?>
	</div>

	<div <?= CHtml::renderAttributes($this->elementsAttributes['rateBlock']) ?>>
	</div>

	<?
	
	// вывод капчи
	if(Yii::app()->commentsagent->useCaptcha)
	{
	?>
	<div <?= CHtml::renderAttributes($this->elementsAttributes['captchaBlock']) ?>>
		<? if($this->captchaLabel) echo CHtml::tag('label', array(), $this->captchaLabel); ?>
		<?= CHtml::textField('CommentForm[captcha]', '', $this->elementsAttributes['captchaField']) ?>
		<?
		
		$this->widget('CCaptcha', array(
			'captchaAction' => 'comments/input/captcha'
		));
		
		?>
	</div>
	<?
	}
	
	// подпись достоверности формы
	$signature = CommentForm::createSignature($parent_id, $owner_id, $model, $timestamp);

	echo CHtml::hiddenField('CommentForm[owner_id]', $owner_id, $this->elementsAttributes['ownerField'])
		.CHtml::hiddenField('CommentForm[model]', $model, $this->elementsAttributes['modelField'])
		.CHtml::hiddenField('CommentForm[parent_id]', $parent_id, $this->elementsAttributes['parentField'])
		.CHtml::hiddenField('CommentForm[timestamp]', $timestamp, $this->elementsAttributes['timestampField'])
		.CHtml::hiddenField('CommentForm[signature]', $signature, $this->elementsAttributes['signatureField'])
	
		// поле-ловушка
		.CHtml::hiddenField('token', md5($signature));
	?>

	<div <?= CHtml::renderAttributes($this->elementsAttributes['buttonsBlock']) ?>>
		<button <?= CHtml::renderAttributes($this->elementsAttributes['submitButton']) ?>><?= $this->submitTitle ?></button>
		<button <?= CHtml::renderAttributes($this->elementsAttributes['cancelButton']) ?>><?= $this->cancelTitle ?></button>
	</div>

	<!-- Место для вывода сообщений -->
	<div <?= CHtml::renderAttributes($this->elementsAttributes['messagesBlock']) ?>></div>

</form>