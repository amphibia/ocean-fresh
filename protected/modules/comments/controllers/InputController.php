<?php

/**
 * @author koshevy
 * Контроллер, обслуживающий систему комментирования - добавление комментариев,
 * постраничный вывод, AJAX-запросы, система голосования.
 * 
 * ППРИМЕЧАНИЕ: AJAX-взаимодействие с формами происходит по ExtCore-JSON протоколу,
 * стандартная Yii AJAX валидация НЕ ДОЛЖНА ИСПОЛЬЗОВАТЬСЯ!
 */
class InputController extends Controller
{

    public function actions(){
        return array('captcha'=> Yii::app()->commentsagent->captchaOptions);
    }

	public function filters(){
		return array('captchaInit + captcha');
	}

	public function filterCaptchaInit($filterChain){
		if(sizeof($_GET)) $_GET[CCaptchaAction::REFRESH_GET_VAR] = current($_GET);
		$filterChain->run();
	}


    public $reservedArguments = array(
        'page' => '/^[0-9]{1,5}$/',
		'offsetItemID' => '/^[0-9]{1,5}$/'
    );

	public function getRbacOperation()
	{
		return array
		(
			"add"			=> "Add comment",
			"add"			=> "Add comment",
			"captcha"		=> "Add comment",
			"like"			=> "Like comment",
			"likeCancel"	=> "Like comment",
			"dislike"		=> "Like comment",
			"dislikeCancel"	=> "Like comment",
			"default"		=> "Comments view",
			"error"			=> NULL,
		);
	}

    public function beforeAction($action)
	{
		// Контроллер может работать только с установленным модулем 'comments'
		// и прилагающимся к нему компонентом 'commentsAgent'.
		if(!Yii::app()->hasModule('comments')) throw new CException(
			Yii::t('comments', 'CommentsWidget require CommentsModule, installed as "comments".'));
		if(!Yii::app()->hasComponent('commentsagent')) throw new CException(
			Yii::t('comments', 'CommentsWidget require CommentsAgentComponent, installed as "commentsagent".'));

        Yii::loadExtConfig($this, __FILE__);
        return parent::beforeAction($action);
    }

	/**
	 * Добавление нового комментария.
	 */
	public function actionAdd()
	{
		if(!isset($_POST['CommentForm']))
			throw new CHttpException(404);

		$this->modelClass = 'CommentForm';
		$this->model->setAttributes($_POST['CommentForm']);

		try{ $result = $this->model->save(); }
		catch(Exception $err)
		{
			$class = get_class($err);
			$code = isset($err->statusCode) ? $err->statusCode : $err->getCode();
			$message = (defined('YII_DEBUG') && YII_DEBUG)
				? $err->getMessage()
				: Yii::t('comment', 'Sorry, system error. Please, try later.');

			if(Yii::app()->request->isAjaxRequest)
			{
				ExtCoreJson::error($message);
				ExtCoreJson::response();
			}
			else{
				if($class == 'CHttpException')
					 throw new $class($code, $message);
				else throw new $class($message);
			}
		}

		// Обработка AJAX-запроса.
		// Ответ по ExtCore JSON-протоколу (AJAX-валидация форм не используется).
		if(Yii::app()->request->isAjaxRequest)
		{
			if($result)
			{
				// Клиенту отправляется идентификатор новой записи чтобы можно
				// было его сразу вывести.
				ExtCoreJson::$data['comment_id'] = $this->model->commentModel->id; 
				ExtCoreJson::message(Yii::app()->commentsagent->successMessage);
			}
			else
			{
				ExtCoreJson::error(Yii::app()->commentsagent->failMessage);
				ExtCoreJson::$data['errorFields'] = array();
				
				// Ошибки модели выводятся с сообщениями.
				foreach($this->model->errors as $field => $errors)
				{
					ExtCoreJson::$data['errorFields'][] = $field;
					foreach($errors as $error) ExtCoreJson::error($error);
				}
			}
			ExtCoreJson::response();
		}

		// Обработка обычной отправки комментария (на случай сбоя JS).
		else
		{
			if($result)
				Yii::app()->user->setFlash('commentsSuccess',
					Yii::app()->commentsagent->successMessage);
			else
			{
				$errorMessage = CHtml::tag('div',  array(),
					Yii::app()->commentsagent->failMessage);

				// сообщения об ошибках из модели формы
				foreach($this->model->errors as $field => $errors)
					foreach($errors as $error)
						$errorMessage .= CHtml::tag('div', array(), $error);

				// сообщения об ошибках из модели самого комментария
				if($this->model->commentModel)
				foreach($this->model->commentModel->errors as $field => $errors)
					foreach($errors as $error)
						$errorMessage .= CHtml::tag('div', array(), $error);

				Yii::app()->user->setFlash('commentsError', nl2br($errorMessage));
			}

			$referer = isset($_SERVER['HTTP_REFERER'])
				? $_SERVER['HTTP_REFERER'] : NULL;

			if($referer){
				$url_info = parse_url($referer);
				$this->redirect($url_info['path']);
			}
			else $this->redirect('/');
		}
	}

	/**
	 * Обращение к одиночному комментарию.
	 * Выводится в том же виде, что и в ленте. Используется для отображения
	 * вновь добавленного комментария на стороне клиента без перезагрузки.
	 *
	 * @param integer $id
	 */
	public function actionComment($id){
		if(!$comment = Comment::cachedInstance((int)$id))
			throw new CHttpException(404);

		$this->widget('comments.CommentsWidget',
			array('singleComment' => $comment));
	}

	/**
	 * Выдачи комментариев по фиксированным страницам.
	 * @param string $model
	 * @param integer $owner_id
	 * @param integer $page номер страницы, если указать 'last' - выведет последнюю
	 * @param integer pageSize размер страницы
	 */
	public function actionPage($model, $owner_id, $pageSize = NULL, $sortDirection = 'DESC', $page = 1)
	{
		$page = (int)$page;
		$owner = $model::cachedInstance((int)$owner_id);
		if(!$owner || !$page || ($page<0)) throw new CHttpException(404);
		$this->widget('comments.CommentsWidget', array(
			'paginationType' => CommentsWidget::PAGES_TOGGLE,
			'owner' => $owner,
			'currentPage' => $page,
			'pageSize' => (($pageSize < 1000) ? $pageSize : 1000),
			'sortDirection' => $sortDirection,
		 ));
	}

	/**
	 * Выдачи комментариев начиная от стартовой позиции.
	 * @param string $model
	 * @param integer $owner_id
	 * @param integer $offsetItemID ID элемента, от которого идет отсчет
	 * @param integer pageSize размер страницы
	 */
	public function actionOffset($model, $owner_id, $offsetItemID = 0, $pageSize = NULL, $sortDirection = 'DESC')
	{
		$owner = $model::cachedInstance((int)$owner_id);
		if(!$owner || !is_numeric($offsetItemID) || ($offsetItemID<0))
			throw new CHttpException(404);
		$offsetItemID = (int)$offsetItemID;
		$offset = $this->_getCommentPosition($offsetItemID, $sortDirection);
		$this->widget('comments.CommentsWidget', array(
			'paginationType' => CommentsWidget::PAGES_ADDITION,
			'owner' => $owner,
			'offset' => $offset,
			'pageSize' => (($pageSize < 1000) ? $pageSize : 1000),
			'sortDirection' => $sortDirection,
		 ));
	}

	/**
	 * Переход к странице с указанным комментарием.
	 * @param integer $id идентификатор комментария
	 * @param integer $pageSize начиная с какой страницы подгружать
	 */
	public function actionGoto($id, $pageSize = NULL, $sortDirection = 'DESC')
	{
		if(!$pageSize)
		$pageSize = Yii::app()->commentsagent->pageSize;
		$pageSize = (int)$pageSize;

		$comment = Comment::cachedInstance($id);
		if(!$comment || !($pageNumber = $this->_getPageNumberByCommentId($comment, $pageSize, $sortDirection)))
			throw new CHttpException(404);

		$ownerClass = $comment->model;
		$owner = $ownerClass::cachedInstance($comment->owner_id);

		$this->widget('comments.CommentsWidget', array(
			'paginationType' => CommentsWidget::PAGES_TOGGLE,
			'owner' => $owner,
			'currentPage' => $pageNumber,
			'pageSize' => $pageSize,
			'selectedItem' => $id,
			'sortDirection' => $sortDirection,
		));
	}

	/**
	 * Переход к странице с указанным комментарием, отображая промежуточные
	 * страницы.
	 * @param integer $id идентификатор комментария
	 * @param integer $offsetItemID ID элемента, начиная от которого нчинать вывод
	 */
	public function actionScrollTo($id, $offsetItemID = 0, $sortDirection = 'DESC')
	{
		$comment = Comment::cachedInstance($id);
		if(!$comment) throw new CHttpException(404);

		$ownerClass = $comment->model;
		$owner = $ownerClass::cachedInstance($comment->owner_id);
		$position = $this->_getCommentPosition($comment, $sortDirection);

		if((int)$offsetItemID)
		{
			$offsetComment = Comment::cachedInstance((int)$offsetItemID);
			if(!$offsetComment) throw new CHttpException(404);
			$offset = $this->_getCommentPosition($offsetComment, $sortDirection);
		}
		else $offset = 0;

		if((int)$offsetItemID)
		{
			$offsetComment = Comment::cachedInstance((int)$offsetItemID);
			if(!$offsetComment) throw new CHttpException(404);
			$offset = $this->_getCommentPosition($offsetComment);
		}
		else $offset = 0;

		$this->widget('comments.CommentsWidget', array(
			'paginationType' => CommentsWidget::PAGES_ADDITION,
			'owner' => $owner,
			'offset' => $offset,
			'pageSize' => $position - $offset,
			'selectedItem' => $id,
			'sortDirection' => $sortDirection,
		 ));
	}

	/**
	 * Выдача списка обновлений - комментариев, созданных в после указанного
	 * периода в указанной ветки. Выборка может быть по последнему уже выведенном
	 * веткам.
	 * 
	 * Результат возвращается в формате JSON:
	 *	{timestamp:'...', items:[ {html:'...', type:'created|edited', id:''}, ... ]}
	 * 
	 * @param integer $timeStamp стартовый период
	 * @param integer $branchParentID комментарий-предок ветки
	 * @param integer $stopElementID элемент, позицией которого ограничивается выборка
	 */
	public function actionUpdates($timeStamp, $branchParentID, $stopElementID = NULL)
	{
		$timeStamp = (int)$timeStamp;
		if(!($branchParent = Comment::cachedInstance($branchParentID)) || !$timeStamp)
			throw new CHttpException(404);

		$comments = $branchParent->descendants()->withoutRoots();
		if(!$stopElementID){
			$leftAttribute = $comments->leftAttribute;
			$comments->dbCriteria->addCondition(
				"(t.{$leftAttribute} <= '{$comments->$leftAttribute}') AND " .
				"((t.date_created > '{$timeStamp}') OR  " .
				"(t.date_modified > '{$timeStamp}'))"
			);
		}

		/** @todo: Реализация не завершена */
		
		$updates = array();
		foreach($comments->findAll() as $comment)
		{
		}
		
	}
	
	/**
	 * Поиск страницы, на которой находится указанный комментарий.
	 * @param mixed $comment ID или объект комментария
	 * @param integer $pageSize размер страницы
	 */
	protected function _getPageNumberByCommentId($comment, $pageSize = NULL, $sortDirection = 'DESC')
	{
		if(!$pageSize) $pageSize = Yii::app()->commentsagent->pageSize;
		$position = $this->_getCommentPosition($comment, $sortDirection);
		return ceil($position / $pageSize);
	}

	/**
	 * Определение текущей позиции комментария.
	 * @param mixed $comment ID или объект комментария
	 */
	protected function _getCommentPosition($comment, $sortDirection = 'DESC')
	{
		$sortDirection = strtoupper($sortDirection);
		$sortDirection = ($sortDirection=='DESC') ? $sortDirection : 'ASC';

		if(!is_object($comment)) $comment = Comment::cachedInstance($comment);
		if(!$comment) return NULL;
		$leftAttr = $comment->leftAttribute;
		$position = Comment::model()->withoutRoots()
			->ownerByID($comment->model, $comment->owner_id);
		$position->dbCriteria->addCondition("t.{$leftAttr} ".(($sortDirection=='ASC')?'<=':'>=')."'{$comment->$leftAttr}'");
		return $position->count();		
	}


	public function actionLike($commentID){
	}

	public function actionLikeCancel($commentID){
	}

	public function actionDislike($commentID){
	}

	public function actionDislikeCancel($commentID){

	}
}
