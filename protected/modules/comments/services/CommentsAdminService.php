<?php

/*
 * Административный сервис - управление комментариями
 */

class CommentsAdminService extends ExtCoreAdminService
{
   /**
     * Список параметров, зарезервированных заранее (ИМЯ=>ПРАВИЛО).
     * Если они встретятся среди значений, а последующее значение будет
     * удовлетворять его правилу, значит это директива перед следующим параметром.
     * 
     * @var array
     */
	public $reservedArguments = array(
		'page' => '/^[0-9]{1,5}$/',
		'parent' => '/^[0-9]{1,5}$/',
	);

    public $modelClass = 'Comment';

    public $dictionary = array(
        'actions' => array(
            'index' => 'Comments manegement',
            'create' => 'Create comment',
        )
    );

	public function filters(){
		return array('ajaxOnly + setAttributes, simpleAttr, manageBar, moveItem, groupOperationsBar, create');
	}
	
	public $views = array();
    public $assets = array();

    /**
     * Список выделенных категорий.
     * @var array
     */
    protected $_selectedCategories = array();
    
    
    protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias('application.modules.comments.views');
        Yii::import('application.modules.comments.models.*');
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

        return parent::beforeAction($action);
    }
	
	public function actionCreate($parent_id = NULL) {
		throw new CHttpException(404);
	}

	/**
     * @param string $orderBy
     * @param string $scope
     * @param string $search
     * @param number $page 
     */
	/*
    public function actionIndex($orderBy = NULL, $scope = NULL, $search = NULL, $page = NULL, $parent = NULL)
    {
		// приёмка внешнего фильтра - по категориям
		if(isset($_POST['selectedCategories']))
		{
			if(is_array($_POST['selectedCategories'])
				&& sizeof($_POST['selectedCategories']))
					$this->_selectedCategories = $_POST['selectedCategories'];

			// применение фильтра по категориям
			$this->model->categories($this->_selectedCategories);
		}

        parent::actionIndex($orderBy, $scope, $search, $page, $parent);
    }
	 * 
	 */

    /**
     * Отдача страницы для работы виджета MTMSelect (при перелистывании)
     * @param integer $pageNumber
     * @param mixed $pageSize 
     */
	/*
    public function actionCategoriesMTMPage($pageNumber, $pageSize, $search = NULL){
        Yii::import(Yii::localExtension('MTMSelect', '*'));
        MTMSelect::getPage($this->model, 'StructCategory', $pageNumber, $pageSize, 'title', $search);
    }
	 * 
	 */
}

