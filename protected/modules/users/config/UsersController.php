<?php

return array
(
	'viewPath' => Yii::getPathOfAlias('application.modules.users.views'),

	'layout' => 'application.modules.users.views.layouts.userInterface',

	/**
	 * Компонент, с которым работает контроллер.
	 * @var UsersComponent
	 */
    'usersComponent' => NULL,

	/**
	 * Метод (функция), которая выводит профайл пользователя.
	 * @var callback
	 */
	'profileMethod' => array(),
	
	/**
	 * Настройки формы регистрации.
	 * @var array
	 */
	'registrationFormOptions' => array(
		'buttons' => array(
			'register' => array('label'=>Yii::t('users', 'Register user'), 'type' => 'submit')
		),
	),

	'views' => array
	(
		'registrationForm' => 'userInterface/registrationForm',
		'registrationSuccessfull' => 'userInterface/registrationSuccessfull',
		'wrongVerifyCode' => 'userInterface/wrongVerifyCode',
		'accountVerified' => 'userInterface/accountVerified',
		'accountVerifyError' => 'userInterface/accountVerifyError',
		'alreadyVerified' => 'userInterface/alreadyVerified',

		'passwordRequestForm' => 'passwordRecovery/requestForm',
		'passwordSendedMessage' => 'passwordRecovery/sendedMessage',
		'passwordChangeForm' => 'passwordRecovery/changeForm',
		'passwordSuccessMessage' => 'passwordRecovery/successMessage',

		'emailVerify' =>  "userInterface/_email/verifyCode",
		'emailRecovery' =>  "passwordRecovery/_email/passwordRecovery",
	),

	'assets' => array('css'=>array('users.css')),

);
