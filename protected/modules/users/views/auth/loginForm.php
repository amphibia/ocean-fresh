<!--<form id="auth_form" method="POST">-->

<?
// для вывода формы нужен будет Bootstrap
Yii::import(Yii::localExtension("bootstrap", "components.*"));
Yii::app()->setComponent("bootstrap", new Bootstrap);

$form = $this->beginWidget('CActiveForm', array(
	'id' => 'auth_form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
	),
    'focus'=>array($this->model,'username'),
));

if(!$this->model->blocked)
{
	?>
	<h2 id="prompt"><?= Yii::t('users', 'Authorization needs'); ?></h2>
	<input type=hidden name="<?= Yii::app()->request->csrfTokenName ?>" value="<?= Yii::app()->request->csrfToken ?>" />
	<?
	
	echo $form->textField($this->model, 'username', array(
		'placeholder' => $this->model->getAttributeLabel('username'),
		'class' => 'input-block-level requiered'
	));
	echo $form->error($this->model, 'username');

	echo $form->passwordField($this->model, 'password', array(
		'placeholder' => $this->model->getAttributeLabel('password'),
		'class' => 'input-block-level requiered'
	));
	echo $form->error($this->model, 'password');

	?><button id="submit" class="btn btn-large btn-info" type="submit"><?= Yii::t('users', 'Login') ?></button><?
}
else
{
	?>
	<div class="errorMessage">
		<?= Yii::t('users', 'Form is temporary blocked.'); ?>
	</div>
	<?
}

$this->endWidget();
?>

	
<div id="serve">
	<div><a target="_blank" href="/users/passwordRecovery"><?= Yii::t('users', 'Forgot password'); ?></a></div>
</div>