<div class="registrationForm">
	<h2><?= Yii::t('users', 'New user registration'); ?></h2>
	<?

	// ассетсы Bootstrap подгружаются прямо из представления на случай если,
	// потребуется сделать представление без него
	Yii::import(Yii::localExtension( "bootstrap", "components.*" ));
	Yii::app()->setComponent("bootstrap", new Bootstrap);

	Yii::setPathOfAlias('bootstrap', Yii::getPathOfAlias(Yii::localExtension('bootstrap')));

	// вывод автоформы
	$this->widget(Yii::localExtension('bootforms', 'BootAutoForm', __FILE__),
		array(
			'form' => $form,
			'autoElements' => true,
			'repeatActionOption' => false,
		)
	);

	?>
</div>