<h2><?= Yii::t('users', 'Your account verified successfull'); ?></h2>

<p>
	<?= Yii::t('users', 'Thanks you for registration at our site.'); ?>
	<?= Yii::t('users', 'You can go to <a href="{profileUrl}">profile</a>.',
			array('{profileUrl}'=>Yii::app()->controller->createUrl('/users/profile'))); ?>
</p>