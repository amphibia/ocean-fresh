<div class="registrationSuccessMessage">

	<h1><?= Yii::t('users', 'Registration complete'); ?></h1>

	<p class="thanks">
		<?= Yii::t('users', 'Thank you for registration on our site!'); ?>
	</p>

	<p class="mustVerify">
		<?= Yii::t('users', 'You must verify your account by clicking on the link sent to your email.'); ?>
	</p>

	<p class="goBack"><a href="/"><?= Yii::t('users', 'Go to main page'); ?></a></p>
	
</div>