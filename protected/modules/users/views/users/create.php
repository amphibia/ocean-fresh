<div class="page-header">
<h1><?= Yii::t('users', 'User create'); ?></h1><br/>
<?
// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('users', 'Users management'),
    'url'=>$this->createUrl('index'),
));

?>
</div>

<?php

$this->widget(Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__));

?><p><?
	echo Yii::t('users', 'Here you can edit base options of user.');
	?> <?
	echo Yii::t('users', 'After create user, you can edit extended user profile.');
?></p><?
?><br/><br/><?

$this->renderPartial($this->views['_form'], compact('form'));

?>
