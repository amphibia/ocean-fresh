<div class="page-header">
<h1><?= Yii::t('users', 'User update'); ?></h1><br/>
<?
// кнопка "в индекс раздела"
$this->widget(Yii::localExtension('bootstrap', 'widgets.BootButton'), array(
    'size'=>'small',
    'buttonType'=>'link',
    'icon'=>'icon-arrow-left',
    'label'=>Yii::t('users', 'Users management'),
    'url'=>$this->createUrl('index'),
));

?>
</div>



<?php

$this->widget(Yii::localExtension('bootstrap', 'widgets.BootAlert', __FILE__));

?><p><?
	echo Yii::t('users', 'Here you can edit base options of user.');
	?> <?
	echo Yii::t('users',
		'For change user profile data, go to <a href="{href}">user profile</a>.',
		array('{href}' => $this->createUrl('profile', array('user_id'=>$this->model->id)))
	);
?></p><?
?><br/><br/><?

$this->renderPartial($this->views['_form'], compact('form'));

?>
