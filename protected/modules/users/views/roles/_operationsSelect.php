
<div class="operationsList">
    <h3><?= Yii::t('users', 'Find roles by operations'); ?></h3>
    <?

    $this->widget(Yii::localExtension('mtmselect', 'MTMSelect'), array(
        'relationClassName' => 'AuthOperation',
		'titleField' => 'name',
        'model' => $this->model,
        'value' => $this->_operationsFilter,
        'sortable' => false,
        'route' => 'OperationsMTMPage',
        'buttonLabel' => Yii::t('users', 'Select operations'),
        'hintTitle' => Yii::t('users', 'Search by operations'),
        'hintText' => Yii::t('users', 'You can view only roles with <i>all</i> specified previllegies.'),
        'noItemsMessage' => Yii::t('mtmselect', '<strong>Cant find operations!</strong><br/>Check your RBAC system.')
    ));

    ?>
</div>