<?php

class AuthRole extends ActiveRecord
{
	public function behaviors(){
		return array(
            'CAdvancedArBehavior' => array(
                'class' => Yii::localExtension('AdvancedArBehavior', 'CAdvancedArBehavior')),

			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created',
				'updateAttribute' => 'date_changed',
			)
		);
	}

    public function rules()
	{
		$purifier = new CHtmlPurifier();
		
        return array
		(
			array('title', 'required'),
			array('title, description', 'filter', 'filter'=>array($purifier, 'purify')),
			array('description', 'length', 'max'=>'512'),
            array('date_created, date_changed', 'numerical', 'integerOnly' => true ),
			array('operations', Yii::localExtension('mtmselect', 'ManyToManyValidator'), 'max'=>32),
			
			array('enabled, super_user, guest_role, default_role', 'numerical'),
			array('guest_role', 'guestRoleCheck'),
			array('default_role', 'defaultRoleCheck'),
        );
    }

	public function guestRoleCheck()
	{
		if($this->guest_role && AuthRole::model()->exists(
			'(guest_role>0) AND (id<>:id)', array(':id'=>$this->id))){
			$this->addError('guest_role',
				Yii::t('users', 'Only one role can be marked as "guest role"!'));
		}
	}
	
	public function defaultRoleCheck()
	{
		if($this->default_role && AuthRole::model()->exists(
			'(default_role>0) AND (id<>:id)', array(':id'=>$this->id))){
			$this->addError('default_role',
				Yii::t('users', 'Only one role can be marked as "default role"!'));
		}
	}
	
    public function relations()
	{
        return array
		(
			'users' => array(self::HAS_MANY, 'AuthUser', 'role_id'),
			'operations' => array(self::MANY_MANY, 'AuthOperation', 'auth_bundle(role_id, operation_id)'),
        );
    }

	protected function beforeDelete() {
		return false;
		
		return parent::beforeDelete();
	}
	
    public function tableName() {
        return "{{auth_role}}";
    }
}