<?php

/**
 * Модель для работы с активностью пользователя.
 *
 * В рабочей таблице сохраняются основные действия пользователя: попытка входа
 * (удачная и неудачная), регистрация (имеется ввиду как непосредственная
 * регистрация на сайте, так и импорт из соцсетей). 
 */
class AuthLogin extends ActiveRecord
{

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created'
			)
		);
	}

	public function beforeValidate()
	{
		if(!$this->isNewRecord) $this->addError($attribute,
			Yii::t('users', 'User login is read-only!'));
		return parent::beforeValidate();
	}

	public function rules()
	{
		return array
        (
			array('user_id, date_created', 'numerical', 'integerOnly' => true),
			array('ip', 'match', 'pattern' => '/^\d{1,3}(\.\d{1,3}){3}$/'),
			array('provider, user_agent, referrer', 'safe'),

			// проверка существования пользователя
			array('user_id', 'userExists'),
		);
	}

	public function relations(){
		return array('user' => array(self::BELONGS_TO, 'User', 'user_id'));
	}

	/**
	 * Проверка существоания пользователя-хозяина.
	 */
	public function userExists($attribute, $params)
	{
		if(User::model()->count("id='{$this->$attribute}'"))
			$this->addError($attribute,
				Yii::t('users', 'Not exists user!'));
	}

    public function tableName() {
        return "{{auth_login}}";
    }
}