<?php

/**
 * Класс работы с пользователем.
 * Работа с профилем вынесена в отдельный класс (может быть переопределён
 * в конфиге), дополнительные связи и свойства также настраиваются в конфиге.
 * 
 * Аттрибуты:
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $status
 * @property integer $date_created
 * @property integer $attensions
 * @property string $registration_type
 * 
 * Реляционные свойства:
 * @property array $logins
 * @property array $roles Роли
 * @property array $profiles
 */
class User extends ActiveRecord
{
    /**
     * Минимальная длина пароля.
     * @var integer
     */
    public $passwordMinLength = 6;

	/**
	 * Связь с моделью учёта активности пользователя (AR-отношение).
	 * Вынесен в отдельное свойство для удобства переопределения профиля
	 * из вне.
	 * 
	 * @var array 
	 */
	public $loginsRelation = array(self::HAS_MANY, 'AuthLogin', 'user_id');

	/**
	 * Статусы пользователя.
	 * @var array
	 */
	public $statusRange = array('verified', 'registered', 'imported', 'banned');

	/**
	 * Статусы пользователя, с которыми может быть выполнен вход.
	 * @var array
	 */
	public $validStatusRange = array('registered', 'imported', 'verified');

	/**
	 * Срок жизни кода подтверждения в секундах.
	 * @var int
	 */
	public $verifyCodeLifetime = 604800; //(7 * 24 * 3600) - 7 дней;

    /**
     * Алгоритм шифрования пароля.
	 * ПРИМЕЧАНИЕ: также используется при формировании кода подтверждения
	 * пользователя.
	 * 
     * @var string
     */
    public $passwordHashAlgo = 'md5';

    /**
     * "Соль" для усиления защиты пароля.
	 * ПРИМЕЧАНИЕ: также используется при формировании кода подтверждения
	 * пользователя.
	 * 
     * @var string
     */    
    public $passwordSalt = 'PASS_SALT_';

	/**
	 * Формат имени пользователя.
	 * @var string
	 */
	public $usernameRule = "/^\w+$/";
	
    /**
     * Отношения, зависимые от таблицы пользователя.
	 * Например, при добавлении модуля для работы с новостями, в котором новости
	 * привязываются к пользователю, укажите в конфиге соответствуюшую связь.
     * @var array
     */
	public $dynamicRelations = array();
	
	/**
	 * Подтверждение пароля.
	 * @var string
	 */
	public $retype_password = NULL;


	
	/** @TODO: кажется, описанные ниже события нигде не использются, а в компоненте
	 * USERS есть равнозначные им, которые действительно применимы */

	/**
	 * Событие: при успешно пройденной регистрации.
	 * @param CEvent $event
	 */
	public function onRegistrationSuccess($event){
		$this->raiseEvent('onRegistrationSuccess', $event); }

	/**
	 * Событие: при ошибки во время регистрации.
	 * @param CEvent $event
	 */
	public function onRegistrationFail($event){
		$this->raiseEvent('onRegistrationFail', $event); }

	/**
	 * При прохождении подтверждения
	 * @param CEvent $event
	 */
	public function onVerify($event){
		$this->raiseEvent('onVerify', $event); }



	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created',
				'updateAttribute' => 'date_changed',
			)
		);
	}

	public function attributeLabels() {
		return array_merge(parent::attributeLabels(),
			array(
				'retype_password' => Yii::t('attributes', 'Retype Password'),
				'role_id' => 'User role',
			)
		);
	}
	
    public function rules()
    {
        return array
        (            
			array('username', 'unique'),
			array('username, role_id', 'required'),
			array('username', 'length', 'max' => 64),
			array('username', 'match', 'pattern' => $this->usernameRule),

			array('password', 'length', 'max' => 32),
			array('password', 'passwordСheck'),

			array('email', 'email'),

			array('status', 'in', 'range' => $this->statusRange),
			array('date_created, attentions', 'numerical', 'integerOnly' => true),

			// приём ролей из вне
			array('role_id', 'roleCheck'),

			array('retype_password', 'safe'),
        );
    }

	/**
	 * Обращение к пользователю по логину (username или email) и паролю.
	 * Ищет только подтверждённых (verified или imported) пользователей,
	 * с назначенной ролью (роль должна быть включена).
	 * 
	 * @param string $login (username или email)
	 * @param string $password
	 * @return User
	 */
	static function findUser($login, $password)
	{
		$userModel = self::model();
		$userModel->dbCriteria->addInCondition('status', $userModel->validStatusRange);
		$user = $userModel->find(
			'(email=:login OR username=:login) AND password=:password AND role_id',
			array(':login'=>$login, ':password'=>$userModel->hashedPassword($password))
		);

		if($user && $user->role && $user->role->enabled)
			 return $user;
		else return NULL;
	}

	/**
	 * Создание кода подтверждения для высылки пользователю на email.
	 * @param int $timestamp
	 * @param string $extendedSalt соль для разных типов кодов
	 * @return string
	 */
	function createVerifyCode($timestamp, $extendedSalt = 'VERIFY_CODE')
	{
		$srcString = $this->passwordSalt.$this->username.$this->email.$timestamp;
		return hash($this->passwordHashAlgo, $extendedSalt.$srcString);
	}

	/**
	 * Сопоставление кода подтверждения с данными пользователя.
	 * @param type $code Код, требующий проверки.
	 * @param type $timestamp
	 * @param string $extendedSalt соль для разных типов кодов
	 * @return boolean
	 */
	public function checkVerifyCode($code, $timestamp, $extendedSalt = 'VERIFY_CODE')
	{
		// проверка актуальности
		$codeAge = time() - $timestamp;
		if(($codeAge > $this->verifyCodeLifetime) || ($codeAge < 0))
			return false;

		return ($code == $this->createVerifyCode($timestamp, $extendedSalt));
	}
	
	/**
	 * Обращение к профилю (если профиль для данного пользователя отсутствует,
	 * он создаётся).
	 * @return AuthProfile
	 */
	public function profile(){
		// создание профиля
		if(!$this->profile){
            $profileModelClass = Yii::app()->user->usersComponent->profileModelClass;
			$profile = new $profileModelClass;
			$profile->user_id = $this->id;
			$profile->save(false);

			return $profile;
		}
		else return $this->profile;
	}
	
    /**
     * Проверка правильности пароля при создании пользователя или при смене пароля.
     * @param string $password
     */
    public function passwordСheck()
    {
        $password = $this->password;

		// проверка длины пароля
		if($this->isNewRecord || $password)
		{
			if(mb_strlen($password, 'utf8') < $this->passwordMinLength)
				$this->addError('password',
					Yii::t('users', 'Password must be at least {passwordMinLength} characters',
						array("{passwordMinLength}" => $this->passwordMinLength)));
			else
			{
				if($this->password != $this->retype_password){
					$this->addError('retype_password', Yii::t('users',
						"Passwords does`t match!")
					);
				}

				$this->password = $this->hashedPassword($password);
			}
		}

        // если не требуется смена пароля, данные о нём удаляются из входящей формы
        else unset($this->password);
    }

	/**
	 * Превращение пароля в хэш.
	 * @param string $password
	 */
	public function hashedPassword($password){
		return hash($this->passwordHashAlgo, $this->passwordSalt.$password);
	}
	
	/**
	 * Проверка роли.
	 */
	public function roleCheck()
	{
		/**
		 * @todo Дописать проверку, что назначаются привеллегии, которые есть
		 * у пользователя, выполняющего операцию.
		 */
		$role = AuthRole::model()->findByPk($this->role_id);
		if(!$role) $this->addError('role_id', Yii::t('users', 'Unexists role!'));
		else
		{
			// поиск супер-пользователей в базе
			$superUsers = self::model();
			$superUsers->dbCriteria->mergeWith(array(
				'with' => 'role',
				'together' => true,
				'condition' => 'super_user>0',
			));

			// как только в базу появляется первый супер-пользователь,
			// никто другой не может назначить роль супер-пользователя
			if($superUsers->exists() && !($this->isNewRecord && $role->super_user == 0))
			{
				// гость, или не супер пользователь не могут ставить роль супер
				// пользователя никому;
				// также, никто кроме супер-пользователя не может снимать пометку
				// super_user
				/** @todo: Проверить эту супер громоздкую логическую конструкцию! */
				if(Yii::app()->user->isGuest
				|| ($role->super_user && !Yii::app()->user->model->role->super_user)
				|| (!Yii::app()->user->model->role->super_user
						&& $this->id && ($oldUser = User::model()->findByPk($this->id))
						&& $oldUser->role->super_user
					) )
				{
					$this->addError('role_id',
						Yii::t('users', 'Only super users can set/unset other super users!' . $this->role_id));
				}
			}
		}
	}

	public function beforeValidate(){
		// изменять super_user может только super_user
		if(isset(Yii::app()->user->model) && $this->role_id)
			if($this->role->super_user && !Yii::app()->user->model->role->super_user){
				throw new CHttpException(403, Yii::t('users', 'You cant change super user!'));
				return false;
			}
		
		return parent::beforeValidate();
	}

	protected function beforeDelete()
	{
		// гость по определению не может никого удалять
		if(Yii::app()->user->isGuest || $this->isNewRecord){
			$this->addError('role', Yii::t('users', 'You cant delete yourself!'));
			return false;
		}

		// пользователь не может удалить сам себя
		if(Yii::app()->user->model->id == $this->id){
			$this->addError('id', Yii::t('users', 'You cant delete yourself!'));
			return false;
		}

		// удалить super_user может только super_user
		if(isset(Yii::app()->user->model))
			if($this->role->super_user && !Yii::app()->user->model->role->super_user){
				$this->addError('role', Yii::t('users', 'You cant delete super user!'));
				return false;
			}
			
		/** @TODO: сделать удаление привязок. **/
		if($this->profile) $this->profile->delete();
		
		return parent::beforeDelete();
	}

	/**
	 * Проверка доступа пользователя к операции.
	 * @param string $operation
	 */
	public function operationCheckAccess($operation)
	{
		// может ли пользователь выполнять хоть какие-то действия
		if(!in_array($this->status, $this->validStatusRange))
			return false;

		// учёт всех операций, к которым требуется доступ
		static $_cache = array();
		if(!in_array($operation, $_cache))
		{
			$_cache[] = $operation;
			$operationExists = AuthOperation::model()->exists(
				'name=:operation',
				array(':operation'=>$operation)
			);

			// добавление операции в таблицу
			if(!$operationExists){
				$operationModel = new AuthOperation;
				$operationModel->name = $operation;
				$operationModel->save();
			}
		}

		// супер-пользователь вне проверок
		if($this->role->super_user) return true;
		else
		{
			// проверка прав (для обычного пользователя)
			$operationsModel = AuthRole::model();
			$operationsModel->dbCriteria->mergeWith(array(
				'condition' => '(operations.name=:operation) AND (t.id=:role_id)',
				'with' => 'operations',
				'together' => true,
				'params' => array(':operation'=>$operation, ':role_id'=>$this->role->id),
			));

			return $operationsModel->exists();
		}
	}

    public function relations()
	{
        $staticRelations = array
		(
            'logins' => $this->loginsRelation,
            'role' => array(self::BELONGS_TO, 'AuthRole', 'role_id'),
            'profile' => array(self::HAS_ONE, Yii::app()->user->usersComponent->profileModelClass, 'user_id'),
        );

        return array_merge($staticRelations, $this->dynamicRelations);
    }

    public function tableName() {
        return "{{auth_user}}";
    }

}