<?php

class AuthOperation extends ActiveRecord
{
	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'date_created'
			)
		);
	}
	
    public function relations()
	{
        return array(
			'roles' => array(self::MANY_MANY, 'AuthRole', 'auth_bundle(operation_id, role_id)'),
			/** @todo: проверить работоспособность комбинации */
			//'users' => array(self::HAS_MANY, 'User', array('id'), 'through'=>'roles'),
		);
    }

    public function tableName() {
        return "{{auth_operation}}";
    }
}

