<?php

/*
 * Административный сервис - управление страницами
 */
class UsersAdminService extends ExtCoreAdminService
{
    public function getRbacOperation(){
        return 'Users management';
    }

	
    public $modelClass = 'User';
    
    public $assets = array(
		'css' => array('admin.css'),
		'js' => array('admin/users.js'),
    );

    public $views = array(
        'create' => 'users/create',
        'update' => 'users/update',
        'index' => 'users/index',
		'_form' => 'users/_form',
		'profile' => 'users/profile',
    );

    public $dictionary = array(
        'actions' => array(
            'index' => 'Users management',
            'create' => 'Add new user',
        )
    );

	public $_rolesFilter = array();

	public $profileForm;

	public function actionProfile($user_id)
	{
		if(!$user = User::model()->findByPk($user_id))
			throw new CHttpException(404, Yii::t('users', 'User not found'));
		$this->modelClass = Yii::app()->user->usersComponent->profileModelClass;

		$this->_model = $user->profile();
		$form = new CForm($this->profileForm, $this->_model);        
		if(isset($_POST[$this->modelClass])){
			$form->loadData();
			$this->_formSaved($form->model->save(), $form); }

		$this->render($this->views['profile'], compact('form'));
	}

    protected function beforeAction($action)
    {
        $this->viewPath = Yii::getPathOfAlias('application.modules.users.views');
        Yii::import('application.modules.users.models.*');
        Yii::loadExtConfig($this, __FILE__);
        Yii::applyAssets($this, __FILE__);

		// отправка клиенту адреса actionSetAttributes
		$setAttributesUrl = $this->createUrl('setattributes', false);
		$setAttributesScript = "$(document).data('setAttributesUrl', '$setAttributesUrl')";
		Yii::app()->clientScript->registerScript(
			'setAttributesUrl', $setAttributesScript, CClientScript::POS_HEAD);
		
        return parent::beforeAction($action);
    }

	/**
	 * Переопределение индекса, с учётом отправленных фильтров.
	 * 
	 * @param type $orderBy
	 * @param type $scope
	 * @param type $search
	 * @param type $page
	 */
	public function actionIndex($orderBy = NULL, $scope = NULL, $search = NULL, $page = NULL, $parent = NULL)
	{
        // приёмка внешнего фильтра - по категориям
        if(isset($_POST['rolesFilter'])){
            if(is_array($_POST['rolesFilter'])
                && sizeof($_POST['rolesFilter']))
                $this->_rolesFilter = $_POST['rolesFilter'];
        }

		// применение фильтра по категориям
		if(sizeof($this->_rolesFilter)){
			$this->model->dbCriteria->addInCondition('role_id', $this->_rolesFilter);
		}

		parent::actionIndex($orderBy, $scope, $search, $page);
	}


    /**
     * Отдача страницы для работы виджета MTMSelect (при перелистывании)
     * @param integer $pageNumber
     * @param mixed $pageSize 
     */
	public function actionRolesMTMPage($pageNumber, $pageSize, $search = NULL){
		Yii::import(Yii::localExtension('MTMSelect', '*'));
		MTMSelect::getPage($this->model, 'AuthRole', $pageNumber, $pageSize, 'name', $search);
	}
}