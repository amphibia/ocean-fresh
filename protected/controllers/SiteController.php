<?php
Yii::import(Yii::localExtension('structlandings', 'controllers.*'));

/**
 * @title {Main site controller}
 * @desc {Description}
 */

class SiteController extends LandingBaseController
{
	public $layout = '//layouts/mainPageLayout';

    public $modelClass = 'StructPage';
}