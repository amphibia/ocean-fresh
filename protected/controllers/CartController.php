<?php

/**
 * Страница Cart
 *
 * @title {Cart}
 */

class CartController extends ExtController
{
    public $layout = '//layouts/structLayout';

    public $views = array
    (
        'category' => '/category',
        'page' => '/page',
    );

    public $category = NULL;

    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }
    
    public function actionIndex()
    {
        $category = &Yii::app()->struct->currentCategory;
        if(!Yii::app()->request->isAjaxRequest) {
            $this->render($this->views['category'], compact('category'));
        }else{
            header("Content-type: application/json; charset=utf-8");
            header('Access-Control-Allow-Origin: *');



            $data = array(
             );

            echo json_encode($data);

        }
    }


    /**
     * Действие по обработки и отправки данных с форм
     * @throws CHttpException
     */
    public function actionSend()
    {
        header('Access-Control-Allow-Origin: *');

        $category = Yii::app()->custom->getCategories('cart');

        //die("POST=<pre>".print_r($_POST, true)."</pre>");
        $orderForm = new OrderForm;

        if (isset($_POST[get_class($orderForm)])) {
            $orderForm->setAttributes($_POST[get_class($orderForm)]);
            if ($orderForm->validate()) {

                $cart = json_decode($orderForm->cart);
                if($cart){

                    //die("<pre>".print_r($cart, true)."</pre>");
                    $newCart = array();
                    foreach($cart as $item) {

                        if(!isSet($item->gram_weight)) $item->gram_weight = 0;

                        //typeCuttingSelected
                        $item->typeCuttingSelected = null;
                        foreach ($item->typeCutting as $typeItem) {
                            if ($typeItem->id == $item->typeCuttingSelected) {
                                $item->typeCuttingSelected = $typeItem->title;
                                break;
                            }
                        }

                        $cartItemForm = new CartItemForm;

                        $cartItemForm->id = $item->id;
                        if ($orderForm->delivery_type != -1) //Если не предзаказ
                        {
                            $cartItemForm->gram_weight = $item->gram_weight;
                            $cartItemForm->qty = $item->qty;
                            $cartItemForm->typeCuttingSelected = $item->typeCuttingSelected;
                        }

                        if (!$cartItemForm->validate()) {
                            //CVarDumper::dump($feedbackForm->errors);
                            if ($cartItemForm->errors) {
                                foreach ($cartItemForm->errors as $fieldKey => $errors) {
                                    if (is_array($errors)) {
                                        foreach ($errors as $error) {
                                            ExtCoreJson::error("{$cartItemForm->getAttributeLabel($fieldKey)}: {$error}");
                                        }
                                    } else {
                                        ExtCoreJson::error($errors);
                                    }
                                }
                            }
                            ExtCoreJson::response();
                        }else{
                            if ($orderForm->delivery_type != -1) //Если не предзаказ
                            {
                                $newCart[] = array(
                                    'id' => $cartItemForm->id,
                                    'gram_weight' => $cartItemForm->gram_weight,
                                    'qty' => $cartItemForm->qty,
                                    'typeCuttingSelected' => $cartItemForm->typeCuttingSelected,
                                );
                            }else{
                                $newCart[] = array(
                                    'id' => $cartItemForm->id,
                                 );
                            }
                        }
                    }
                    if ($orderForm->delivery_type != -1) //Если не предзаказ
                    {
                        $data = array(
                            'fio' => $orderForm->fio,
                            'phone' => $orderForm->phone,
                            'email' => $orderForm->email,
                            'delivery_type' => ($orderForm->delivery_type == 1)?'Доставка':'Самовывоз',
                            'type_payment' => Yii::t('cart', $orderForm->type_payment),
                            'cart' => $newCart
                        );

                        if($orderForm->delivery_type == 1){
                            $data['delivery_address'] = $orderForm->delivery_address;
                            $data['delivery_time'] = $orderForm->delivery_time;
                        }
                    }else{
                        $data = array(
                            'fio' => $orderForm->fio,
                            'phone' => $orderForm->phone,
                            'delivery_time' => $orderForm->delivery_time,
                            'pre_order_qty' => $orderForm->pre_order_qty,
                            'type_payment' => Yii::t('cart', $orderForm->type_payment),
                            'cart' => $newCart
                        );
                    }

                    $data['delivery_zone'] =  Yii::t('cart', $orderForm->delivery_zone);

                    //echo "<pre>".print_r($data, true)."</pre>";
                    $data['cart'] = Yii::app()->cart->testCart($data['cart'], $orderForm->delivery_type, $orderForm->delivery_zone, $category);

                    //die("<pre>".print_r($data, true)."</pre>");

                    if ($orderForm->delivery_type != -1) //Если не предзаказ
                    {
                        $totalPrice = 0;
                        foreach($data['cart'] as $key => $item){
                            $totalPrice += $item['price'] * $item['qty'];
                        }
                        //$data['totalPrice'] = $totalPrice;
                    }


                   if($orderForm->delivery_type == 1 && ($orderForm->delivery_zone == 'out' || $totalPrice <= $category->limit_order)){
                        $data['price_delivery'] =  $category->price_delivery;
                        $totalPrice = $totalPrice + $data['price_delivery'];
                    } else {
                        $data['price_delivery'] =  0;
                    }

                    $data['totalPrice'] = $totalPrice;
                    $data['date_order'] =  Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm:ss", strtotime("now"));
                    
                    //die("<pre>".print_r($data, true)."</pre>");

                    

                    $html = Yii::app()->cart->getCartJsonToHtml($data);
                    //die($html);

                    //Сохранение заказа в базу
                    $categorySendOrder = Yii::app()->custom->getCategories('sendOrder');
                    $orderPage = new StructPage();
                    $orderPage->categories = array($categorySendOrder->id);
                    $orderPage->first_category_index = $categorySendOrder->id;
                    $orderPage->title = Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm:ss", strtotime("now"));
                    $orderPage->alias = 'page' . strtotime("now") . rand();
                    //$orderPage->published = 'hidden';
                    $orderPage->published = 'published';
                    $orderPage->full_text = $html;
                    $orderPage->save();

                    $orderPage->fio = $orderForm->fio;
                    $orderPage->phone = $orderForm->phone;
                    if ($orderForm->delivery_type != -1) //Если не предзаказ
                    {
                        $orderPage->email = $orderForm->email;
                        $orderPage->delivery_type = $data['delivery_type'];
                    }
                    $orderPage->delivery_time = $orderForm->delivery_time;
                    $orderPage->delivery_zone = $orderForm->delivery_zone;
                    $orderPage->delivery_address = $orderForm->delivery_address;
                    $orderPage->type_payment = $orderForm->type_payment;
                    $orderPage->save();

                    //Сохраняем данные в сессии для онлайн оплаты
                    $data['id'] = $orderPage->id;
                    //$data['type_payment'] = $orderForm->type_payment;
                    
                    $_SESSION['order'] = $data;

                    //Отправка на почту
                    $email = (trim($categorySendOrder->email))?trim($categorySendOrder->email):FeedbackOptionsForm::instance()->to_email;
                    ///*
                    Yii::app()->mailer->sendMessage
                    (
                        array
                        (
                            //'to' => FeedbackOptionsForm::instance()->to_email,
                            'to' => $email,
                            'bcc' => FeedbackOptionsForm::instance()->bcc,
                            'content' => $html,
                        ), 'default'
                    );

                    // Отправка уведомления клиенту
                    Yii::app()->mailer->sendMessage(
                        array
                        (
                            'to' => $orderPage->email,
                            'subject' => Yii::t('emails', 'Ваш заказ'),
                            'content' => $html,
                        ), 'default'
                    );
                    //*/

                    ExtCoreJson::message(Yii::t('custom', 'Спасибо, Ваша заявка отправлена.'));
                }else{
                    ExtCoreJson::error('Не верные данные корзины');
                }


                ExtCoreJson::response();
            } else {
                //CVarDumper::dump($feedbackForm->errors);
                if ($orderForm->errors) {
                    foreach ($orderForm->errors as $fieldKey => $errors) {
                        if (is_array($errors)) {
                            foreach ($errors as $error) {
                                ExtCoreJson::error("{$orderForm->getAttributeLabel($fieldKey)}: {$error}");
                            }
                        } else {
                            ExtCoreJson::error($errors);
                        }
                    }
                }
                ExtCoreJson::response();
            }
        } else throw new CHttpException(404);
    }

    public function actionForm(){
        $this->render('/templates/cart/form');
    }

    public function actionTest(){
        //die("<pre>".print_r($_REQUEST, true)."</pre>");
        //die("<pre>".print_r($_SESSION, true)."</pre>");
        //echo Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm:ss", strtotime("+4 hours"));
    }

    public function actionOnlinePayment(){
        $regim = 'work';

        if(isSet($_SESSION['order']['id'])) {
            $id = (int)$_SESSION['order']['id'];
            $page = StructPage::model()->findByPk($id);
            if ($page) {

                if($page->type_payment == 'online_card') {
                    if($regim == 'work'){
                        $C_SHARED_KEY = 162;
                        $MERCHANT = 'EC711013';
                        $TERMINAL = 'EC711013';
                        $CLIENT_ID = 'TOO OCEAN FRESH GROUP';
                        $URL = 'https://www.atfbank.kz/ecom/api';
                        $BACKREF = 'http://test.oceanfresh.kz/cart/sent?redirect=true';
                    } else {
                        $C_SHARED_KEY = 122;
                        $MERCHANT = 'ECOMM001';
                        $TERMINAL = 'ECOMM001';
                        $CLIENT_ID = '0';
                        $URL = 'https://46.101.210.91:8182/ecom/api';
                        $BACKREF = 'http://local.oceanfresh.kz/cart/sent?redirect=true';
                    }

                    $vSign = '';

                    $DESC_ORDER = "";
                    foreach ($_SESSION['order']['cart'] as $item) {
                        $DESC_ORDER .= $item['title'] . " - " . $item['qty'] . " шт.\r\n";
                    }

                    //$ORDER = strtotime("now");
                    $ORDER = str_pad($id, 6, '0', STR_PAD_LEFT);
                    $AMOUNT = $_SESSION['order']['totalPrice'];
                    $CURRENCY = 'KZT';
                    $NONCE = strtotime("now");
                    $DESC = $_SESSION['order']['fio'] . ' | ' . $_SESSION['order']['phone'];
                    //*
                    //$DESC_ORDER = htmlspecialchars($page->full_text, ENT_QUOTES);
                    //*/
                    //$DESC_ORDER = 'dsfd';
                    $EMAIL = $_SESSION['order']['email'];
                    $Ucaf_Flag = '';
                    $Ucaf_Authentication_Data = '';

                    $vs = $C_SHARED_KEY .
                        $ORDER . ";" .
                        $AMOUNT . ";" .
                        $CURRENCY . ";" .
                        $MERCHANT . ";" .
                        $TERMINAL . ";" .
                        $NONCE . ";" .
                        $CLIENT_ID . ";" .
                        preg_replace("/[\n\r]/", "", $DESC) . ";" .
                        preg_replace("/[\n\r]/", "", $DESC_ORDER) . ";" .
                        $EMAIL . ";" .
                        $BACKREF . ";" .
                        $Ucaf_Flag . ";" .
                        $Ucaf_Authentication_Data . ";";

                    //if(isSet($_POST["ORDER"])){
                    $vSign = hash("sha512", $vs);
                    //}

                    $_SESSION['order']['vSign'] = $vSign;

                    $this->render('/templates/cart/onlinePayment', compact('vSign', 'ORDER', 'AMOUNT', 'CURRENCY', 'MERCHANT', 'TERMINAL', 'NONCE', 'CLIENT_ID', 'DESC', 'DESC_ORDER', 'EMAIL', 'BACKREF', 'URL', 'regim'));

                } else {
                    $this->redirect('/cart/sent?cash=1');
                }

            }
        }
    }

    public function actionSent(){
        //die("<pre>".print_r($_REQUEST, true)."</pre>");
        $this->render('/templates/cart/sent');
    }




}