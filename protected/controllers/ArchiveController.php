<?php

/**
 * Архив содержимого по текущей категории.
 * Контроллер является дополнением к системе модуля Struct, и работает
 * только при условии использования последнего.
 * 
 *
 * @title {Archive (news, events, etc)}
 * @desc {Controller with date order and implemented date filter in calendar.}
 */

class ArchiveController extends ExtController
{
	public $modelClass = 'StructPage';

	/**
	 * Категория, к которой привязан контроллер.
	 * @var StructCategory
	 */
	public $category = NULL;

	/**
	 * Нужно ли отображать материалы потомков
	 * @var boolean
	 */
	public $useDescendantsPages = NULL;

	/**
	 * Максимальный уровень, на котором материалы потомков отображаются в разделе.
	 * Если NULL, отображатся все уровни.
	 * @var integer
	 */
	public $descendantsDepth = NULL;

	/**
	 * Используемое поле даты.
	 * @var string
	 */
	public $usedDateField = NULL;

	/**
	 * Выбранный год.
	 * Может указываться в настройках как по умолчанию, но при передаче методам
	 * контроллера они будут переписаны явно.
	 * @var integer 
	 */
	public $year = NULL;

	/**
	 * Выбранный месяц.
	 * Может указываться в настройках как по умолчанию, но при передаче методам
	 * контроллера они будут переписаны явно.
	 * @var integer
	 */
	public $month = NULL;

	/**
	 * Выбранный день.
	 * Может указываться в настройках как по умолчанию, но при передаче методам
	 * контроллера они будут переписаны явно.
	 * @var integer
	 */
	public $day = NULL;

	/**
	 * Использованная страница.
	 * Может указываться в настройках как по умолчанию, но при передаче методам
	 * контроллера они будут переписаны явно.
	 * @var integer
	 */
	public $page = NULL;

	/**
	 * Поле, по которому осуществляется сортировка.
	 * Можно указывать значение по умолчанию.
	 * Если указывается явно в аргументах к методам класса, переписывается явно.
	 * @var string
	 */
	public $sort = NULL;


    public $reservedArguments = array('page' => '/^[0-9]{1,5}$/');

	/**
	 * Список подразделов используемой категории.
	 * Значение инициализируется в методе _currentCategoryScope.
	 * @var array
	 */
	protected $_subCategoriesID = NULL;


	/**
	 * Года, за которые есть материал.
	 * @var array
	 */
	protected $_years = array();

	/**
	 * Месяцы, за которые есть материал (в указанном году).
	 * @var array
	 */
	protected $_months = array();

	/**
	 * Дни, за которые есть материал (в указанном месяце).
	 * @var array
	 */
	protected $_days = array();


	protected function beforeAction($action)
	{
		// работает только как прикрепленный к категории контроллер
		if(!Yii::app()->struct->currentCategory)
			throw new CHttpException(404);
		
		Yii::loadExtConfig($this, __FILE__);
		$this->category = Yii::app()->struct->currentCategory;
		$this->viewPath = Yii::app()->struct->structController->viewPath;
		return parent::beforeAction($action);
	}

	public function actionIndex($year = NULL, $month = NULL, $day = NULL, $sort = NULL, $page = NULL)
	{
		$this->_currentCategoryScope($this->model);

		// параметры контроллера из аргументов
		foreach(compact('year', 'month', 'day', 'page') as $key => $value)
			if($value) $this->$key = (int)$value;
			
		$sortDate = compact('year', 'month', 'day');
		$this->_initUsedDates();
		if(array_sum($sortDate)) $this->model->dateRange($sortDate, 'date_display');

		$this->model->getActiveDataProvider()->pagination->currentPage = $page?$page-1:$page;
		$this->model->dbCriteria->group = 't.id';

		$this->render(
			Yii::app()->struct->structController->views['category'],
			array('category'=>$this->category)
		);
	}
	
	/**
	 * Интерфейс для смены календаря по AJAX.
	 */
	public function actionCalendar($year = NULL, $month = NULL, $day = NULL){
		
		$this->_currentCategoryScope($this->model);

		// параметры контроллера из аргументов
		foreach(compact('year', 'month', 'day', 'page') as $key => $value)
			if($value) $this->$key = $value;

		$this->_initUsedDates();
		
		$this->outputArchiveCalendar();
	}

	/**
	 * Вывод календаря, работающего с этим архивом.
	 * Календарь обновляется по AJAX, обращаясь к действию actionCalendar.
	 */
	public function outputArchiveCalendar()
	{
		$categoryUrl = Yii::app()->struct->currentCategory->createUrl();
		$this->widget
		(
			'ext.inlinecalendar.InlineCalendarWidget',
			array
			(
				'year'=>$this->year, 'month'=>$this->month, 'day'=>$this->day,
				'acceptedYears' => $this->_years,
				'acceptedMonths' => $this->_months,
				'acceptedDays' => $this->_days,
				'route' => "{$categoryUrl}/index",
				'ajaxRoute' => "{$categoryUrl}/calendar"
			)
		);
	}

	/**
	 * Отсортировка в модели по использумеой категории.
	 * @param array $model StructCategory
	 */
	protected function _currentCategoryScope(&$model)
	{
		$category = &$this->category;
		$descendantsID = array($this->category->id);

		// вывод материалов, привязанных к разделам-потомкам (если требуется)
		if($this->useDescendantsPages)
		{
			// массив с подразделами еще не 
			if(!$this->_subCategoriesID)
			{
				$this->_subCategoriesID = $descendantsID;
				
				// Запрос к ID всех потомков. ActiveRecord не используется
				// для экономии ресурсов.
				$descendants = Yii::app()->db->createCommand()
					->select('id')->from($this->category->tableName())
					->where(
						"({$category->leftAttribute} > :left) AND " .
						"({$category->rightAttribute} < :right)",
						array(
							':left' => $category->{$category->leftAttribute},
							':right' => $category->{$category->rightAttribute}
					));

				// ограничение по максимальной глубине
				if($this->descendantsDepth)
				{
					$maxLevel = $category->{$category->levelAttribute}
						+ $this->descendantsDepth;

					$descendantsResult->where(
						"{$category->levelAttribute} <= :maxLevel",
						array(':maxLevel' => $maxLevel)
					);
				}

				foreach($descendants->queryAll() as $row)
					$this->_subCategoriesID[] = $row['id'];
			}
			
			$descendantsID = $this->_subCategoriesID;
		}

		$model->categories($descendantsID)->published();
	}

	/**
	 * Получение списка используемых дат за указанный период.
	 */
	protected function _initUsedDates()
	{
		$dates = $this->model->extractDates(
			$this->year, $this->month, $this->usedDateField
		);

		$this->_years = $dates['years'];
		$this->_months = $dates['months'];
		$this->_days = $dates['days'];
	}

}

?>
