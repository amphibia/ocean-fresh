<?php

/**
 * Поиск по сайту
 * @title {Search controller}
 * @desc {Search controller}
 */
class SearchController extends Controller
{

    public $layout = '//layouts/structLayout';

    /**
     * Не предобратавает полученные аргументы.
     * @var boolean
     */
    public $_useAutoArguments = true;

    /**
     * Класс рабочей модели.
     * @var string
     */
    public $searchInModels = array(
        'StructPage' => array('title', 'short_text', 'full_text', 'data'),
        'StructCategory' => array('title', 'short_text', 'full_text', 'data')
    );

    public $results = array();

    public function init()
    {
        parent::init();
        Yii::loadExtConfig($this, __FILE__);
    }

    public function actionIndex()
    {
        if (isset($_REQUEST['search'])) {
            $query = htmlspecialchars(trim(strip_tags($_REQUEST['search'])));
            if ($query && count($this->searchInModels)) {
                foreach ($this->searchInModels as $model => $columns) {
                    $model = $model::model()->published();
                    $model->getDbCriteria()->addCondition('in_search = 1', 'AND');
                    $criteria = new CDbCriteria();
                    foreach ($columns as $column) {
                        $criteria->addSearchCondition($column, $query, true, 'OR');
                    }
                    $results = $model->findAll($criteria);

                    $this->results = array_merge($this->results, $results);
                }
            }
        }

        // вывод формы
        $this->render('/search', compact('query'));
    }

}
