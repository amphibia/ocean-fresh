<?php

/**
 * Страница Catalog
 *
 * @title {Catalog}
 */

class CatalogController extends ExtController
{
    public $layout = '//layouts/structLayout';

    public $views = array
    (
        'category' => '/category',
        'page' => '/page',
    );

    public $category = NULL;
    public $pageSize = 1000;

    public function actionIndex($numPage = 1, $ajax = false)
    {
        $category = &Yii::app()->struct->currentCategory;
        //if(!Yii::app()->request->isAjaxRequest) {
        if(!$ajax) {
            $this->render($this->views['category'], compact('category'));
        }else{
            header("Content-type: application/json; charset=utf-8");
            header('Access-Control-Allow-Origin: *');

            if($category->_level == 5) $categoryCatalogLevel1 = $category;
            else $categoryCatalogLevel1 = $category->parent;

            $categories = array();
            if($categoryCatalogLevel1->id == $category->id){
                foreach($categoryCatalogLevel1->children as $children){
                    $categories[$children->id] = $children->id;
                }
            }else{
                $categories[$category->id] = $category->id;
            }

            $model                                  = new StructPage;
            //$model->dbCriteria->order               = 't.date_display desc';
            $model->dbCriteria->join                = 'JOIN struct_category AS cat ON cat.id=t.first_category_index';
            $model->dbCriteria->order               = 'cat._left, t.position';
            $dataProvider                          = $model->published()->categories($categories)->getActiveDataProvider();
            $dataProvider->pagination->pageSize     = $this->pageSize;
            $dataProvider->pagination->currentPage  = $numPage-1;
            $pages                                  = $dataProvider->data;
            $pageCount                              = $dataProvider->pagination->pageCount;
            $pagination                             = $dataProvider->pagination;

            //die("<pre>".print_r($pages, true)."</pre>");

            //type_payment штуки,килограмм,сложный
            $type_payment = array(
                '' => 'items',
                'штуки' => 'items',
                'килограмм' => 'kilogram',
                'сложный' => 'hard',
                'предзаказ' => 'pre_order',
            );

            //Типы разделки
            $typeCutting = array();
            if($categoryCatalogLevel1->short_text) {
                $short_text = str_replace("\r", '', $categoryCatalogLevel1->short_text);
                $filters = explode("\n", $short_text);
                $n = 0;
                foreach ($filters as $item) {

                    $typeCutting[] = array(
                        'id' => 'id'.$categoryCatalogLevel1->id . '-' . $n,
                        'title' => $item,
                    );
                    $n++;
                }
            }

            //$inStock = self::inStock(); //Товары в наличии

            $items = array();
            foreach($pages as $page){
                $imgUrl = '';
                if ($image = Yii::app()->storage->decodeImages($page->img)){
                    $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
                }

                $imgBigUrl = '';
                if ($image = Yii::app()->storage->decodeImages($page->img_big)){
                    $imgBigUrl = Yii::app()->storage->createUrl($image[0], 'original');
                }

                if($page->category->type_payment == 'в товаре') {
                    $page_type_payment = $type_payment[$page->type_payment];
                }else{
                    $page_type_payment = $type_payment[$page->category->type_payment];
                }

                //if(!$page->category->type_payment) $page->category->type_payment = 'штуки';

                $item = array(
                    'id' => (int)$page->id,
                    'image' => $imgUrl,
                    //'imageBig' => $imgBigUrl,
                    //'name' => $page->name,
                    'title' => $page->title,
                    'price' => (int)$page->price,
                    'old_price' => (int)$page->old_price,
                    'refinement' => $page->refinement,
                    //'recipeTitle' => $page->recipe_title,
                    'sidebar_desc' => $page->short_text,
                    //'html' => $page->full_text,
                    //'recipeHref' => $page->createUrl(),
                    'href' => $page->createUrl(),
                    'type_payment' => $page_type_payment,
                    'grams' => (int)$page->grams,
                    'type_not_cart' => ($page_type_payment == 'pre_order')?true:false,
                    //'text_stock' => ((int)$page->old_price)?Yii::t('custom', 'Акции'):'',
                );

                //Значки акции и в наличии
                if(((int)$page->old_price) || $page->in_stock){
                    $badge = array();
                    if((int)$page->old_price){
                        $badge[] = array(
                            'text' => Yii::t('custom', 'Акции'),
                            'color' => 'red'
                        );
                    }
                    /*
                    if(isSet($inStock[$page->id])){
                        $badge[] = array(
                            'text' => Yii::t('custom', 'В наличии'),
                            'color' => 'green'
                        );
                    }
                    */
                    if($page->in_stock){
                        $badge[] = array(
                            'text' => Yii::t('custom', 'В наличии'),
                            'color' => 'green'
                        );
                    }
                    $item['badge'] = $badge;
                }

                $items[] = $item;
            }

            $data = array(
                'numPage' => $numPage,
                'typeCutting' => $typeCutting,
                //'text_stock' => Yii::t('custom', 'Акции'),
                'items' => $items,
            );

            echo json_encode($data);

        }
    }


    public function actionItem($ajax = false)
    {
        $category = &Yii::app()->struct->currentCategory;
        $page = &Yii::app()->struct->currentPage;

        //if(!Yii::app()->request->isAjaxRequest) {
        if(!$ajax) {
                $this->render($this->views['page'], compact('category', 'page'));
        }else{
            header("Content-type: application/json; charset=utf-8");
            header('Access-Control-Allow-Origin: *');

            if($category->_level == 5) $categoryCatalogLevel1 = $category;
            else $categoryCatalogLevel1 = $category->parent;

            //Типы разделки
            $typeCutting = array();
            if($categoryCatalogLevel1->short_text) {
                $short_text = str_replace("\r", '', $categoryCatalogLevel1->short_text);
                $filters = explode("\n", $short_text);
                $n = 0;
                foreach ($filters as $item) {

                    $typeCutting[] = array(
                        'id' => 'id'.$categoryCatalogLevel1->id . '-' . $n,
                        'title' => $item,
                    );
                    $n++;
                }
            }

            $items = array();
            
            $imgUrl = '';
            if ($image = Yii::app()->storage->decodeImages($page->img)){
                $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
            }

            $imgBigUrl = '';
            if ($image = Yii::app()->storage->decodeImages($page->img_big)){
                $imgBigUrl = Yii::app()->storage->createUrl($image[0], 'original');
            }

            //type_payment штуки,килограмм,сложный
            $type_payment = array(
                '' => 'items',
                'штуки' => 'items',
                'килограмм' => 'kilogram',
                'сложный' => 'hard',
                'предзаказ' => 'pre_order',
            );

            if($page->category->type_payment == 'в товаре') {
                $page_type_payment = $type_payment[$page->type_payment];
            }else{
                $page_type_payment = $type_payment[$page->category->type_payment];
            }

            //if(!$page->category->type_payment) $page->category->type_payment = 'штуки';

            //Рекомендованные продукты
            $page->recommended_products = str_replace("\r", '', $page->recommended_products);
            $page->recommended_products =  explode("\n", $page->recommended_products);

            $criteria = new CDbCriteria();
            $criteria->addInCondition('alias', $page->recommended_products);
            $pages = StructPage::model()->published()->findAll($criteria);

            $recommended_products = array();
            if(is_array($pages)) {
                foreach ($pages as $recommended_page) {
                    $recommended_products[] = array(
                        'title' => $recommended_page->title,
                        'href' => $recommended_page->createUrl(),
                    );
                }
            }

            $item = array(
                    'id' => (int)$page->id,
                    'image' => $imgUrl,
                    'imageBig' => $imgBigUrl,
                    'name' => $page->name,
                    'title' => $page->title,
                    'price' => (int)$page->price,
                    'old_price' => (int)$page->old_price,
                    'refinement' => $page->refinement,
                    'recipeTitle' => $page->recipe_title,
                    'sidebar_desc' => $page->short_text,
                    'html' => $page->full_text,
                    'recipeHref' => $page->recipe_href,
                    'type_payment' => $page_type_payment,
                    'grams' => (int)$page->grams,
                    'typeCutting' => $typeCutting,
                    'href' => $page->createUrl(),
                    'type_not_cart' => ($page_type_payment == 'pre_order')?true:false,
                    'recommended_products' => $recommended_products,
                    //'text_stock' => ((int)$page->old_price)?Yii::t('custom', 'Акции'):'',

            );

            //$inStock = self::inStock(); //Товары в наличии
            //Значки акции и в наличии
            if(((int)$page->old_price) || $page->in_stock){
                $badge = array();
                if((int)$page->old_price){
                    $badge[] = array(
                        'text' => Yii::t('custom', 'Акции'),
                        'color' => 'red'
                    );
                }
                /*
                if(isSet($inStock[$page->id])){
                    $badge[] = array(
                        'text' => Yii::t('custom', 'В наличии'),
                        'color' => 'green'
                    );
                }
                */
                if($page->in_stock){
                    $badge[] = array(
                        'text' => Yii::t('custom', 'В наличии'),
                        'color' => 'green'
                    );
                }
                $item['badge'] = $badge;
            }


            $data = array(
                'typeCutting' => $typeCutting,
                'text_stock' => Yii::t('custom', 'Акции'),
                'item' => $item,
            );

            echo json_encode($data);

        }
    }

    /*
     * Подготавливает массив товаров в наличии
     */
    protected function inStock(){
        $in_stock = array();
        $category = Yii::app()->custom->getCategories('in_stock');
        foreach($category->children as $children){
            foreach($children->pages as $page){
                $in_stock[$page->id] = $page->title;
            }
        }
        return $in_stock;
    }

}