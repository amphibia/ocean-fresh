<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword html
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @param self single_image terms_img "Сроки доставки"
 * @param self full_textarea terms_list "Сроки доставки"
 *
 * @param self tiny_mce table "Таблица"

 * @param self single_image areas_img "Районы доставки"
 * @param self full_textarea areas_list "Районы доставки"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>


<!-- .content -->
<main class="content">
    <div class="content-in fixed-width content-centered delivery-page">
        <div class="content-section">
            <h1><?= $category->title ?></h1>
            <?= $category->short_text ?>
        </div>
        <div class="content-section">
            <div class="section-title">
                <h3 class="title-3 title-icon">
                    <?php if ($image = Yii::app()->storage->decodeImages($category->terms_img)): ?>
                        <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>"/>
                        <span><?= $image[0]['title'] ?></span>
                    <?php endif ?>
                </h3>
            </div>
            <div class="delivery-list">
                <div class="delivery-list-in">
                    <?php
                    $list = str_replace("\r", '', $category->terms_list);
                    $list =  explode("\n", $list);
                    $n = 1;
                    foreach($list as $item):
                    ?>
                    <div class="delivery-item">
                        <div class="item-title"><?= $n ?></div>
                        <div class="item-meta"><?= $item ?></div>
                    </div>
                    <?php $n++; endforeach ?>
                </div>
            </div>
            <div class="table-delivery">
                <?= $category->table ?>
            </div>
            <div class="section-title">
                <h3 class="title-3 title-icon">
                    <?php if ($image = Yii::app()->storage->decodeImages($category->areas_img)): ?>
                        <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>"/>
                        <span><?= $image[0]['title'] ?></span>
                    <?php endif ?>
                </h3>
            </div>
            <div class="delivery-list">
                <div class="delivery-list-in">
                    <?php
                    $list = str_replace("\r", '', $category->areas_list);
                    $list =  explode("\n", $list);
                    $n = 1;
                    foreach($list as $item):
                    ?>
                    <div class="delivery-item">
                        <div class="item-title"><?= $n ?></div>
                        <div class="item-meta"><?= $item ?></div>
                    </div>
                    <?php $n++; endforeach ?>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->