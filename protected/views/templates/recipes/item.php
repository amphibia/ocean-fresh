<!-- .content -->
<main class="content">
    <?php
    $top_img = '/dist/images/samples/top-banner-pic.jpg';
    if ($image = Yii::app()->storage->decodeImages($page->top_img)) $top_img = Yii::app()->storage->createUrl($image[0], 'original');
    ?>

    <div class="top-banner" style="background-image: url(<?= $top_img ?>)">
        <div class="top-banner-title"> <b><?= nl2br($page->short_text) ?></b> </div>
        <div class="top-banner-info">
            <div class="info-item">
                <div class="info-icon"> <i class="icon icon-peoples"></i> </div> <small><?= $page->persons ?></small> </div>
            <div class="info-item">
                <div class="info-icon"> <i class="icon icon-desc"></i> </div> <small><?= Yii::t('custom', 'Подготовка:') ?> <?= $page->preparation2 ?></small> </div>
            <div class="info-item">
                <div class="info-icon"> <i class="icon icon-timer"></i> </div> <small><?= Yii::t('custom', 'Приготовление:') ?> <?= $page->preparation ?></small> </div>
        </div>
    </div>
    <section class="section section-recipes-inside">
        <div class="recipes-recomend-items">
            <?php if ($images = Yii::app()->storage->decodeImages($page->gallery)): ?>
                <?php foreach($images as $img): ?>
                    <div class="recomend-item">
                        <div class="recomend-img"> <img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt=""> </div>
                        <div class="recomend-text"> <small><?= $img['title'] ?></small> <strong><?= $img['description'] ?></strong> </div>
                    </div>

                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div class="m-content-sidebar">
            <div class="content-sidebar-title">
                <h3><?= Yii::t('custom', 'Ингредиенты:') ?></h3>
                <i class="icon icon-book"></i>
            </div>
            <ul>
                <?php
                $ingredients = str_replace("\r", '', $page->ingredients);
                $ingredients =  explode("\n", $ingredients);
                foreach($ingredients as $item):
                ?>
                <li><b><?= $item ?></b></li>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="content-in content-aside">
            <div class="content-main">
                <div class="content-main-title">
                    <h2><?= Yii::t('custom', 'Способ приготовления:') ?></h2>
                </div>
                <div class="content-items">

                    <?php if(trim($page->step1_title)): ?>
                        <div class="content-section">
                            <?php if ($image = Yii::app()->storage->decodeImages($page->step1_img)): ?>
                                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                            <?php endif ?>
                            <h3><?= $page->step1_title ?></h3>
                            <?= $page->step1_text ?>
                        </div>
                    <?php endif ?>

                    <?php if(trim($page->step2_title)): ?>
                        <div class="content-section">
                            <?php if ($image = Yii::app()->storage->decodeImages($page->step2_img)): ?>
                                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                            <?php endif ?>
                            <h3><?= $page->step2_title ?></h3>
                            <?= $page->step2_text ?>
                        </div>
                    <?php endif ?>

                    <?php if(trim($page->step3_title)): ?>
                        <div class="content-section">
                            <?php if ($image = Yii::app()->storage->decodeImages($page->step3_img)): ?>
                                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                            <?php endif ?>
                            <h3><?= $page->step3_title ?></h3>
                            <?= $page->step3_text ?>
                        </div>
                    <?php endif ?>

                    <?php if(trim($page->step4_title)): ?>
                        <div class="content-section">
                            <?php if ($image = Yii::app()->storage->decodeImages($page->step4_img)): ?>
                                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                            <?php endif ?>
                            <h3><?= $page->step4_title ?></h3>
                            <?= $page->step4_text ?>
                        </div>
                    <?php endif ?>

                    <?php if(trim($page->step5_title)): ?>
                        <div class="content-section">
                            <?php if ($image = Yii::app()->storage->decodeImages($page->step5_img)): ?>
                                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                            <?php endif ?>
                            <h3><?= $page->step5_title ?></h3>
                            <?= $page->step5_text ?>
                        </div>
                    <?php endif ?>

                    <?php if(trim($page->step6_title)): ?>
                        <div class="content-section">
                            <?php if ($image = Yii::app()->storage->decodeImages($page->step6_img)): ?>
                                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                            <?php endif ?>
                            <h3><?= $page->step6_title ?></h3>
                            <?= $page->step6_text ?>
                        </div>
                    <?php endif ?>

                    <?php if(trim($page->step7_title)): ?>
                        <div class="content-section">
                            <?php if ($image = Yii::app()->storage->decodeImages($page->step7_img)): ?>
                                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="">
                            <?php endif ?>
                            <h3><?= $page->step7_title ?></h3>
                            <?= $page->step7_text ?>
                        </div>
                    <?php endif ?>

                </div>
                <div class="recipes-inside-btn-back"> <a href="<?= $category->createUrl(); ?>" class="btn btn-success btn-back"><?= Yii::t('custom', 'Назад к заказу') ?></a> </div>
                <div class="recipes-inside-social-links">
                    <div class="social-links">
                        <div class="social-title"> <b><?= Yii::t('custom', 'Поделись с друзьями:') ?></b> </div>
                        <div class="likely">
                            <div class="twitter"><?= Yii::t('custom', 'Твитнуть') ?></div>
                            <div class="facebook"><?= Yii::t('custom', 'Поделиться') ?></div>
                            <div class="vkontakte"><?= Yii::t('custom', 'Поделиться') ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="recipes-more">
                <h2><?= Yii::t('custom', 'Другие рецепты') ?></h2>

                <div class="resipe-list-smaller">
                    <?php $n=0; foreach($category->pages as $nextPage):
                        if($nextPage->id == $page->id) continue;
                        if($n > 2) break;
                            ?>
                    <a href="recipe-inside.html" class="recipe-item-card">
                        <?php if ($image = Yii::app()->storage->decodeImages($nextPage->img)): ?>
                            <div class="recipe-item-card__pic" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                        <?php endif ?>
                        <div class="recipe-item-card__content">
                            <div class="recipe-item-card__title"><?= $nextPage->title ?></div>
                            <div class="recipe-item-card__info"><i class="icon icon-timer"></i><?= Yii::t('custom', 'Приготовление:') ?> <?= $nextPage->preparation ?></div>
                        </div>
                    </a>
                    <?php $n++; endforeach ?>
                </div>
            </div>
            <div class="content-sidebar" data-stycky="">
                <div class="content-sidebar-title">
                    <h3><?= Yii::t('custom', 'Ингредиенты:') ?></h3>
                    <i class="icon icon-book"></i>
                </div>
                <ul>
                    <?php
                    $ingredients = str_replace("\r", '', $page->ingredients);
                    $ingredients =  explode("\n", $ingredients);
                    foreach($ingredients as $item):
                    ?>
                    <li><b><?= $item ?></b></li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </section>
    <div class="overlay"></div>
</main>
<!-- end .content -->
