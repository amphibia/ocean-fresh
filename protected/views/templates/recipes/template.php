<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery on
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 *
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages text preparation "Приготовление"
 * @param pages text persons " Персоны"
 * @param pages text preparation2 "Подготовка"
 * @param pages single_image top_img "Иллюстрация в шапке"
 *
 * @param pages single_image step1_img "Шаг 1. Иллюстрация"
 * @param pages text step1_title "Шаг 1. Заголовок"
 * @param pages tiny_mce step1_text "Шаг 1. Текст"

 * @param pages single_image step2_img "Шаг 2. Иллюстрация"
 * @param pages text step2_title "Шаг 2. Заголовок"
 * @param pages tiny_mce step2_text "Шаг 2. Текст"

 * @param pages single_image step3_img "Шаг 3. Иллюстрация"
 * @param pages text step3_title "Шаг 3. Заголовок"
 * @param pages tiny_mce step3_text "Шаг 3. Текст"

 * @param pages single_image step4_img "Шаг 4. Иллюстрация"
 * @param pages text step4_title "Шаг 4. Заголовок"
 * @param pages tiny_mce step4_text "Шаг 4. Текст"

 * @param pages single_image step5_img "Шаг 5. Иллюстрация"
 * @param pages text step5_title "Шаг 5. Заголовок"
 * @param pages tiny_mce step5_text "Шаг 5. Текст"

 * @param pages single_image step6_img "Шаг 6. Иллюстрация"
 * @param pages text step6_title "Шаг 6. Заголовок"
 * @param pages tiny_mce step6_text "Шаг 6. Текст"

 * @param pages single_image step7_img "Шаг 7. Иллюстрация"
 * @param pages text step7_title "Шаг 7. Заголовок"
 * @param pages tiny_mce step7_text "Шаг 7. Текст"
 *
 * @param pages full_textarea ingredients "Ингредиенты"

 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

if($category->_level == 4) $categoryLevel1 = $category;
else $categoryLevel1 = $category->parent;

$countAllPages = 0;
$filtrMenu = array();
foreach($categoryLevel1->children as $children){
    $filtrMenu[$children->title] = array(
        'id' => $children->id,
        'url' => $children->createUrl(),
        'countPages' => count($children->pages),
    );
    $countAllPages += $filtrMenu[$children->title]['countPages'];
}

?>

<!-- .content -->
<main class="content">
    <div class="app" id="app">
        <div class="catalog-navbar">
            <ul class="catalog-breadcrumbs">
                <?= Yii::app()->custom->breadCramps(
                    array(
                        'showMain' => true,
                        'showLatestAlias' => true,
                        'startCategoryAlias' => 'root',
                        //'startCategoryTechId' => 'root',
                    ));
                ?>
            </ul>
            <ul class="catalog-menu">
                <li class="catalog-menu-item<?= ($category->id == $categoryLevel1->id)?' active':'' ?>"><a href="<?= $categoryLevel1->createUrl() ?>"><?= Yii::t('custom', 'Все') ?></a><span class="badge"><?= $countAllPages ?></span></li>
                <?php foreach($filtrMenu as $title => $value): ?>
                    <li class="catalog-menu-item<?= ($category->id == $value['id'])?' active':'' ?>"><a href="<?= $value['url'] ?>"><?= $title ?></a><span class="badge"><?= $value['countPages'] ?></span></li>
                <?php endforeach ?>
            </ul>

            <a href="cart.html" class="navbar-cart-info">
                <i class="icon icon-cart"></i>
                <total-cart-count></total-cart-count>
            </a> </div>
        <div class="recipes-list">
            <?php if($category->id == $categoryLevel1->id): //Выводим материалы подкатегорий
                foreach($categoryLevel1->children as $category):
                    ?>
                    <?php foreach($category->pages as $page): ?>
                    <a href="<?= $page->createUrl() ?>" class="recipe-item-card">
                        <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                            <div class="recipe-item-card__pic" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                        <?php endif ?>
                        <div class="recipe-item-card__content">
                            <div class="recipe-item-card__title"><?= $page->title ?></div>
                            <div class="recipe-item-card__info"><i class="icon icon-timer"></i> <?= Yii::t('custom', 'Приготовление:') ?> <?= $page->preparation ?></div>
                        </div>
                    </a>
                    <?php endforeach ?>
                <?php endforeach ?>
            <?php else: //Выводим материалы данной категории ?>
                <?php foreach($category->pages as $page): ?>
                    <a href="<?= $page->createUrl() ?>" class="recipe-item-card">
                        <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                            <div class="recipe-item-card__pic" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)"></div>
                        <?php endif ?>
                        <div class="recipe-item-card__content">
                            <div class="recipe-item-card__title"><?= $page->title ?></div>
                            <div class="recipe-item-card__info"><i class="icon icon-timer"></i> <?= Yii::t('custom', 'Приготовление:') ?> <?= $page->preparation ?></div>
                        </div>
                    </a>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->
