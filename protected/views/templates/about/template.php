<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery on
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content html
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self text title2 "Заголовок 2"
 * @param self single_image img "Иллюстрация к статье"
 * @param self text clients_title "Наши клиенты"
 * @param self gallery clients "Наши клиенты"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>

<!-- .content -->
<main class="content">
    <div class="content-in fixed-width content-centered">
        <div class="content-section">
            <h1><?= $category->title ?></h1>
            <?php if ($image = Yii::app()->storage->decodeImages($category->img)): ?>
                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
            <?php endif ?>
        </div>
        <div class="content-section">
            <h3><?= $category->title2 ?></h3>
            <p><b><?= nl2br($category->short_text) ?></b></p>
            <div class="items-list">
                <div class="row">
                    <?php if ($images = Yii::app()->storage->decodeImages($category->gallery)): ?>
                        <?php foreach($images as $img): ?>
                            <div class="col-lg-4">
                                <div class="item">
                                    <div class="item-list-head"> <span class="pic">
                                    <img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt="">
                                </span> </div>
                                    <div class="item-list-text"> <b><?= $img['title'] ?></b> </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="content-section">
            <?= $category->full_text ?>
        </div>

        <div class="content-section section-partners">
            <h3><?= $category->clients_title ?></h3>
            <div class="list-partners">
                <?php if ($images = Yii::app()->storage->decodeImages($category->clients)): ?>
                    <?php foreach($images as $img): ?>
                        <a href="<?= $img['description'] ?>" class="item-partner"><img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt="<?= $img['title'] ?>"></a>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>


        <div class="overlay"></div>
</main>
<!-- end .content -->

