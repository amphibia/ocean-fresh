<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery on
 * @option self foreword none
 * @option self content html
 *
 * @param self full_textarea code_head "Код в HEAD"
 * @param self full_textarea code_before_end_body "Код в конце BODY"
 *
 * @param self text phone "Телефон"
 * @param self text address "Адрес"
 * @param self text facebook "facebook"
 * @param self text instagram "instagram"

 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword text
 * @option children content html
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages content none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages text link "Ссылка"

 */

?>

<!-- .content -->
<main class="content">
    <div class="promo-slider">

        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach($category->pages as $page): ?>
                    <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                        <div class="swiper-slide" style="background-image: url(<?= Yii::app()->storage->createUrl($image[0], 'original') ?>)">
                            <div class="promo-slider-text">
                                <h2><?= nl2br($page->short_text) ?></h2> <a href="<?= $page->link ?>" class="btn btn-success"><?= Yii::t('custom', 'Подробнее') ?></a>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
        </div>
        <div class="swiper-controls">
            <div class="swiper-action-btn swiper-prev"><i class="icon icon-arrow-left"></i></div>
            <div class="swiper-action-btn swiper-next"><i class="icon icon-arrow-right"></i></div>
            <div class="swiper-pagination"></div>
        </div>
        <div class="promo-menu">
            <ul class="list-unstyled">
                <?php
                $categoryCatalog = Yii::app()->custom->getCategories('catalog');
                foreach($categoryCatalog->children as $children):
                    if(!$children->show_main) continue;
                ?>
                <li class="menu-item">
                    <a href="<?= $children->createUrl() ?>">
                        <div class="item-head"> <span class="item-title"><?= $children->title ?></span>
                            <div class="item-pic">
                                <?php if ($image = Yii::app()->storage->decodeImages($children->img_main1)): ?>
                                    <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="" class="pic">
                                <?php endif ?>
                                <?php if ($image = Yii::app()->storage->decodeImages($children->img_main2)): ?>
                                    <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt="" class="pic">
                                <?php endif ?>
                            </div>
                        </div> <span class="item-get"><?= Yii::t('custom', 'Перейти в каталог') ?></span> </a>
                </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->