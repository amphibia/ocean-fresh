<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self single_image img "Иллюстрация к статье"
 * @param self text title1 "Заголовок 1"
 * @param self tiny_mce text1 "Текст 1"
 * @param self text title2 "Заголовок 2"
 * @param self tiny_mce text2 "Текст 2"
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages dropdownlist region "Регион" "Afghanistan,Albania,Algeria,American Samoa,Andorra,Angola,Anguilla,Antarctica,Antigua and Barbuda,Argentina,Armenia,Aruba,Australia,Austria,Azerbaijan,Bahamas,Bahrain,Bangladesh,Barbados,Belarus,Belgium,Belize,Benin,Bermuda,Bhutan,Bolivia Plurinational State of,Bonaire Sint Eustatius and Saba,Bosnia and Herzegovina,Botswana,Bouvet Island,Brazil,British Indian Ocean Territory,Brunei Darussalam,Bulgaria,Burkina Faso,Burundi,Cabo Verde,Cambodia,Cameroon,Canada,Cayman Islands,Central African Republic,Chad,Chile,China,Christmas Island,Cocos (Keeling) Islands,Colombia,Comoros,Congo,Congo the Democratic Republic of the,Cook Islands,Costa Rica,Croatia,Cuba,Curaçao,Cyprus,Czech Republic,Côte d'Ivoire,Denmark,Djibouti,Dominica,Dominican Republic,Ecuador,Egypt,El Salvador,Equatorial Guinea,Eritrea,Estonia,Ethiopia,Falkland Islands (Malvinas),Faroe Islands,Fiji,Finland,France,French Guiana,French Polynesia,French Southern Territories,Gabon,Gambia,Georgia,Germany,Ghana,Gibraltar,Greece,Greenland,Grenada,Guadeloupe,Guam,Guatemala,Guernsey,Guinea,Guinea-Bissau,Guyana,Haiti,Heard Island and McDonald Islands,Holy See,Honduras,Hong Kong,Hungary,Iceland,India,Indonesia,Iran Islamic Republic of,Iraq,Ireland,Isle of Man,Israel,Italy,Jamaica,Japan,Jersey,Jordan,Kazakhstan,Kenya,Kiribati,Korea Democratic People's Republic of,Korea Republic of,Kuwait,Kyrgyzstan,Lao People's Democratic Republic,Latvia,Lebanon,Lesotho,Liberia,Libya,Liechtenstein,Lithuania,Luxembourg,Macao,Macedonia the former Yugoslav Republic of,Madagascar,Malawi,Malaysia,Maldives,Mali,Malta,Marshall Islands,Martinique,Mauritania,Mauritius,Mayotte,Mexico,Micronesia Federated States of,Moldova Republic of,Monaco,Mongolia,Montenegro,Montserrat,Morocco,Mozambique,Myanmar,Namibia,Nauru,Nepal,Netherlands,New Caledonia,New Zealand,Nicaragua,Niger,Nigeria,Niue,Norfolk Island,Northern Mariana Islands,Norway,Oman,Pakistan,Palau,Palestine State of,Panama,Papua New Guinea,Paraguay,Peru,Philippines,Pitcairn,Poland,Portugal,Puerto Rico,Qatar,Romania,Russian Federation,Rwanda,Réunion,Saint Barthélemy,Saint Helena Ascension and Tristan da Cunha,Saint Kitts and Nevis,Saint Lucia,Saint Martin (French part),Saint Pierre and Miquelon,Saint Vincent and the Grenadines,Samoa,San Marino,Sao Tome and Principe,Saudi Arabia,Senegal,Serbia,Seychelles,Sierra Leone,Singapore,Sint Maarten (Dutch part),Slovakia,Slovenia,Solomon Islands,Somalia,South Africa,South Georgia and the South Sandwich Islands,South Sudan,Spain,Sri Lanka,Sudan,Suriname,Svalbard and Jan Mayen,Swaziland,Sweden,Switzerland,Syrian Arab Republic,Taiwan Province of China,Tajikistan,Tanzania United Republic of,Thailand,Timor-Leste,Togo,Tokelau,Tonga,Trinidad and Tobago,Tunisia,Turkey,Turkmenistan,Turks and Caicos Islands,Tuvalu,Uganda,Ukraine,United Arab Emirates,United Kingdom of Great Britain and Northern Ireland,United States Minor Outlying Islands,United States of America,Uruguay,Uzbekistan,Vanuatu,Venezuela Bolivarian Republic of,Viet Nam,Virgin Islands British,Virgin Islands U.S.,Wallis and Futuna,Western Sahara,Yemen,Zambia,Zimbabwe"
 * @param pages text title1 "Заголовок 1"
 *
 * @param pages single_image img2 "Иллюстрация 2"
 * @param pages text title2 "Заголовок 2"
 * @param pages tiny_mce text2 "Текст 2"

 * @param pages single_image img3 "Иллюстрация 3"
 * @param pages text title3 "Заголовок 3"
 * @param pages tiny_mce text3 "Текст 3"

 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */
/*
$iso = file_get_contents('https://gist.githubusercontent.com/doublecompile/3ff937a22626113747fb/raw/482865c09c15304cf0bffd6a69a9a730d94d7ed0/countries.json');
$iso = json_decode($iso);

function mySort($a, $b)
{
    if ($a->name == $b->name) {
        return 0;
    }
    return ($a->name < $b->name) ? -1 : 1;
}
usort($iso->countries, 'mySort');
//die("<pre>".print_r($iso, true)."</pre>");
*/
/*
$iso2 = array();
foreach($iso->countries as $val){
    $name = str_replace(',', '', $val->name);
    $iso2[$name] = strtolower($val->code);
}
$iso = json_encode($iso2);
die($iso);
*/

$iso = '{"Afghanistan":"af","Albania":"al","Algeria":"dz","American Samoa":"as","Andorra":"ad","Angola":"ao","Anguilla":"ai","Antarctica":"aq","Antigua and Barbuda":"ag","Argentina":"ar","Armenia":"am","Aruba":"aw","Australia":"au","Austria":"at","Azerbaijan":"az","Bahamas":"bs","Bahrain":"bh","Bangladesh":"bd","Barbados":"bb","Belarus":"by","Belgium":"be","Belize":"bz","Benin":"bj","Bermuda":"bm","Bhutan":"bt","Bolivia Plurinational State of":"bo","Bonaire Sint Eustatius and Saba":"bq","Bosnia and Herzegovina":"ba","Botswana":"bw","Bouvet Island":"bv","Brazil":"br","British Indian Ocean Territory":"io","Brunei Darussalam":"bn","Bulgaria":"bg","Burkina Faso":"bf","Burundi":"bi","Cabo Verde":"cv","Cambodia":"kh","Cameroon":"cm","Canada":"ca","Cayman Islands":"ky","Central African Republic":"cf","Chad":"td","Chile":"cl","China":"cn","Christmas Island":"cx","Cocos (Keeling) Islands":"cc","Colombia":"co","Comoros":"km","Congo":"cg","Congo the Democratic Republic of the":"cd","Cook Islands":"ck","Costa Rica":"cr","Croatia":"hr","Cuba":"cu","Cura\u00e7ao":"cw","Cyprus":"cy","Czech Republic":"cz","C\u00f4te d\'Ivoire":"ci","Denmark":"dk","Djibouti":"dj","Dominica":"dm","Dominican Republic":"do","Ecuador":"ec","Egypt":"eg","El Salvador":"sv","Equatorial Guinea":"gq","Eritrea":"er","Estonia":"ee","Ethiopia":"et","Falkland Islands (Malvinas)":"fk","Faroe Islands":"fo","Fiji":"fj","Finland":"fi","France":"fr","French Guiana":"gf","French Polynesia":"pf","French Southern Territories":"tf","Gabon":"ga","Gambia":"gm","Georgia":"ge","Germany":"de","Ghana":"gh","Gibraltar":"gi","Greece":"gr","Greenland":"gl","Grenada":"gd","Guadeloupe":"gp","Guam":"gu","Guatemala":"gt","Guernsey":"gg","Guinea":"gn","Guinea-Bissau":"gw","Guyana":"gy","Haiti":"ht","Heard Island and McDonald Islands":"hm","Holy See":"va","Honduras":"hn","Hong Kong":"hk","Hungary":"hu","Iceland":"is","India":"in","Indonesia":"id","Iran Islamic Republic of":"ir","Iraq":"iq","Ireland":"ie","Isle of Man":"im","Israel":"il","Italy":"it","Jamaica":"jm","Japan":"jp","Jersey":"je","Jordan":"jo","Kazakhstan":"kz","Kenya":"ke","Kiribati":"ki","Korea Democratic People\'s Republic of":"kp","Korea Republic of":"kr","Kuwait":"kw","Kyrgyzstan":"kg","Lao People\'s Democratic Republic":"la","Latvia":"lv","Lebanon":"lb","Lesotho":"ls","Liberia":"lr","Libya":"ly","Liechtenstein":"li","Lithuania":"lt","Luxembourg":"lu","Macao":"mo","Macedonia the former Yugoslav Republic of":"mk","Madagascar":"mg","Malawi":"mw","Malaysia":"my","Maldives":"mv","Mali":"ml","Malta":"mt","Marshall Islands":"mh","Martinique":"mq","Mauritania":"mr","Mauritius":"mu","Mayotte":"yt","Mexico":"mx","Micronesia Federated States of":"fm","Moldova Republic of":"md","Monaco":"mc","Mongolia":"mn","Montenegro":"me","Montserrat":"ms","Morocco":"ma","Mozambique":"mz","Myanmar":"mm","Namibia":"na","Nauru":"nr","Nepal":"np","Netherlands":"nl","New Caledonia":"nc","New Zealand":"nz","Nicaragua":"ni","Niger":"ne","Nigeria":"ng","Niue":"nu","Norfolk Island":"nf","Northern Mariana Islands":"mp","Norway":"no","Oman":"om","Pakistan":"pk","Palau":"pw","Palestine State of":"ps","Panama":"pa","Papua New Guinea":"pg","Paraguay":"py","Peru":"pe","Philippines":"ph","Pitcairn":"pn","Poland":"pl","Portugal":"pt","Puerto Rico":"pr","Qatar":"qa","Romania":"ro","Russian Federation":"ru","Rwanda":"rw","R\u00e9union":"re","Saint Barth\u00e9lemy":"bl","Saint Helena Ascension and Tristan da Cunha":"sh","Saint Kitts and Nevis":"kn","Saint Lucia":"lc","Saint Martin (French part)":"mf","Saint Pierre and Miquelon":"pm","Saint Vincent and the Grenadines":"vc","Samoa":"ws","San Marino":"sm","Sao Tome and Principe":"st","Saudi Arabia":"sa","Senegal":"sn","Serbia":"rs","Seychelles":"sc","Sierra Leone":"sl","Singapore":"sg","Sint Maarten (Dutch part)":"sx","Slovakia":"sk","Slovenia":"si","Solomon Islands":"sb","Somalia":"so","South Africa":"za","South Georgia and the South Sandwich Islands":"gs","South Sudan":"ss","Spain":"es","Sri Lanka":"lk","Sudan":"sd","Suriname":"sr","Svalbard and Jan Mayen":"sj","Swaziland":"sz","Sweden":"se","Switzerland":"ch","Syrian Arab Republic":"sy","Taiwan Province of China":"tw","Tajikistan":"tj","Tanzania United Republic of":"tz","Thailand":"th","Timor-Leste":"tl","Togo":"tg","Tokelau":"tk","Tonga":"to","Trinidad and Tobago":"tt","Tunisia":"tn","Turkey":"tr","Turkmenistan":"tm","Turks and Caicos Islands":"tc","Tuvalu":"tv","Uganda":"ug","Ukraine":"ua","United Arab Emirates":"ae","United Kingdom of Great Britain and Northern Ireland":"gb","United States Minor Outlying Islands":"um","United States of America":"us","Uruguay":"uy","Uzbekistan":"uz","Vanuatu":"vu","Venezuela Bolivarian Republic of":"ve","Viet Nam":"vn","Virgin Islands British":"vg","Virgin Islands U.S.":"vi","Wallis and Futuna":"wf","Western Sahara":"eh","Yemen":"ye","Zambia":"zm","Zimbabwe":"zw"}';
$iso = json_decode($iso);
//die("<pre>".print_r($iso, true)."</pre>");

/*
$list = '';
foreach($iso as $key => $val){
    if($list) $list .= ','.$key;
    else $list = $key;
}
die($list);
*/

$regions = '';
foreach($category->pages as $page){
    if($regions) $regions .= ",'".$iso->{$page->region}."'";
    else $regions = "'".$iso->{$page->region}."'";
}
//die($regions);
?>

<!-- .content -->
<main class="content">
    <div class="about-map-section">
        <div class="about-section-content">
            <h2><?= $category->title ?></h2>
            <p><?= nl2br($category->short_text) ?></p>
        </div>
        <div class="m-about-map">
            
            <?php foreach($category->pages as $page): ?>
                <div class="aboute-map-item">
                    <b><?= $page->title ?></b>
                    <p><?= nl2br($page->short_text) ?></p>
                    <a href="<?= $page->createUrl() ?>" class="country-info-modal__link"><?= Yii::t('custom', 'ПОДРОБНЕЕ') ?></a>
                </div>
            <?php endforeach ?>

            <!-- <div class="aboute-map-item">   
                <b><?= $category->title1 ?></b>
                <?= $category->text1 ?>
            </div>
            <div class="aboute-map-item">
                <b><?= $category->title2 ?></b>
                <?= $category->text2 ?>
            </div> -->
        </div>
        <div id="about-map" style="width: 100%;height:650px;" data-region="<?= $regions ?>">
            <?php foreach($category->pages as $page): ?>
            <div class="popover-about-map popover-about__<?= $iso->{$page->region} ?>">
                <?php if ($image = Yii::app()->storage->decodeImages($page->img)): ?>
                    <div class="pic-popover"><img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>"></div>
                <?php endif ?>
                <b><?= $page->title ?></b>
                <p><?= nl2br($page->short_text) ?></p>
                <a href="<?= $page->createUrl() ?>" class="country-info-modal__link"><?= Yii::t('custom', 'ПОДРОБНЕЕ') ?></a>
            </div>
            <?php endforeach ?>
        </div>
        <div class="about-map-modal"> <span class="hide-modal"><i class="icon icon-close"></i></span>
            <div class="scroll-block">
                <div class="content-in fixed-width content-centered">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->


