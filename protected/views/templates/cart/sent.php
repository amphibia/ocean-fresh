<?php
$categoryCatalog = Yii::app()->custom->getCategories('catalog');
?>
<main class="content">
    <div class="catalog">
        <div class="catalog-navbar fixed"><a href="cart.html" class="navbar-cart-info"><i class="icon icon-cart"></i>
                <span class="total-cart-count">0</span></a></div>
        <div class="cart" style="padding-top: 100px;">
            <div class="cart-send">
                <div class="text-center">
                    <?php if (isSet($_REQUEST['order']) && $_REQUEST['res_code'] == 0): ?>
                        <div class="section-title"><h1 class="title-1"><?= Yii::t('custom', 'Заказ отправлен') ?> <br> № <?= $_REQUEST['order'] ?></h1></div>
                        <div class="section-text"><p><?= Yii::t('custom', 'Наши операторы перезвонят вам в течение нескольких минут.') ?></p></div>
                        <a href="<?= $categoryCatalog->createUrl() ?>" class="btn btn-success"><?= Yii::t('custom', 'Заказать еще') ?></a></div>
                    <?php elseif(isSet($_REQUEST['cash'])): ?>
                        <div class="section-title"><h1 class="title-1"><?= Yii::t('custom', 'Заказ отправлен') ?></h1></div>
                        <div class="section-text"><p><?= Yii::t('custom', 'Наши операторы перезвонят вам в течение нескольких минут.') ?></p></div>
                        <a href="<?= $categoryCatalog->createUrl() ?>" class="btn btn-success"><?= Yii::t('custom', 'Заказать еще') ?></a></div>
                    <?php else: ?>
                        <div class="section-title"><h1 class="title-1"><?= Yii::t('custom', 'Ошибка!') ?></h1></div>
                    <?php endif ?>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
</main>
