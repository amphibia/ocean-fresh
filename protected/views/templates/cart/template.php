<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @param self single_image img_icon1 "Иконка 1"
 * @param self text title1 "Заголовок 1"
 * @param self tiny_mce text1 "Текст 1"
 *
 * @param self single_image img_icon2 "Иконка 2"
 * @param self text title2 "Заголовок 2"
 * @param self tiny_mce text2 "Текст 2"

 * @param self text price_delivery "Цена доставки"
 * @param self text limit_order "Лимит заказа"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>


<!-- .content -->
<main class="content">
    <div class="app" id="app">
        <div class="catalog">
            <div class="loader" v-if="loading"></div>
            <div class="catalog-navbar">
                <ul class="catalog-breadcrumbs">
                    <?= Yii::app()->custom->breadCramps(
                        array(
                            'showMain' => true,
                            'showLatestAlias' => true,
                            'startCategoryAlias' => 'root',
                            //'startCategoryTechId' => 'root',
                        ));
                    ?>
                </ul> <a href="#" class="navbar-cart-info">
                    <i class="icon icon-cart"></i>
                    <total-cart-count></total-cart-count>
                </a> </div>
            <div class="cart">
                <div class="shopping-cart">
                    <cart></cart>

                    <div class="cart-info">
                        <div class="cart-info-item">
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img_icon1)): ?>
                                <div class="cart-info-pic"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                            <?php endif ?>
                            <?php if(trim($category->title1)): ?>
                            <p><b><?= $category->title1 ?></b></p>
                            <?php endif ?>
                            <?= $category->text1 ?>
                        </div>
                        <div class="cart-info-item">
                            <?php if ($image = Yii::app()->storage->decodeImages($category->img_icon2)): ?>
                                <div class="cart-info-pic"> <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" alt=""> </div>
                            <?php endif ?>
                            <?php if(trim($category->title2)): ?>
                                <p><b><?= $category->title2 ?></b></p>
                            <?php endif ?>
                            <?= $category->text2 ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->

