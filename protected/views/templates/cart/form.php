<?php
$formModel = new OrderForm;
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,

    'action' => "/cart/send",

    'clientOptions' => array(
        'validateOnChange' => true,
        'validateOnSubmit' => true,
        'afterValidate' => 'js:formSend',
    ),

    'htmlOptions' => array(
        //'id' => 'call-order-form2',
        //'class' => 'clearfix'
    ),

));
?>

<div class="checkout-block">
    <!----><span class="total-sum">Итого: 499 ₸</span>
    <div class="checkout-form">
        <h2 class="checkout-form-title">Форма для заказа</h2>
        <p class="checkout-form-desc"><span class="required">*</span>Обязательно для заполнения</p>
        <div class="form-group">
            <div class="form-control-item">
                <?= $form->textField($formModel, 'fio', array(
                    'placeholder' => $formModel->getAttributeLabel('fio'),
                    'class' => 'form-control input',
                    'aria-required' => 'true',
                    'aria-invalid' => 'false',
                )); ?>
                <?= $form->error($formModel, 'fio'); ?>
            </div>
            <div class="form-control-item">
                <?= $form->textField($formModel, 'phone', array(
                    'placeholder' => $formModel->getAttributeLabel('phone'),
                    'class' => 'form-control input',
                    'aria-required' => 'true',
                    'aria-invalid' => 'false',
                )); ?>
                <?= $form->error($formModel, 'phone'); ?>
            </div>
            <div class="form-control-item">
                <?= $form->textField($formModel, 'email', array(
                    'placeholder' => $formModel->getAttributeLabel('email'),
                    'class' => 'form-control input',
                    'aria-required' => 'true',
                    'aria-invalid' => 'false',
                )); ?>
            </div>
            <div class="form-control-item">
                <div class="radio"><input name="OrderForm[delivery_type]" id="delivery-type-1" type="radio" value="delivery"><label
                        for="delivery-type-1">Доставка</label>
                </div>
                <div class="radio"><input name="OrderForm[delivery_type]" id="delivery-type-2" type="radio" value="pickup"><label
                        for="delivery-type-2">Самовывоз</label>
                </div>
            </div><!---->
            <div class="form-control-item">
                <button class="btn btn-success btn-block">оформить заказ</button>
            </div>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
<?php exit ?>


