<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self single_image img "Иллюстрация к статье"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

if (!Yii::app()->request->isAjaxRequest)
    echo CHtml::tag('div', array('class' => 'text'), $category->full_text);


if ($this->model->count)
    $this->widget(Yii::localExtension('ActiveTable', 'ActiveTable'), array(
        'model' => $this->model,
        'buttons' => array(),
        'headerButtons' => false,
        'useHeader' => true,
        'fieldsOrder' => array('title', 'short_text'),
        'route' => '/' . $category->alias,
        'useGroupOperations' => false,
        'useAlternativePagination' => false,
        'useSearch' => false,

        'preConvertors' => array
        (
            'title' => function ($fieldName, $fieldValue, $row, $isHeader = FALSE, $model) use ($category) {
                if ($isHeader) return $fieldValue;
                return CHtml::link($fieldValue, $category->createUrl() . "/{$row['alias']}");
            }
        ),
    ));

?>