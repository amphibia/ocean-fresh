<?
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content text
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self text gps_lat "Координата метки на карте (GPS Lat)"
 * @param self text gps_lng "Координата метки на карте (GPS Lng)"
 * @param self text gps_zoom "GPS Zoom"
 * @param self single_image marker_icon "Иконка на карте"
 * @param self textarea_512 _seo_keywords "Seo Keywords"
 * @param self textarea_512 _seo_description "Seo Description"
 * @param self text phone "Телефон"
 * @param self text email "E-mail"
 *
 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword none
 * @option children foreword_required off
 * @option children foreword_length_min none
 * @option children foreword_length_max none
 * @option children content none
 * @option children content_required off
 * @option children content_length_min none
 * @option children content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields off
 * @option pages show_gallery off
 * @option pages foreword none
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content text
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 */

Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBBSqpxyJdOLoinJI2PLsQnN7du342oMD0', CClientScript::POS_END);
?>

<!-- .content -->
<main class="content">
    <section class="section section-contacts">
        <div class="content-in content-aside">
            <div class="content-main">
                <div class="map-container">
                    <?php
                    $imgUrl = '/dist/images/map-marker.svg';
                    if ($image = Yii::app()->storage->decodeImages($category->marker_icon)) $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
                    ?>
                    <div class="map" id="mapContacts" data-lat="<?= $category->gps_lat ?>" data-lng="<?= $category->gps_lng ?>" data-marker="<?= $imgUrl ?>"> </div>
                </div>
            </div>
            <div class="content-sidebar">
                <div class="content-sidebar-title">
                    <h2><?= Yii::t('custom', 'Адрес') ?></h2>
                </div>
                <div class="content-sidebar-in">
                    <h3><?= $category->short_text ?></h3>
                    <div class="contact-item"> <i class="icon icon-marker"></i>
                        <?php
                        $category->full_text = str_replace('{', '<span>', $category->full_text);
                        $category->full_text = str_replace('}', '</span>', $category->full_text);
                        ?>
                        <p><?= $category->full_text ?></p>
                    </div>
                    <div class="contact-item"> <i class="icon icon-phone"></i>
                        <p><?= $category->phone ?></p>
                    </div>
                    <div class="contact-item"> <i class="icon icon-mail"></i>
                        <p><?= $category->email ?></p>
                    </div>
                </div>
            </div>
            <div class="mobile-map">
                <div class="map-container">
                    <?php
                    $imgUrl = '/dist/images/map-marker.svg';
                    if ($image = Yii::app()->storage->decodeImages($category->marker_icon)) $imgUrl = Yii::app()->storage->createUrl($image[0], 'original');
                    ?>
                    <div class="map" id="m-mapContacts" data-lat="<?= $category->gps_lat ?>" data-lng="<?= $category->gps_lng ?>" data-marker="<?= $imgUrl ?>"></div>
                </div>
            </div>
        </div>
    </section>
    <div class="overlay"></div>
</main>
<!-- end .content -->