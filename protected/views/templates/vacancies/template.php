<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword none
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content html
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self textarea_512 _seo_keywords "Seo Keywords"
 * @param self textarea_512 _seo_description "Seo Description"
 *
 * @option pages enabled on
 * @option pages show_tech_fields off
 * @option pages show_gallery off
 * @option pages foreword none
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content html
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 */
?>

<? define('PAGE_SIZE', 4); ?>

<?
$categoryUrl = $category->createUrl();
$dataProvider = $this->model->getActiveDataProvider();
$dataProvider->pagination->pageSize = PAGE_SIZE;
$dataProvider->pagination->route = $categoryUrl;
$categoryUrl = $category->createUrl();
?>
<div class="text"><?= $category->full_text ?></div>

<? if ($dataProvider->totalItemCount): ?>
    <div class="items">
        <? foreach ($dataProvider->data as $vacancy): ?>
            <div class="item">
                <h3><?= $vacancy->title ?></h3>

                <div class="description"><?= $vacancy->full_text ?></div>
            </div>
        <? endforeach ?>
    </div>
<? else: ?>
    <br/><br/>
    <h5><?= Yii::t('custom', 'В данный момент, нет ни одной активной вакансии. Зайдите позже.') ?></h5>
<? endif ?>

<?
$this->widget('CLinkPager', array(
    'cssFile' => false,
    'pages' => $dataProvider->pagination,
    'maxButtonCount' => 7,
    'htmlOptions' => array('class' => 'pagination'),
    'selectedPageCssClass' => 'active',
    'hiddenPageCssClass' => 'disabled',
    'firstPageCssClass' => 'hidden',
    'lastPageCssClass' => 'hidden',

    'prevPageLabel' => Yii::t('custom', '← Назад'),
    'nextPageLabel' => Yii::t('custom', 'Вперед →'),
    'lastPageLabel' => '',
    'firstPageLabel' => '',

    'header' => '',
));
?>
<div class="clearfix"></div>