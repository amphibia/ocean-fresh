<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword html
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages text date_stock "Дата акции"
 * @param pages text type_stock "Тип акции"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>

<!-- .content -->
<main class="content">
    <div class="content-in fixed-width content-centered delivery-page">
        <div class="stock">
            <div class="section-title">
                <h2><?= $category->title ?></h2>
                <?= $category->short_text ?>
            </div>
            <div class="stock__list">
                <?php foreach($category->pages as $page): ?>
                <div class="list__item">
                    <div class="column column-image">
                        <?php if ($images = Yii::app()->storage->decodeImages($page->img)): ?>
                            <div class="item__img" style="background-image: url(<?= Yii::app()->storage->createUrl($images[0], 'original') ?>)"></div>
                        <?php endif ?>
                        <div class="item__badge"><?= $page->type_stock ?></div>
                    </div>
                    <div class="column column-desc">
                        <div class="item__title"><?= $page->title ?></div>
                        <div class="item__text"><?= nl2br($page->short_text) ?></div>
                        <div class="item__date"><?= $page->date_stock ?></div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->

