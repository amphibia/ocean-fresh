<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword html
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content none
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages dropdownlist region "Регион" "Auvergne-Rhône-Alpes, Bourgogne-Franche-Comté, Bretagne, Centre-Val de Loire, Corse, Grand Est, Guadeloupe, Hauts-de-France, Île-de-France, Mayotte, Normandie, Nouvelle-Aquitaine, Occitanie, Pays de la Loire, Provence-Alpes-Côte d Azur, La Réunion"
 * @param pages text title1 "Заголовок 1"
 *
 * @param pages single_image img2 "Иллюстрация 2"
 * @param pages text title2 "Заголовок 2"
 * @param pages tiny_mce text2 "Текст 2"

 * @param pages single_image img3 "Иллюстрация 3"
 * @param pages text title3 "Заголовок 3"
 * @param pages tiny_mce text3 "Текст 3"

 * @param pages single_image img4 "Иллюстрация 4"
 * @param pages text title4 "Заголовок 4"
 * @param pages tiny_mce text4 "Текст 4"

 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */


$regionsFrance = array(
    'Auvergne-Rhône-Alpes' => 'FR-ARA',
    'Bourgogne-Franche-Comté' => 'FR-BFC',
    'Bretagne' => 'FR-BRE',
    'Centre-Val de Loire' => 'FR-CVL',
    'Corse' => 'FR-COR',
    'Grand Est' => 'FR-GES',
    'Guadeloupe' => 'FR-GUA',
    'Hauts-de-France' => 'FR-HDF',
    'Île-de-France' => 'FR-IDF',
    'Mayotte' => 'FR-MAY',
    'Normandie' => 'FR-NOR',
    'Nouvelle-Aquitaine' => 'FR-NAQ',
    'Occitanie' => 'FR-OCC',
    'Pays de la Loire' => 'FR-PDL',
    'Provence-Alpes-Côte d Azur' => 'FR-PAC',
    'La Réunion' => 'FR-LRE',
);

/*
$regions = '';
foreach($regionsFrance as $key => $item){
    $regions .= ', '.$key;
}
die($regions);
*/

$regions = '';
foreach($category->pages as $page){
    if($regions) $regions .= ",'".mb_strtolower($regionsFrance[trim($page->region)])."'";
    else $regions = "'".mb_strtolower($regionsFrance[trim($page->region)])."'";
}
?>

<!-- .content -->
<main class="content">
    <div class="map-oysters">
        <div class="section-title">
            <h2><?= $category->title ?></h2>
            <?= $category->short_text ?>
        </div>
        <div class="section-content">
            <div class="oysters-map" style="width: 100%; height: 600px" data-regions="<?= $regions ?>">
                <?php foreach($category->pages as $page): ?>
                <div class="popover-oysters-map popover-oysters__<?= mb_strtolower($regionsFrance[trim($page->region)]) ?>">
                    <div class="pic-popover">
                        <?php if ($images = Yii::app()->storage->decodeImages($page->img)): ?>
                            <img src="<?= Yii::app()->storage->createUrl($images[0], 'original') ?>"/>
                        <?php endif ?>
                    </div> <b><?= $page->title ?></b>
                    <p><?= nl2br($page->short_text) ?></p> <a href="<?= $page->createUrl() ?>" class="country-info-modal__link"><?= Yii::t('custom', 'ПОДРОБНЕЕ') ?></a>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="oysters-map-modal"> <span class="hide-modal"><i class="icon icon-close"></i></span>
            <div class="scroll-block">
                <div class="content-in fixed-width content-centered">
                    <?php /*
                    <div class="content-section">
                        <h1>Продукты из франции</h1> <img src="./images/samples/pic-about-01.jpg" class="img-fluid" alt="">
                        <h3> Устрицы Жилардо </h3>
                        <p> Выращиваются эти устрицы в провинции Бретань, что располагается на территории Франции. Растут они в глубинах морей несколько лет, а потом попадают в специальные садки – клеры, что являются, по сути, специально созданными «природными»
                            бассейнами. Там устрицы также находятся какое-то количество времени до окончания периода своего созревания. </p>
                        <p> Благодаря такому способу выращивания их сладковатый вкус и запах становится более йодированным и морским, в нем появляется легкая ореховая нотка. А так же, как утверждают ценители тонкого вкуса, «арбузное» послевкусие. </p>
                    </div>
                    <div class="content-section"> <img src="./images/samples/pic-about-02.jpg" class="img-fluid" alt="">
                        <h3> Французкие сыры </h3>
                        <p> Выращиваются эти устрицы в провинции Бретань, что располагается на территории Франции. Растут они в глубинах морей несколько лет, а потом попадают в специальные садки – клеры, что являются, по сути, специально созданными «природными»
                            бассейнами. Там устрицы также находятся какое-то количество времени до окончания периода своего созревания. </p>
                        <p> Благодаря такому способу выращивания их сладковатый вкус и запах становится более йодированным и морским, в нем появляется легкая ореховая нотка. А так же, как утверждают ценители тонкого вкуса, «арбузное» послевкусие. </p>
                    </div>
                    */ ?>
                </div>
            </div>
        </div>

    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->

