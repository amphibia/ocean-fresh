<div class="content-section">
    <h1><?= $page->title1 ?></h1>
    <?php if ($image = Yii::app()->storage->decodeImages($page->img2)): ?>
        <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
    <?php endif ?>
    <h3><?= $page->title2 ?></h3>
    <?= $page->text2 ?>
</div>
<div class="content-section">
    <?php if ($image = Yii::app()->storage->decodeImages($page->img3)): ?>
        <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
    <?php endif ?>
     <h3><?= $page->title3 ?></h3>
    <?= $page->text3 ?>
</div>
<div class="content-section">
    <?php if ($image = Yii::app()->storage->decodeImages($page->img4)): ?>
        <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
    <?php endif ?>
    <h3><?= $page->title4 ?></h3>
    <?= $page->text4 ?>
</div>

<?php exit ?>