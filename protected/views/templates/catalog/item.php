<!-- .content -->
<main class="content">
    <div class="app" id="app">
        <div class="loader" v-if="loading"></div>
        <div class="catalog">
            <div class="catalog-navbar">
                <ul class="catalog-breadcrumbs">
                    <?= Yii::app()->custom->breadCramps(
                        array(
                            'showMain' => true,
                            'showLatestAlias' => true,
                            'startCategoryAlias' => 'root',
                            //'startCategoryTechId' => 'root',
                        ));
                    ?>
                </ul>
                <?php $cartCategory = Yii::app()->custom->getCategories('cart'); ?>
                <a href="<?= $cartCategory->createUrl() ?>" class="navbar-cart-info">
                    <i class="icon icon-cart"></i>
                    <total-cart-count></total-cart-count>
                </a> </div>
            <product-inside></product-inside>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->
