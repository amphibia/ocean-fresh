<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery off
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self single_image img_main1 "Иконка на главной 1"
 * @param self single_image img_main2 "Иконка на главной 2"
 * @param self toggler show_main "Показывать на главной"
 *
 * @option children enabled on
 * @option children show_tech_fields on
 * @option children show_gallery off
 * @option children foreword none
 * @option children foreword_required off
 * @option children foreword_length_min none
 * @option children foreword_length_max none
 * @option children content none
 * @option children content_required off
 * @option children content_length_min none
 * @option children content_length_max none
 * @param children dropdownlist type_payment "Тип оплаты" "в товаре,штуки,килограмм,сложный,предзаказ"
 *
 * @option pages enabled on
 * @option pages show_tech_fields on
 * @option pages show_gallery off
 * @option pages foreword text
 * @option pages foreword_required off
 * @option pages foreword_length_min none
 * @option pages foreword_length_max none
 * @option pages content html
 * @option pages content_required off
 * @option pages content_length_min none
 * @option pages content_length_max none
 * @param pages single_image img "Иллюстрация к статье"
 * @param pages single_image img_big "Иллюстрация большая"
 * @param pages text name "Название"
 * @param pages required_numerical_text price "Цена"
 * @param pages required_numerical_text old_price "Старая цена (Для акций)"
 * @param pages text refinement "Уточнение цены"
 * @param pages text recipe_title "Рецепт. Заголовок"
 * @param pages text recipe_href "Ссылка на рецепт"
 * @param pages text grams "Граммы"
 * @param pages full_textarea recommended_products "Рекомендованные продукты"
 * @param pages dropdownlist type_payment "Тип оплаты" "штуки,килограмм,сложный,предзаказ"
 * @param pages toggler in_stock "В наличии"
 *
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

if($category->_level == 5) $categoryCatalogLevel1 = $category;
else $categoryCatalogLevel1 = $category->parent;

$countAllPages = 0;
$filtrMenu = array();
foreach($categoryCatalogLevel1->children as $children){
    $filtrMenu[$children->title] = array(
        'id' => $children->id,
        'url' => $children->createUrl(),
        'countPages' => count($children->pages),
    );
    $countAllPages += $filtrMenu[$children->title]['countPages'];
}

?>

<!-- .content -->
<main class="content">
    <div class="app">
        <div class="loader" v-if="loading"></div>
        <div class="catalog-navbar">
            <ul class="catalog-breadcrumbs">
                <?= Yii::app()->custom->breadCramps(
                    array(
                        'showMain' => true,
                        'showLatestAlias' => true,
                        'startCategoryAlias' => 'root',
                        //'startCategoryTechId' => 'root',
                    ));
                ?>
            </ul>
            <ul class="catalog-menu">
                <?php foreach($filtrMenu as $title => $value): ?>
                <li class="catalog-menu-item<?= ($category->id == $value['id'])?' active':'' ?>"><a href="<?= $value['url'] ?>"><?= $title ?></a><span class="badge"><?= $value['countPages'] ?></span></li>
                <?php endforeach ?>
                <li class="catalog-menu-item<?= ($category->id == $categoryCatalogLevel1->id)?' active':'' ?>"><a href="<?= $categoryCatalogLevel1->createUrl() ?>"><?= Yii::t('custom', 'Все') ?></a><span class="badge"><?= $countAllPages ?></span></li>
            </ul>
            <?php $cartCategory = Yii::app()->custom->getCategories('cart'); ?>
            <a href="<?= $cartCategory->createUrl() ?>" class="navbar-cart-info">
                <i class="icon icon-cart"></i>
                <total-cart-count></total-cart-count>
            </a> </div>
        <div class="catalog">
            <products></products>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->

