<? define('OTHER_NEWS_CONT', 6); ?>
<? $categoryUrl = $category->createUrl(); ?>

<div class="column left">

        <span class="publish-date">
            <?= Yii::app()->dateFormatter->formatDateTime($page->date_display, 'long', NULL); ?>
        </span>

    <h2><?= $page->title ?></h2>
    <? if ($image = Yii::app()->storage->decodeImages($page->image)): ?>
        <div class="image-container">
            <img src="<?= Yii::app()->storage->createUrl($image[0], 'inContent') ?>"
                 title="<?= $image[0]['title'] ?>"/>
        </div>
    <? endif ?>

    <div class="text"><?= $page->full_text ?></div>


</div>

<div class="column right">
    <?
    $news = StructPage::model()->published()->categories(array($category->id));
    $news->dbCriteria->mergeWith(array(
        'condition' => "t.id <> :id",
        'params' => array('id' => $page->id),
        'limit' => OTHER_NEWS_CONT,
        'order' => 'date_display DESC'
    ));

    if (sizeof($news = $news->findAll())):
        ?>
        <div class="news others">
            <a href="<?= $categoryUrl ?>">
                <h2><?= Yii::t('custom', 'Другие новости'); ?></h2>
            </a>
            <? foreach ($news as $newsItem): ?>
                <div class="item">
                    <span class="publish-date">
                        <?= Yii::app()->dateFormatter->formatDateTime($newsItem->date_display, 'long', NULL); ?>
                    </span>
                    <a href="<?= "$categoryUrl/{$newsItem->alias}" ?>"><?= $newsItem->title ?></a>

                    <p><?= nl2br($newsItem->short_text) ?></p>
                </div>
            <? endforeach; ?>

            <a class="more" href="<?= $categoryUrl ?>">
                <?= Yii::t('custom', 'Все новости') ?>
            </a>
        </div>
    <? endif ?>

</div>