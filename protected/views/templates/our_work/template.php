<?php
/**
 * @option self enabled on
 * @option self show_tech_fields on
 * @option self show_gallery on
 * @option self foreword text
 * @option self foreword_required off
 * @option self foreword_length_min none
 * @option self foreword_length_max none
 * @option self content none
 * @option self content_required off
 * @option self content_length_min none
 * @option self content_length_max none
 * @param self single_image img "Иллюстрация к статье"
 * @param self text suppliers_title "Поставщики. Заголовок"
 * @param self full_textarea suppliers_list "Поставщики. Список"
 * @param self text supply_title "Поставка морепродуктов. Заголовок"
 * @param self gallery supply_gallery "Поставка морепродуктов. Иконки"
 * @param self text title_top "Заголовок. Оснащение склада"

 * @param self single_image img1 "Иллюстрация 1"
 * @param self text title1 "Заголовок 1"
 * @param self tiny_mce text1 "Текст 1"

 * @param self single_image img2 "Иллюстрация 2"
 * @param self text title2 "Заголовок 2"
 * @param self tiny_mce text2 "Текст 2"

 * @param self single_image img3 "Иллюстрация 3"
 * @param self text title3 "Заголовок 3"
 * @param self tiny_mce text3 "Текст 3"

 * @param self single_image img4 "Иллюстрация 4"
 * @param self text title4 "Заголовок 4"
 * @param self tiny_mce text4 "Текст 4"

 * @param self text title_bottom "Заголовок внизу"
 * @param self single_image img_bottom "Иллюстрация внизу"
 * @param self full_textarea text_bottom "Текст внизу"
 */

/* Custom fields list:

    'text' => 'Text field'
    'required_text' => 'Required text field'
    'required_numerical_text' => 'Required numerical text field'
    'indexed_text' => 'Indexed text field'
    'textarea_512' => 'Medium textarea (512 symbols max)'
    'full_textarea' => 'Full textarea'
    'tiny_mce' => 'TinyMCE visual editor'
    'toggler' => 'Toggle button (on/of)'
    'dropdownlist' => 'Dropdownlist'
    //'checkboxlist' => 'Checkboxlist'
    //'radiobuttonlist' => 'Radiobuttonlist'
    'gallery' => 'Gallery'
    'single_image' => 'Single image'
    'documents' => 'Attached documents'
    'document' => 'Single attached document'
    'datepicker' => 'Datepicker'
    'datepickerOptional' => 'Datepicker optional'
    'daterange' => 'Daterange'
    'email' => 'Email'
    'ip' => 'Incoming IP'
    'regexp' => 'Text with regular expression'
    'color' => 'Color picker'
    'vote' => 'Vote'
 */

?>

<!-- .content -->
<main class="content">
    <div class="content-in fixed-width content-centered">
        <div class="content-section">
            <h1><?= $category->title ?></h1>
            <p><b><?= nl2br($category->short_text) ?></b></p>

            <?php
            /*
            if ($image = Yii::app()->storage->decodeImages($category->img)): ?>
                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
            <?php endif
            */
            ?>

            <div class="content-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php if ($images = Yii::app()->storage->decodeImages($category->gallery)): ?>
                            <?php foreach($images as $img): ?>
                                <div class="swiper-slide"> <img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" class="img-fluid" alt=""> </div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                </div>
                <div class="swiper-controls">
                    <div class="swiper-action-btn swiper-prev"><i class="icon icon-arrow-left"></i></div>
                    <div class="swiper-action-btn swiper-next"><i class="icon icon-arrow-right"></i></div>
                </div>
            </div>

        </div>
        
        <?php if(trim($category->suppliers_list)): ?>
        <div class="content-section">
            <h1><?= $category->suppliers_title ?></h1>
            <div class="items-list">
                <div class="row">
                    <?php
                    $category->suppliers_list = str_replace("\r", '', $category->suppliers_list);
                    $category->suppliers_list =  explode("\n", $category->suppliers_list);
                    $n=1; foreach($category->suppliers_list as $item):
                    ?>
                    <div class="col-lg-4">
                        <div class="item">
                            <div class="item-list-head"> <span class="num"><?= $n ?></span> </div>
                            <div class="item-list-text"> <b><?= $item ?></b> </div>
                        </div>
                    </div>
                    <?php $n ++; endforeach ?>
                </div>
            </div>
        </div>
        <?php endif ?>
        <div class="content-section">
            <h1><?= $category->supply_title ?></h1>
            <div class="items-list">
                <div class="row">
                    <?php if ($images = Yii::app()->storage->decodeImages($category->supply_gallery)): ?>
                        <?php foreach($images as $img): ?>
                        <div class="col-lg-4">
                            <div class="item">
                                <div class="item-list-head">
                                    <span class="pic"><img src="<?= Yii::app()->storage->createUrl($img, 'original') ?>" alt=""></span>
                                </div>
                                <div class="item-list-text"><?= $img['title'] ?></div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="content-section">
            <h1><?= $category->title_top ?></h1>
            <?php if ($image = Yii::app()->storage->decodeImages($category->img1)): ?>
                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
            <?php endif ?>
            <?php if(trim($category->title1)): ?>
            <h3><?= $category->title1 ?></h3>
            <?= $category->text1 ?>
            <?php endif ?>

            <?php if ($image = Yii::app()->storage->decodeImages($category->img2)): ?>
                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
            <?php endif ?>
            <?php if(trim($category->title2)): ?>
            <h3><?= $category->title2 ?></h3>
            <?= $category->text2 ?>
            <?php endif ?>

            <?php if ($image = Yii::app()->storage->decodeImages($category->img3)): ?>
                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
            <?php endif ?>
            <?php if(trim($category->title3)): ?>
            <h3><?= $category->title3 ?></h3>
            <?= $category->text3 ?>
            <?php endif ?>

            <?php if ($image = Yii::app()->storage->decodeImages($category->img4)): ?>
                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
            <?php endif ?>
            <?php if(trim($category->title4)): ?>
            <h3><?= $category->title4 ?></h3>
            <?= $category->text4 ?>
            <?php endif ?>

        </div>
        <div class="content-section">
            <h1><?= $category->title_bottom ?></h1>
            <?php if ($image = Yii::app()->storage->decodeImages($category->img_bottom)): ?>
                <img src="<?= Yii::app()->storage->createUrl($image[0], 'original') ?>" class="img-fluid" alt="">
            <?php endif ?>
            <p><b><?= nl2br($category->text_bottom) ?></b></p>
        </div>
    </div>
    <div class="overlay"></div>
</main>
<!-- end .content -->

