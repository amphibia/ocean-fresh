<?php
$categories = Yii::app()->custom->getCategories(array('mainMenu'));

// Атрибуты тега body
$bodyAttributes = array();
if (isset($currentCategory)) {
    if(Yii::app()->struct->currentTemplate)
    {
        $bodyAttributes['data-template'] = Yii::app()->struct->currentTemplate;
    }
    else{
        $bodyAttributes['data-template'] = 'mainpage';
    }
}

Yii::app()->clientScript->scriptMap = array('jquery.js' => false);
//Yii::app()->clientScript->registerScriptFile('/js/jquery-1.11.2.min.js');

Yii::app()->clientScript->registerCssFile('/dist/styles/vendor.min.css');
Yii::app()->clientScript->registerCssFile('/dist/styles/main.min.css');

//Yii::app()->clientScript->registerScriptFile('/js/forms.js');
Yii::app()->clientScript->registerScriptFile('https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('/dist/scripts/main.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('/dist/scripts/app.js', CClientScript::POS_END);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <script src="/dist/scripts/vendor.min.js"></script>

    <!-- IE SUPPORT -->
    <!--[if lte IE 9]>
    <script src="/js/libs/jquery.placeholder.min.js"></script>
    <script>$().ready(function(){ $('[placeholder]').placeholder(); });</script>
    <![endif]-->

    <meta charset="utf-8" />
    <?php
    if (
        (($currentCategoryOrPage = Yii::app()->struct->currentPage) || ($currentCategoryOrPage = Yii::app()->struct->currentCategory))
        && $currentCategoryOrPage->issetCustomField('_seo_keywords') && $currentCategoryOrPage->_seo_keywords
        && $currentCategoryOrPage->issetCustomField('_seo_description') && $currentCategoryOrPage->_seo_description
    ):
        ?>
        <meta name="keywords" content="<?= $currentCategoryOrPage->_seo_keywords ?>" />
        <meta name="description" content="<?= $currentCategoryOrPage->_seo_description ?>" />
    <?php elseIf(
        $mainPageCategory
        && $mainPageCategory->issetCustomField('_seo_keywords') && $mainPageCategory->_seo_keywords
        && $mainPageCategory->issetCustomField('_seo_description') && $mainPageCategory->_seo_description
    ): ?>
        <meta name="keywords" content="<?= $mainPageCategory->_seo_keywords ?>" />
        <meta name="description" content="<?= $mainPageCategory->_seo_description ?>" />
    <?php endIf; ?>

    <title><?= Yii::app()->custom->getPageTitle() ?></title>

    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="theme-color" content="#000000">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <?= $mainPageCategory->code_head ?>
</head>

<body <?= CHtml::renderAttributes($bodyAttributes) ?>>

<!--[if lt IE 10]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser.
    Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]-->

<!-- .wrapper -->
<div class="wrapper" id="app">
    <div class="wrapper-in">

        <div class="m-header">
            <div class="m-logo">
                <a href="/"><img src="/dist/images/m-logo.svg" alt=""></a>
            </div>
            <?php $cartCategory = Yii::app()->custom->getCategories('cart'); ?>
            <a href="<?= $cartCategory->createUrl() ?>" class="navbar-cart-info">
                <i class="icon icon-cart"></i>
                <total-cart-count></total-cart-count>
            </a>
            <div class="sandwich-menu">
                <span></span>
            </div>
        </div>
        <!-- .sidebar -->
        <div class="sidebar">
            <div class="sidebar-wrap"> <a href="/" class="logo">
                    <img src="/dist/images/logo.svg" alt="">
                </a>
                <!-- nav-main -->
                <nav class="nav-main">
                    <ul class="list-unstyled">
                        <?php foreach($categories['mainMenu']->children as $children): ?>
                        <li data-alias="<?= $children->alias ?>" class="nav-item"> <a href="<?= $children->createUrl() ?>"><?= $children->title ?><?= (count($children->children))?' <i class="icon icon-arrow-right"></i>':'' ?></a>
                            <?php if(count($children->children)): ?>
                            <ul class="dropdown-menu">
                                <?php foreach($children->children as $children2): ?>
                                <li class="dropdown-item"><a href="<?= $children2->createUrl() ?>"><span><?= $children2->title ?></span></a><i class="icon icon-arrow-down"></i>
                                    <?php if(count($children2->children)): ?>
                                    <ul class="sub-dropdown-menu">
                                        <?php foreach($children2->children as $children3): ?>
                                        <li class="sub-dropdown-item"><a href="<?= $children3->createUrl() ?>"><?= $children3->title ?></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                    <?php endif ?>
                                </li>
                                <?php endforeach ?>
                            </ul>
                            <?php endif ?>
                        </li>
                        <?php endforeach ?>
                    </ul>
                </nav>
                <!-- end nav-main -->
            </div>
            <!-- .footer -->
            <footer class="footer">
                <div class="footer-contacts"> <i class="icon icon-phone"></i> <b class="contacts-number">
                        <?= $mainPageCategory->phone ?>
                    </b> <span><?= $mainPageCategory->address ?></span> </div>
                <ul class="social-icons">
                    <li class="social-item"> <a href="<?= $mainPageCategory->facebook ?>" target="_blank"><i class="icon icon-facebook"></i></a> </li>
                    <li class="social-item"> <a href="<?= $mainPageCategory->instagram ?>" target="_blank"><i class="icon icon-instagram"></i></a> </li>
                </ul>

                <?php if (count(Yii::app()->lang->languages) > 1): ?>
                    <div class="link_lang">
                        <?php
                        foreach (Yii::app()->lang->languages as $key => $value):
                            if (Yii::app()->language == $key) {
                                continue;
                            }
                            ?>
                            <a href="?language=<?= $key ?>"><?= Yii::t('language', $value['long']) ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif ?>

                <!--p class="footer-made-in"> <?= Yii::t('custom', 'Сделано в') ?> <a href="http://amphibia.kz/">amphibia</a> </p-->
            </footer>
            <!-- end .footer -->
        </div>
        <!-- end .sidebar -->
