	<div class="m-footer">
		<div class="m-footer-contacts"> 
			<i class="icon icon-phone"></i>
			<div class="info-adress"> 
				<b class="contacts-number">
					<?= $mainPageCategory->phone ?>
				</b> 
				<span><?= $mainPageCategory->address ?></span>
			</div>
		</div>
		<ul class="social-icons">
			<li class="social-item"> 
				<a href="<?= $mainPageCategory->facebook ?>" target="_blank"><i class="icon icon-facebook"></i></a>
			</li>
			<li class="social-item">
				<a href="<?= $mainPageCategory->instagram ?>" target="_blank"><i class="icon icon-instagram"></i></a>
			</li>
		</ul>
		
		<?php if (count(Yii::app()->lang->languages) > 1): ?>
		<div class="link_lang">
			<?php
			foreach (Yii::app()->lang->languages as $key => $value):
			if (Yii::app()->language == $key) {
				continue;
			}
			?>
			<a href="?language=<?= $key ?>"><?= Yii::t('language', $value['long']) ?></a>
			<?php endforeach; ?>
		</div>
		<?php endif ?>
		
		<!--p class="m-footer-made-in"> <?= Yii::t('custom', 'Сделано в') ?> <a href="http://amphibia.kz/">amphibia</a> </p-->
		<p class="m-footer-copyright">© <?= Yii::t('custom', '2017, OceanFresh. Все права защищены.') ?></p>
	</div>
</div>
</div>
<!-- end .wrapper -->
<?= $mainPageCategory->code_before_end_body ?>
</body>

</html>