<?
// Определение, не является ли этот пользователь "super_user".
// Для этого, должен быть установлен модуль "users".
$superUser = false;
if( Yii::app()->hasComponent('users')
	&& isset(Yii::app()->user->model)
	&& isset(Yii::app()->user->model->role)
	&& Yii::app()->user->model->role->super_user)
	$superUser = true;

isset($code) or $code = 500;	// ошибка по умолчанию
isset($message) or $message = 'No message!';

// сообщение не для отладки
if(!defined('YII_DEBUG') || !YII_DEBUG || !$superUser)
	$message = isset(Yii::app()->params['systemErrors'][$code])
		? Yii::app()->params['systemErrors'][$code] : NULL;

$protocol = strtolower(preg_replace('/[^A-Za-z]*/', '', $_SERVER['SERVER_PROTOCOL']));
$currentUrl = "$protocol://" . $_SERVER['HTTP_HOST'] .$_SERVER['REQUEST_URI'];

if(!Yii::app()->request->isAjaxRequest)
{ ?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?= Yii::t('users', 'Error {error}', array('{error}'=>$code)); ?></title>
	<link rel="stylesheet" type="text/css" media="all" href="/css/error.css" />
</head>
<? } ?>


<body lang="<?= Yii::app()->language ?>" error_code="<?= $code ?>">

	<div class="container root-container">

		<h1><?= $code; ?></h1>
		<div class="error">
			<?= CHtml::encode($message); ?>
		</div>
		
		<p>
			<?= Yii::t('app', 'Current URL is:') ?>
			<a href="<?=$currentUrl?>"><?=$currentUrl?></a>
		</p>

		<p>
			<a href="/"><?= Yii::t('app', 'Go to main page') ?></a>
		</p>

<? if(!Yii::app()->request->isAjaxRequest){ ?>
	</div>

</body>

</html>
<? } ?>