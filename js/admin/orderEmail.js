$(document).ready(function()
{
	/**
	 * Переключение видимости блока с учетными данными
	 * email-аккаунта (для SMTP).
	 */
	function toggleAccountData(scoore){
		if($("#FeedbackOptionsForm_type").val()=='mail')
			 $('.accountData').slideUp(scoore);
		else $('.accountData').slideDown(scoore);
	}

	toggleAccountData(0);
	$('#FeedbackOptionsForm_type').change(function(){
		toggleAccountData(100); });
	$('#FeedbackOptionsForm_type').keypress(function(){
		toggleAccountData(100); });
});